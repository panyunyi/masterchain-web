/**
 * Created by dora on 2017/5/19.
 *
 */

/**
 * 项目配置
 *
 * 此文件中的配置信息均为生产环境配置。如果开发环境有不同的配置，只需要将不同的那部分配置项
 * 写入到 settings.development.js 文件中
 */

let config = {
    session_secret: 'masterchainweb_secret', // 务必修改
    auth_cookie_name: 'masterchainweb',
    cache_maxAge: Math.floor(Date.now() / 1000) + 24 * 60 * 60, //1 hours
    serverPort: 8884,
    lang: 'zh-CN', // 设置默认语言
    languages: ['zh-CN', 'zh-TW'], // 可选语言

    // 密码盐
    encrypt_key: 'masChainTs',
    salt_aes_key: "masChainTs_", // 可以解密，秘钥必须为：8/16/32位
    salt_md5_key: "masChainTs", // MD5的盐，用于加密密码
    encryptApp_key: '751f621ea5c8f930',
    encryptApp_vi: '2624750004598718',

    // 数据库配置
    mongo_connection_uri: 'mongodb://masterChain1990:IuMIcBi~~sCiNooYu@127.0.0.1:27017/masterchainweb',

    // 七牛配置
    openqn: true, //是否开启,若为true 则下面的信息必须配置正确完整
    openOss: true, //是否开启OSS
    region: 'oss-cn-hongkong',
    accessKeyId: 'LTAIvvOdHcwl9MqN',
    accessKeySecret: 'PXrZrL4ALVFF8Rah2TLorEAVlraAAK',
    bucket: 'masterchain', //上传的目标资源空间
    endPoint: 'oss-cn-hongkong.aliyuncs.com', //上传的目标资源空间
    origin: 'https://cdn.html-js.cn', // cdn域名
    fsizeLimit: 1024 * 1024 * 5, // 上传文件大小限制默认为5M
    assetsCdn: true, // 静态资源使用cnd.请在build完成后将 elemt.*.js 上传的七牛的融合cdn
    jg_appkey: "e82bc65200da14f85829057a", // 极光推送 appkey
    jg_masterSecret: "cc02016ad1a842c411ac7ce6", //极光推送 masterSecret
    googlePublicKey: 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlQGF6O89a0qgUcx2LRv6ryTIm9Kfx8uokbOOihpz/xuqVtQhKCpbnWxt9mp4hHzNnzNRVBjZNiWX2qpaNGf7G3r5QsuFaU2VESVpKPiIILCJgoxV4EGpV1T4S6ng5jYlDgAsEYkbrenKKt8wNAiSfCENLE90il0oYd/qWIkbcbZ3UyH718Ete8YJxz5GD7x2cYnvsEM4RnPVKNta+/9BK8Yr6KccfBzCq90P+DWpitfUtXVO1xX+poZ1WrDKWOS6uU1b9BDoQ477X9jb7XxS56ypE32VHPlA9Pb+lCm/nOSwyknZOkPOmlLfZbVjCToab71OIv5bRkBr69mg+/W4OQIDAQAB',

    openRedis: true,
    redis_host: '127.0.0.1',
    // redis Host配置
    redis_port: 6379,
    redis_psd: 'HuiPing520@Xiaoshen@520',
    redis_db: 0,
    redis_ttl: 12, // 过期时间12小时

    // 站点基础信息配置
    DORACMSAPI: 'http://api.html-js.cn', // 系统服务提供商
    SYSTEMTEMPFORDER: process.cwd() + '/views/', // 系统模板安装目录
    TEMPSTATICFOLDER: process.cwd() + '/public/themes/', // 模板静态文件路径
    SYSTEMLOGPATH: '/home/doraData/logsdir/doracms', // 服务器日志保存目录
    YunOssDomain: 'http://masterchain.oss-cn-hongkong.aliyuncs.com/', // 阿里云访问路径
    transferServer: "http://172.31.88.223:8090/transfer", // 转账 MVPC
    upChainServer: "http://172.31.88.223:8090/dataonchain", // MVPC余额查询
    transferMecServer: "http://172.31.88.223:8090/mintapprove", //用户获得了MEC代币
    transferMecByUser: "http://172.31.88.223:8090/reward", //用户打赏，用户间转账
    getLeftMecApi: "http://172.31.88.223:8090/mecApproveBalance", // MEC余额查询
    getLeftMsttApi: "http://172.31.88.223:8090/balance", // MSTT余额查询
    buyClassMecApi: "http://172.31.88.223:8090/decreaseAllowance", // 使用MEC购买大师课
    getWalletsApi: "http://172.31.88.223:8090/wallets", // 申请钱包
    // 邮件相关设置
    email_findPsd: 'findPsd',
    email_reg_active: 'reg_active',
    email_notice_contentMsg: 'notice_contentMsg',
    email_notice_admin_byContactUs: 'notice_site_messages',
    email_notice_user_byContactUs: 'notice_user_site_messages',
    email_notice_contentBug: 'notice_contentBug',
    email_notice_user_contentMsg: 'notice_user_contentMsg',
    email_notice_user_reg: 'notice_user_reg',
    email_sendMessageCode: 'email_sendMessageCode', // 发送邮箱验证码

    // 信息提示相关
    system_illegal_param: '非法参数',
    system_noPower: '对不起，您无权执行该操作！',
    system_atLeast_one: '请选择至少一项后再执行删除操作！',
    system_batch_delete_not_allowed: '对不起，该模块不允许批量删除！',
    system_error_imageType: '文件格式不正确，请重新上传',
    system_error_upload: '上传失败，请稍后重试',

    // 用户行为类型
    user_action_type_login_first: 'user_action_type_login_first', // 首次登陆
    user_action_type_register: 'user_action_type_register', // 用户注册
    user_action_type_browse: 'user_action_type_browse', // 浏览
    user_action_type_give_thumbs_up: 'user_action_type_give_thumbs_up', // 点赞
    user_action_type_comment: 'user_action_type_comment', // 评论
    user_action_type_community_comment: 'user_action_type_community_comment', // 社群评论
    user_action_type_appreciate: 'user_action_type_appreciate', // 打赏
    user_action_type_appreciate_in: 'user_action_type_appreciate_in', // 打赏收入
    user_action_type_appreciate_out: 'user_action_type_appreciate_out', // 打赏支出
    user_action_type_collection: 'user_action_type_collection', // 收藏
    user_action_type_follow: 'user_action_type_follow', // 关注
    user_action_type_forward: 'user_action_type_forward', // 转发
    user_action_type_community_building: 'user_action_type_community_building', // 社群建设
    user_action_type_invitation: 'user_action_type_invitation', // 邀请
    user_action_type_report: 'user_action_type_report', // 举报
    user_action_type_review: 'user_action_type_review', // 审核
    user_action_type_creat_content: 'user_action_type_creat_content', // 内容创作
    user_action_type_creat_identity: 'user_action_type_creat_identity', // 实名认证
    user_action_type_shop_send: 'user_action_type_shop_send', // 商城会员发放
    user_bill_type_transfer_coins: 'user_bill_type_transfer_coins', // 提币
    user_bill_type_recharge: 'user_bill_type_recharge', // 充值
    user_bill_type_subscribe: 'user_bill_type_subscribe', // 课程订购
    user_bill_type_buyVip: 'user_bill_type_buyVip', // 购买会员
    user_bill_type_signIn: 'user_bill_type_signIn', // 签到
    user_bill_type_brithday: 'user_bill_type_brithday', // 生日

    iapPriceMap: {
        'com.dragon.masterchain.mec.1': 1,
        'com.dragon.masterchain.mec.3': 3,
        'com.dragon.masterchain.mec.6.v': 6,
        'com.dragon.masterchain.mec.12': 12,
        'com.dragon.masterchain.mec.30': 30,
        'com.dragon.masterchain.mec.60': 60
    }

};

if (process.env.NODE_ENV !== 'production') {
    Object.assign(config, require('./settings.development'))
}

module.exports = config