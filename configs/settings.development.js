/**
 *
 * @author    Jerry Bendy
 * @since     2019-07-18
 */

/**
 * 开发环境特殊配置
 */

module.exports = {

    // mongo_connection_uri: 'mongodb://masterChain1990:IuMIcBi~~sCiNooYu@47.52.231.97:27017/masterchainweb',
    mongo_connection_uri: 'mongodb://127.0.0.1:27017/masterchainweb',

    transferServer: "http://47.91.254.14:8090/transfer", // 转账 MVPC
    upChainServer: "http://47.91.254.14:8090/dataonchain", // MVPC余额查询
    transferMecServer: "http://47.91.254.14:8090/mintapprove", //用户获得了MEC代币
    transferMecByUser: "http://47.91.254.14:8090/reward", //用户打赏，用户间转账
    getLeftMecApi: 'http://47.91.254.14:8090/mecApproveBalance', // MEC余额查询
    getLeftMsttApi: 'http://47.91.254.14:8090/balance', // MSTT余额查询
    buyClassMecApi: 'http://47.91.254.14:8090/decreaseAllowance', // 使用MEC购买大师课
    getWalletsApi: 'http://47.91.254.14:8090/wallets', // 申请钱包
}