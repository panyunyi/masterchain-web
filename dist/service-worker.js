/**
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

// DO NOT EDIT THIS GENERATED OUTPUT DIRECTLY!
// This file should be overwritten as part of your build process.
// If you need to extend the behavior of the generated service worker, the best approach is to write
// additional code and include it using the importScripts option:
//   https://github.com/GoogleChrome/sw-precache#importscripts-arraystring
//
// Alternatively, it's possible to make changes to the underlying template file and then use that as the
// new base for generating output, via the templateFilePath option:
//   https://github.com/GoogleChrome/sw-precache#templatefilepath-string
//
// If you go that route, make sure that whenever you update your sw-precache dependency, you reconcile any
// changes made to this original template file with your modified copy.

// This generated service worker JavaScript will precache your site's resources.
// The code needs to be saved in a .js file at the top-level of your site, and registered
// from your pages in order to be used. See
// https://github.com/googlechrome/sw-precache/blob/master/demo/app/js/service-worker-registration.js
// for an example of how you can register this script and handle various service worker events.

/* eslint-env worker, serviceworker */
/* eslint-disable indent, no-unused-vars, no-multiple-empty-lines, max-nested-callbacks, space-before-function-paren, quotes, comma-spacing */
'use strict';

var precacheConfig = [["/static/css/admin.747c05d.css","ac155d1be07c16b548b9690605e39ffe"],["/static/img/401.089007e.gif","089007e721e1f22809c0313b670a36f1"],["/static/img/404.a57b6f3.png","a57b6f31fa77c50f14d756711dea4158"],["/static/img/doracms-logo.7fef98d.png","7fef98d4b49a14f558388cb0d1afbb7c"],["/static/img/element-icons.535877f.woff","535877f50039c0cb49a6196a5b7517cd"],["/static/img/element-icons.732389d.ttf","732389ded34cb9c52dd88271f1345af9"],["/static/js/0.8fe6178.js","c9917fb0f58bd37c769dddfd3965b6fa"],["/static/js/1.8c3c183.js","caba61c0c0a5160ba9219992cc331bca"],["/static/js/10.db26162.js","1d77517c5f5a038db0202d89e15a949f"],["/static/js/100.458dd89.js","84d415babcba06adc3e46a750b752a9e"],["/static/js/101.0aa3cf1.js","3589f6add93fdf4cf87273902926836c"],["/static/js/102.d5aa25e.js","633c64b9099c6122eeda0b4f4944eb11"],["/static/js/103.f7b6f9e.js","2f3ae3d7b9bdb0b22205b4f4afef112d"],["/static/js/104.9487c31.js","fda33a0bcfe735191bc1535ac3c2a06c"],["/static/js/105.37bb6f5.js","069e606eefc880f2bc263f7ef796aabe"],["/static/js/106.056ffe2.js","6da6edbfae9313f3ec1dd4ba0b0d50f4"],["/static/js/107.0f09af7.js","a4c759892d04b95e4058b46b6b748923"],["/static/js/108.8b72173.js","a295b75419044e59dd5ecd5ecf47d032"],["/static/js/109.be2b799.js","005553d1b9af23204316f7113cdd3d9d"],["/static/js/11.c2aea37.js","972db6f149f2f6c938a3e7087e142c93"],["/static/js/110.287b9e9.js","025f5272805c6801e99f97c73ba1651b"],["/static/js/111.f7a9c85.js","c8762da1945bb602415ec94c241d643b"],["/static/js/112.a8ad806.js","c00862e1c59e9ff2f7d7865cb1d682fa"],["/static/js/113.9dfdb8c.js","6f728c5a9af1989b208b601ea72b2f3a"],["/static/js/114.c17ea01.js","ba7365ff5f75424aa6fd721b8d4ecf3e"],["/static/js/115.65be650.js","05f29b8a700c895792b3821ef08a15a9"],["/static/js/116.5e38507.js","f3380b64f26b29ba584aa22c027a1aa1"],["/static/js/117.ad07a7d.js","f305bb5198e98208c894e825c5edff29"],["/static/js/118.c893343.js","e9bf65fd1527e018f4ba5b4aeb84f81f"],["/static/js/119.55cdbc7.js","b7527d4473335f3895159912a96b900e"],["/static/js/12.3bf60d4.js","fee4cd5efc95f3d6e79be5888bff875d"],["/static/js/120.4e5227b.js","94844bfc854971e1e397e495f349841c"],["/static/js/121.743df10.js","3810d5594272d28c3625d59f537320e8"],["/static/js/122.a97da0a.js","624981b95d743d9cd4a1d381267a0a5f"],["/static/js/123.b43d008.js","1ed5ac36695ae22b3ab8a840c46bbce7"],["/static/js/124.b058c65.js","7950cb45f0404ba7eb74d8c70060a3b5"],["/static/js/125.2c4b4ac.js","e6bd0b6108e7d14d041e74109253d655"],["/static/js/126.7eb41be.js","4e5cf49e751e907c488b9d47ed4c1658"],["/static/js/127.a9f3a99.js","6a755d2df3173966de9195745f6ff8c7"],["/static/js/128.fb7d8a4.js","b0a7d25749ce0386d3d98f97d3535f18"],["/static/js/129.a1f230f.js","32d3c41cc702a36105efeb03dba6f2f2"],["/static/js/13.829d38e.js","428c5a77705b2d3c4292ac29d5ef7a25"],["/static/js/130.87fb6f2.js","0c664834497f5665784635f6eae72fa6"],["/static/js/131.f3f65cf.js","62351fe8b593b74bace6b21d14693641"],["/static/js/132.c1af20e.js","24eb95d4cdb8fb6826889d5b61c0d4bb"],["/static/js/133.d06f1ca.js","74737e0455609e7bb899179fde0df6b7"],["/static/js/134.726ceac.js","00395a4ad55be6adf557f1ee7593d801"],["/static/js/135.a5d4fc2.js","16810358df38c94388269f8dc583f110"],["/static/js/136.6c4f487.js","121fbb90636daa1de5e9fffca41746a6"],["/static/js/137.db457f7.js","ae3613c1ddd4ac27983a890f3fcd5980"],["/static/js/138.5cad019.js","e6294c5f7ec268709001bddfc3d85b7d"],["/static/js/139.dc568b5.js","c84a2e83d16e0eafead809c47a783bdd"],["/static/js/14.34bb182.js","07653aa53f5513d9678cf3c01f43cdaa"],["/static/js/140.3b0454c.js","b8773f1e8f86757900342a24e17c2615"],["/static/js/141.6cec48f.js","583d429ffea50fb97e385fe7c0eb736f"],["/static/js/142.bfa2635.js","3aaf867994c561170ec503cea2763df6"],["/static/js/143.644ad52.js","8b965eba29b2a6403ccf9a3f37f8832d"],["/static/js/144.4e36a71.js","38927b911e651d665877b9372d0004c3"],["/static/js/145.bb35360.js","4656076f56eb51f73cf149ac90355411"],["/static/js/146.87d1e5f.js","1263ec327cae41f40145cb5daea8453a"],["/static/js/147.3cd16b7.js","c57c56b0ed5db60248d9428b58cd09a7"],["/static/js/148.b615e20.js","c526bdb8017e0c713b5c3e688394352a"],["/static/js/149.85a080a.js","30b7e5dfd1b8c74caaf508022801f2d4"],["/static/js/15.f822110.js","e8a7d4f5ff1063370851cbb1a6d34c67"],["/static/js/150.ec6d4ea.js","64224d7c62b945431501bd164c607c80"],["/static/js/151.12d6968.js","835e615af4440006ee6c1c5cdc82819b"],["/static/js/152.16ab99a.js","905e90f0b9df52298a57a061c64c6f9e"],["/static/js/153.8c40138.js","b480b420107a20cb679666439e8733ec"],["/static/js/154.7515543.js","8b9591f96a6bd1a76ac6549338da1b51"],["/static/js/155.f7711eb.js","101dc58fcb617b4c022a26396624a3cb"],["/static/js/156.9ae0647.js","dc6deca8b21087e3eda3104fcc657fe6"],["/static/js/157.a6675df.js","ba53b21529305cae3dc917c6961a72a1"],["/static/js/158.b7b33c4.js","79828b093d58f2bb28e1dc4bcc0eb1c5"],["/static/js/159.e46f588.js","dc817c76b3de2926c3ea4b6dbf13536f"],["/static/js/16.5c51d64.js","3e23cb636ca6638bf838b2e9570ff194"],["/static/js/160.89adc08.js","38f95c77968e11b9d124a9da24dd92df"],["/static/js/161.bc087a0.js","3deb2ef801193f92704159cf538d330c"],["/static/js/162.408c0c7.js","c54531596a30b8ad46203987ab128bec"],["/static/js/163.49cf464.js","4850a67d869759bd9dc9b8afd96d7765"],["/static/js/164.da7e247.js","f483ac165cdeb75f47d2032908fe2ed7"],["/static/js/165.931f42d.js","a230f7ffca2fa467ea4b0a17111f45c8"],["/static/js/166.6929799.js","bd44c0a7b0a0ddbff292328b894ffb76"],["/static/js/167.bfada3e.js","1e75d1a442b2020d3a31e69ce41b99d1"],["/static/js/168.7bcb41e.js","c5194f623be1e5a3fa986b76e91300a1"],["/static/js/169.d6a0860.js","3cb80bc38e8ec0c07a3515da289fcbb8"],["/static/js/17.344f14a.js","c8e5c990954399f567c5893e4c8e02eb"],["/static/js/170.5763f75.js","20065d58930d5c87583f5389c7a35596"],["/static/js/171.b04db23.js","fc3903980a0ff781300560cab60fb2dd"],["/static/js/172.aee9b11.js","09e494993e17694708f0bc678b96c635"],["/static/js/173.7438d6a.js","57e356577e9aba84985bba34b7ae4e2b"],["/static/js/174.8227baf.js","a28a9e2509f70845153e74f173665f70"],["/static/js/175.77e9cc8.js","d2a6620ef017c2409b4e43bfa6bfea5c"],["/static/js/176.22538c9.js","607a98b241ff927fac91dac3347dbaa6"],["/static/js/177.dfa9603.js","7cc988303f4c2be5cf95155129014ee8"],["/static/js/178.98b2b85.js","9ed6d492e0f13b938bb692a583071dcd"],["/static/js/179.ab21b95.js","43c9a2bc1c744b253b5b2e0b979940d9"],["/static/js/18.e88dca3.js","f6312e2c3d980cc14034078d75529998"],["/static/js/180.f0542e3.js","a979a3e5d903252a24a1d2a534ec0ba0"],["/static/js/181.40c8efd.js","bca924e975844fccdbaac405af7af8de"],["/static/js/19.aa6e82d.js","3b7549a420e9616a8d8f006d0b729c8a"],["/static/js/2.0c162b1.js","646e14400bb5a4db8e82dd934a629092"],["/static/js/20.e1de38c.js","488576fe87b7a6b80ef59ee6789e5a4f"],["/static/js/21.5cd47b4.js","ad011f0bea06acabd645448913443f0d"],["/static/js/22.2f1d80d.js","515164f990b37a4cbf62fd7dcb376b11"],["/static/js/23.6ba9a4f.js","be3ab0d1d2409614df1f9aa50296e765"],["/static/js/24.dccc00a.js","13fc636cac1d3cde3685ec806e488776"],["/static/js/25.6b2607c.js","09da62b777829332284292e419fc151f"],["/static/js/26.b506236.js","badda6b44bfffa467471412040e597b3"],["/static/js/27.94548ab.js","f1afa2c463fe67dce5d34980cee90363"],["/static/js/28.73733a9.js","97158c77618f78f1666fdd38fb694cd1"],["/static/js/29.cc16618.js","0d238e29dafb407a3a0b35535201c829"],["/static/js/3.27a2c1a.js","8a98b0ebb404eb69834257b48ba6455b"],["/static/js/30.7ac7ba8.js","5718c932c2a320b90662b6c3d3cd03f9"],["/static/js/31.40811c4.js","822dbe73829c0a370a90ff7d7a1005de"],["/static/js/32.d695c5b.js","ed19fbc539968049c7f71b3d3955ffcb"],["/static/js/33.d250915.js","ab731d8dad09a6ed0c39de3917cfb7fb"],["/static/js/34.5e7ea77.js","9f6148829c69e20868561559aa741b1c"],["/static/js/35.a76f095.js","b7a655f130c5e9a30450af35b8ce3564"],["/static/js/36.0a53e1a.js","bdf112a713c477910012ad86fee829ae"],["/static/js/37.1e09366.js","10ecc51ac2aea3d3c250d24e57821f3f"],["/static/js/38.68d3fc3.js","f3efd6471da1203e9e78efcbc21a2de7"],["/static/js/39.84b4be0.js","272f77e371444104774ae9238588af31"],["/static/js/4.9376756.js","bd0b4d4c6625837330dad293605253e2"],["/static/js/40.855f217.js","25967f6a3b982753351e9afe5186284a"],["/static/js/41.774546b.js","c54256fa5fe0742faf79e4b2fe22fefe"],["/static/js/42.2abd58d.js","78a5435468dd808f3cdf8d0e074092fa"],["/static/js/43.5679a5f.js","4139a52a4b63293b400a5c635e44ab7b"],["/static/js/44.8f08586.js","e2e1443dfec44f8b38935a19085ab974"],["/static/js/45.afc35c7.js","bb85923ed08fe8904daf772a66cf86d7"],["/static/js/46.d79cde7.js","706809d1383eed89dc8ade79093104ca"],["/static/js/47.4c1a511.js","f123df79766c7737ac26809a014ac47b"],["/static/js/48.f0c1fd7.js","4c9a82fb519e5776ca0843f8ab50c947"],["/static/js/49.01ef3be.js","7beda15b3b2f263f9f52908759115e51"],["/static/js/5.bba32f0.js","0b05f37e367f18aa82bc00e85bb177ea"],["/static/js/50.5797003.js","2d5e55f6796e458b827f51de73c6613f"],["/static/js/51.015a2e6.js","d32da446f0fda6cb34923f4eac0139fd"],["/static/js/52.0161e80.js","dc5ebdff3f33a7c0a9ba31aad05ea050"],["/static/js/53.ff42074.js","a0b5205e6df4ce67f9ac5d1fc966ce7d"],["/static/js/54.0076aae.js","c9003721ff8901397d765e15aa30ba22"],["/static/js/55.90e08fd.js","da8df7001542222f10aab035d7c98b04"],["/static/js/56.25e97eb.js","08ed1dac3161cb10c2dc2c5e59e4d0eb"],["/static/js/57.368f6a8.js","17cfb8e0210ff051dfed3bf6274cef63"],["/static/js/58.4039860.js","32b8a376efe9c0e31846c7659655a0ff"],["/static/js/59.2506d72.js","ebf0b1292ca69c0f841b0166670a6aab"],["/static/js/6.1161cd7.js","123fa16b5e68a58870222b8ac589f7fb"],["/static/js/60.eeaee3e.js","d7935e54a2a5912c74ad8f273cee0b37"],["/static/js/61.967de92.js","f3df7336d081abc7b8dc9510c74c735c"],["/static/js/62.0f08d0f.js","5ac7eb4364c25ecd2f9d90f711970c5d"],["/static/js/63.448eb16.js","1f1c993970958336a4aec3924013def8"],["/static/js/64.4fbc096.js","8ee3ede8454312b10ff4ce0f1aaf02c4"],["/static/js/65.0c805dd.js","cfc3f434f83aee262f8506251399195b"],["/static/js/66.abb0fd1.js","92e07196a36c1cc0396a4faa21e5ed82"],["/static/js/67.b3f9928.js","69e0e705baf161c47054ea96ac5820b0"],["/static/js/68.dff01c4.js","d25dd490d2292843d3e59e7199b9cc52"],["/static/js/69.8e25b69.js","7f0a863d77ce772ad5784282837999f7"],["/static/js/7.a25b6bf.js","3730a73388c8eab67a650735a368d976"],["/static/js/70.1459159.js","b24bcb2b4641d2cb0e705214c15b219e"],["/static/js/71.5fca1a6.js","d3a935451c0178b9e30d2361f30336fe"],["/static/js/72.729a1ed.js","e4cc4df3818f5a3dda0d55a05872f57e"],["/static/js/73.64e4993.js","81662461e78c7e558318fb777088e20e"],["/static/js/74.0c6403b.js","9beaf485c1eaf2b1e8685faef6491825"],["/static/js/75.eb0655b.js","cbd66478e83a3d0054ed294ba8908b69"],["/static/js/76.74efaa9.js","3798bf06ac5806df04d69838059dc085"],["/static/js/77.a0fe2d2.js","c3b50499097f8d0bae4fd30e75d38edf"],["/static/js/78.27c3930.js","c84f61cbecf2e508a326a5132e3b5eeb"],["/static/js/79.9c62aa8.js","66d9c7792c110b1e3266b3c2218df6a4"],["/static/js/8.26b022a.js","3c6e3e3f41dd7b190da78d97d99d099b"],["/static/js/80.91b13b7.js","b499f1998740d0f2007efb02b5168a91"],["/static/js/81.3e12428.js","f177a9a7a7492e0895bd9f03bc82ea95"],["/static/js/82.a9b37e6.js","16fade77049eed2db6815a08d8a7985a"],["/static/js/83.060fa38.js","daee05b23156a44d9ff77647313c9012"],["/static/js/84.130b0cb.js","0e73237e17670e644c8c3eb45acb1a4b"],["/static/js/85.324bfc8.js","78c147a24d8bc90f65a814e9607061c6"],["/static/js/86.5e695c9.js","7952a8a6f1eeb28da70c3d6c43a799f5"],["/static/js/87.1da8d52.js","10bff43345a83c538242097cfa0b147a"],["/static/js/88.c3ace26.js","92c8241e699da758979de8756d09ff73"],["/static/js/89.ddbe56b.js","1b5987596af26f941c523b201a5569e7"],["/static/js/9.cfa150f.js","623083cbfabfc12afc1ceee17a220c51"],["/static/js/90.26ffc2e.js","c70edc1f9a49d7e38e5f63c73142531f"],["/static/js/91.bc51a9f.js","4abd2323b6865b3cd522a3633dc50fc2"],["/static/js/92.85e8f1e.js","ca208878119bbf70de708059f6f6af1d"],["/static/js/93.5e82b5d.js","02fef09c8529ca559f649160f25b46a0"],["/static/js/94.224e12b.js","dfd8027660373b5c756835b1e6686a85"],["/static/js/95.26033ed.js","aee96d805a966d4f586256327b0d4601"],["/static/js/96.06c3eb1.js","5ed359ad5cda6d7bbb60e2c55a6f99b7"],["/static/js/97.b9fa40b.js","b68fef6729190320372f7a999b7ebaca"],["/static/js/98.b94fe96.js","d5c99eb58fe143f4816d9bfe9db6ef70"],["/static/js/99.ad88144.js","9653fc796f5d925f93c01cebf25acfae"],["/static/js/admin.ac8d295.js","1af6a169405a9a3ac61ce1043891d7a1"],["/static/js/manifest.b715214.js","d34ce4ab9b5bd3430d5ea2d4e617b627"],["/static/js/vendor.68111c6.js","2b1d4ad99ef137b9ec435aa91b3d5e59"]];
var cacheName = 'sw-precache-v3-doracms-vue2-ssr-' + (self.registration ? self.registration.scope : '');


var ignoreUrlParametersMatching = [/^utm_/];



var addDirectoryIndex = function(originalUrl, index) {
    var url = new URL(originalUrl);
    if (url.pathname.slice(-1) === '/') {
      url.pathname += index;
    }
    return url.toString();
  };

var cleanResponse = function(originalResponse) {
    // If this is not a redirected response, then we don't have to do anything.
    if (!originalResponse.redirected) {
      return Promise.resolve(originalResponse);
    }

    // Firefox 50 and below doesn't support the Response.body stream, so we may
    // need to read the entire body to memory as a Blob.
    var bodyPromise = 'body' in originalResponse ?
      Promise.resolve(originalResponse.body) :
      originalResponse.blob();

    return bodyPromise.then(function(body) {
      // new Response() is happy when passed either a stream or a Blob.
      return new Response(body, {
        headers: originalResponse.headers,
        status: originalResponse.status,
        statusText: originalResponse.statusText
      });
    });
  };

var createCacheKey = function(originalUrl, paramName, paramValue,
                           dontCacheBustUrlsMatching) {
    // Create a new URL object to avoid modifying originalUrl.
    var url = new URL(originalUrl);

    // If dontCacheBustUrlsMatching is not set, or if we don't have a match,
    // then add in the extra cache-busting URL parameter.
    if (!dontCacheBustUrlsMatching ||
        !(url.pathname.match(dontCacheBustUrlsMatching))) {
      url.search += (url.search ? '&' : '') +
        encodeURIComponent(paramName) + '=' + encodeURIComponent(paramValue);
    }

    return url.toString();
  };

var isPathWhitelisted = function(whitelist, absoluteUrlString) {
    // If the whitelist is empty, then consider all URLs to be whitelisted.
    if (whitelist.length === 0) {
      return true;
    }

    // Otherwise compare each path regex to the path of the URL passed in.
    var path = (new URL(absoluteUrlString)).pathname;
    return whitelist.some(function(whitelistedPathRegex) {
      return path.match(whitelistedPathRegex);
    });
  };

var stripIgnoredUrlParameters = function(originalUrl,
    ignoreUrlParametersMatching) {
    var url = new URL(originalUrl);
    // Remove the hash; see https://github.com/GoogleChrome/sw-precache/issues/290
    url.hash = '';

    url.search = url.search.slice(1) // Exclude initial '?'
      .split('&') // Split into an array of 'key=value' strings
      .map(function(kv) {
        return kv.split('='); // Split each 'key=value' string into a [key, value] array
      })
      .filter(function(kv) {
        return ignoreUrlParametersMatching.every(function(ignoredRegex) {
          return !ignoredRegex.test(kv[0]); // Return true iff the key doesn't match any of the regexes.
        });
      })
      .map(function(kv) {
        return kv.join('='); // Join each [key, value] array into a 'key=value' string
      })
      .join('&'); // Join the array of 'key=value' strings into a string with '&' in between each

    return url.toString();
  };


var hashParamName = '_sw-precache';
var urlsToCacheKeys = new Map(
  precacheConfig.map(function(item) {
    var relativeUrl = item[0];
    var hash = item[1];
    var absoluteUrl = new URL(relativeUrl, self.location);
    var cacheKey = createCacheKey(absoluteUrl, hashParamName, hash, /./);
    return [absoluteUrl.toString(), cacheKey];
  })
);

function setOfCachedUrls(cache) {
  return cache.keys().then(function(requests) {
    return requests.map(function(request) {
      return request.url;
    });
  }).then(function(urls) {
    return new Set(urls);
  });
}

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return setOfCachedUrls(cache).then(function(cachedUrls) {
        return Promise.all(
          Array.from(urlsToCacheKeys.values()).map(function(cacheKey) {
            // If we don't have a key matching url in the cache already, add it.
            if (!cachedUrls.has(cacheKey)) {
              var request = new Request(cacheKey, {credentials: 'same-origin'});
              return fetch(request).then(function(response) {
                // Bail out of installation unless we get back a 200 OK for
                // every request.
                if (!response.ok) {
                  throw new Error('Request for ' + cacheKey + ' returned a ' +
                    'response with status ' + response.status);
                }

                return cleanResponse(response).then(function(responseToCache) {
                  return cache.put(cacheKey, responseToCache);
                });
              });
            }
          })
        );
      });
    }).then(function() {
      
      // Force the SW to transition from installing -> active state
      return self.skipWaiting();
      
    })
  );
});

self.addEventListener('activate', function(event) {
  var setOfExpectedUrls = new Set(urlsToCacheKeys.values());

  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.keys().then(function(existingRequests) {
        return Promise.all(
          existingRequests.map(function(existingRequest) {
            if (!setOfExpectedUrls.has(existingRequest.url)) {
              return cache.delete(existingRequest);
            }
          })
        );
      });
    }).then(function() {
      
      return self.clients.claim();
      
    })
  );
});


self.addEventListener('fetch', function(event) {
  if (event.request.method === 'GET') {
    // Should we call event.respondWith() inside this fetch event handler?
    // This needs to be determined synchronously, which will give other fetch
    // handlers a chance to handle the request if need be.
    var shouldRespond;

    // First, remove all the ignored parameters and hash fragment, and see if we
    // have that URL in our cache. If so, great! shouldRespond will be true.
    var url = stripIgnoredUrlParameters(event.request.url, ignoreUrlParametersMatching);
    shouldRespond = urlsToCacheKeys.has(url);

    // If shouldRespond is false, check again, this time with 'index.html'
    // (or whatever the directoryIndex option is set to) at the end.
    var directoryIndex = 'index.html';
    if (!shouldRespond && directoryIndex) {
      url = addDirectoryIndex(url, directoryIndex);
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond is still false, check to see if this is a navigation
    // request, and if so, whether the URL matches navigateFallbackWhitelist.
    var navigateFallback = '';
    if (!shouldRespond &&
        navigateFallback &&
        (event.request.mode === 'navigate') &&
        isPathWhitelisted([], event.request.url)) {
      url = new URL(navigateFallback, self.location).toString();
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond was set to true at any point, then call
    // event.respondWith(), using the appropriate cache key.
    if (shouldRespond) {
      event.respondWith(
        caches.open(cacheName).then(function(cache) {
          return cache.match(urlsToCacheKeys.get(url)).then(function(response) {
            if (response) {
              return response;
            }
            throw Error('The cached response that was expected is missing.');
          });
        }).catch(function(e) {
          // Fall back to just fetch()ing the request if some unexpected error
          // prevented the cached response from being valid.
          console.warn('Couldn\'t serve response for "%s" from cache: %O', event.request.url, e);
          return fetch(event.request);
        })
      );
    }
  }
});







