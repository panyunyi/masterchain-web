const express = require('express')
const router = express.Router()
router.caseSensitive = true
router.strict = true
const {
    AdminUser,
    AdminGroup,
    AdminResource,
    ContentCategory,
    Content,
    ContentTag,
    User,
    Message,
    SystemConfig,
    DataOptionLog,
    SystemOptionLog,
    UserNotify,
    Notify,
    Ads,
    ContentTemplate,
    Community,
    CommunityTag,
    Special,
    MasterCategory,
    SiteMessage,
    ClassType,
    ClassInfo,
    ClassContent,
    ClassFeedback,
    CommunityContent,
    UserActionHis,
    ClassScore,
    BillRecord,
    Mstt_book,
    Reward_his,
    Review_his,
    User_mi_mp_his,
    User_ev_his,
    User_reward_his,
    Threshold_master,
    Action_limit_master,
    Weight_master,
    ClassPayRecord,
    ResetAmount,
    Agreement,
    HelpCenter,
    FeedBack,
    CurrencyApproval,
    CreativeRight,
    ReportRecord,
    IdentityAuthentication,
    SignIn,
    OpenVipRecord,
    VipSetMeal,
    CommunityMessage,
    HotSearch,
    VersionManage,
    Integralonfig,
    //MgRoutersController
} = require('../lib/controller');
const {
    service,
    authSession,
    authToken,
    authPower,
    validatorUtil
} = require('../../utils');

const generalFun = require("../lib/utils/generalFun");

// 管理员退出
router.get('/logout', (req, res) => {
    req.session.adminlogined = false;
    req.session.adminPower = '';
    req.session.adminUserInfo = '';
    res.send({
        status: 200
    });
});

// 获取管理员信息
router.get('/getUserSession', (req, res, next) => {
    if (req.session.adminlogined) {
        next()
    } else {
        res.send({
            data: {
                state: false,
                userInfo: {}
            }
        });
    }
}, AdminUser.getUserSession)

// 获取后台基础信息
router.get('/getSitBasicInfo', authSession, authPower, AdminUser.getBasicSiteInfo)

/**
 * 管理员管理
 */
router.get('/adminUser/getList', authToken, authPower, AdminUser.getAdminUsers)

router.post('/adminUser/addOne', authToken, authPower, AdminUser.addAdminUser)

router.post('/adminUser/updateOne', authToken, authPower, AdminUser.updateAdminUser)

router.get('/adminUser/deleteUser', authToken, authPower, AdminUser.delAdminUser)


/**
 * 角色管理
 */
router.get('/adminGroup/getList', authToken, authPower, AdminGroup.getAdminGroups)

router.post('/adminGroup/addOne', authToken, authPower, AdminGroup.addAdminGroup)

router.post('/adminGroup/updateOne', authToken, authPower, AdminGroup.updateAdminGroup)

router.get('/adminGroup/deleteGroup', authToken, authPower, AdminGroup.delAdminGroup)


/**
 * 资源管理
 * 
 */

router.get('/adminResource/getList', authToken, authPower, AdminResource.getAdminResources)

router.post('/adminResource/addOne', authToken, authPower, AdminResource.addAdminResource)

router.post('/adminResource/updateOne', authToken, authPower, AdminResource.updateAdminResource)

router.get('/adminResource/deleteResource', authToken, authPower, AdminResource.delAdminResource)

/**
 * 系统配置
 * 此api名称尽量不要改
 */
router.get('/systemConfig/getConfig', authToken, authPower, SystemConfig.getSystemConfigs)

router.post('/systemConfig/updateConfig', authToken, authPower, SystemConfig.updateSystemConfig)


/**
 * 专题类别管理
 * 
 */

router.get('/contentCategory/getList', authToken, authPower, ContentCategory.getContentCategories)

router.post('/contentCategory/addOne', authToken, authPower, ContentCategory.addContentCategory)

router.post('/contentCategory/updateOne', authToken, authPower, ContentCategory.updateContentCategory)

router.get('/contentCategory/deleteCategory', authToken, authPower, ContentCategory.delContentCategory)

/**
 * 文档管理
 * 
 */

router.get('/content/getList', authToken, authPower, Content.getContents)

router.get('/content/getContent', authToken, authPower, Content.getOneContent)

router.post('/content/addOne', authToken, authPower, Content.addContent)

router.post('/content/updateOne', authToken, authPower, Content.updateContent)

router.post('/content/topContent', authToken, authPower, Content.updateContentToTop)

router.post('/content/roofContent', authToken, authPower, Content.roofPlacement)

router.get('/content/deleteContent', authToken, authPower, Content.delContent)

router.get('/content/getAliOSSConfig', authToken, Content.getAliOSSConfig)

// 给文章分配用户
router.post('/content/redictContentToUsers', authToken, authPower, Content.redictContentToUsers)
// 给文章分配专题
router.post('/content/redictContentToSpecials', authToken, authPower, Content.redictContentToSpecials)

/**
 * tag管理
 */
router.get('/contentTag/getList', authToken, authPower, ContentTag.getContentTags)

router.post('/contentTag/addOne', authToken, authPower, ContentTag.addContentTag)

router.post('/contentTag/updateOne', authToken, authPower, ContentTag.updateContentTag)

router.get('/contentTag/deleteTag', authToken, authPower, ContentTag.delContentTag)

/**
 * 留言管理
 */
router.get('/contentMessage/getList', authToken, authPower, Message.getMessages)

router.post('/contentMessage/addOne', authToken, authPower, Message.postMessages)

router.get('/contentMessage/deleteMessage', authToken, authPower, Message.delMessage)

/**
 * 注册用户管理
 */
router.get('/regUser/getList', authToken, authPower, User.getUsers)

router.post('/regUser/updateOne', authToken, authPower, User.updateUser)

router.get('/regUser/deleteUser', authToken, authPower, User.delUser)


/**
 * 数据备份
 */

//获取备份数据列表
router.get('/backupDataManage/getBakList', authToken, authPower, DataOptionLog.getDataBakList);

//备份数据库执行
router.post('/backupDataManage/backUp', authToken, authPower, DataOptionLog.backUpData);

//删除备份数据
router.get('/backupDataManage/deleteDataItem', authToken, authPower, DataOptionLog.delDataItem);

/**
 * 系统操作日志
 */

router.get('/systemOptionLog/getList', authToken, authPower, SystemOptionLog.getSystemOptionLogs);

//删除操作日志
router.get('/systemOptionLog/deleteLogItem', authToken, authPower, SystemOptionLog.delSystemOptionLogs);

// 清空日志
router.get('/systemOptionLog/deleteAllLogItem', authToken, authPower, (req, res, next) => {
    req.query.ids = 'all';
    next()
}, SystemOptionLog.delSystemOptionLogs);


/**
 * 系统消息
 */

router.get('/systemNotify/getList', authToken, authPower, (req, res, next) => {
    req.query.systemUser = req.session.adminUserInfo._id;
    next()
}, UserNotify.getUserNotifys);

//删除操作日志
router.get('/systemNotify/deleteNotifyItem', authToken, authPower, UserNotify.delUserNotify);

// 设为已读消息
router.get('/systemNotify/setHasRead', authToken, authPower, (req, res, next) => {
    req.query.systemUser = req.session.adminUserInfo._id;
    next()
}, UserNotify.setMessageHasRead);

/**
 * 系统公告
 */

router.get('/systemAnnounce/getList', authToken, authPower, (req, res, next) => {
    req.query.type = '1';
    next()
}, Notify.getNotifys);

// 删除公告
router.get('/systemAnnounce/deleteItem', authToken, authPower, Notify.delNotify);

//发布系统公告
router.post('/systemAnnounce/addOne', authToken, authPower, Notify.addOneSysNotify);


/**
 * 广告管理
 */

router.get('/ads/getList', authToken, authPower, Ads.getAds);

// 获取单个广告
router.get('/ads/getOne', authToken, authPower, Ads.getOneAd);

// 新增广告
router.post('/ads/addOne', authToken, authPower, Ads.addAds);

// 更新单个广告
router.post('/ads/updateOne', authToken, authPower, Ads.updateAds);

// 删除广告
router.get('/ads/delete', authToken, authPower, Ads.delAds);

// 获取模板文件列表
router.get('/template/getTemplateForderList', authToken, authPower, ContentTemplate.getContentDefaultTemplate);

// 读取文件内容
router.get('/template/getTemplateFileText', authToken, authPower, ContentTemplate.getFileInfo);

// 更新文件内容
router.post('/template/updateTemplateFileText', authToken, authPower, ContentTemplate.updateFileInfo);

// 获取已安装的模板列表
router.get('/template/getMyTemplateList', authToken, authPower, ContentTemplate.getMyTemplateList);

// 新增模板单元
router.post('/template/addTemplateItem', authToken, authPower, ContentTemplate.addTemplateItem);

// 删除模板单元
router.get('/template/delTemplateItem', authToken, authPower, ContentTemplate.delTemplateItem);

// 获取默认模板的模板单元列表
router.get('/template/getTemplateItemlist', authToken, authPower, ContentTemplate.getTempItemForderList);

// 获取模板市场中的模板列表
router.get('/template/getTempsFromShop', authToken, authPower, ContentTemplate.getTempsFromShop);

// 安装模板
router.get('/template/installTemp', authToken, authPower, ContentTemplate.installTemp);

// 启用模板
router.get('/template/enableTemp', authToken, authPower, ContentTemplate.enableTemp);

// 卸载模板
router.get('/template/uninstallTemp', authToken, authPower, ContentTemplate.uninstallTemp);

/**
 * 
 * 社群管理
 * 
 */
router.get('/community/getList', authToken, authPower, Community.getCommunity);

router.post('/community/addOne', authToken, authPower, Community.addCommunity);

router.post('/community/updateOne', authToken, authPower, Community.updateCommunity);

router.get('/community/delete', authToken, authPower, Community.delCommunity);

/**
 * 
 * 社群标签管理
 * 
 */
router.get('/communityTag/getList', authToken, authPower, (req, res, next) => {
    req.query.model = 'full';
    next();
}, CommunityTag.getCommunityTags);

router.post('/communityTag/addOne', authToken, authPower, CommunityTag.addCommunityTag);

router.post('/communityTag/updateOne', authToken, authPower, CommunityTag.updateCommunityTag);

router.get('/communityTag/delete', authToken, authPower, CommunityTag.delCommunityTag);

/**
 * 
 * 专题管理
 * 
 */
router.get('/special/getList', authToken, authPower, Special.getSpecials);

router.post('/special/addOne', authToken, authPower, Special.addSpecial);

router.post('/special/updateOne', authToken, authPower, Special.updateSpecial);

router.get('/special/delete', authToken, authPower, Special.delSpecial);

/**
 * 
 * 大师类别管理
 * 
 */
router.get('/masterCategory/getList', authToken, authPower, MasterCategory.getMasterCategorys);

router.post('/masterCategory/addOne', authToken, authPower, MasterCategory.addMasterCategory);

router.post('/masterCategory/updateOne', authToken, authPower, MasterCategory.updateMasterCategory);

router.get('/masterCategory/delete', authToken, authPower, MasterCategory.delMasterCategory);


router.get('/siteMessage/getList', authToken, authPower, SiteMessage.getSiteMessages);

// router.post('/siteMessage/addOne', SiteMessage.addSiteMessage);

router.post('/siteMessage/updateOne', authToken, authPower, SiteMessage.updateSiteMessage);

router.get('/siteMessage/delete', authToken, authPower, SiteMessage.delSiteMessage);

router.get('/classType/getList', authToken, authPower, ClassType.getClassTypes);

router.post('/classType/addOne', authToken, authPower, ClassType.addClassType);

router.post('/classType/updateOne', authToken, authPower, ClassType.updateClassType);

router.get('/classType/delete', authToken, authPower, ClassType.delClassType);

router.get('/classInfo/getList', authToken, authPower, ClassInfo.getClassInfos);

router.post('/classInfo/addOne', authToken, authPower, ClassInfo.addClassInfo);

router.post('/classInfo/updateOne', authToken, authPower, ClassInfo.updateClassInfo);

router.get('/classInfo/delete', authToken, authPower, ClassInfo.delClassInfo);

router.get('/classContent/getList', authToken, authPower, ClassContent.getClassContents);

router.post('/classContent/addOne', authToken, authPower, ClassContent.addClassContent);

router.post('/classContent/updateOne', authToken, authPower, ClassContent.updateClassContent);

router.get('/classContent/delete', authToken, authPower, ClassContent.delClassContent);

router.get("/classManagePreview.html", authToken, authPower, generalFun.getDataForPlayerShow);

router.get('/classFeedback/getList', authToken, authPower, ClassFeedback.getClassFeedBacks)

// router.post('/classFeedback/addOne', ClassFeedback.postClassFeedBacks)

router.get('/classFeedback/delete', authToken, authPower, ClassFeedback.delClassFeedBack)

router.get('/communityContent/getList', authToken, authPower, CommunityContent.getCommunityContents);

// router.post('/communityContent/addOne', CommunityContent.addCommunityContent);

router.post('/communityContent/updateOne', authToken, authPower, CommunityContent.updateCommunityContent);

router.get('/communityContent/delete', authToken, authPower, CommunityContent.delCommunityContent);

router.get('/userActionHis/getList', authToken, authPower, UserActionHis.getUserActionHiss);

router.post('/userActionHis/updateOne', authToken, authPower, UserActionHis.updateUserActionHis);

router.get('/userActionHis/delete', authToken, authPower, UserActionHis.delUserActionHis);

router.get('/classScore/getList', authToken, authPower, ClassScore.getClassScores);

// router.post('/classScore/addOne', ClassScore.addClassScore);

// router.post('/classScore/updateOne', authToken, authPower, ClassScore.updateClassScore);

router.get('/classScore/delete', authToken, authPower, ClassScore.delClassScore);

router.get('/billRecord/getList', authToken, authPower, BillRecord.getBillRecords);

// 邀请列表
router.get('/invitionRecord/getList', authToken, authPower, BillRecord.getInvitionRecords);

// router.post('/billRecord/addOne', authToken, authPower, BillRecord.addBillRecord);

// router.get('/billRecord/delete', authToken, authPower, BillRecord.delBillRecord);

router.get('/mstt_book/getList', authToken, authPower, Mstt_book.getMstt_books);

router.post('/mstt_book/addOne', authToken, authPower, Mstt_book.addMstt_book);

router.post('/mstt_book/updateOne', authToken, authPower, Mstt_book.updateMstt_book);

router.get('/mstt_book/delete', authToken, authPower, Mstt_book.delMstt_book);

router.get('/reward_his/getList', authToken, authPower, Reward_his.getReward_hiss);

router.post('/reward_his/addOne', authToken, authPower, Reward_his.addReward_his);

router.post('/reward_his/updateOne', authToken, authPower, Reward_his.updateReward_his);

router.get('/reward_his/delete', authToken, authPower, Reward_his.delReward_his);

router.get('/review_his/getList', authToken, authPower, Review_his.getReview_hiss);

router.post('/review_his/addOne', authToken, authPower, Review_his.addReview_his);

router.post('/review_his/updateOne', authToken, authPower, Review_his.updateReview_his);

router.get('/review_his/delete', authToken, authPower, Review_his.delReview_his);

router.get('/user_mi_mp_his/getList', authToken, authPower, User_mi_mp_his.getUser_mi_mp_hiss);

router.post('/user_mi_mp_his/addOne', authToken, authPower, User_mi_mp_his.addUser_mi_mp_his);

router.post('/user_mi_mp_his/updateOne', authToken, authPower, User_mi_mp_his.updateUser_mi_mp_his);

router.get('/user_mi_mp_his/delete', authToken, authPower, User_mi_mp_his.delUser_mi_mp_his);

router.get('/user_ev_his/getList', authToken, authPower, User_ev_his.getUser_ev_hiss);

router.post('/user_ev_his/addOne', authToken, authPower, User_ev_his.addUser_ev_his);

router.post('/user_ev_his/updateOne', authToken, authPower, User_ev_his.updateUser_ev_his);

router.get('/user_ev_his/delete', authToken, authPower, User_ev_his.delUser_ev_his);

router.get('/user_reward_his/getList', authToken, authPower, User_reward_his.getUser_reward_hiss);

router.post('/user_reward_his/addOne', authToken, authPower, User_reward_his.addUser_reward_his);

router.post('/user_reward_his/updateOne', authToken, authPower, User_reward_his.updateUser_reward_his);

router.get('/user_reward_his/delete', authToken, authPower, User_reward_his.delUser_reward_his);

router.get('/threshold_master/getList', authToken, authPower, Threshold_master.getThreshold_masters);

router.post('/threshold_master/addOne', authToken, authPower, Threshold_master.addThreshold_master);

router.post('/threshold_master/updateOne', authToken, authPower, Threshold_master.updateThreshold_master);

router.get('/threshold_master/delete', authToken, authPower, Threshold_master.delThreshold_master);

router.get('/action_limit_master/getList', authToken, authPower, Action_limit_master.getAction_limit_masters);

router.post('/action_limit_master/addOne', authToken, authPower, Action_limit_master.addAction_limit_master);

router.post('/action_limit_master/updateOne', authToken, authPower, Action_limit_master.updateAction_limit_master);

router.get('/action_limit_master/delete', authToken, authPower, Action_limit_master.delAction_limit_master);

router.get('/weight_master/getList', authToken, authPower, Weight_master.getWeight_masters);

router.post('/weight_master/addOne', authToken, authPower, Weight_master.addWeight_master);

router.post('/weight_master/updateOne', authToken, authPower, Weight_master.updateWeight_master);

router.get('/weight_master/delete', authToken, authPower, Weight_master.delWeight_master);

router.get('/classPayRecord/getList', authToken, authPower, ClassPayRecord.getClassPayRecords);

// router.post('/classPayRecord/addOne', authToken, authPower, ClassPayRecord.addClassPayRecord);

// router.get('/classPayRecord/delete', authToken, authPower, ClassPayRecord.delClassPayRecord);


router.get('/resetAmount/getList', authToken, authPower, ResetAmount.getResetAmounts);

router.post('/resetAmount/addOne', authToken, authPower, ResetAmount.addResetAmount);

router.post('/resetAmount/updateOne', authToken, authPower, ResetAmount.updateResetAmount);

router.get('/resetAmount/delete', authToken, authPower, ResetAmount.delResetAmount);

router.get('/agreement/getList', authToken, authPower, Agreement.getAgreements);

router.post('/agreement/addOne', authToken, authPower, Agreement.addAgreement);

router.post('/agreement/updateOne', authToken, authPower, Agreement.updateAgreement);

router.get('/agreement/delete', authToken, authPower, Agreement.delAgreement);

router.get('/helpCenter/getList', authToken, authPower, HelpCenter.getHelpCenters);

router.post('/helpCenter/addOne', authToken, authPower, HelpCenter.addHelpCenter);

router.post('/helpCenter/updateOne', authToken, authPower, HelpCenter.updateHelpCenter);

router.get('/helpCenter/delete', authToken, authPower, HelpCenter.delHelpCenter);

router.get('/feedBack/getList', authToken, authPower, FeedBack.getFeedBacks);

router.post('/feedBack/addOne', authToken, authPower, FeedBack.addFeedBack);

router.post('/feedBack/updateOne', authToken, authPower, FeedBack.updateFeedBack);

router.get('/feedBack/delete', authToken, authPower, FeedBack.delFeedBack);

router.get('/currencyApproval/getList', authToken, authPower, CurrencyApproval.getCurrencyApprovals);

// router.post('/currencyApproval/addOne', authToken, authPower, CurrencyApproval.addCurrencyApproval);

router.post('/currencyApproval/updateOne', authToken, authPower, CurrencyApproval.updateCurrencyApproval);

// router.get('/currencyApproval/delete', authToken, authPower, CurrencyApproval.delCurrencyApproval);

router.get('/creativeRight/getList', authToken, authPower, CreativeRight.getCreativeRights);

// router.post('/creativeRight/addOne', authToken, authPower, CreativeRight.addCreativeRight);

router.post('/creativeRight/updateOne', authToken, authPower, CreativeRight.updateCreativeRight);

// router.get('/creativeRight/delete', authToken, authPower, CreativeRight.delCreativeRight);

router.get('/reportRecord/getList', authToken, authPower, ReportRecord.getReportRecords);

// router.post('/reportRecord/addOne', authToken, authPower, ReportRecord.addReportRecord);

router.post('/reportRecord/updateOne', authToken, authPower, ReportRecord.updateReportRecord);

// router.get('/reportRecord/delete', authToken, authPower, ReportRecord.delReportRecord);

router.get('/identityAuthentication/getList', authToken, authPower, IdentityAuthentication.getIdentityAuthentications);

// router.post('/identityAuthentication/addOne', authToken, authPower, IdentityAuthentication.addIdentityAuthentication);

router.post('/identityAuthentication/updateOne', authToken, authPower, IdentityAuthentication.updateIdentityAuthentication);

// router.get('/identityAuthentication/delete', authToken, authPower, IdentityAuthentication.delIdentityAuthentication);

router.get('/signIn/getList', authToken, authPower, SignIn.getSignIns);

router.post('/signIn/addOne', authToken, authPower, SignIn.addSignIn);


router.get('/openVipRecord/getList', authToken, authPower, OpenVipRecord.getOpenVipRecords);

//   router.post('/openVipRecord/addOne', authToken, authPower, OpenVipRecord.addOpenVipRecord);

//   router.post('/openVipRecord/updateOne', authToken, authPower, OpenVipRecord.updateOpenVipRecord);

//   router.get('/openVipRecord/delete', authToken, authPower, OpenVipRecord.delOpenVipRecord);

router.get('/vipSetMeal/getList', authToken, authPower, VipSetMeal.getVipSetMeals);

router.post('/vipSetMeal/addOne', authToken, authPower, VipSetMeal.addVipSetMeal);

router.post('/vipSetMeal/updateOne', authToken, authPower, VipSetMeal.updateVipSetMeal);

//   router.get('/vipSetMeal/delete', authToken, authPower, VipSetMeal.delVipSetMeal);

/**
 * 社群评论管理
 */
router.get('/communityMessage/getList', authToken, authPower, CommunityMessage.getCommunityMessages);

router.post('/communityMessage/addOne', authToken, authPower, CommunityMessage.postCommunityMessages);

router.get('/communityMessage/deleteMessage', authToken, authPower, CommunityMessage.delCommunityMessage);

/**
 * 热搜管理
 */

router.get('/hotSearch/getList', authToken, authPower, HotSearch.getHotSearchs);

router.get('/hotSearch/delete', authToken, authPower, HotSearch.delHotSearch);

router.get('/versionManage/getList', authToken, authPower, VersionManage.getVersionManages);

router.post('/versionManage/updateOne', authToken, authPower, VersionManage.updateVersionData);



router.get('/mbtManage/getList', authToken, authPower, BillRecord.getShopMallMbtList);

router.post('/mbtManage/addOne', authToken, authPower, BillRecord.addBillRecordByType);

//   router.get('/mbtManage/delete', authToken, authPower, MbtManage.delMbtManage);

router.get('/integralonfig/getList', authToken, authPower, Integralonfig.getIntegralonfigs);

//   router.post('/integralonfig/addOne', authToken, authPower, Integralonfig.addIntegralonfig);

router.post('/integralonfig/updateOne', authToken, authPower, Integralonfig.updateIntegralonfig);

//   router.get('/integralonfig/delete', authToken, authPower, Integralonfig.delIntegralonfig);
// router.get('/adminUser/makeWallets', authToken, User.makeWallets);
//ManageRouters











module.exports = router