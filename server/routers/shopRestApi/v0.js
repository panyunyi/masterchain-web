/**
 * api
 * 
 */
const express = require('express')
const router = express.Router()
router.caseSensitive = true
router.strict = true

const {
  siteFunc,
  authPower
} = require('../../../utils');

// const authAdminToken = require('../../../utils/authAdminToken');

const {
  BillRecord,
  User,
  AdminUser,
  SystemConfig,
  VipSetMeal,
  OpenVipRecord
} = require('../../lib/controller');


const _ = require('lodash');
const qr = require('qr-image')


//---------------------------------------------------------用户相关api

/**
 * @api {post} /shop/v0/adminUser/getToken 获取管理员token
 * @apiDescription 获取管理员token
 * @apiName getToken
 * @apiGroup AdminUser
 * @apiParam {string} userName 管理员用户名
 * @apiParam {string} password 管理员密码
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *  {
 *    data: {
 *      token: "eyJkYXRhIjp7InVzZXJJZCI6Inp3d2RKdkxtUCIsInBob25lTnVtIjoxNTIyMDA2NDI5NH0sImNyZWF0ZWQiOjE1NDI2NDMyNTAsImV4cCI6MzYwMH0=.SW3JVAjkQUX0mgrSBuOirB3kQV6NNatlc4j/qW7SxTM="
 *    } 
 *    message: "登录成功"
 *    server_time: 1542089573405
 *    status: 200
 *  }  
 * @apiError {json} result
 * @apiErrorExample {json} Error-Response:
 *  {
 *    data: {
 *       "token" : ""
 *    }
 *    message: "登录信息错误"
 *    server_time: 1542082677922
 *    status: 500
 *  }
 * @apiSampleRequest http://localhost:8884/shop/v0/adminUser/getToken
 * @apiVersion 1.0.0
 */
router.post('/adminUser/getToken', AdminUser.loginActionForShop);

/**
 * @api {post} /shop/v0/user/sendVerificationCode 发送验证码
 * @apiDescription 发送验证码
 * @apiName sendVerificationCode
 * @apiGroup User
 * @apiParam {string} phoneNum 手机号(eq:15220064294)
 * @apiParam {string} countryCode 国家代码（eq: 86）
 * @apiParam {string} email  邮箱
 * @apiParam {string} messageType 发送验证码类别（0、注册 1、登录，2、忘记资金密码找回, 3、忘记密码，4、身份验证, 5、管理员登录，6、游客绑定邮箱或手机号）
 * @apiParam {string} sendType 发送验证码形式（1: 短信验证码  2:邮箱验证码）
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 * {
 *     "status": 200,
 *     "message": "send Code success",
 *     "server_time": 1542382533904,
 *     "data": {
 *         "messageCode": "378047"
 *     }
 * }  
 * @apiError {json} result
 * @apiErrorExample {json} Error-Response:
 *  {
 *    data: {}
 *    message: "验证码错误"
 *    server_time: 1542082677922
 *    status: 500
 *  }
 * @apiSampleRequest http://localhost:8884/shop/v0/user/sendVerificationCode
 * @apiVersion 1.0.0
 */
router.post('/user/sendVerificationCode', User.sendVerificationCode);


/**
 * @api {post} /shop/v0/user/doReg 用户注册
 * @apiDescription 用户注册
 * @apiName doReg
 * @apiGroup User
 * @apiParam {string} regType 注册类型（1:手机号注册  2:邮箱注册）
 * @apiParam {string} phoneNum 手机号（eq:15220064294）
 * @apiParam {string} countryCode 国家代码（eq: 86）
 * @apiParam {string} messageCode 手机验证码/邮箱验证码(eq: 123456)
 * @apiParam {string} email 注册邮箱
 * @apiParam {string} password 密码
 * @apiParam {string} logo 用户头像
 * @apiParam {string} userName 用户名
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "注册成功",
 *    "server_time": 1544246883076,
 *    "data": {}
 *}
 * @apiError {json} result
 * @apiErrorExample {json} Error-Response:
 *  {
 *    data: {}
 *    message: "注册失败"
 *    server_time: 1542082677922
 *    status: 500
 *  }
 * @apiSampleRequest http://localhost:8884/shop/v0/user/doReg
 * @apiVersion 1.0.0
 */
router.post('/user/doReg', User.regAction);


/**
 * @api {post} /shop/v0/user/doLogin 用户统一登录
 * @apiDescription 用户登录（密码，验证码，邮箱登录，通过参数 loginType 区分类型）
 * @apiName doLogin
 * @apiGroup User
 * @apiParam {string} phoneNum 手机号（eq:15220064294）
 * @apiParam {string} countryCode 国家代码（eq: 86）
 * @apiParam {string} email 用户邮箱(xx@qq.com)
 * @apiParam {string} loginType 登录类型 (1:手机验证码登录 2:手机号密码登录 3:邮箱密码登录,4:邮箱验证码登录)
 * @apiParam {string} messageCode 手机验证码(eq: 123456)
 * @apiParam {string} password 密码 // 非必填，与短信验证码选其一
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *  {
 *    data: {
 *      comments: ""
 *      date: "2018-11-13 12:09:29"
 *      email: "doramart@qq.com"
 *      enable: true
 *      group: "0"
 *      id: "zwwdJvLmP"
 *      logo: "/upload/images/defaultlogo.png"
 *      userName: "doramart"
 *      walletAddress: "0x2ed28eDEbD296Bf085C582f5187E4F987e6214e6",
 *      token: "eyJkYXRhIjp7InVzZXJJZCI6Inp3d2RKdkxtUCIsInBob25lTnVtIjoxNTIyMDA2NDI5NH0sImNyZWF0ZWQiOjE1NDI2NDMyNTAsImV4cCI6MzYwMH0=.SW3JVAjkQUX0mgrSBuOirB3kQV6NNatlc4j/qW7SxTM="
 *    } 
 *    message: "登录成功"
 *    server_time: 1542089573405
 *    status: 200
 *  }  
 * @apiError {json} result
 * @apiErrorExample {json} Error-Response:
 *  {
 *    data: {}
 *    message: "验证码错误"
 *    server_time: 1542082677922
 *    status: 500
 *  }
 * @apiSampleRequest http://localhost:8884/shop/v0/user/doLogin
 * @apiVersion 1.0.0
 */
router.post('/user/doLogin', User.loginAction);

/**
 * @api {get} /shop/v0/user/getUserInfoById 获取用户的基本信息
 * @apiDescription 根据ID获取用户基本信息
 * @apiName /user/getUserInfoById
 * @apiGroup User
 * @apiParam {string} token MasterApi授权的鉴权参数
 * @apiParam {string} id 用户ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功 getUserInfoById",
 *  "server_time": 1542966968054,
 *  "data": {
 *     "_id": "zwwdJvLmP",
 *     "userName": "doramart",
 *     "__v": 0,
 *     "category": [
 *      
 *    ],
 *     "group": "0", // 0 普通用户 1 大师
 *     "logo": "/upload/images/defaultlogo.png",
 *     "date": "2018-11-13 12:09:29",
 *     "comments": "",
 *     "enable": true,
 *     "id": "zwwdJvLmP",
 *     "comments": "这里是作者简介",
 *     "content_num": 5, // 言论数
 *     "total_reward_num": 0  // 打赏总额
 *     "total_despiseNum": 0  // 踩帖总数
 *     "total_likeNum": 0  // 文章被点赞总数
 *     "watch_num": 1, // 关注人数
 *     "follow_num": 0 // 被关注人数(粉丝)
 *     "comments_num": 0, //参与评论数量,
 *     "favorites_num": 0, // 收藏数量
 *     "birthBoxState": false  // 是否弹生日礼物领取窗口
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/shop/v0/user/getUserInfoById
 * @apiVersion 1.0.0
 */
router.get('/user/getUserInfoById', siteFunc.checkAdminUserSessionForApi, User.getUserInfoById)



/**
 * @api {post} /shop/v0/user/addBillRecordByType 增加/减少MBT数量
 * @apiDescription 根据指定用户ID增加/减少MBT数量
 * @apiName addBillRecordByType
 * @apiGroup User
 * @apiParam {string} token MasterApi授权的鉴权参数
 * @apiParam {string} userId 用户ID
 * @apiParam {string} coins 币的数量（正数为加，负数为减）
 * @apiParam {string} actionType 操作类型 (1:折抵，2:活动奖励，3:回冲, 9:其它)
 * @apiParam {string} beInvitedUser 如果actionType=='1',传关联用户ID(被邀请者ID)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "addBillRecordByType",
 *    "server_time": 1544246883076,
 *    "data": {}
 *}
 * @apiError {json} result
 * @apiErrorExample {json} Error-Response:
 *  {
 *    data: {}
 *    message: "余额不足"
 *    server_time: 1542082677922
 *    status: 500
 *  }
 * @apiSampleRequest http://localhost:8884/shop/v0/user/addBillRecordByType
 * @apiVersion 1.0.0
 */
router.post('/user/addBillRecordByType', siteFunc.checkAdminUserSessionForApi, BillRecord.addBillRecordByType);


/**
 * @api {get} /shop/v0/vip/getSetMeal 获取VIP套餐列表
 * @apiDescription 获取VIP套餐列表
 * @apiName /vip/getSetMeal
 * @apiGroup VIP
 * @apiParam {string} token MasterApi授权的鉴权参数
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "getVipSetMeals",
 *    "server_time": 1546160983360,
 *    "data": [
 *        {
 *            "_id": "S-GgNzIc8",
 *            "time": "12", // 套餐时长
 *            "comments": "12个月40MEC",
 *            "__v": 0,
 *            "date": "2018-12-30 17:00:36", 
 *            "default": true, // 是否默认/推荐
 *            "state": true,  
 *            "unit": "MEC", // 套餐单位
 *            "coins": 40, // 套餐价格
 *            "id": "S-GgNzIc8"
 *        },
 *        {
 *            "_id": "eMjvA-G1G",
 *            "time": "3",
 *            "comments": "3个月12MEC",
 *            "__v": 0,
 *            "date": "2018-12-30 16:59:39",
 *            "default": false,
 *            "state": true,
 *            "unit": "MEC",
 *            "coins": 12,
 *            "id": "eMjvA-G1G"
 *        },
 *        {
 *            "_id": "XGT2f6gjn",
 *            "time": "1",
 *            "comments": "5MEC",
 *            "__v": 0,
 *            "date": "2018-12-30 16:49:07",
 *            "default": false,
 *            "state": true,
 *            "unit": "MEC",
 *            "coins": 5,
 *            "id": "XGT2f6gjn"
 *        }
 *    ]
 *}
 * @apiSampleRequest http://localhost:8884/shop/v0/vip/getSetMeal
 * @apiVersion 1.0.0
 */
router.get('/vip/getSetMeal', siteFunc.checkAdminUserSessionForApi, VipSetMeal.getVipSetMeals);


/**
 * @api {post} /shop/v0/vip/extensionPeriod 延长会员有效期
 * @apiDescription 延长会员有效期
 * @apiName /vip/extensionPeriod
 * @apiGroup VIP
 * @apiParam {string} token MasterApi授权的鉴权参数
 * @apiParam {string} userId 指定的会员ID
 * @apiParam {string} setMeal 指定会员套餐ID
 * @apiParam {string} comments 延长备注 必填
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功",
 *    "server_time": 1543150055651,
 *    "data": {
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/shop/v0/vip/extensionPeriod
 * @apiVersion 1.0.0
 */
router.post('/vip/extensionPeriod', siteFunc.checkAdminUserSessionForApi, OpenVipRecord.extensionPeriod);

/**
 * @api {get} /shop/v0/vip/getBasicInfo 查询有效期/MPVC/MEC/MBT
 * @apiDescription 查询有效期/MPVC/MEC/MBT
 * @apiName /vip/getBasicInfo
 * @apiGroup VIP
 * @apiParam {string} token MasterApi授权的鉴权参数
 * @apiParam {string} userId 用户ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功 getBasicInfo",
 *  "server_time": 1543484196749,
 *  "data": {
 *     "total_MEC_num": 0.1,  // MEC金额
 *     "total_MEC_unit": "MEC",  // MEC单位
 *     "total_MVPC_num": 0,  // MVPC金额
 *     "total_MVPC_unit": "MVPC", // MVPC单位
 *     "total_MBT_num": 0,  // MBT金额
 *     "total_MBT_unit": "MBT", // MBT单位
 *     "isVip": false // 是否是vip
 *     "endTime": "2020-04-19" // 会员有效期
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/shop/v0/vip/getBasicInfo
 * @apiVersion 1.0.0
 */
router.get('/vip/getBasicInfo', siteFunc.checkAdminUserSessionForApi, BillRecord.getShopBasicInfo);



module.exports = router