/**
 * api
 * 
 */
const express = require('express')
const router = express.Router()
router.caseSensitive = true
router.strict = true

const {
  siteFunc
} = require('../../../utils');
const authToken = require('../../../utils/middleware/token');
const generalFun = require("../../lib/utils/generalFun");
const {
  AdminUser,
  ContentCategory,
  ContentTag,
  Content,
  FeedBack,
  User,
  Message,
  SystemConfig,
  UserNotify,
  Ads,
  Community,
  CommunityTag,
  CommunityContent,
  MasterCategory,
  SiteMessage,
  ClassContent,
  UserActionHis,
  ClassInfo,
  ClassType,
  ClassScore,
  BillRecord,
  WalletAddress,
  ClassPayRecord,
  ResetAmount,
  CurrencyApproval,
  CreativeRight,
  ReportRecord,
  IdentityAuthentication,
  SignIn,
  Special,
  OpenVipRecord,
  VipSetMeal,
  CommunityMessage,
  HotSearch,
  HelpCenter,
  VersionManage,
  Switches
} = require('../../lib/controller');


const _ = require('lodash');
const qr = require('qr-image')


//---------------------------------------------------------用户相关api

/**
 * @api {post} /api/v0/user/sendVerificationCode 发送验证码
 * @apiDescription 发送验证码
 * @apiName sendVerificationCode
 * @apiGroup User
 * @apiParam {string} phoneNum 手机号(eq:15220064294)
 * @apiParam {string} countryCode 国家代码（eq: 86）
 * @apiParam {string} email  邮箱
 * @apiParam {string} messageType 发送验证码类别（0、注册 1、登录，2、忘记资金密码找回, 3、忘记密码，4、身份验证, 5、管理员登录，6、游客绑定邮箱或手机号）
 * @apiParam {string} sendType 发送验证码形式（1: 短信验证码  2:邮箱验证码）
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 * {
 *     "status": 200,
 *     "message": "send Code success",
 *     "server_time": 1542382533904,
 *     "data": {
 *         "messageCode": "378047"
 *     }
 * }  
 * @apiError {json} result
 * @apiErrorExample {json} Error-Response:
 *  {
 *    data: {}
 *    message: "验证码错误"
 *    server_time: 1542082677922
 *    status: 500
 *  }
 * @apiSampleRequest http://localhost:8884/api/v0/user/sendVerificationCode
 * @apiVersion 1.0.0
 */
router.post('/user/sendVerificationCode', User.sendVerificationCode);

/**
 * @api {post} /api/v0/user/doLogin 用户登录
 * @apiDescription 用户登录
 * @apiName doLogin
 * @apiGroup User
 * @apiParam {string} phoneNum 手机号（eq:15220064294）
 * @apiParam {string} countryCode 国家代码（eq: 86）
 * @apiParam {string} email 用户邮箱(xx@qq.com)
 * @apiParam {string} loginType 登录类型 (1:手机验证码登录 2:手机号密码登录 3:邮箱密码登录,4:邮箱验证码登录)
 * @apiParam {string} messageCode 手机验证码(eq: 123456)
 * @apiParam {string} password 密码 // 非必填，与短信验证码选其一
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *  {
 *    data: {
 *      comments: ""
 *      date: "2018-11-13 12:09:29"
 *      email: "doramart@qq.com"
 *      enable: true
 *      group: "0"
 *      id: "zwwdJvLmP"
 *      logo: "/upload/images/defaultlogo.png"
 *      userName: "doramart"
 *      walletAddress: "0x2ed28eDEbD296Bf085C582f5187E4F987e6214e6",
 *      token: "eyJkYXRhIjp7InVzZXJJZCI6Inp3d2RKdkxtUCIsInBob25lTnVtIjoxNTIyMDA2NDI5NH0sImNyZWF0ZWQiOjE1NDI2NDMyNTAsImV4cCI6MzYwMH0=.SW3JVAjkQUX0mgrSBuOirB3kQV6NNatlc4j/qW7SxTM="
 *    } 
 *    message: "登录成功"
 *    server_time: 1542089573405
 *    status: 200
 *  }  
 * @apiError {json} result
 * @apiErrorExample {json} Error-Response:
 *  {
 *    data: {}
 *    message: "验证码错误"
 *    server_time: 1542082677922
 *    status: 500
 *  }
 * @apiSampleRequest http://localhost:8884/api/v0/user/doLogin
 * @apiVersion 1.0.0
 */
router.post('/user/doLogin', User.loginAction);

/**
 * @api {post} /api/v0/user/touristLogin 游客登录
 * @apiDescription 游客登录
 * @apiName touristLogin
 * @apiGroup User
 * @apiParam {string} userCode 客户端传递加签字符串
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *  {
 *    data: {
 *      comments: ""
 *      date: "2018-11-13 12:09:29"
 *      enable: true
 *      group: "0"
 *      id: "zwwdJvLmP"
 *      logo: "/upload/images/defaultlogo.png"
 *      userName: "doramart"
 *      walletAddress: "0x2ed28eDEbD296Bf085C582f5187E4F987e6214e6",
 *      token: "eyJkYXRhIjp7InVzZXJJZCI6Inp3d2RKdkxtUCIsInBob25lTnVtIjoxNTIyMDA2NDI5NH0sImNyZWF0ZWQiOjE1NDI2NDMyNTAsImV4cCI6MzYwMH0=.SW3JVAjkQUX0mgrSBuOirB3kQV6NNatlc4j/qW7SxTM="
 *    } 
 *    message: "登录成功"
 *    server_time: 1542089573405
 *    status: 200
 *  }  
 * @apiError {json} result
 * @apiErrorExample {json} Error-Response:
 *  {
 *    data: {}
 *    message: "登录失败"
 *    server_time: 1542082677922
 *    status: 500
 *  }
 * @apiSampleRequest http://localhost:8884/api/v0/user/touristLogin
 * @apiVersion 1.0.0
 */
router.post('/user/touristLogin', User.touristLoginAction);

/**
 * @api {post} /api/v0/user/doReg 用户注册
 * @apiDescription 用户注册
 * @apiName doReg
 * @apiGroup User
 * @apiParam {string} regType 注册类型（1:手机号注册  2:邮箱注册）
 * @apiParam {string} phoneNum 手机号（eq:15220064294）
 * @apiParam {string} countryCode 国家代码（eq: 86）
 * @apiParam {string} messageCode 手机验证码/邮箱验证码(eq: 123456)
 * @apiParam {string} email 注册邮箱
 * @apiParam {string} password 密码
 * @apiParam {string} logo 用户头像
 * @apiParam {string} userName 用户名
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "注册成功",
 *    "server_time": 1544246883076,
 *    "data": {}
 *}
 * @apiError {json} result
 * @apiErrorExample {json} Error-Response:
 *  {
 *    data: {}
 *    message: "注册失败"
 *    server_time: 1542082677922
 *    status: 500
 *  }
 * @apiSampleRequest http://localhost:8884/api/v0/user/doReg
 * @apiVersion 1.0.0
 */
router.post('/user/doReg', User.regAction);


/**
 * @api {post} /api/v0/user/bindInfo 游客绑定邮箱或手机号
 * @apiDescription 游客绑定邮箱或手机号
 * @apiName bindInfo
 * @apiGroup User
 * @apiParam {string} type 绑定类型（1:手机号  2:邮箱）
 * @apiParam {string} phoneNum 手机号（eq:15220064294）
 * @apiParam {string} countryCode 国家代码（eq: 86）
 * @apiParam {string} messageCode 手机验证码/邮箱验证码(eq: 123456)
 * @apiParam {string} email 注册邮箱
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "绑定成功",
 *    "server_time": 1544246883076,
 *    "data": {}
 *}
 * @apiError {json} result
 * @apiErrorExample {json} Error-Response:
 *  {
 *    data: {}
 *    message: "绑定失败"
 *    server_time: 1542082677922
 *    status: 500
 *  }
 * @apiSampleRequest http://localhost:8884/api/v0/user/bindInfo
 * @apiVersion 1.0.0
 */
router.post('/user/bindInfo', siteFunc.checkUserSessionForApi, User.bindEmailOrPhoneNum);

/**
 * @api {post} /api/v0/user/resetPassword 忘记密码找回
 * @apiDescription 忘记密码找回
 * @apiName resetPassword
 * @apiGroup User
 * @apiParam {string} phoneNum 手机号（eq:15220064294）
 * @apiParam {string} countryCode 国家代码（eq: 86）
 * @apiParam {string} messageCode 手机验证码(eq: 123456)
 * @apiParam {string} email 邮箱(eq: xx@qq.com)
 * @apiParam {string} type 找回方式(1:通过手机号找回，2:通过邮箱找回)
 * @apiParam {string} password 密码
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 * {
 *     "status": 200,
 *     "message": "操作成功！",
 *     "server_time": 1544536543533,
 *     "data": {}
 * }
 * @apiSampleRequest http://localhost:8884/api/v0/user/resetPassword
 * @apiVersion 1.0.0
 */
router.post('/user/resetPassword', User.resetMyPassword);


/**
 * @api {post} /api/v0/user/modifyMyPsd 修改密码
 * @apiDescription 修改密码
 * @apiName modifyMyPsd
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} oldPassword 原密码
 * @apiParam {string} password 新密码
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 * {
 *     "status": 200,
 *     "message": "密码修改成功！",
 *     "server_time": 1544536543533,
 *     "data": {}
 * }
 * @apiSampleRequest http://localhost:8884/api/v0/user/modifyMyPsd
 * @apiVersion 1.0.0
 */
router.post('/user/modifyMyPsd', siteFunc.checkUserSessionForApi, User.modifyMyPsd);


/**
 * @api {get} /api/v0/user/userInfo 获取登录用户基本信息
 * @apiDescription 获取登录用户信基本信息,需要登录态
 * @apiName userInfo
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *  {
 *    data: {
 *      userInfo: {
 *        comments: ""
 *        phoneNum: "15229908899"
 *        countryCode: "86"
 *        date: "2018-11-13 12:09:29"
 *        email: "doramart@qq.com"
 *        enable: true
 *        group: "0"
 *        id: "zwwdJvLmP"
 *        logo: "/upload/images/defaultlogo.png"
 *        msg_count: { }
 *        userName: "doramart"
 *     }
 *     status: 200
 *  }
 * @apiSampleRequest http://localhost:8884/api/v0/user/userInfo
 * @apiVersion 1.0.0
 */
router.get('/user/userInfo', siteFunc.checkUserSessionForApi, function (req, res, next) {

  User.getOneUserByParams({
    _id: req.session.user._id
  }).then((user) => {
    if (!_.isEmpty(user)) {
      req.session.user = user;
      res.send({
        status: 200,
        data: {
          userInfo: user
        }
      })
    } else {
      res.send({
        status: 500,
        data: {}
      })
    }
  }).catch((err) => {
    res.send({
      status: 500,
      message: err.message,
      data: {}
    })
  })
});


/**
 * @api {get} /api/v0/user/followMaster 关注大师
 * @apiDescription 关注大师（需要登录状态,后台会通过读取用户session获取用户ID）
 * @apiName /user/followMaster
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} masterId 大师id(eq:yNYHEw3-e)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功",
 *  "server_time": 1542251075220,
 *  "data": {
 *    
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/followMaster
 * @apiVersion 1.0.0
 */
router.get('/user/followMaster', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.followState = 'in';
  next()
}, User.followMaster)

/**
 * @api {get} /api/v0/user/cancelFollowMaster 取消关注大师
 * @apiDescription 取消关注大师（需要登录状态,后台会通过读取用户session获取用户ID）
 * @apiName /user/cancelFollowMaster
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} masterId 大师id(eq:yNYHEw3-e)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功",
 *  "server_time": 1542251075220,
 *  "data": {
 *    
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/cancelFollowMaster
 * @apiVersion 1.0.0
 */
router.get('/user/cancelFollowMaster', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.followState = 'out';
  next()
}, User.followMaster)



/**
 * @api {get} /api/v0/user/addTags 关注标签
 * @apiDescription 关注标签（需要登录状态,后台会通过读取用户session获取用户ID）
 * @apiName /user/addTags
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} tagId 标签id(eq:yNYHEw3-e)
 * @apiParam {string} type 方式(1:关注 0:取消关注)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功",
 *  "server_time": 1542251075220,
 *  "data": {
 *    
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/addTags
 * @apiVersion 1.0.0
 */
router.get('/user/addTags', siteFunc.checkUserSessionForApi, (req, res, next) => {
  next()
}, User.addTags)


/**
 * @api {get} /api/v0/user/masterList 查询大师列表
 * @apiDescription 根据条件查询大师列表,带分页
 * @apiName /user/masterList
 * @apiGroup User
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} searchkey 搜索关键字
 * @apiParam {string} follow 我关注的（eq:{follow:'1'}）
 * @apiParam {string} sortby 按字段排序(1：粉丝)
 * @apiParam {string} category 大师行业类别ID eq:{category:'yOgDdV8_b'}）
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功 user",
 *    "server_time": 1542426437624,
 *    "data": [
 *        {
 *           "_id": "yNYHEw3-e",
 *           "userName": "yoooyu",
 *           "email": "doramart@163.com",
 *           "phoneNum": 15220000000,
 *           "__v": 0,
 *           "name": "大师兄",
 *           "watchers": [],
 *           "followers": [
 *               "zwwdJvLmP"
 *           ],
 *           "category": [
 *               {
 *                   "_id": "yOgDdV8_b",
 *                   "name": "企业家大师1",
 *                   "date": "2018-11-17 11:47:17",
 *                   "id": "yOgDdV8_b"
 *               }
 *           ],
 *           "group": "1",
 *           "logo": "/upload/images/defaultlogo.png",
 *           "date": "2018-11-17 09:59:52",
 *           "comments": "",
 *           "enable": true,
 *           "id": "yNYHEw3-e",
 *           "content_num": 0,  // 帖子数量
 *           "watch_num": 0,  // 关注大师数量
 *           "follow_num": 0,
 *           "comments_num": 0, //参与评论数量,
 *           "favorites_num": 0, // 收藏数量
 *        }
 *    ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/masterList
 * @apiVersion 1.0.0
 */
router.get('/user/masterList', siteFunc.checkUserLoginState, (req, res, next) => {
  req.query.group = '1';
  next();
}, User.getUsers);

/**
 * @api {get} /api/v0/user/getRandomMasters 查询随机推荐大师列表
 * @apiDescription 查询随机推荐大师列表
 * @apiName /user/getRandomMasters
 * @apiGroup User
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功 user",
 *    "server_time": 1542426437624,
 *    "data": [
 *        {
 *           "_id": "yNYHEw3-e",
 *           "userName": "yoooyu",
 *           "email": "doramart@163.com",
 *           "phoneNum": 15220000000,
 *           "__v": 0,
 *           "name": "大师兄",
 *           "watchers": [],
 *           "followers": [
 *               "zwwdJvLmP"
 *           ],
 *           "category": [
 *               {
 *                   "_id": "yOgDdV8_b",
 *                   "name": "企业家大师1",
 *                   "date": "2018-11-17 11:47:17",
 *                   "id": "yOgDdV8_b"
 *               }
 *           ],
 *           "group": "1",
 *           "logo": "/upload/images/defaultlogo.png",
 *           "date": "2018-11-17 09:59:52",
 *           "comments": "",
 *           "enable": true,
 *           "id": "yNYHEw3-e",
 *           "content_num": 0,  // 帖子数量
 *           "watch_num": 0,  // 关注大师数量
 *           "follow_num": 0,
 *           "comments_num": 0, //参与评论数量,
 *           "favorites_num": 0, // 收藏数量
 *        }
 *    ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/getRandomMasters
 * @apiVersion 1.0.0
 */
router.get('/user/getRandomMasters', siteFunc.checkUserLoginState, User.getRandomMasters);

/**
 * @api {get} /api/v0/user/getUserInfoById 获取大师的基本信息
 * @apiDescription 根据ID获取大师基本信息
 * @apiName /user/getUserInfoById
 * @apiGroup User
 * @apiParam {string} id 大师ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功 getUserInfoById",
 *  "server_time": 1542966968054,
 *  "data": {
 *     "_id": "zwwdJvLmP",
 *     "userName": "doramart",
 *     "__v": 0,
 *     "category": [
 *      
 *    ],
 *     "group": "0", // 0 普通用户 1 大师
 *     "logo": "/upload/images/defaultlogo.png",
 *     "date": "2018-11-13 12:09:29",
 *     "comments": "",
 *     "enable": true,
 *     "id": "zwwdJvLmP",
 *     "comments": "这里是作者简介",
 *     "content_num": 5, // 言论数
 *     "total_reward_num": 0  // 打赏总额
 *     "total_despiseNum": 0  // 踩帖总数
 *     "total_likeNum": 0  // 文章被点赞总数
 *     "watch_num": 1, // 关注人数
 *     "follow_num": 0 // 被关注人数(粉丝)
 *     "comments_num": 0, //参与评论数量,
 *     "favorites_num": 0, // 收藏数量
 *     "birthBoxState": false  // 是否弹生日礼物领取窗口
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/getUserInfoById
 * @apiVersion 1.0.0
 */
router.get('/user/getUserInfoById', siteFunc.checkUserLoginState, User.getUserInfoById)

/**
 * @api {get} /api/v0/user/getMyFollowContents 查询我关注的大师文章
 * @apiDescription 查询我关注的大师文章，需要登录态
 * @apiName /user/getMyFollowContents
 * @apiGroup User
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} searchkey 搜索关键字
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *   "status": 200,
 *   "message": "contentlist",
 *   "server_time": 1542380520270,
 *   "data": [
 *       {
 *           "_id": "Y1XFYKL52",
 *           "title": "如何优化vue的内存占用？",
 *           "stitle": "如何优化vue的内存占用？",
 *           "sortPath": "",
 *           "keywords": "",
 *           "author": {
 *               "_id": "4JiWCMhzg",
 *               "userName": "doramart",
 *               "name": "生哥",
 *               "logo": "/upload/smallimgs/img1448202744000.jpg"
 *           },
 *           "discription": "在使用了一段时间的vue.js开发移动端h5页面（电商类页面，会有一些数据量较大但一次渲染就可以的商品列表）后",
 *           "comments": "在使用了一段时间的vue.js开发移动端h5页面（电商类页面，会有一些数据量较大但一次渲染就可以的商品列表）后，感觉相比于传统的jquery（zepto）和前端模板引擎的组合能有效的提升开发效率和代码维护性，但是也存在一些问题，比如说内存占用问题，用vue.js开发的页面内存占用相比于传统方式会更多，而且传统的开发方式页面渲染完后，还能对不需要的js对象进行垃圾回收，但vue.js里面的一些指令对象、watcher对象、data数据等似乎目前都没有找到比较好的垃圾回收的方式。想问下对于那些只用渲染一次的页面部分（比如数据量较大的列表页）有没有比较合适的内存优化方案（比如释放指令对象、watcher对象、data对象）？举个例子：比如其中的lazy指令，以及对于items这个基本上只用渲染一次的data等有没有可以优化内存占用的方法",
 *           "likeNum": 0,
 *           "commentNum": 0,
 *           "clickNum": 1,
 *           "isTop": 0,
 *           "state": true,
 *           "updateDate": "2018-11-16",
 *           "date": "2018-11-16 23:00:16",
 *           "appShowType": "0", // app端展示模式 0，1，2，3
 *           "sImg": "/upload/images/img20181116225948.jpeg",
 *           "tags": [
 *               {
 *                "_id": "Y3DTgmHK3",
 *                "name": "区块链",
 *                "date": "2018-11-16 23:02:00",
 *                "id": "Y3DTgmHK3"
 *                }
 *            ],
 *           "categories": [
 *                {
 *                "_id": "Nycd05pP",
 *                "name": "人工智能",
 *                "defaultUrl": "artificial-intelligence",
 *                "enable": true,
 *                "date": "2018-11-16 23:02:00",
 *                "id": "Nycd05pP"
 *                }
 *            ],
 *           "type": "1",
 *           "id": "Y1XFYKL52"
 *        }
 *    ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/getMyFollowContents
 * @apiVersion 1.0.0
 */
router.get('/user/getMyFollowContents', siteFunc.checkUserSessionForApi, Content.getMyFollowList);


/**
 * @api {get} /api/v0/user/askContentThumbsUp 帖子点赞
 * @apiDescription 帖子点赞，需要登录态
 * @apiName /user/askContentThumbsUp
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} contentId 帖子ID/评论ID/社群帖ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功！",
 *    "server_time": 1543372263586,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/askContentThumbsUp
 * @apiVersion 1.0.0
 */
router.get('/user/askContentThumbsUp', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.praiseState = 'in';
  next();
}, User.askContentThumbsUp)

/**
 * @api {get} /api/v0/user/cancelContentThumbsUp 取消帖子点赞
 * @apiDescription 取消帖子点赞
 * @apiName /user/cancelContentThumbsUp
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} contentId 帖子ID/评论ID/社群帖ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功！",
 *    "server_time": 1543372263586,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/askContentThumbsUp
 * @apiVersion 1.0.0
 */
router.get('/user/cancelContentThumbsUp', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.praiseState = 'out';
  next();
}, User.askContentThumbsUp)


/**
 * @api {get} /api/v0/user/favoriteContent 收藏帖子
 * @apiDescription 收藏帖子，需要登录态,包含普通帖子，专题或社群帖子
 * @apiName /user/favoriteContent
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} contentId 帖子ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功！",
 *    "server_time": 1543372263586,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/favoriteContent
 * @apiVersion 1.0.0
 */
router.get('/user/favoriteContent', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.favoriteState = 'in';
  next();
}, User.favoriteContent)

/**
 * @api {get} /api/v0/user/cancelFavoriteContent 取消收藏帖子
 * @apiDescription 取消收藏帖子，需要登录态
 * @apiName /user/cancelFavoriteContent
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} contentId 帖子ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功！",
 *    "server_time": 1543372263586,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/favoriteContent
 * @apiVersion 1.0.0
 */
router.get('/user/cancelFavoriteContent', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.favoriteState = 'out';
  next();
}, User.favoriteContent)


/**
 * @api {get} /api/v0/user/getMyFavoriteContents 获取我/其它用户收藏的帖子列表
 * @apiDescription 获取我/其它用户收藏的帖子列表 带分页
 * @apiName /user/getMyFavoriteContents
 * @apiGroup User
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} userId 指定用户ID，非必填
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *   "status": 200,
 *   "message": "contentlist",
 *   "server_time": 1542380520270,
 *   "data": [
 *       {
 *           "_id": "Y1XFYKL52",
 *           "title": "如何优化vue的内存占用？",
 *           "stitle": "如何优化vue的内存占用？",
 *           "sortPath": "",
 *           "keywords": "",
 *           "author": {
 *               "_id": "4JiWCMhzg",
 *               "userName": "doramart",
 *               "name": "生哥",
 *               "logo": "/upload/smallimgs/img1448202744000.jpg"
 *           },
 *           "discription": "在使用了一段时间的vue.js开发移动端h5页面（电商类页面，会有一些数据量较大但一次渲染就可以的商品列表）后",
 *           "comments": "在使用了一段时间的vue.js开发移动端h5页面（电商类页面，会有一些数据量较大但一次渲染就可以的商品列表）后，感觉相比于传统的jquery（zepto）和前端模板引擎的组合能有效的提升开发效率和代码维护性，但是也存在一些问题，比如说内存占用问题，用vue.js开发的页面内存占用相比于传统方式会更多，而且传统的开发方式页面渲染完后，还能对不需要的js对象进行垃圾回收，但vue.js里面的一些指令对象、watcher对象、data数据等似乎目前都没有找到比较好的垃圾回收的方式。想问下对于那些只用渲染一次的页面部分（比如数据量较大的列表页）有没有比较合适的内存优化方案（比如释放指令对象、watcher对象、data对象）？举个例子：比如其中的lazy指令，以及对于items这个基本上只用渲染一次的data等有没有可以优化内存占用的方法",
 *           "likeNum": 0,
 *           "commentNum": 0,
 *           "clickNum": 1,
 *           "isTop": 0,
 *           "state": true,
 *           "updateDate": "2018-11-16",
 *           "date": "2018-11-16 23:00:16",
 *           "appShowType": "0", // app端展示模式 0，1，2，3
 *           "sImg": "/upload/images/img20181116225948.jpeg",
 *           "tags": [
 *               {
 *                "_id": "Y3DTgmHK3",
 *                "name": "区块链",
 *                "date": "2018-11-16 23:02:00",
 *                "id": "Y3DTgmHK3"
 *                }
 *            ],
 *           "categories": [
 *                {
 *                "_id": "Nycd05pP",
 *                "name": "人工智能",
 *                "defaultUrl": "artificial-intelligence",
 *                "enable": true,
 *                "date": "2018-11-16 23:02:00",
 *                "id": "Nycd05pP"
 *                }
 *            ],
 *           "type": "1",
 *           "id": "Y1XFYKL52"
 *        }
 *    ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/getMyFavoriteContents
 * @apiVersion 1.0.0
 */
router.get('/user/getMyFavoriteContents', siteFunc.checkUserLoginState, Content.getMyFavoriteContents);


/**
 * @api {get} /api/v0/user/despiseContent 踩帖子
 * @apiDescription 踩帖子，需要登录态
 * @apiName /user/despiseContent
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} contentId 帖子ID/评论ID/社群帖ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功！",
 *    "server_time": 1543372263586,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/favoriteContent
 * @apiVersion 1.0.0
 */
router.get('/user/despiseContent', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.despiseState = 'in';
  next();
}, User.despiseContent)

/**
 * @api {get} /api/v0/user/cancelDespiseContent 取消踩帖子
 * @apiDescription 取消踩帖子，需要登录态
 * @apiName /user/cancelDespiseContent
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} contentId 帖子ID/评论ID/社群帖ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功！",
 *    "server_time": 1543372263586,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/cancelDespiseContent
 * @apiVersion 1.0.0
 */
router.get('/user/cancelDespiseContent', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.despiseState = 'out';
  next();
}, User.despiseContent)



/**
 * @api {post} /api/v0/user/rewordContent 帖子打赏
 * @apiDescription 帖子打赏
 * @apiName /user/rewordContent
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} coins 打赏金额
 * @apiParam {string} unit 打赏金额单位
 * @apiParam {string} password 资金密码
 * @apiParam {string} comments 打赏备注（非必填）
 * @apiParam {string} contentId 帖子ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功！",
 *    "server_time": 1543372263586,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/rewordContent
 * @apiVersion 1.0.0
 */
router.post('/user/rewordContent', siteFunc.checkUserSessionForApi, User.rewordContent)


/**
 * @api {post} /api/v0/user/rechargeCoins 账户充值
 * @apiDescription 账户充值
 * @apiName /user/rechargeCoins
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} unit 充值金额单位
 * @apiParam {string} coins 充值金额
 * @apiParam {string} comments 充值备注（非必填）
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功！",
 *    "server_time": 1543372263586,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/rechargeCoins
 * @apiVersion 1.0.0
 */
router.post('/user/rechargeCoins', siteFunc.checkUserSessionForApi, User.rechargeCoins)


/**
 * @api {post} /api/v0/user/authIapTransactionVoucher 内购参数iap验证
 * @apiDescription 内购参数iap验证
 * @apiName /user/authIapTransactionVoucher
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} receiptData 校验字符串
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功！",
 *    "server_time": 1543372263586,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/authIapTransactionVoucher
 * @apiVersion 1.0.0
 */
router.post('/user/authIapTransactionVoucher', siteFunc.checkUserSessionForApi, User.authIapTransactionVoucher)

/**
 * @api {post} /api/v0/user/authAndroidTransactionVoucher Google内购Andrioid参数验证
 * @apiDescription Google内购Andrioid参数验证
 * @apiName /user/authAndroidTransactionVoucher
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} chargeId 充值金额的ID
 * @apiParam {string} inappDataSignature 加签
 * @apiParam {string} orderId (认证参数)
 * @apiParam {string} packageName (认证参数)
 * @apiParam {string} productId (认证参数)
 * @apiParam {string} purchaseTime (认证参数)
 * @apiParam {string} purchaseState (认证参数)
 * @apiParam {string} developerPayload (认证参数)
 * @apiParam {string} purchaseToken (认证参数)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功！",
 *    "server_time": 1543372263586,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/authAndroidTransactionVoucher
 * @apiVersion 1.0.0
 */
router.post('/user/authAndroidTransactionVoucher', siteFunc.checkUserSessionForApi, User.authAndroidTransactionVoucher)


/**
 * @api {post} /api/v0/user/askForCash 申请提现
 * @apiDescription 申请提现(需后台审核)
 * @apiName /user/askForCash
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} unit 提现数额单位
 * @apiParam {string} coins 提现金额
 * @apiParam {string} walletAddress 提现钱包ID
 * @apiParam {string} comments 提现备注（非必填）
 * @apiParam {string} fundPassword 资金密码
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "CurrencyApproval",
 *    "server_time": 1545115126152,
 *    "data": {
 *       "state": "待审核", // 待审核
 *       "leftCoins": 9899  // 余额
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/askForCash
 * @apiVersion 1.0.0
 */
router.post('/user/askForCash', siteFunc.checkUserSessionForApi, CurrencyApproval.addCurrencyApproval)


/**
 * @api {post} /api/v0/user/askForCreativeRight 申请创作权限
 * @apiDescription 申请创作权限(需后台审核)
 * @apiName /user/askForCreativeRight
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} comments 申请原因（必填）
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "CurrencyApproval",
 *    "server_time": 1545115126152,
 *    "data": {
 *       "state": "待审核", // 待审核
 *       "leftCoins": 9899  // 余额
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/askForCreativeRight
 * @apiVersion 1.0.0
 */
router.post('/user/askForCreativeRight', siteFunc.checkUserSessionForApi, CreativeRight.addCreativeRight)


/**
 * @api {post} /api/v0/user/identityAuthentication 申请身份认证
 * @apiDescription 申请身份认证(需后台审核)
 * @apiName /user/identityAuthentication
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} messageCode 手机/邮箱验证码
 * @apiParam {string} type (1:手机号验证，2:邮箱验证)
 * @apiParam {string} name 姓名
 * @apiParam {string} cardType 证件类型(0 居民身份证 1护照 2其他有效证件)
 * @apiParam {string} areaType 证件区域(0 大陆 1港澳 2台湾 3外籍)
 * @apiParam {string} idCardNo 证件号码
 * @apiParam {string} cardFront 证件照正面(上传后返回的路径)
 * @apiParam {string} cardBack 证件照反面(上传后返回的路径)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "IdentityAuthentication",
 *    "server_time": 1545115126152,
 *    "data": {
 *       "state": "待审核", // 待审核
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/identityAuthentication
 * @apiVersion 1.0.0
 */
router.post('/user/identityAuthentication', siteFunc.checkUserSessionForApi, IdentityAuthentication.addIdentityAuthentication)

// 完善大师认证信息
router.post('/user/identityMasterInfo', siteFunc.checkUserSessionForApi, IdentityAuthentication.identityMasterInfo)


/**
 * @api {get} /api/v0/user/getIdentityAuthInfo 获取认证信息
 * @apiDescription 获取认证信息
 * @apiName /user/getIdentityAuthInfo
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "get data success",
 *    "server_time": 1545543658476,
 *    "data": {
 *        "_id": "FKCWLsL_V",
 *        "name": "肖生",
 *        "cardType": "0",
 *        "idCardNo": "429001198704260011",
 *        "cardFront": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/1075054319700676608.png",
 *        "cardBack": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/1075054319700676608.png",
 *        "user": "x1y1tUgIJ",
 *        "__v": 0,
 *        "comments": null,
 *        "state": "1", // -1:未提交审核 0:待审核 1:审核成功 2:审核失败
 *        "updatetime": "2018-12-22 22:48:36",
 *        "date": "2018-12-22 22:41:35",
 *        "id": "FKCWLsL_V"
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/getIdentityAuthInfo
 * @apiVersion 1.0.0
 */
router.get('/user/getIdentityAuthInfo', siteFunc.checkUserSessionForApi, (req, res, next) => {
  next();
}, IdentityAuthentication.getIdentityAuthenticationInfo)


/**
 * @api {post} /api/v0/user/report 举报
 * @apiDescription 举报(需后台审核)
 * @apiName /user/report
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} comments 举报内容(非必填)
 * @apiParam {string} type 举报类型（0:普通帖子/专题 1:评论 2:社群帖子 3:社群帖子评论）必填
 * @apiParam {string} contentId 举报对象ID 必填
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "ReportRecord",
 *    "server_time": 1545194101782,
 *    "data": {
 *    "id": "ZCUDrH3jq"
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/report
 * @apiVersion 1.0.0
 */
router.post('/user/report', siteFunc.checkUserSessionForApi, ReportRecord.addReportRecord)


/**
 * @api {post} /api/v0/user/postMessages 帖子评论/留言
 * @apiDescription 帖子评论/留言，需要登录态
 * @apiName /user/postMessages
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} contentId 帖子ID
 * @apiParam {string} replyAuthor 回复者ID (二级留言必填)
 * @apiParam {string} relationMsgId 回复目标留言ID (二级留言必填)
 * @apiParam {string} content 评论内容
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功！",
 *    "server_time": 1543372263586,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/postMessages
 * @apiVersion 1.0.0
 */
router.post('/user/postMessages', siteFunc.checkUserSessionForApi, Message.postMessages)


/**
 * @api {get} /api/v0/user/checkPhoneNumExist 校验注册手机号是否存在
 * @apiDescription 校验注册手机号是否存在
 * @apiName /user/checkPhoneNumExist
 * @apiGroup User
 * @apiParam {string} countryCode 国家代码
 * @apiParam {string} phoneNum 注册手机号
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "checkPhoneNumExist success",
 *  "server_time": 1544245281332,
 *  "data": {
 *    "checkState": true  // true/存在  false/不存在
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/checkPhoneNumExist
 * @apiVersion 1.0.0
 */
router.get('/user/checkPhoneNumExist', User.checkPhoneNumExist)

/**
 * @api {get} /api/v0/user/checkHadSetLoginPassword 校验用户是否已设置登录密码
 * @apiDescription 校验用户是否已设置登录密码，需要登录态
 * @apiName /user/checkHadSetLoginPassword
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "checkHadSetLoginPassword success",
 *  "server_time": 1544245281332,
 *  "data": {
 *    "checkState": true  // true/已设置  false/未设置
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/checkHadSetLoginPassword
 * @apiVersion 1.0.0
 */
router.get('/user/checkHadSetLoginPassword', siteFunc.checkUserSessionForApi, User.checkHadSetLoginPassword)


/**
 * @api {post} /api/v0/user/updateInfo 修改用户信息
 * @apiDescription 修改用户信息，需要登录态
 * @apiName /user/updateInfo
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} profession 职业
 * @apiParam {string} industry 行业
 * @apiParam {string} experience 教育经历
 * @apiParam {string} logo 头像
 * @apiParam {string} gender 性别 0男 1女
 * @apiParam {string} userName 昵称
 * @apiParam {string} introduction 一句话介绍
 * @apiParam {string} comments 个人简介
 * @apiParam {string} province 所在省份
 * @apiParam {string} city 所在城市
 * @apiParam {string} birth 出生年月日 2018-09-21
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功！",
 *    "server_time": 1543372263586,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/updateInfo
 * @apiVersion 1.0.0
 */
router.post('/user/updateInfo', siteFunc.checkUserSessionForApi, User.updateUser);


/**
 * @api {post} /api/v0/user/updateUserBeInvitationCode 提交邀请码
 * @apiDescription 提交邀请码，需要登录态
 * @apiName /user/updateUserBeInvitationCode
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} invitationCode 邀请码
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "updateUserBeInvitationCode",
 *    "server_time": 1543372263586,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/updateUserBeInvitationCode
 * @apiVersion 1.0.0
 */
router.post('/user/updateUserBeInvitationCode', siteFunc.checkUserSessionForApi, User.updateUserBeInvitationCode);

/**
 * @api {get} /api/v0/user/getUserNotifys 获取用户私信列表
 * @apiDescription 获取用户私信列表，带分页，需要登录态(ps: 注意私信和消息是分开的，有点差别)
 * @apiName /user/getUserNotifys
 * @apiGroup Messages
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功 userNotify",
 *    "server_time": 1542530587287,
 *    "data": [
 *        {
 *            "_id": "myh0RzkV3H",
 *            "user": "zwwdJvLmP",
 *            "notify": {
 *               "_id": "5mCBWXS-B",
 *               "title": "这是一条私信，不要偷偷打开哟。。。",
 *               "content": "<p>这是一条私信，不要偷偷打开哟。。。</p>",
 *               "date": "2018-11-18 16:43:07",
 *               "id": "5mCBWXS-B"
 *            },
 *            "__v": 0,
 *            "date": "2018-11-18 16:13:47",
 *            "isRead": false,
 *            "id": "myh0RzkV3H"
 *       }
 *    ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/getUserNotifys
 * @apiVersion 1.0.0
 */
router.get('/user/getUserNotifys', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.user = req.session.user._id;
  next()
}, UserNotify.getUserNotifys);


/**
 * @api {get} /api/v0/user/setNoticeRead 设置私信为已读
 * @apiDescription 设置私信为已读，消息类型包含系统公告，大师课公告等
 * @apiName /user/setNoticeRead
 * @apiGroup Messages
 * @apiParam {string} ids 消息id,多个id用逗号隔开
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "设置已读成功",
 *    "server_time": 1542529985218,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/setNoticeRead?ids=yvOk_g1y71
 * @apiVersion 1.0.0
 */
router.get('/user/setNoticeRead', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.user = req.session.user._id;
  next()
}, UserNotify.setMessageHasRead);

// 删除用户消息
router.get('/user/delUserNotify', siteFunc.checkUserSessionForApi, UserNotify.delUserNotify);

/**
 * @api {get} /api/v0/user/getMessages 我的评论
 * @apiDescription 我的评论 带分页
 * @apiName /user/getMessages
 * @apiGroup User
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} author 获取指定用户的评论列表，此处传用户ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功 message",
 *  "server_time": 1542899024811,
 *  "data": [
 *    {
 *  "_id": "tYVGV-HTL",
 *  "author": {
 *     "_id": "zwwdJvLmP",
 *     "userName": "doramart",
 *     "logo": "/upload/images/defaultlogo.png",
 *     "date": "2018-11-13 12:09:29",
 *     "enable": true,
 *     "id": "zwwdJvLmP"
 *   },
 *  "contentId": {
 *  "_id": "R8_iIMwF1",
 *  "title": "海底捞的致命缺点是什么？",
 *  "stitle": "海底捞的致命缺点是什么？",
 *  "updateDate": "2018-11-22",
 *  "date": "2018-11-22 23:03:44",
 *  "id": "R8_iIMwF1"
 *      },
 *  "__v": 0,
 *  "content": "这也是一条留言",
 *  "hasPraise": false,
 *  "praiseNum": 0,
 *  "date": "3 天前",
 *  "utype": "0",
 *  "id": "tYVGV-HTL"
 *    },
 *    {
 *  "_id": "4wv0tcLjH",
 *  "author": {
 *  "_id": "zwwdJvLmP",
 *  "userName": "doramart",
 *  "logo": "/upload/images/defaultlogo.png",
 *  "date": "2018-11-13 12:09:29",
 *  "enable": true,
 *  "id": "zwwdJvLmP"
 *      },
 *  "contentId": {
 *  "_id": "vGVoKV0g_",
 *  "title": "有哪一刹那让你对日本的美好印象瞬间破灭？",
 *  "stitle": "有哪一刹那让你对日本的美好印象瞬间破灭？",
 *  "updateDate": "2018-11-22",
 *  "date": "2018-11-22 23:03:44",
 *  "id": "vGVoKV0g_"
 *      },
 *  "__v": 0,
 *  "content": "这是一条留言",
 *  "hasPraise": false,
 *  "praiseNum": 0,
 *  "date": "3 天前",
 *  "utype": "0",
 *  "id": "4wv0tcLjH"
 *    }
 *  ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/getMessages
 * @apiVersion 1.0.0
 */
router.get('/user/getMessages', siteFunc.checkUserLoginState, Message.getMessages);


/**
 * @api {get} /api/v0/user/getUserContents 查询我的创作列表
 * @apiDescription 查找我的创作，带分页
 * @apiName /user/getUserContents
 * @apiGroup User
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} userId 指定用户的ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "contentlist",
 *  "server_time": 1542891752268,
 *  "data": [
 *    {
 *      "_id": "jRInYUowi",
 *      "title": "六小龄童做了什么事引来这么多的口诛笔伐？",
 *      "stitle": "六小龄童做了什么事引来这么多的口诛笔伐？",
 *      "author": null,
 *      "discription": "蓉城圆滚滚： 今天上午刚好听完六小龄童的现场讲座，有视频有真相，还是先附上一段现场版视频吧～ 这是我们省图书馆的一个讲座系列活动，叫“巴蜀讲坛”。通常会在周末邀请很多知名专家学者给大家做一些学术或者科普讲…",
 *      "comments": "9:00的时候开始入场，我虽然到的挺早，但还是只在报告厅的倒数第二排的角落里找到了个位置～大家陆续进场后，工作人员就开始在台上频繁地说：讲座结束后，章老师会有签名活动，为了避免拥挤，大家现在就可以在报告厅门口去购书～～（其实省图挺少接这种签售活动，就算接，一般也是安排在楼上的小活动室里，而不会在这个大报告厅，果然明星还是比较有面儿的哇～）讲座准时开始的，第一个环节是捐赠仪式，大约是捐赠了6套还是8套章老师的书给图书馆来着↓↓↓（红色衣服的是章老师，旁边那位是图书馆某个部门的主任～其实，我大约记得之前几位第一次来馆里办讲座的老师都是副馆长来致欢迎词的～）",
 *      "uAuthor": {
 *        "_id": "zwwdJvLmP",
 *        "userName": "doramart",
 *        "logo": "/upload/images/defaultlogo.png",
 *        "date": "2018-11-22 21:02:32",
 *        "id": "zwwdJvLmP"
 *      },
 *      "__v": 0,
 *      "keywords": null,
 *      "likeNum": 0, // 点赞总数
 *      "commentNum": 0, // 留言总数
 *      "favoriteNum": 0, // 收藏总数
 *      "despiseNum": 0, // 踩帖总数
 *      "clickNum": 1,  
 *      "isTop": 1,
 *      "state": true,
 *      "updateDate": "2018-11-18",
 *      "date": "2018-11-18 20:49:29",
 *      "appShowType": "0",
 *      "sImg": "/upload/images/img20181118203911.jpeg",
 *      "tags": [
 *        
 *      ],
 *      "categories": [
 *        
 *      ],
 *      "type": "1",
 *      "id": "jRInYUowi"
 *      "hasPraised": false // 当前用户是否已赞👍
 *      "total_reward_num": 0  // 打赏总额
 *      "hasReworded": false // 当前用户是否已打赏💰
 *      "hasComment": false // 当前用户是否已评论
 *      "hasFavorite": false // 当前用户是否已收藏
 *      "hasDespise": false // 当前用户是否已踩
 *    },
 *    ...
 *  ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/getUserContents
 * @apiVersion 1.0.0
 */
router.get('/user/getUserContents', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.areaType == '1';
  next();
}, Content.getContents);


/**
 * @api {get} /api/v0/user/getMyCommunity 查询我/其它用户关注的社群
 * @apiDescription 查询我的关注的社群 带分页
 * @apiName /user/getMyCommunity
 * @apiGroup User
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} userId 如果查询指定用户关注的社群，传此参数
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "getMyFollowCommunities",
 *  "server_time": 1545895055932,
 *  "data": [
 *    {
 *      "_id": "zDGsbThS_",
 *      "name": "糖果集",
 *      "comments": "糖果集",
 *      "creator": "-FvK_J0lH",
 *      "__v": 0,
 *      "tags": [
 *        "J9J8_8qYz0"
 *      ],
 *      "recommend": false,
 *      "date": "2018-11-15 09:48:24",
 *      "sImg": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1544623358228.png",
 *      "id": "zDGsbThS_",
 *      "watch_num": 3, // 关注人数
 *      "had_watched": false,  //本人是否已关注
 *      "content_num": 12  // 内容总数
 *    },
 *    ...
 *  ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/getMyCommunity
 * @apiVersion 1.0.0
 */
router.get('/user/getMyCommunity', siteFunc.checkUserLoginState, Community.getMyFollowCommunities);


/**
 * @api {get} /api/v0/user/getMyFollowInfos 我的关注
 * @apiDescription 我的关注，获取我关注的大师，专题，已经关注大师的帖子等相关信息，目前不带分页
 * @apiName /user/getMyFollowInfos
 * @apiGroup User
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "getMyFollowInfos",
 *  "server_time": 1542900168427,
 *  "data": {
 *   "watchersList": [ // 我关注的大师
 *      {
 *       "_id": "yNYHEw3-e",
 *       "userName": "yoooyu",
 *       "name": "大师兄",
 *       "date": "2018-11-22 23:22:48",
 *       "id": "yNYHEw3-e"
 *      }
 *    ],
 *   "watchSpecialList": [ // 我关注的专题
 *      {
 *       "_id": "eF0Djunws",
 *       "name": "人工智能",
 *       "comments": "关注人工智能，人人都有机会",
 *       "sImg": "/upload/images/img20181117213151.png",
 *       "date": "2018-11-22 23:22:48",
 *       "id": "eF0Djunws"
 *      }
 *    ],
 *   "watchMasterContents": [ // 我关注的相关内容
 *      {
 *       "_id": "Y1XFYKL52",
 *       "title": "如何优化vue的内存占用？",
 *       "stitle": "如何优化vue的内存占用？",
 *       "sortPath": "",
 *       "keywords": "",
 *       "uAuthor": "yNYHEw3-e",
 *       "discription": "在使用了一段时间的vue.js开发移动端h5页面（电商类页面，会有一些数据量较大但一次渲染就可以的商品列表）后",
 *       "comments": "<p><span>在使用了一段时间的vue.js开发移动端h5页面（电商类页面，会有一些数据量较大但一次渲染就可以的商品列表）后，感觉相比于传统的jquery（zepto）和前端模板引擎的组合能有效的提升开发效率和代码维护性，但是也存在一些问题，比如说内存占用问题，用vue.js开发的页面内存占用相比于传统方式会更多，而且传统的开发方式页面渲染完后，还能对不需要的js对象进行垃圾回收，但vue.js里面的一些指令对象、watcher对象、data数据等似乎目前都没有找到比较好的垃圾回收的方式。</span><br /><br /><span>想问下对于那些只用渲染一次的页面部分（比如数据量较大的列表页）有没有比较合适的内存优化方案（比如释放指令对象、watcher对象、data对象）？</span><br /><br /><span>举个例子：</span><br /><br /><span>比如其中的lazy指令，以及对于items这个基本上只用渲染一次的data等有没有可以优化内存占用的方法</span></p>",
 *       "twiterAuthor": "",
 *       "translate": "",
 *       "bearish": [
 *          
 *        ],
 *       "profitable": [
 *          
 *        ],
 *       "isFlash": false,
 *       "__v": 0,
 *       "author": "",
 *       "likeNum": 0,
 *       "commentNum": 0,
 *       "clickNum": 5,
 *       "isTop": 1,
 *       "state": true,
 *       "updateDate": "2018-11-16",
 *       "date": "2018-11-16 23:00:16",
 *       "appShowType": "0",
 *       "sImg": "/upload/images/img20181116225948.jpeg",
 *       "tags": [
 *       "Y3DTgmHK3"
 *        ],
 *       "categories": [
 *          
 *        ],
 *       "type": "1",
 *       "id": "Y1XFYKL52"
 *      }
 *    ]
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/getMyFollowInfos
 * @apiVersion 1.0.0
 */
router.get('/user/getMyFollowInfos', siteFunc.checkUserSessionForApi, User.getMyFollowInfos);


// 用户注销
router.get('/user/logOut', siteFunc.checkUserSessionForApi, User.logOut);

// 找回密码
router.get('/user/confirmEmail', generalFun.getDataForResetPsdPage)

//提交验证邮箱
router.post('/user/sentConfirmEmail', (req, res, next) => {
  next();
}, User.sentConfirmEmail);

//点击找回密码链接跳转页面
router.get('/user/reset_pass', User.reSetPass);

router.post('/user/updateNewPsd', User.updateNewPsd);

// 发送邮件给管理员
router.post('/user/postEmailToAdminUser', User.postEmailToAdminUser);

// 管理员登录
router.post('/admin/doLogin', AdminUser.loginAction);

// 获取管理员登录二维码
router.get('/getImgCode', User.getImgCode);


//---------------------------------------------------------文档相关api


/**
 * @api {get} /api/v0/content/getList 查询帖子列表
 * @apiDescription 根据参数获取对应的帖子列表,默认按时间查询，可作为发现栏目列表
 * @apiName /content/getList
 * @apiGroup Contents
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} searchkey 搜索关键字
 * @apiParam {string} type 文档类型 1、普通，2、专题，3、视频，4、音频
 * @apiParam {string} topicId 针对专题，传专题ID
 * @apiParam {string} isTop 推荐帖子(isTop:1)
 * @apiParam {string} sortby 按字段排序(热门：clickNum)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 {
 *   "status": 200,
 *   "message": "contentlist",
 *   "server_time": 1542380520270,
 *   "data": [
 *       {
 *           "_id": "Y1XFYKL52",
 *           "title": "如何优化vue的内存占用？",
 *           "stitle": "如何优化vue的内存占用？",
 *           "sortPath": "",
 *           "keywords": "",
 *           "author": {
 *               "_id": "4JiWCMhzg",
 *               "userName": "doramart",
 *               "name": "生哥",
 *               "logo": "/upload/smallimgs/img1448202744000.jpg"
 *           },
 *           "discription": "在使用了一段时间的vue.js开发移动端h5页面（电商类页面，会有一些数据量较大但一次渲染就可以的商品列表）后",
 *           "comments": "在使用了一段时间的vue.js开发移动端h5页面（电商类页面，会有一些数据量较大但一次渲染就可以的商品列表）后，感觉相比于传统的jquery（zepto）和前端模板引擎的组合能有效的提升开发效率和代码维护性，但是也存在一些问题，比如说内存占用问题，用vue.js开发的页面内存占用相比于传统方式会更多，而且传统的开发方式页面渲染完后，还能对不需要的js对象进行垃圾回收，但vue.js里面的一些指令对象、watcher对象、data数据等似乎目前都没有找到比较好的垃圾回收的方式。想问下对于那些只用渲染一次的页面部分（比如数据量较大的列表页）有没有比较合适的内存优化方案（比如释放指令对象、watcher对象、data对象）？举个例子：比如其中的lazy指令，以及对于items这个基本上只用渲染一次的data等有没有可以优化内存占用的方法",
 *           "translate": "",
 *           "bearish": [],
 *           "profitable": [],
 *           "from": "1",
 *           "likeUserIds": [],
 *           "likeNum": 0, // 点赞总数
 *           "commentNum": 0, // 留言总数
 *           "favoriteNum": 0, // 收藏总数
 *           "despiseNum": 0, // 踩帖总数
 *           "clickNum": 1,
 *           "isTop": 0,
 *           "state": true,
 *           "updateDate": "2018-11-16",
 *           "date": "2018-11-16 23:00:16",
 *           "appShowType": "0", // app端展示模式 0，1，2，3
 *           "duration": "0:01",, // 针对视频的视频时长
 *           "sImg": "/upload/images/img20181116225948.jpeg",
 *           "tags": [
 *               {
 *                "_id": "Y3DTgmHK3",
 *                "name": "区块链",
 *                "date": "2018-11-16 23:02:00",
 *                "id": "Y3DTgmHK3"
 *                }
 *            ],
 *           "categories": [
 *                {
 *                "_id": "Nycd05pP",
 *                "name": "人工智能",
 *                "defaultUrl": "artificial-intelligence",
 *                "enable": true,
 *                "date": "2018-11-16 23:02:00",
 *                "id": "Nycd05pP"
 *                }
 *            ],
 *           "type": "1",
 *           "id": "Y1XFYKL52"
 *           "hasPraised": false // 当前用户是否已赞👍
 *           "hasReworded": false // 当前用户是否已打赏💰
 *           "hasComment": false // 当前用户是否已评论
 *           "hasFavorite": false // 当前用户是否已收藏
 *           "hasDespise": false // 当前用户是否已踩
 *           "total_reward_num": 0  // 打赏总额
 *           "videoUrl": 0  // 视频URL
 *           "audioUrl": 0  // 音频URL
 *        }
 *    ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/content/getList?isTop=1
 * @apiVersion 1.0.0
 */
router.get('/content/getList', siteFunc.checkUserLoginState, (req, res, next) => {
  req.query.state = '2';
  next()
}, Content.getContents);

router.get('/content/getRecommendedList', siteFunc.checkUserLoginState, (req, res, next) => {
  next()
}, Content.getTopIndexContents);


/**
 * @api {get} /api/v0/content/getRecContents 查询推荐文档(文档详情页)
 * @apiDescription 查询推荐文档(文档详情页) 默认5篇
 * @apiName /content/getRecContents
 * @apiGroup Contents
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} tags 文章标签id集合（Y1XFYKL52,多个用逗号隔开）
 * @apiParam {string} contentId 当前文章id
 * @apiParam {string} type 当前文章类型 1.普通 3.视频 4.音频
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 {
 *   "status": 200,
 *   "message": "contentlist",
 *   "server_time": 1542380520270,
 *   "data": [
 *       {
 *           "_id": "Y1XFYKL52",
 *           "title": "如何优化vue的内存占用？",
 *           "stitle": "如何优化vue的内存占用？",
 *           "sortPath": "",
 *           "keywords": "",
 *           "author": {
 *               "_id": "4JiWCMhzg",
 *               "userName": "doramart",
 *               "name": "生哥",
 *               "logo": "/upload/smallimgs/img1448202744000.jpg"
 *           },
 *           "discription": "在使用了一段时间的vue.js开发移动端h5页面（电商类页面，会有一些数据量较大但一次渲染就可以的商品列表）后",
 *           "comments": "在使用了一段时间的vue.js开发移动端h5页面（电商类页面，会有一些数据量较大但一次渲染就可以的商品列表）后，感觉相比于传统的jquery（zepto）和前端模板引擎的组合能有效的提升开发效率和代码维护性，但是也存在一些问题，比如说内存占用问题，用vue.js开发的页面内存占用相比于传统方式会更多，而且传统的开发方式页面渲染完后，还能对不需要的js对象进行垃圾回收，但vue.js里面的一些指令对象、watcher对象、data数据等似乎目前都没有找到比较好的垃圾回收的方式。想问下对于那些只用渲染一次的页面部分（比如数据量较大的列表页）有没有比较合适的内存优化方案（比如释放指令对象、watcher对象、data对象）？举个例子：比如其中的lazy指令，以及对于items这个基本上只用渲染一次的data等有没有可以优化内存占用的方法",
 *           "translate": "",
 *           "bearish": [],
 *           "profitable": [],
 *           "from": "1",
 *           "likeUserIds": [],
 *           "likeNum": 0, // 点赞总数
 *           "commentNum": 0, // 留言总数
 *           "favoriteNum": 0, // 收藏总数
 *           "despiseNum": 0, // 踩帖总数
 *           "clickNum": 1,
 *           "isTop": 0,
 *           "state": true,
 *           "updateDate": "2018-11-16",
 *           "date": "2018-11-16 23:00:16",
 *           "appShowType": "0", // app端展示模式 0，1，2，3
 *           "duration": "0:01",, // 针对视频的视频时长
 *           "sImg": "/upload/images/img20181116225948.jpeg",
 *           "tags": [
 *               {
 *                "_id": "Y3DTgmHK3",
 *                "name": "区块链",
 *                "date": "2018-11-16 23:02:00",
 *                "id": "Y3DTgmHK3"
 *                }
 *            ],
 *           "categories": [
 *                {
 *                "_id": "Nycd05pP",
 *                "name": "人工智能",
 *                "defaultUrl": "artificial-intelligence",
 *                "enable": true,
 *                "date": "2018-11-16 23:02:00",
 *                "id": "Nycd05pP"
 *                }
 *            ],
 *           "type": "1",
 *           "id": "Y1XFYKL52"
 *           "hasPraised": false // 当前用户是否已赞👍
 *           "hasReworded": false // 当前用户是否已打赏💰
 *           "hasComment": false // 当前用户是否已评论
 *           "hasFavorite": false // 当前用户是否已收藏
 *           "hasDespise": false // 当前用户是否已踩
 *           "total_reward_num": 0  // 打赏总额
 *        }
 *    ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/content/getRecContents
 * @apiVersion 1.0.0
 */
router.get('/content/getRecContents', siteFunc.checkUserLoginState, Content.getRecContents);



/**
 * @api {get} /api/v0/content/getListByEv 查询热门帖子（根据文章EV）
 * @apiDescription 查询热门帖子（根据文章EV）
 * @apiName /content/getListByEv
 * @apiGroup Contents
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 {
 *   "status": 200,
 *   "message": "getListByEv",
 *   "server_time": 1542380520270,
 *   "data": [
 *       {
 *           "_id": "Y1XFYKL52",
 *           "title": "如何优化vue的内存占用？",
 *           "stitle": "如何优化vue的内存占用？",
 *           "sortPath": "",
 *           "keywords": "",
 *           "author": {
 *               "_id": "4JiWCMhzg",
 *               "userName": "doramart",
 *               "name": "生哥",
 *               "logo": "/upload/smallimgs/img1448202744000.jpg"
 *           },
 *           "discription": "在使用了一段时间的vue.js开发移动端h5页面（电商类页面，会有一些数据量较大但一次渲染就可以的商品列表）后",
 *           "comments": "在使用了一段时间的vue.js开发移动端h5页面（电商类页面，会有一些数据量较大但一次渲染就可以的商品列表）后，感觉相比于传统的jquery（zepto）和前端模板引擎的组合能有效的提升开发效率和代码维护性，但是也存在一些问题，比如说内存占用问题，用vue.js开发的页面内存占用相比于传统方式会更多，而且传统的开发方式页面渲染完后，还能对不需要的js对象进行垃圾回收，但vue.js里面的一些指令对象、watcher对象、data数据等似乎目前都没有找到比较好的垃圾回收的方式。想问下对于那些只用渲染一次的页面部分（比如数据量较大的列表页）有没有比较合适的内存优化方案（比如释放指令对象、watcher对象、data对象）？举个例子：比如其中的lazy指令，以及对于items这个基本上只用渲染一次的data等有没有可以优化内存占用的方法",
 *           "translate": "",
 *           "bearish": [],
 *           "profitable": [],
 *           "from": "1",
 *           "likeUserIds": [],
 *           "likeNum": 0, // 点赞总数
 *           "commentNum": 0, // 留言总数
 *           "favoriteNum": 0, // 收藏总数
 *           "despiseNum": 0, // 踩帖总数
 *           "clickNum": 1,
 *           "isTop": 0,
 *           "state": true,
 *           "updateDate": "2018-11-16",
 *           "date": "2018-11-16 23:00:16",
 *           "appShowType": "0", // app端展示模式 0，1，2，3
 *           "duration": "0:01",, // 针对视频的视频时长
 *           "sImg": "/upload/images/img20181116225948.jpeg",
 *           "tags": [
 *               {
 *                "_id": "Y3DTgmHK3",
 *                "name": "区块链",
 *                "date": "2018-11-16 23:02:00",
 *                "id": "Y3DTgmHK3"
 *                }
 *            ],
 *           "categories": [
 *                {
 *                "_id": "Nycd05pP",
 *                "name": "人工智能",
 *                "defaultUrl": "artificial-intelligence",
 *                "enable": true,
 *                "date": "2018-11-16 23:02:00",
 *                "id": "Nycd05pP"
 *                }
 *            ],
 *           "type": "1",
 *           "id": "Y1XFYKL52"
 *           "hasPraised": false // 当前用户是否已赞👍
 *           "hasReworded": false // 当前用户是否已打赏💰
 *           "hasComment": false // 当前用户是否已评论
 *           "hasFavorite": false // 当前用户是否已收藏
 *           "hasDespise": false // 当前用户是否已踩
 *           "total_reward_num": 0  // 打赏总额
 *        }
 *    ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/content/getListByEv
 * @apiVersion 1.0.0
 */
router.get('/content/getListByEv', siteFunc.checkUserLoginState, (req, res, next) => {
  next()
}, Content.getHotContentsByEv);


/**
 * @api {get} /api/v0/hotSearch/getList 查询热搜关键词
 * @apiDescription 查询热搜关键词
 * @apiName /hotSearch/getList
 * @apiGroup Contents
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "getHotSearchs",
 *    "server_time": 1547632790307,
 *    "data": [
 *        {
 *          "_id": "KLI-Dl3H-",
 *          "word": "第一",
 *          "frequency": 1,
 *          "updatetime": "2019-01-16 17:59:50",
 *          "date": "2019-01-16 17:59:50",
 *          "id": "KLI-Dl3H-"
 *        },
 *        {
 *          "_id": "L0JrW3KWl",
 *          "word": "多少",
 *          "frequency": 1,
 *          "updatetime": "2019-01-16 17:59:50",
 *          "date": "2019-01-16 17:59:50",
 *          "id": "L0JrW3KWl"
 *        }
 *    ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/hotSearch/getList
 * @apiVersion 1.0.0
 */
router.get('/hotSearch/getList', HotSearch.getHotSearchs);


/**
 * @api {get} /api/v0/special/getList 获取专题列表
 * @apiDescription 获取专题列表，带分页
 * @apiName /special/getList
 * @apiGroup Contents
 * @apiParam {string} creator 创建者ID
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} searchkey 搜索关键字
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "getSpecials",
 *  "server_time": 1546065144366,
 *  "data": [
 *    {
 *      "_id": "-nG-fQZfq",
 *      "comments": "互联网专题申请互联网专题申请互联网专题",
 *      "name": "互联网专题申请",
 *      "category": {
 *         "_id": "1YLWdtpKSB0",
 *         "name": "心理学",
 *         "date": "2018-12-29 14:32:24",
 *         "id": "1YLWdtpKSB0"
 *      },
 *      "creator": {
 *         "_id": "oggNx7PkB",
 *         "userName": "Tests2000",
 *         "date": "2018-12-29 14:32:24",
 *         "id": "oggNx7PkB"
 *      },
 *      "approver": "4JiWCMhzg",
 *      "__v": 0,
 *      "date": "2018-12-29 13:49:09",
 *      "sImg": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1544623778539.png",
 *      "id": "-nG-fQZfq",
 *      "watch_num": 0,
 *      "had_watched": false
 *    }
 *  ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/special/getList
 * @apiVersion 1.0.0
 */
router.get('/special/getList', siteFunc.checkUserLoginState, Special.getSpecials)


/**
 * @api {get} /api/v0/special/getMyFollowSpecials 获取我/其它用户关注的专题
 * @apiDescription 获取关注专题列表，带分页，需要登录态
 * @apiName /special/getMyFollowSpecials
 * @apiGroup Contents
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} searchkey 搜索关键字
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} userId 获取指定用户关注的专题列表
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "getMyFollowSpecials",
 *    "server_time": 1542275258971,
 *    "data": [
 *       {
 *         "_id": "eF0Djunws",
 *         "name": "人工智能",
 *         "comments": "关注人工智能，人人都有机会",
 *         "__v": 0,
 *         "users": [],
 *         "date": "2018-11-15 17:13:51",
 *         "id": "eF0Djunws"
 *         "sImg": "/upload/images/img20181117213151.png"
 *         "watch_num": 1
 *         "had_watched": true
 *         "content_num": 10
 *       }
 *   ]
 * }
 * @apiSampleRequest http://localhost:8884/api/v0/special/getMyFollowSpecials
 * @apiVersion 1.0.0
 */
router.get('/special/getMyFollowSpecials', siteFunc.checkUserLoginState, Special.getMyFollowSpecials)


/**
 * @api {get} /api/v0/content/getMyAttentionContents 查询我关注的大师的文章和我关注的专题文章集合
 * @apiDescription 查询我关注的大师的文章和我关注的专题文章集合，按时间排序，带分页
 * @apiName /content/getMyAttentionContents
 * @apiGroup Contents
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 {
 *   "status": 200,
 *   "message": "contentlist",
 *   "server_time": 1542380520270,
 *   "data": [
 *       {
 *           "_id": "Y1XFYKL52",
 *           "title": "如何优化vue的内存占用？",
 *           "stitle": "如何优化vue的内存占用？",
 *           "sortPath": "",
 *           "keywords": "",
 *           "author": {
 *               "_id": "4JiWCMhzg",
 *               "userName": "doramart",
 *               "name": "生哥",
 *               "logo": "/upload/smallimgs/img1448202744000.jpg"
 *           },
 *           "discription": "在使用了一段时间的vue.js开发移动端h5页面（电商类页面，会有一些数据量较大但一次渲染就可以的商品列表）后",
 *           "comments": "在使用了一段时间的vue.js开发移动端h5页面（电商类页面，会有一些数据量较大但一次渲染就可以的商品列表）后，感觉相比于传统的jquery（zepto）和前端模板引擎的组合能有效的提升开发效率和代码维护性，但是也存在一些问题，比如说内存占用问题，用vue.js开发的页面内存占用相比于传统方式会更多，而且传统的开发方式页面渲染完后，还能对不需要的js对象进行垃圾回收，但vue.js里面的一些指令对象、watcher对象、data数据等似乎目前都没有找到比较好的垃圾回收的方式。想问下对于那些只用渲染一次的页面部分（比如数据量较大的列表页）有没有比较合适的内存优化方案（比如释放指令对象、watcher对象、data对象）？举个例子：比如其中的lazy指令，以及对于items这个基本上只用渲染一次的data等有没有可以优化内存占用的方法",
 *           "translate": "",
 *           "bearish": [],
 *           "profitable": [],
 *           "from": "1",
 *           "likeUserIds": [],
 *           "likeNum": 0, // 点赞总数
 *           "commentNum": 0, // 留言总数
 *           "favoriteNum": 0, // 收藏总数
 *           "despiseNum": 0, // 踩帖总数
 *           "clickNum": 1,
 *           "isTop": 0,
 *           "state": true,
 *           "updateDate": "2018-11-16",
 *           "date": "2018-11-16 23:00:16",
 *           "appShowType": "0", // app端展示模式 0，1，2，3
 *           "duration": "0:01",, // 针对视频的视频时长
 *           "sImg": "/upload/images/img20181116225948.jpeg",
 *           "tags": [
 *               {
 *                "_id": "Y3DTgmHK3",
 *                "name": "区块链",
 *                "date": "2018-11-16 23:02:00",
 *                "id": "Y3DTgmHK3"
 *                }
 *            ],
 *           "categories": [
 *                {
 *                "_id": "Nycd05pP",
 *                "name": "人工智能",
 *                "defaultUrl": "artificial-intelligence",
 *                "enable": true,
 *                "date": "2018-11-16 23:02:00",
 *                "id": "Nycd05pP"
 *                }
 *            ],
 *           "type": "1",
 *           "id": "Y1XFYKL52"
 *           "hasPraised": false // 当前用户是否已赞👍
 *           "hasReworded": false // 当前用户是否已打赏💰
 *           "hasComment": false // 当前用户是否已评论
 *           "hasFavorite": false // 当前用户是否已收藏
 *           "hasDespise": false // 当前用户是否已踩
 *           "total_reward_num": 0  // 打赏总额
 *        }
 *    ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/content/getMyAttentionContents
 * @apiVersion 1.0.0
 */
router.get('/content/getMyAttentionContents', siteFunc.checkUserSessionForApi, Content.getMyAttentionContents);

/**
 * @api {get} /api/v0/user/addSpecial 关注专题
 * @apiDescription 关注专题（需要登录状态,后台会通过读取用户session获取用户ID）
 * @apiName /user/addSpecial
 * @apiGroup User
 * @apiParam {string} specialId 专题id(eq:eF0Djunws)
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功",
 *  "server_time": 1542251075220,
 *  "data": {
 *    
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/addSpecial
 * @apiVersion 1.0.0
 */
router.get('/user/addSpecial', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.specialState = 'in';
  next()
}, User.addSpecial)

/**
 * @api {get} /api/v0/user/delSpecial 取消关注专题
 * @apiDescription 取消关注专题（需要登录状态,后台会通过读取用户session获取用户ID）
 * @apiName /user/delSpecial
 * @apiGroup User
 * @apiParam {string} specialId 专题id(eq:eF0Djunws)
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功",
 *  "server_time": 1542251075220,
 *  "data": {
 *    
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/user/delSpecial
 * @apiVersion 1.0.0
 */
router.get('/user/delSpecial', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.specialState = 'out';
  next()
}, User.addSpecial)

/**
 * @api {get} /api/v0/masterCategory/getList 获取大师类别列表
 * @apiDescription 获取大师类别列表，带分页
 * @apiName /masterCategory/getList
 * @apiGroup User
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} searchkey 搜索关键字
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "getMasterCategorys",
 *    "server_time": 1542275258971,
 *    "data": [
 *       {
 *         "_id": "eF0Djunws",
 *         "name": "人工智能",
 *         "comments": "关注人工智能，人人都有机会",
 *         "__v": 0,
 *         "sImg": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1546875582758.jpg",
 *         "date": "2018-11-15 17:13:51",
 *         "id": "eF0Djunws",
 *         "follow_num": 3,
 *         "follow_users": "yoooyu,master6,master7,master8"
 *       }
 *   ]
 * }
 * @apiSampleRequest http://localhost:8884/api/v0/masterCategory/getList
 * @apiVersion 1.0.0
 */
router.get('/masterCategory/getList', MasterCategory.getMasterCategorys)


/**
 * @api {post} /api/v0/upload/files 文件上传(OSS)
 * @apiDescription 文件上传，上传用户头像等
 * @apiName /api/v0/upload/files
 * @apiGroup User
 * @apiParam {file} file 文件
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "get data success",
 *    "server_time": 1544167579835,
 *    "data": 
 *    {
 *       "path": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1544167579253.png" // 文件链接
 *       "type": "0" // 文件类型  0:图片 1:视频
 *       "thumbnail": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1544167579253.png" // 视频缩略图
 *    } 
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/upload/files
 * @apiVersion 1.0.0
 */



/**
 * @api {get} /api/v0/dynamic/getList 获取指定用户动态
 * @apiDescription 获取指定用户动态
 * @apiName /dynamic/getList
 * @apiGroup User
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} userId 指定用户ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "getUserActionHiss",
 *  "server_time": 1546180214436,
 *  "data": [
 *    {
 *      "type": "comment",
 *      "targetUser": {
 *      "_id": "QmTiscdva",
 *      "userName": "Gavin",
 *      "walletAddress": "0x6d59d3148801f9cff2b5bfd0a0bf0a8706cdbfe5",
 *      "date": "2018-12-30 22:30:14",
 *      "id": "QmTiscdva"
 *      },
 *      "targetContent": {
 *        
 *      },
 *      "targetMessage": {
 *      "_id": "QMl-lloT6",
 *      "contentId": {
 *         "_id": "ahmY2yaNg",
 *         "title": "击败亚马逊、谷歌 中国科技公司图像识别技术创纪录",
 *         "updateDate": "2018-12-30 22:30:14",
 *         "date": "2018-12-30 22:30:14",
 *         "id": "ahmY2yaNg"
 *        },
 *      "author": {
 *         "_id": "QmTiscdva",
 *         "userName": "Gavin",
 *         "date": "2018-12-30 22:30:14",
 *         "id": "QmTiscdva"
 *        },
 *      "state": false,
 *      "date": "2018-12-30 22:30:14",
 *      "id": "QMl-lloT6"
 *      },
 *      "targetCommunityContent": {
 *        
 *      }
 *    },
 *    ...
 *  ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/dynamic/getList
 * @apiVersion 1.0.0
 */
router.get('/dynamic/getList', UserActionHis.getUserDynamic)



/**
 * @api {post} /api/v0/userJob/signIn 签到
 * @apiDescription 签到
 * @apiName /userJob/signIn
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiGroup User
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "簽到 成功",
 *    "server_time": 1545793965889,
 *    "data": {
 *       "cumulative": 3  // 已累计天数
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/userJob/signIn
 * @apiVersion 1.0.0
 */
router.post('/userJob/signIn', siteFunc.checkUserSessionForApi, SignIn.addSignIn)

/**
 * @api {get} /api/v0/userJob/getSignList 签到列表
 * @apiDescription 签到列表，带分页
 * @apiName /userJob/getSignList
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiGroup User
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "getSignIns",
 *    "server_time": 1545794926320,
 *    "data": [
 *        {
 *         "_id": "rz2aXnPXA",
 *         "__v": 0,
 *         "user": [
 *                {
 *                  "_id": "zwwdJvLmP",
 *                  "userName": "doramart",
 *                  "date": "2018-12-26 11:28:46",
 *                  "id": "zwwdJvLmP"
 *                }
 *            ],
 *         "date": "2018-12-26",
 *         "id": "rz2aXnPXA"
 *        }
 *    ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/userJob/getSignList
 * @apiVersion 1.0.0
 */
router.get('/userJob/getSignList', siteFunc.checkUserSessionForApi, SignIn.getSignIns)


/**
 * @api {get} /api/v0/userJob/getBasicSignInInfo 获取签到汇总信息
 * @apiDescription 获取签到汇总信息，需要登录态
 * @apiName /userJob/getBasicSignInInfo
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiGroup User
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "getBasicSignInInfo",
 *    "server_time": 1545834005585,
 *    "data": {
 *       "continuitySignRecords": 1, // 连续签到天数
 *       "totalSignRecords": 1,  // 累计签到天数
 *       "signInIncome": 10 //累计获得奖励
 *       "todayHadSignIn": false //今天是否已签到
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/userJob/getBasicSignInInfo
 * @apiVersion 1.0.0
 */
router.get('/userJob/getBasicSignInInfo', siteFunc.checkUserSessionForApi, SignIn.getBasicSignInInfo)


/**
 * @api {post} /api/v0/userJob/feedback 用户反馈
 * @apiDescription 用户提交反馈，需要登录态
 * @apiName /userJob/feedback
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} comments 反馈内容
 * @apiParam {string} phoneNum 联系方式
 * @apiGroup User
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "迴響 成功",
 *    "server_time": 1546263607993,
 *    "data": {
 *       "id": "6MP59v-r8"
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/userJob/feedback
 * @apiVersion 1.0.0
 */
router.post('/userJob/feedback', siteFunc.checkUserSessionForApi, FeedBack.addFeedBack)



/**
 * @api {get} /api/v0/userJob/inviteInfo 获取邀请统计
 * @apiDescription 获取邀请统计
 * @apiName /userJob/inviteInfo
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiGroup User
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "getInvitationList",
 *    "server_time": 1548037382973,
 *    "data": {
 *       "invited_num": 0,  // 邀请总人数
 *       "totalBill_num": 0 // 邀请获得MBT总数量
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/userJob/inviteInfo
 * @apiVersion 1.0.0
 */
router.get('/userJob/inviteInfo', siteFunc.checkUserSessionForApi, BillRecord.getInvitationInfo)

/**
 * @api {get} /api/v0/userJob/forwardingAward app转发激励
 * @apiDescription app转发激励
 * @apiName /userJob/forwardingAward
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} contentId 转发内容的ID（普通文档，专题）
 * @apiGroup User
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "forwardingAward",
 *    "server_time": 1548037382973,
 *    "data": {
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/userJob/forwardingAward
 * @apiVersion 1.0.0
 */
router.get('/userJob/forwardingAward', siteFunc.checkUserSessionForApi, User.appForwardAward)

/**
 * @api {get} /api/v0/userJob/birthdayReward 生日奖励
 * @apiDescription 生日奖励
 * @apiName /userJob/birthdayReward
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiGroup User
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "birthdayReward",
 *    "server_time": 1548037382973,
 *    "data": {
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/userJob/birthdayReward
 * @apiVersion 1.0.0
 */
router.get('/userJob/birthdayReward', siteFunc.checkUserSessionForApi, User.appBirthAward)


/**
 * @api {get} /api/v0/userJob/getBrowseRecord 获取浏览记录
 * @apiDescription 获取浏览记录
 * @apiName /userJob/getBrowseRecord
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiGroup User
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 * {
 *    "status": 200,
 *    "message": "BrowseRecord success",
 *    "server_time": 1552553417615,
 *  "data": {
 *     "_id": "ri5WaXugX",
 *     "title": "谷歌AI首席科学家：想当研究科学家，一事无成你受得了吗？",
 *     "stitle": "谷歌AI首席科学家：想当研究科学家，一事无成你受得了吗？",
 *     "sortPath": "",
 *     "keywords": "",
 *     "uAuthor": {
 *        "_id": "uUKsBv5y_",
 *        "userName": "master10",
 *        "logo": "https://cg2010studio.files.wordpress.com/2015/12/cartoonavatar2.jpg",
 *        "date": "2018-12-19 01:42:06",
 *        "id": "uUKsBv5y_"
 *     },
 *     "discription": "AI研究科学家不是那么好当的！近日谷歌AI首席科学家Vincent Vanhoucke发表在Medium上的文章引来众人关注。在本文中，他列举了成为研究科学家所要面对的9大挑战，看完这篇内容或许可以在立志投身于科学事业前，给你先“泼一盆冷水”。",
 *     "__v": 0,
 *     "author": {
 *        "userName": "doramart",
 *        "name": "超管",
 *        "logo": "/upload/smallimgs/img1448202744000.jpg"
 *     },
 *     "simpleComments": [
 *       {
 *        "type": "contents",
 *        "content": "做一名研究人员可能会让你的人生非常充实并得到他人的认可。但我知道很多学生在做研究时受到前景的压力，一时陷入工程的舒适区。他们通常把这个阶段视为个人失败，觉得自己“不够优秀”。而根据我个人的经验，这从来就不是个人价值或者天赋的问题：在研究中成长需要某种不同的气质，这种气质往往与工程师成长的原因有些矛盾。以下是我见过的研究人员在职业生涯的某个阶段不得不面对的一些主要压力：\n  \n做研究要解决的是有多个答案（或没有答案）的不适定问题\n                               *        "
 *       },
 *       {
 *        "type": "video",
 *        "content": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/1071402816293179392.mp4"
 *       },
 *       {
 *        "type": "contents",
 *        "content": " \n\n大学教育很大程度上教会了你如何用特定的方案解决适定问题，但用这种方式去对待研究却注定失败。你在研究中做的很多事并不会让你接近答案，而是让你更好地理解问题。 \n"
 *       },
 *       {
 *        "type": "image",
 *        "content": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/1075054319700676608.png"
 *       },
 *       {
 *        "type": "contents",
 *        "content": " \n用学到的东西，而不是取得的研究进展来衡量自己的进步，是一个人在研究环境中必须经历的重要范式转变之一。\n\n"
 *       }
 *     ],
 *     "likeNum": 0,
 *     "commentNum": 10,
 *     "clickNum": 77,
 *     "isTop": 1,
 *     "state": true,
 *     "updateDate": "2018-12-08 21:37:38",
 *     "date": "2018-12-08 21:37:38",
 *     "duration": "0:01",
 *     "videoArr": [
 *        "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/1071402816293179392.mp4"
 *     ],
 *     "imageArr": [
 *        "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/1075054319700676608.png"
 *     ],
 *     "appShowType": "3",
 *     "videoImg": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/1071402816293179392_thumbnail.jpg", // 视频缩略图（当appShowType=3）
 *     "sImg": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1544276235259.jpg",
 *     "tags": [
 *       {
 *        "_id": "aGE-YfyLRjx",
 *        "name": "化学",
 *        "date": "2018-12-19 01:42:06",
 *        "id": "aGE-YfyLRjx"
 *       },
 *       {
 *        "_id": "_euYIiOqvLA",
 *        "name": "体育",
 *        "date": "2018-12-19 01:42:06",
 *        "id": "_euYIiOqvLA"
 *       }
 *     ],
 *     "categories": [
 *       
 *     ],
 *     "type": "1",
 *     "id": "ri5WaXugX",
 *     "hasPraised": false,
 *     "hasReworded": false,
 *     "hasComment": true,
 *     "hasFavorite": false,
 *     "hasDespise": false,
 *     "total_reward_num": 0,
 *     "favoriteNum": 0,
 *     "despiseNum": 0
 *   }
 * }
 * @apiSampleRequest http://localhost:8884/api/v0/userJob/getBrowseRecord
 * @apiVersion 1.0.0
 */
router.get('/userJob/getBrowseRecord', siteFunc.checkUserSessionForApi, Content.getBrowseRecord)


/**
 * @api {get} /api/v0/userJob/getSwitches 获取系统开关配置
 * @apiDescription 获取系统开关配置
 * @apiName /userJob/getSwitches
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiGroup User
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "birthdayReward",
 *    "server_time": 1548037382973,
 *    "data": {
 *        "__v": 0,
 *        "user": "LCMCJjLoR",
 *        "date": "2019-04-17 14:03:33",
 *        "followMasterContent": false,  // 关注的大师发布了新文章
 *        "followMasterClass": false,  // 关注的大师发布了课程
 *        "subscribeClass": false,  // 购买课程
 *        "appreciateContent": false,  // 打赏文章
 *        "thumbsUpContent": false, // 点赞文章
 *        "thumbsUpComments": false, // 赞同回答
 *        "followSpecial": false,  // 关注了我的专题
 *        "followCommunity": false, // 关注了我的社群
 *        "follow": false, //关注我
 *        "commentOrReply": false, //评论或回复我
 *        "_id": "hVUWO9pPa",
 *        "id": "hVUWO9pPa"
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/userJob/getSwitches
 * @apiVersion 1.0.0
 */
router.get('/userJob/getSwitches', siteFunc.checkUserSessionForApi, Switches.getSwitches)

/**
 * @api {post} /api/v0/userJob/updateSwitches 修改系统开关配置
 * @apiDescription 修改系统开关配置,开关参数可以只传修改的字段
 * @apiName /userJob/updateSwitches
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string}  followMasterContent    关注的大师发布了新文章 (1:开，0:关)
 * @apiParam {string}  followMasterClass    关注的大师发布了课程(1:开，0:关)
 * @apiParam {string}  subscribeClass    购买课程(1:开，0:关)
 * @apiParam {string}  appreciateContent    打赏文章(1:开，0:关)
 * @apiParam {string}  thumbsUpContent   点赞文章(1:开，0:关)
 * @apiParam {string}  thumbsUpComments   赞同回答(1:开，0:关)
 * @apiParam {string}  followSpecial    关注了我的专题(1:开，0:关)
 * @apiParam {string}  followCommunity   关注了我的社群(1:开，0:关)
 * @apiParam {string}  follow   关注我(1:开，0:关)
 * @apiParam {string}  commentOrReply   评论或回复我(1:开，0:关)
 * @apiGroup User
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "updateSwitches",
 *    "server_time": 1548037382973,
 *    "data": {
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/userJob/updateSwitches
 * @apiVersion 1.0.0
 */
router.post('/userJob/updateSwitches', siteFunc.checkUserSessionForApi, Switches.updateSwitches)


// 查询简单的文档列表
router.get('/content/getSimpleListByParams', (req, res, next) => {
  req.query.state = '2';
  next()
}, Content.getContents)

/**
 * @api {post} /api/v0/content/getContent 查询帖子详情
 * @apiDescription 查询帖子详情(只普通帖子或专题)
 * @apiName /api/v0/content/getContent
 * @apiGroup Contents
 * @apiParam {string} id 帖子Id
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 * {
 *  "status": 200,
 *  "message": "content",
 *  "server_time": 1545154926558,
 *  "data": {
 *     "_id": "ri5WaXugX",
 *     "title": "谷歌AI首席科学家：想当研究科学家，一事无成你受得了吗？",
 *     "stitle": "谷歌AI首席科学家：想当研究科学家，一事无成你受得了吗？",
 *     "sortPath": "",
 *     "keywords": "",
 *     "uAuthor": {
 *        "_id": "uUKsBv5y_",
 *        "userName": "master10",
 *        "logo": "https://cg2010studio.files.wordpress.com/2015/12/cartoonavatar2.jpg",
 *        "date": "2018-12-19 01:42:06",
 *        "id": "uUKsBv5y_"
 *     },
 *     "discription": "AI研究科学家不是那么好当的！近日谷歌AI首席科学家Vincent Vanhoucke发表在Medium上的文章引来众人关注。在本文中，他列举了成为研究科学家所要面对的9大挑战，看完这篇内容或许可以在立志投身于科学事业前，给你先“泼一盆冷水”。",
 *     "__v": 0,
 *     "author": {
 *        "userName": "doramart",
 *        "name": "超管",
 *        "logo": "/upload/smallimgs/img1448202744000.jpg"
 *     },
 *     "simpleComments": [
 *       {
 *        "type": "contents",
 *        "content": "做一名研究人员可能会让你的人生非常充实并得到他人的认可。但我知道很多学生在做研究时受到前景的压力，一时陷入工程的舒适区。他们通常把这个阶段视为个人失败，觉得自己“不够优秀”。而根据我个人的经验，这从来就不是个人价值或者天赋的问题：在研究中成长需要某种不同的气质，这种气质往往与工程师成长的原因有些矛盾。以下是我见过的研究人员在职业生涯的某个阶段不得不面对的一些主要压力：\n  \n做研究要解决的是有多个答案（或没有答案）的不适定问题\n                               *        "
 *       },
 *       {
 *        "type": "video",
 *        "content": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/1071402816293179392.mp4"
 *       },
 *       {
 *        "type": "contents",
 *        "content": " \n\n大学教育很大程度上教会了你如何用特定的方案解决适定问题，但用这种方式去对待研究却注定失败。你在研究中做的很多事并不会让你接近答案，而是让你更好地理解问题。 \n"
 *       },
 *       {
 *        "type": "image",
 *        "content": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/1075054319700676608.png"
 *       },
 *       {
 *        "type": "contents",
 *        "content": " \n用学到的东西，而不是取得的研究进展来衡量自己的进步，是一个人在研究环境中必须经历的重要范式转变之一。\n\n"
 *       }
 *     ],
 *     "likeNum": 0,
 *     "commentNum": 10,
 *     "clickNum": 77,
 *     "isTop": 1,
 *     "state": true,
 *     "updateDate": "2018-12-08 21:37:38",
 *     "date": "2018-12-08 21:37:38",
 *     "duration": "0:01",
 *     "videoArr": [
 *        "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/1071402816293179392.mp4"
 *     ],
 *     "imageArr": [
 *        "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/1075054319700676608.png"
 *     ],
 *     "appShowType": "3",
 *     "videoImg": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/1071402816293179392_thumbnail.jpg", // 视频缩略图（当appShowType=3）
 *     "sImg": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1544276235259.jpg",
 *     "tags": [
 *       {
 *        "_id": "aGE-YfyLRjx",
 *        "name": "化学",
 *        "date": "2018-12-19 01:42:06",
 *        "id": "aGE-YfyLRjx"
 *       },
 *       {
 *        "_id": "_euYIiOqvLA",
 *        "name": "体育",
 *        "date": "2018-12-19 01:42:06",
 *        "id": "_euYIiOqvLA"
 *       }
 *     ],
 *     "categories": [
 *       
 *     ],
 *     "type": "1",
 *     "id": "ri5WaXugX",
 *     "hasPraised": false,
 *     "hasReworded": false,
 *     "hasComment": true,
 *     "hasFavorite": false,
 *     "hasDespise": false,
 *     "total_reward_num": 0,
 *     "favoriteNum": 0,
 *     "despiseNum": 0
 *   }
 * }
 * @apiSampleRequest http://localhost:8884/api/v0/content/getContent
 * @apiVersion 1.0.0
 */
router.get('/content/getContent', siteFunc.checkUserLoginState, (req, res, next) => {
  req.query.state = '2';
  next()
}, Content.getOneContent)

// 获取我的文档
router.get('/content/getMyContent', siteFunc.checkUserSessionForApi, Content.getMyContent)

// 更新喜欢文档
router.get('/content/updateLikeNum', siteFunc.checkUserSessionForApi, Content.updateLikeNum)

// 添加或更新文章
router.post('/masterClass/addOne', siteFunc.checkUserSessionForApi, (req, res, next) => {
  next();
}, ClassContent.addClassContent)

router.post('/masterClass/updateOne', siteFunc.checkUserSessionForApi, (req, res, next) => {
  next();
}, ClassContent.updateClassContent)

router.post('/masterClass/updateClassVideo', siteFunc.checkUserSessionForApi, (req, res, next) => {
  next();
}, ClassContent.updateClassVideo)

router.get('/masterClass/delOne', siteFunc.checkUserSessionForApi, (req, res, next) => {
  next();
}, ClassContent.delClassContent)

router.post('/classInfo/addOne', siteFunc.checkUserSessionForApi, (req, res, next) => {
  next();
}, ClassInfo.addClassInfo)

router.post('/classInfo/updateOne', siteFunc.checkUserSessionForApi, (req, res, next) => {
  next();
}, ClassInfo.updateClassInfo)

router.get('/classInfo/delOne', siteFunc.checkUserSessionForApi, (req, res, next) => {
  next();
}, ClassInfo.delClassInfo)


/**
 * @api {post} /api/v0/content/addOne 投稿
 * @apiDescription 投稿(文章新增)
 * @apiName /content/addOne
 * @apiParam {string}  token 登录时返回的参数鉴权
 * @apiParam {string}  title   文档标题，必填
 * @apiParam {string}  discription  文档简介，必填
 * @apiParam {string}  sImg  文档首图(url)，必填
 * @apiParam {string}  type    文档类型，必填(1:普通，0:专题)
 * @apiParam {string}  draft   是否草稿(1:是，0:不是)
 * @apiParam {string}  tags   文档标签ID(目前只传1个)，必填
 * @apiParam {string}  keywords   关键字,非必填
 * @apiParam {string}  comments   文档详情(html 格式，必填)
 * @apiParam {string}  simpleComments   文档详情简约版(html 格式，客户端端传值和comments字段一样，必填)
 * @apiGroup Contents
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "addContent",
 *    "server_time": 1548037382973,
 *    "data": {
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/content/addOne
 * @apiVersion 1.0.0
 */
router.post('/content/addOne', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.role = 'user';
  next();
}, Content.addContent)

router.post('/content/updateOne', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.role = 'user';
  next();
}, Content.updateContent)

//文章二维码生成
router.get('/content/qrImg', (req, res, next) => {
  let detailLink = req.query.detailLink;
  try {
    let img = qr.image(detailLink, {
      size: 10
    });
    res.writeHead(200, {
      'Content-Type': 'image/png'
    });
    img.pipe(res);
  } catch (e) {
    res.writeHead(414, {
      'Content-Type': 'text/html'
    });
    res.end('<h1>414 Request-URI Too Large</h1>');
  }
});


// 获取类别列表
router.get('/contentCategory/getList', (req, res, next) => {
  req.query.enable = true;
  next()
}, ContentCategory.getContentCategories)

/**
 * @api {get} /api/v0/contentTag/getList 获取标签列表
 * @apiDescription 获取标签列表
 * @apiName /contentTag/getList
 * @apiGroup Contents
 * @apiParam {string} token 登录返回的token
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 * {
 *  "status": 200,
 *  "message": "communityTags",
 *  "server_time": 1542248521670,
 *  "data": [
 * {
 *      _id: "C41iXBDb5",
 *      name: "即时要闻",
 *      alias: "",
 *      comments: "即时要闻",
 *      __v: 0,
 *      date: "2019-01-04 16:54:41",
 *      id: "C41iXBDb5"
 * },
 * {
 *      _id: "C41iXBDb6",
 *      name: "测试",
 *      alias: "",
 *      comments: "测试",
 *      __v: 0,
 *      date: "2019-01-04 16:54:41",
 *      id: "C41iXBDb6"
 * },
 * {
 *      _id: "ZWlsRWWaL",
 *      name: "主笔专辑",
 *      alias: "",
 *      comments: "主笔专辑",
 *      __v: 0,
 *      date: "2019-01-04 16:54:35",
 *      id: "ZWlsRWWaL"
 * },
 * ...
 *  ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/contentTag/getList
 * @apiVersion 1.0.0
 */
router.get('/contentTag/getList', siteFunc.checkUserLoginState, (req, res, next) => {
  req.query.model = 'full';
  next()
}, ContentTag.getContentTags)

//--------------------------------------------------------------社群部分接口

/**
 * @api {get} /api/v0/communityTags/getList 查询社群标签列表
 * @apiDescription 获取全部社群标签
 * @apiName /communityTags/getList
 * @apiGroup Community
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 * {
 *  "status": 200,
 *  "message": "communityTags",
 *  "server_time": 1542248521670,
 *  "data": [
 *   {
 *     "_id": "2LuHVWM-n",
 *     "name": "摄影",
 *     "comments": "摄影",
 *     "__v": 0,
 *     "date": "2018-11-14 19:27:58",
 *     "sImg": "/upload/images/img20181115091608.png",
 *     "id": "2LuHVWM-n"
 *    },
 *   {
 *     "_id": "zst1J1bnp",
 *     "name": "区块链",
 *     "comments": "区块链",
 *     "__v": 0,
 *     "date": "2018-11-14 19:09:09",
 *     "sImg": "/upload/images/img20181114192548.jpeg",
 *     "id": "zst1J1bnp"
 *     "community_num": 0 // 社群数量
 *    }
 *  ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/communityTags/getList
 * @apiVersion 1.0.0
 */
router.get('/communityTags/getList', (req, res, next) => {
  req.query.model = 'full';
  next()
}, CommunityTag.getCommunityTags)

/**
 * @api {get} /api/v0/community/getCommunity 查询社群列表
 * @apiDescription 获取社群列表，带分页
 * @apiName /community/getCommunity
 * @apiGroup Community
 * @apiParam {string} tag 指定社群标签
 * @apiParam {string} creator 指定创建者ID
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} searchkey 搜索关键字
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "CommunityList",
 *  "server_time": 1542249299465,
 *  "data": [
 *   {
 *    "_id": "zDGsbThS_",
 *    "name": "糖果集",
 *    "comments": "糖果集",
 *    "open": "0",  // 1公开，0不公开
 *    "__v": 0,
 *    "users": [
 *     ],
 *    "tags": [
 *     {
 *        "_id": "zst1J1bnp",
 *        "name": "区块链",
 *        "date": "2018-11-15 10:34:59",
 *        "id": "zst1J1bnp"
 *       }
 *     ],
 *    "recommend": false,
 *    "date": "2018-11-15 09:48:24",
 *    "sImg": "/upload/images/img20181115094822.png",
 *    "id": "zDGsbThS_"
 *    "watch_num": 1, // 关注人数
 *    "had_watched": false, //当前用户是否已关注
 *    "content_num": 6 // 帖子总数
 *   },
 *   {
 *    "_id": "vcdfJGjqU",
 *    "name": "币币交易所",
 *    "comments": "币币交易所",
 *    "open": "0",  // 1公开，0不公开
 *    "__v": 0,
 *    "users": [
 *     ],
 *    "tags": [
 *       {
 *        "_id": "2LuHVWM-n",
 *        "name": "摄影",
 *        "date": "2018-11-15 10:34:59",
 *        "id": "2LuHVWM-n"
 *       },
 *       {
 *        "_id": "zst1J1bnp",
 *        "name": "区块链",
 *        "date": "2018-11-15 10:34:59",
 *        "id": "zst1J1bnp"
 *       }
 *     ],
 *    "questions": [
 *     {
 *       "question": "你喜欢的宠物11?",
 *       "answer": ""
 *     },
 *     {
 *       "question": "家乡是哪里3",
 *       "answer": ""
 *     }
 *    ],
 *    "needAuth": "0",
 *    "recommend": false,
 *    "date": "2018-11-15 09:23:16",
 *    "sImg": "/upload/images/img20181115092314.jpg",
 *    "id": "vcdfJGjqU"
 *    "watch_num": 1, // 关注人数
 *    "had_watched": false, //当前用户是否已关注
 *    "content_num": 6 // 帖子总数
 *    "total_rewards": 30.0 // 总收益
 *   }
 * ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/community/getCommunity
 * @apiVersion 1.0.0
 */
router.get('/community/getCommunity', siteFunc.checkUserLoginState, Community.getCommunity)

/**
 * @api {get} /api/v0/community/getHotCommunity 查询热门社群列表
 * @apiDescription 查询热门社群列表，带分页
 * @apiName /community/getHotCommunity
 * @apiGroup Community
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "CommunityList",
 *  "server_time": 1542249299465,
 *  "data": [
 *   {
 *    "_id": "zDGsbThS_",
 *    "name": "糖果集",
 *    "comments": "糖果集",
 *    "open": "0",  // 1公开，0不公开
 *    "__v": 0,
 *    "users": [
 *     ],
 *    "tags": [
 *     {
 *        "_id": "zst1J1bnp",
 *        "name": "区块链",
 *        "date": "2018-11-15 10:34:59",
 *        "id": "zst1J1bnp"
 *       }
 *     ],
 *    "recommend": false,
 *    "date": "2018-11-15 09:48:24",
 *    "sImg": "/upload/images/img20181115094822.png",
 *    "id": "zDGsbThS_"
 *    "watch_num": 1, // 关注人数
 *    "had_watched": false, //当前用户是否已关注
 *    "content_num": 6 // 帖子总数
 *   },
 *   {
 *    "_id": "vcdfJGjqU",
 *    "name": "币币交易所",
 *    "comments": "币币交易所",
 *    "open": "0",  // 1公开，0不公开
 *    "__v": 0,
 *    "users": [
 *     ],
 *    "tags": [
 *       {
 *        "_id": "2LuHVWM-n",
 *        "name": "摄影",
 *        "date": "2018-11-15 10:34:59",
 *        "id": "2LuHVWM-n"
 *       },
 *       {
 *        "_id": "zst1J1bnp",
 *        "name": "区块链",
 *        "date": "2018-11-15 10:34:59",
 *        "id": "zst1J1bnp"
 *       }
 *     ],
 *    "questions": [
 *     {
 *       "question": "你喜欢的宠物11?",
 *       "answer": ""
 *     },
 *     {
 *       "question": "家乡是哪里3",
 *       "answer": ""
 *     }
 *    ],
 *    "needAuth": "0",
 *    "recommend": false,
 *    "date": "2018-11-15 09:23:16",
 *    "sImg": "/upload/images/img20181115092314.jpg",
 *    "id": "vcdfJGjqU"
 *    "watch_num": 1, // 关注人数
 *    "had_watched": false, //当前用户是否已关注
 *    "content_num": 6 // 帖子总数
 *    "total_rewards": 30.0 // 总收益
 *   }
 * ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/community/getHotCommunity
 * @apiVersion 1.0.0
 */
router.get('/community/getHotCommunity', siteFunc.checkUserLoginState, Community.getHotCommunity)


/**
 * @api {get} /api/v0/community/getCommunityContents 查询指定社群下的帖子列表
 * @apiDescription 查询指定社群下的帖子列表，带分页
 * @apiName /community/getCommunityContents
 * @apiGroup Community
 * @apiParam {string} communityId 指定社群Id
 * @apiParam {string} type 0.普通帖子 1.公告 
 * @apiParam {string} sortby 1.默认按时间 2.点赞数
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} searchkey 搜索关键字
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "getCommunityContents",
 *  "server_time": 1545051564965,
 *  "data": [
 *    {
 *     "_id": "JG_Zi-tdt1",
 *     "user": {
 *     "_id": "zwwdJvLmP",
 *     "userName": "doramart",
 *     "date": "2018-12-17 20:59:24",
 *     "id": "zwwdJvLmP"
 *      },
 *     "community": {
 *     "_id": "zDGsbThS_",
 *     "name": "糖果集",
 *     "comments": "糖果集",
 *     "users": [
 *          
 *        ],
 *     "creator": "-FvK_J0lH",
 *     "__v": 0,
 *     "tags": [
 *          {
 *            "_id": "J9J8_8qYz0",
 *            "name": "科技",
 *            "comments": "科技",
 *            "__v": 0,
 *            "date": "2018-11-19 15:36:26",
 *            "sImg": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1544624013830.png",
 *            "id": "J9J8_8qYz0"
 *          }
 *        ],
 *     "recommend": false,
 *     "date": "2018-11-15 09:48:24",
 *     "sImg": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1544623358228.png",
 *     "id": "zDGsbThS_"
 *      },
 *      "likeNum": 0, // 点赞总数
 *      "commentNum": 0, // 留言总数
 *      "favoriteNum": 0, // 收藏总数
 *      "despiseNum": 0, // 踩帖总数
 *     "mediaArr": [
 *     {
 *        "_id": "5qBNe4r2MF",
 *        "type": "0",
 *        "link": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/1075054319700676608.png",
 *        "content": "YjaVyFEbT",
 *        "__v": 0,
 *        "date": "2018-12-21 16:19:46",
 *        "id": "5qBNe4r2MF"
 *     },
 *     ...
 *     ],
 *     "comments": "写一个“宅男拯救世界”的故事。我们先寻找一个最短的序列，可以包含  的所有排列。  的所有排列有下面六种：可以看到，  包含了以上所有的排列作为子列，并且不难验证，他是长度最短的拥有这个性质的序列。我们现在考虑对  这个序列问同样的问题，长度最短，并且包含所有  的各种排列的序列有多长呢？数学家把这种序列叫做“超排列”。如果我们仔细思考几分钟，不难发现一种很自然的归纳的构造方法：假设  个元素的超排列长度是  ，我们可以把这个序列“分拆”成若干个长度为  的子列，然后在其中“插入”第  个元素，我们可以得到一个新序列，包含  个元素的所有排列，而且长度只有  。\n\n\n\n",
 *     "__v": 0,
 *     "date": "2018-11-22 22:25:04",
 *     "state": true,
 *     "id": "JG_Zi-tdt1"
 *      "hasPraised": false // 当前用户是否已赞👍
 *      "total_reward_num": 0  // 打赏总额
 *      "hasReworded": false // 当前用户是否已打赏💰
 *      "hasComment": false // 当前用户是否已评论
 *      "hasFavorite": false // 当前用户是否已收藏
 *      "hasDespise": false // 当前用户是否已踩
 *    },
 *    ...
 *  ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/community/getCommunityContents
 * @apiVersion 1.0.0
 */
router.get('/community/getCommunityContents', siteFunc.checkUserLoginState, CommunityContent.getCommunityContents)



/**
 * @api {get} /api/v0/community/getAttentionCommunityContents 查询我关注的社群下的帖子列表
 * @apiDescription 查询我关注的社群下的帖子列表，带分页
 * @apiName /community/getMyAttentionCommunityContents
 * @apiGroup Community
 * @apiParam {string} token 登录鉴权参数
 * @apiParam {string} sortby 1.默认按时间 2.按热度
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} searchkey 搜索关键字
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "getMyAttentionCommunityContents",
 *  "server_time": 1545051564965,
 *  "data": [
 *    {
 *     "_id": "JG_Zi-tdt1",
 *     "user": {
 *     "_id": "zwwdJvLmP",
 *     "userName": "doramart",
 *     "date": "2018-12-17 20:59:24",
 *     "id": "zwwdJvLmP"
 *      },
 *     "community": {
 *     "_id": "zDGsbThS_",
 *     "name": "糖果集",
 *     "comments": "糖果集",
 *     "users": [
 *          
 *        ],
 *     "creator": "-FvK_J0lH",
 *     "__v": 0,
 *     "tags": [
 *          {
 *            "_id": "J9J8_8qYz0",
 *            "name": "科技",
 *            "comments": "科技",
 *            "__v": 0,
 *            "date": "2018-11-19 15:36:26",
 *            "sImg": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1544624013830.png",
 *            "id": "J9J8_8qYz0"
 *          }
 *        ],
 *     "recommend": false,
 *     "date": "2018-11-15 09:48:24",
 *     "sImg": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1544623358228.png",
 *     "id": "zDGsbThS_"
 *      },
 *      "likeNum": 0, // 点赞总数
 *      "commentNum": 0, // 留言总数
 *      "favoriteNum": 0, // 收藏总数
 *      "despiseNum": 0, // 踩帖总数
 *     "mediaArr": [
 *     {
 *        "_id": "5qBNe4r2MF",
 *        "type": "0",
 *        "link": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/1075054319700676608.png",
 *        "content": "YjaVyFEbT",
 *        "__v": 0,
 *        "date": "2018-12-21 16:19:46",
 *        "id": "5qBNe4r2MF"
 *     },
 *     ...
 *     ],
 *     "comments": "写一个“宅男拯救世界”的故事。我们先寻找一个最短的序列，可以包含  的所有排列。  的所有排列有下面六种：可以看到，  包含了以上所有的排列作为子列，并且不难验证，他是长度最短的拥有这个性质的序列。我们现在考虑对  这个序列问同样的问题，长度最短，并且包含所有  的各种排列的序列有多长呢？数学家把这种序列叫做“超排列”。如果我们仔细思考几分钟，不难发现一种很自然的归纳的构造方法：假设  个元素的超排列长度是  ，我们可以把这个序列“分拆”成若干个长度为  的子列，然后在其中“插入”第  个元素，我们可以得到一个新序列，包含  个元素的所有排列，而且长度只有  。\n\n\n\n",
 *     "__v": 0,
 *     "date": "2018-11-22 22:25:04",
 *     "state": true,
 *     "id": "JG_Zi-tdt1"
 *      "hasPraised": false // 当前用户是否已赞👍
 *      "total_reward_num": 0  // 打赏总额
 *      "hasReworded": false // 当前用户是否已打赏💰
 *      "hasComment": false // 当前用户是否已评论
 *      "hasFavorite": false // 当前用户是否已收藏
 *      "hasDespise": false // 当前用户是否已踩
 *    },
 *    ...
 *  ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/community/getAttentionCommunityContents
 * @apiVersion 1.0.0
 */
router.get('/community/getAttentionCommunityContents', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.attention = '1';
  next();
}, CommunityContent.getCommunityContents)


/**
 * @api {get} /api/v0/community/getOne 查询社群详情
 * @apiDescription 获取社群详情
 * @apiName /community/getOne
 * @apiGroup Community
 * @apiParam {string} id 社群id(eq:zDGsbThS_)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 * {
 *  "status": 200,
 *  "message": "get community success",
 *  "server_time": 1542247222802,
 *  "data": {
 *    "_id": "zDGsbThS_",
 *    "name": "糖果集",
 *    "comments": "糖果集",
 *    "__v": 0,
 *    "users": [
 *    ],
 *    "tags": [
 *      {
 *       "_id": "zst1J1bnp",
 *       "name": "区块链",
 *       "date": "2018-11-15 10:00:22",
 *       "id": "zst1J1bnp"
 *      }
 *    ],
 *    "recommend": false,
 *    "questions": [
 *     {
 *       "question": "你喜欢的宠物11?",
 *       "answer": ""
 *     },
 *     {
 *       "question": "家乡是哪里3",
 *       "answer": ""
 *     }
 *   ],
 *    "needAuth": "0",
 *    "date": "2018-11-15 09:48:24",
 *    "sImg": "/upload/images/img20181115094822.png",
 *    "id": "zDGsbThS_"
 *    "had_watched_success": false, //是否已成功加入
 *    "watch_num": 1,
 *    "had_watched": false, // 是否已申请加入
 *    "content_num": 0,
 *    "total_rewards": 0
 *  }
 * }
 * @apiSampleRequest http://localhost:8884/api/v0/community/getOne
 * @apiVersion 1.0.0
 */
router.get('/community/getOne', siteFunc.checkUserLoginState, (req, res, next) => {
  next()
}, Community.getOneCommunity)



/**
 * @api {post} /api/v0/community/createOne 创建社群
 * @apiDescription 创建社群
 * @apiName /community/createOne
 * @apiGroup Community
 * @apiParam {string} name 社群名称
 * @apiParam {string} sImg 社群图标
 * @apiParam {string} open 是否公开 1:公开，0:不公开
 * @apiParam {string} needAuth 是否公开 1:需要认证，0:不需要认证
 * @apiParam {string} questions 入群问题,不公开需要设置问题 ("[{"question":"你喜欢的宠物?","answer":"海豚"},{"question":"家乡是哪里","answer":"中国"}]")
 * @apiParam {string} tags 社群标签Id（可多选，','号隔开）
 * @apiParam {string} comments 社群备注
 * @apiParam {string} token 登录鉴权参数
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "创建成功",
 *  "server_time": 1542251075220,
 *  "data": {
 *    "id": "JTc9SRvCb"
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/community/createOne
 * @apiVersion 1.0.0
 */
router.post('/community/createOne', siteFunc.checkUserSessionForApi, (req, res, next) => {
  next()
}, Community.addCommunity)

/**
 * @api {post} /api/v0/community/updateOne 更新社群信息
 * @apiDescription 更新社群信息，不更新问题
 * @apiName /community/updateOne
 * @apiGroup Community
 * @apiParam {string} token 登录鉴权参数
 * @apiParam {string} id 社群id(eq:zDGsbThS_)
 * @apiParam {string} name 社群名称
 * @apiParam {string} sImg 社群图标
 * @apiParam {string} open 是否公开 1:公开，0:不公开
 * @apiParam {string} tags 社群标签Id（可多选，','号隔开）
 * @apiParam {string} comments 社群备注
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "更新成功",
 *  "server_time": 1542251075220,
 *  "data": {
 *    
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/community/updateOne
 * @apiVersion 1.0.0
 */
router.post('/community/updateOne', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.isApi = true;
  next();
}, Community.updateCommunity)

/**
 * @api {post} /api/v0/community/updateQuestions 更新社群问题
 * @apiDescription 更新社群问题
 * @apiName /community/updateQuestions
 * @apiGroup Community
 * @apiParam {string} token 登录鉴权参数
 * @apiParam {string} communityId 社群id(eq:zDGsbThS_)
 * @apiParam {string} questions 入群问题 ("[{"question":"你喜欢的宠物?","answer":"海豚"},{"question":"家乡是哪里","answer":"中国"}]")
 * @apiParam {string} needAuth 是否需要认证(1:是，0:否)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "更新成功",
 *  "server_time": 1542251075220,
 *  "data": {
 *    
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/community/updateQuestions
 * @apiVersion 1.0.0
 */
router.post('/community/updateQuestions', siteFunc.checkUserSessionForApi, (req, res, next) => {
  next();
}, Community.updateQuestions)



/**
 * @api {get} /api/v0/community/addUsers 加入社群
 * @apiDescription 加入社群（需要登录状态,后台会通过读取用户session获取用户ID）
 * @apiName /community/addUsers
 * @apiGroup Community
 * @apiParam {string} communityId 社群id(eq:zDGsbThS_)
 * @apiParam {string} questions 入群问题/答案 ("[{"question":"你喜欢的宠物?","answer":"海豚"},{"question":"家乡是哪里","answer":"中国"}]")
 * @apiParam {string} token 登录返回的token
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功",
 *  "server_time": 1542251075220,
 *  "data": {
 *    
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/community/addUsers
 * @apiVersion 1.0.0
 */
router.get('/community/addUsers', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.communityState = 'in';
  next()
}, User.addCommunity)

// router.get('/community/addUsersTest', User.addCommunityTest);
// router.get('/community/renderJoinTime', User.renderJoinTime);

/**
 * @api {get} /api/v0/community/delUsers 退出社群
 * @apiDescription 退出社群（需要登录状态,后台会通过读取用户session获取用户ID）
 * @apiName /community/delUsers
 * @apiGroup Community
 * @apiParam {string} token 登录返回的token
 * @apiParam {string} communityId 社群id(eq:zDGsbThS_)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功",
 *  "server_time": 1542251075220,
 *  "data": {
 *    
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/community/delUsers
 * @apiVersion 1.0.0
 */
router.get('/community/delUsers', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.communityState = 'out';
  next()
}, User.addCommunity);

/**
 * @api {get} /api/v0/community/kickOut 踢出社群
 * @apiDescription 踢出社群（需要登录状态,后台会通过读取用户session获取用户ID）
 * @apiName /community/kickOut
 * @apiGroup Community
 * @apiParam {string} token 登录返回的token
 * @apiParam {string} communityId 社群id(eq:zDGsbThS_)
 * @apiParam {string} userId 成员id(eq:zDGsbThS_)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功",
 *  "server_time": 1542251075220,
 *  "data": {
 *    
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/community/kickOut
 * @apiVersion 1.0.0
 */
router.get('/community/kickOut', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.communityState = 'out';
  next()
}, User.kickOutCommunity);

/**
 * @api {get} /api/v0/community/agreeJoin 同意加入社群
 * @apiDescription 同意加入社群（需要登录状态,后台会通过读取用户session获取用户ID）
 * @apiName /community/agreeJoin
 * @apiGroup Community
 * @apiParam {string} token 登录返回的token
 * @apiParam {string} communityId 社群id(eq:zDGsbThS_)
 * @apiParam {string} userId 成员id(eq:zDGsbThS_)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功",
 *  "server_time": 1542251075220,
 *  "data": {
 *    
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/community/agreeJoin
 * @apiVersion 1.0.0
 */
router.get('/community/agreeJoin', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.communityState = 'in';
  next()
}, User.kickOutCommunity);

/**
 * @api {get} /api/v0/community/refuseJoin 拒绝加入社群
 * @apiDescription 拒绝加入社群（需要登录状态,后台会通过读取用户session获取用户ID）
 * @apiName /community/refuseJoin
 * @apiGroup Community
 * @apiParam {string} token 登录返回的token
 * @apiParam {string} communityId 社群id(eq:zDGsbThS_)
 * @apiParam {string} userId 成员id(eq:zDGsbThS_)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功",
 *  "server_time": 1542251075220,
 *  "data": {
 *    
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/community/refuseJoin
 * @apiVersion 1.0.0
 */
router.get('/community/refuseJoin', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.communityState = 'refuse';
  next()
}, User.kickOutCommunity);




/**
 * @api {get} /api/v0/community/getUsers 获取社群成员列表
 * @apiDescription 获取社群成员列表
 * @apiName /community/getUsers
 * @apiGroup Community
 * @apiParam {string} communityId 社群id(eq:zDGsbThS_)
 * @apiParam {string} type 1:正式成员 0:待审核
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功 getCommunityUsers",
 *    "server_time": 1559205794450,
 *    "data": [
 *        {
 *            "_id": "x1y1tUgIJ",
 *            "userName": "uuDorac",
 *            "watchCommunityState": [
 *                {
 *                    "community": "vcdfJGjqU",
 *                    "state": "1", // 已加入
 *                    "accessDate": '2019-05-19 00:00:00' //通过审核时间
 *                },
 *                {
 *                    "community": "wNMRYvOQp",
 *                    "state": "2",  // 被拒绝
 *                    "refuseDate": '2019-05-19 00:00:00' //拒绝时间
 *                },
 *                {
 *                    "community": "jeal72AC_",
 *                    "state": "0",  // 未加入
 *                    "joinDate": '2019-05-19 00:00:00' // 申请时间
 *                },
 *                {
 *                    "community": "7tOSaTi-b",
 *                    "state": "1",
 *                    "accessDate": '2019-05-19 00:00:00' //通过审核时间
 *                }
 *            ],
 *            "authInfo": {
 *                "_id": "hAfxV3fRe",
 *                "name": "看看看",
 *                "updatetime": "2019-06-19 16:27:55",
 *                "date": "2019-06-19 16:27:55",
 *                "id": "hAfxV3fRe"
 *            }
 *            "lastCommunitySpeach": '2019-05-19 00:00:00' //最近发言时间
 *            "category": [],
 *            "gender": "0",
 *            "birth": "1770-01-01",
 *            "group": "1",
 *            "logo": "https://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1544242058736.png",
 *            "date": "2018-12-22 21:25:10",
 *            "introduction": "",
 *            "comments": "",
 *            "creativeRight": false,
 *            "enable": true,
 *            "id": "x1y1tUgIJ",
 *            "content_num": 1,
 *            "watch_num": 4,
 *            "follow_num": 0,
 *            "had_followed": false,
 *            "comments_num": 1,
 *            "favorites_num": 0,
 *            "total_reward_num": 0,
 *            "total_community_likeNum": 0, // 社群帖子点赞总数
 *            "total_communityContent_Num": 0, // 社群帖子总数
 *        }
 *        ...
 *    ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/community/getUsers
 * @apiVersion 1.0.0
 */
router.get('/community/getUsers', (req, res, next) => {
  next()
}, User.getCommunityUsers)



/**
 * @api {post} /api/v0/communityContent/add 发社群帖
 * @apiDescription 发社群帖（需要登录状态,后台会通过读取用户session获取用户ID）
 * @apiName /communityContent/add
 * @apiGroup Community
 * @apiParam {string} token 登录返回的token
 * @apiParam {string} comments 内容
 * @apiParam {string} type 0.普通帖子 1.公告
 * @apiParam {string} thumbnail 帖子缩略图
 * @apiParam {string} mediaArr 媒体集合，type：0图片，1视频 (JSON字符串格式："[{"type":"0","link":"http://....xx.jpg"},{"type":"1","link":"http://....xx.mp4","videoImg":"http://....xx.jpg"}]")
 * @apiParam {string} community 所在社群id(eq:zDGsbThS_)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "addCommunityContent",
 *    "server_time": 1545380386395,
 *    "data": {
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/communityContent/add
 * @apiVersion 1.0.0
 */
router.post('/communityContent/add', siteFunc.checkUserSessionForApi, CommunityContent.addCommunityContent)


/**
 * @api {get} /api/v0/communityContent/del 删除社群帖子
 * @apiDescription 删除社群帖子（需要登录状态,后台会通过读取用户session获取用户ID）
 * @apiName /communityContent/del
 * @apiGroup Community
 * @apiParam {string} token 登录返回的token
 * @apiParam {string} id 传帖子ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "delCommunityContent",
 *    "server_time": 1545380386395,
 *    "data": {
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/communityContent/del
 * @apiVersion 1.0.0
 */
router.get('/communityContent/del', siteFunc.checkUserSessionForApi, CommunityContent.delCommunityContent)

// pc端获取社群留言列表
router.get('/communityMessage/getList', siteFunc.checkUserLoginState, CommunityMessage.getCommunityMessages)

/**
 * @api {post} /api/v0/commnunityMessage/post 社群帖子评论/留言
 * @apiDescription 社群帖子评论/留言，需要登录态
 * @apiName /commnunityMessage/post
 * @apiGroup Community
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} contentId 社群帖子ID
 * @apiParam {string} replyAuthor 回复者ID (二级留言必填)
 * @apiParam {string} relationMsgId 回复目标留言ID (二级留言必填)
 * @apiParam {string} content 评论内容
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功！",
 *    "server_time": 1543372263586,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/commnunityMessage/postMessages
 * @apiVersion 1.0.0
 */
router.post('/commnunityMessage/post', siteFunc.checkUserSessionForApi, CommunityMessage.postCommunityMessages)


/**
 * @api {get} /api/v0/communityMessage/getAppList 获取社群评论列表，带二级留言
 * @apiDescription 获取社群评论列表,带分页
 * @apiName /communityMessage/getAppList
 * @apiGroup Community
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} contentId 社群帖子ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 * {
 *  "status": 200,
 *  "message": "操作成功 message",
 *  "server_time": 1545056507297,
 *  "data": [
 *    {
 *     "_id": "kdtUhIMSK",
 *     "contentId": {
 *         "_id": "ri5WaXugX",
 *         "title": "谷歌AI首席科学家：想当研究科学家，一事无成你受得了吗？",
 *         "stitle": "谷歌AI首席科学家：想当研究科学家，一事无成你受得了吗？",
 *         "updateDate": "2018-12-17 22:21:47",
 *         "date": "2018-12-17 22:21:47",
 *         "id": "ri5WaXugX"
 *      },
 *     "replyAuthor": "",
 *     "adminReplyAuthor": "",
 *     "relationMsgId": "",
 *     "author": {
 *         "_id": "zwwdJvLmP",
 *         "userName": "doramart",
 *         "logo": "https://cg2010studio.files.wordpress.com/2015/12/cartoonavatar2.jpg",
 *         "date": "2018-11-13 12:09:29",
 *         "enable": true,
 *         "id": "zwwdJvLmP"
 *      },
 *     "__v": 0,
 *     "content": "这是第四条评论",
 *     "hasPraise": false,
 *     "praiseNum": 0,
 *     "date": "2018-12-17 21:52:11",
 *     "utype": "0",
 *     "state": true,
 *     "id": "kdtUhIMSK",
 *     "childMessages": [
 *         {
 *            "_id": "PsMZx4G37",
 *            "contentId": {
 *                "_id": "ri5WaXugX",
 *                "title": "谷歌AI首席科学家：想当研究科学家，一事无成你受得了吗？",
 *                "stitle": "谷歌AI首席科学家：想当研究科学家，一事无成你受得了吗？",
 *                "updateDate": "2018-12-17 22:21:47",
 *                "date": "2018-12-17 22:21:47",
 *                "id": "ri5WaXugX"
 *              },
 *            "replyAuthor": "zwwdJvLmP",
 *            "adminReplyAuthor": "",
 *            "relationMsgId": "kdtUhIMSK",
 *            "author": {
 *            "_id": "zwwdJvLmP",
 *            "userName": "doramart",
 *            "logo": "https://cg2010studio.files.wordpress.com/2015/12/cartoonavatar2.jpg",
 *            "date": "2018-11-13 12:09:29",
 *            "enable": true,
 *            "id": "zwwdJvLmP"
 *              },
 *            "__v": 0,
 *            "content": "第四条评论的第6条回复",
 *            "hasPraise": false,
 *            "praiseNum": 0,
 *            "comment_num": 6,
 *            "date": "2018-12-17 21:54:04",
 *            "utype": "0",
 *            "state": true,
 *            "id": "PsMZx4G37"
 *         },
 *         ...
 *       ]
 *     },
 *     ...
 *   ]
 * }
 * @apiSampleRequest http://localhost:8884/api/v0/communityMessage/getAppList
 * @apiVersion 1.0.0
 */
router.get('/communityMessage/getAppList', siteFunc.checkUserLoginState, CommunityMessage.getAppCommunityMessages);


/**
 * @api {get} /api/v0/communityMessage/delete 删除社群评论
 * @apiDescription 删除社群评论，需要登录态
 * @apiName /communityMessage/delete
 * @apiGroup Community
 * @apiParam {string} ids 目标评论id，多个用英文逗号隔开
 * @apiParam {string} token 登录鉴权参数
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 * {
 *  "status": 200,
 *  "message": "操作成功 message",
 *  "server_time": 1545056507297,
 *  "data": [
 *   ]
 * }
 * @apiSampleRequest http://localhost:8884/api/v0/communityMessage/getAppList
 * @apiVersion 1.0.0
 */
router.get('/communityMessage/delete', siteFunc.checkUserLoginState, CommunityMessage.delCommunityMessage);



/**
 * @api {get} /api/v0/message/getList 获取帖子留言列表
 * @apiDescription 获取帖子留言列表,带分页
 * @apiName /message/getList
 * @apiGroup Contents
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} contentId 帖子ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功 message",
 *  "server_time": 1542899024811,
 *  "data": [
 *    {
 *  "_id": "tYVGV-HTL",
 *  "author": {
 *     "_id": "zwwdJvLmP",
 *     "userName": "doramart",
 *     "logo": "/upload/images/defaultlogo.png",
 *     "date": "2018-11-13 12:09:29",
 *     "enable": true,
 *     "id": "zwwdJvLmP"
 *   },
 *  "contentId": {
 *  "_id": "R8_iIMwF1",
 *  "title": "海底捞的致命缺点是什么？",
 *  "stitle": "海底捞的致命缺点是什么？",
 *  "updateDate": "2018-11-22",
 *  "date": "2018-11-22 23:03:44",
 *  "id": "R8_iIMwF1"
 *      },
 *  "__v": 0,
 *  "content": "这也是一条留言",
 *  "hasPraise": false,
 *  "praiseNum": 0,
 *  "date": "3 天前",
 *  "utype": "0",
 *  "id": "tYVGV-HTL"
 *    },
 *    {
 *  "_id": "4wv0tcLjH",
 *  "author": {
 *  "_id": "zwwdJvLmP",
 *  "userName": "doramart",
 *  "logo": "/upload/images/defaultlogo.png",
 *  "date": "2018-11-13 12:09:29",
 *  "enable": true,
 *  "id": "zwwdJvLmP"
 *      },
 *  "contentId": {
 *  "_id": "vGVoKV0g_",
 *  "title": "有哪一刹那让你对日本的美好印象瞬间破灭？",
 *  "stitle": "有哪一刹那让你对日本的美好印象瞬间破灭？",
 *  "updateDate": "2018-11-22",
 *  "date": "2018-11-22 23:03:44",
 *  "id": "vGVoKV0g_"
 *      },
 *  "__v": 0,
 *  "content": "这是一条留言",
 *  "hasPraise": false,
 *  "praiseNum": 0,
 *  "date": "3 天前",
 *  "utype": "0",
 *  "id": "4wv0tcLjH"
 *    }
 *  ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/message/getList
 * @apiVersion 1.0.0
 */
router.get('/message/getList', siteFunc.checkUserLoginState, Message.getMessages)


/**
 * @api {get} /api/v0/message/getAppList App获取留言列表，带二级留言
 * @apiDescription 获取帖子留言列表,带分页
 * @apiName /message/getAppList
 * @apiGroup Contents
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} contentId 帖子ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 * {
 *  "status": 200,
 *  "message": "操作成功 message",
 *  "server_time": 1545056507297,
 *  "data": [
 *    {
 *     "_id": "kdtUhIMSK",
 *     "contentId": {
 *         "_id": "ri5WaXugX",
 *         "title": "谷歌AI首席科学家：想当研究科学家，一事无成你受得了吗？",
 *         "stitle": "谷歌AI首席科学家：想当研究科学家，一事无成你受得了吗？",
 *         "updateDate": "2018-12-17 22:21:47",
 *         "date": "2018-12-17 22:21:47",
 *         "id": "ri5WaXugX"
 *      },
 *     "replyAuthor": "",
 *     "adminReplyAuthor": "",
 *     "relationMsgId": "",
 *     "author": {
 *         "_id": "zwwdJvLmP",
 *         "userName": "doramart",
 *         "logo": "https://cg2010studio.files.wordpress.com/2015/12/cartoonavatar2.jpg",
 *         "date": "2018-11-13 12:09:29",
 *         "enable": true,
 *         "id": "zwwdJvLmP"
 *      },
 *     "__v": 0,
 *     "content": "这是第四条评论",
 *     "hasPraise": false,
 *     "praiseNum": 0,
 *     "date": "2018-12-17 21:52:11",
 *     "utype": "0",
 *     "state": true,
 *     "id": "kdtUhIMSK",
 *     "childMessages": [
 *         {
 *            "_id": "PsMZx4G37",
 *            "contentId": {
 *                "_id": "ri5WaXugX",
 *                "title": "谷歌AI首席科学家：想当研究科学家，一事无成你受得了吗？",
 *                "stitle": "谷歌AI首席科学家：想当研究科学家，一事无成你受得了吗？",
 *                "updateDate": "2018-12-17 22:21:47",
 *                "date": "2018-12-17 22:21:47",
 *                "id": "ri5WaXugX"
 *              },
 *            "replyAuthor": "zwwdJvLmP",
 *            "adminReplyAuthor": "",
 *            "relationMsgId": "kdtUhIMSK",
 *            "author": {
 *            "_id": "zwwdJvLmP",
 *            "userName": "doramart",
 *            "logo": "https://cg2010studio.files.wordpress.com/2015/12/cartoonavatar2.jpg",
 *            "date": "2018-11-13 12:09:29",
 *            "enable": true,
 *            "id": "zwwdJvLmP"
 *              },
 *            "__v": 0,
 *            "content": "第四条评论的第6条回复",
 *            "hasPraise": false,
 *            "praiseNum": 0,
 *            "comment_num": 6,
 *            "date": "2018-12-17 21:54:04",
 *            "utype": "0",
 *            "state": true,
 *            "id": "PsMZx4G37"
 *         },
 *         ...
 *       ]
 *     },
 *     ...
 *   ]
 * }
 * @apiSampleRequest http://localhost:8884/api/v0/message/getAppList
 * @apiVersion 1.0.0
 */
router.get('/message/getAppList', siteFunc.checkUserLoginState, Message.getAppMessages);



// 获取系统配置信息
router.get('/systemConfig/getConfig', (req, res, next) => {
  req.query.model = 'simple';
  next()
}, SystemConfig.getSystemConfigs)

//---------------------------------------------------------广告相关api

/**
 * @api {get} /api/v0/ads/getOne 查找指定位置 banner
 * @apiDescription 根据ID获取广告(单图或轮播图),包含 发现、热门、推荐、精品、大师课顶部banner。注意：返回结果中如果 state=true 才显示
 * @apiName /ads/getOne
 * @apiGroup Ads
 * @apiParam {string} name 广告位名称（find/发现，hot/热门，recommend/推荐，boutique/精品，masterClass/大师课，videoRec/视频推荐页，audioRec/音频推荐页）
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *  {
 *      "status": 200,
 *      "message": "ads",
 *      "server_time": 1542116428394,
 *      "data": {
 *        "_id": "kYMDJLFDcb",
 *        "name": "masterClass",
 *        "comments": "大师课顶部轮播图",
 *        "__v": 0,
 *        "items": [
 *          {
 *            "_id": "MXoW1aBxk",
 *            "title": "",
 *            "link": "https://masterchain.work",
 *            "appLink": "MXoW1aBxk", // app链接（文章id或普通url）
 *            "appLinkType": "0", // app链接形式 0 文章，1 普通链接
 *            "width": null,
 *            "alt": "发现ads1",
 *            "sImg": "/upload/images/img20181113213438.jpg",
 *            "__v": 0,
 *            "date": "2018-11-13T13:34:56.988Z",
 *            "target": "_blank",
 *            "height": null
 *          },
 *          {
 *            "_id": "h8YuPIf6Aj",
 *            "title": "",
 *            "link": "https://masterchain.work",
 *            "appLink": "MXoW1aBxk", // app链接（文章id或普通url）
 *            "appLinkType": "0", // app链接形式 0 文章，1 普通链接
 *            "width": null,
 *            "alt": "发现ads2",
 *            "sImg": "/upload/images/img20181113213454.jpg",
 *            "__v": 0,
 *            "date": "2018-11-13T13:34:56.991Z",
 *            "target": "_blank",
 *            "height": null
 *          }
 *        ],
 *        "date": "2018-11-13 21:34:56",
 *        "height": null,
 *        "state": true,
 *        "carousel": true,
 *        "type": "1",
 *        "id": "kYMDJLFDcb"
 *      }
 *  }
 * @apiSampleRequest http://localhost:8884/api/v0/ads/getOne?name=masterClass
 * @apiVersion 1.0.0
 */
router.get('/ads/getOne', (req, res, next) => {
  req.query.state = true;
  next()
}, Ads.getOneAd)

// 获取可见的所有广告信息
router.get('/ads/getAll', (req, res, next) => {
  req.query.state = true;
  next()
}, Ads.getAds)


/**
 * @api {get} /api/v0/siteMessage/getList 获取用户消息列表
 * @apiDescription 获取用户消息列表，带分页，包含 赞赏、关注和评论 文章驳回等 消息，需要登录态
 * @apiName /siteMessage/getList
 * @apiGroup Messages
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} type 消息类别(1、赞赏 ，2、关注 ，3、评论或回复，4、点赞 5、社群评论与回复 6、文章驳回)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "getSiteMessages",
 *    "server_time": 1543917647397,
 *    "data": [
 *        {
 *            "_id": "2hl6nHtY0",
 *            "type": "2",
 *            "activeUser": {
 *               "_id": "zwwdJvLmP",
 *               "userName": "doramart",
 *               "phoneNum": 15220064294,
 *               "category": [],
 *               "group": "0",
 *               "logo": "/upload/images/defaultlogo.png",
 *               "date": "2018-11-13 12:09:29",
 *               "comments": "",
 *               "enable": true,
 *               "id": "zwwdJvLmP",
 *               "content_num": 5,
 *               "watch_num": 1,
 *               "follow_num": 0,
 *               "had_followed": false
 *            },
 *            "passiveUser": {
 *               "_id": "yNYHEw3-e",
 *               "userName": "yoooyu",
 *               "category": [
 *                  "yOgDdV8_b"
 *                ],
 *               "group": "1",
 *               "logo": "/upload/images/defaultlogo.png",
 *               "date": "2018-11-17 09:59:52",
 *               "enable": true,
 *               "id": "yNYHEw3-e"
 *            },
 *              "message": {
 *                "_id": "ClvqfXbjw",
 *                "contentId": {
 *                    "_id": "HBFOrbWYz",
 *                    "title": "2018年，你一定被这十首歌逼疯过",
 *                    "date": "2018-12-25 21:50:57",
 *                    "updateDate": "2019-01-01 14:56:34",
 *                    "id": "HBFOrbWYz"
 *                },
 *                "content": "多重评论再一次",
 *                "date": "2019-01-01 14:56:34",
 *                "id": "ClvqfXbjw"
 *              },
 *            "__v": 0,
 *            "isRead": false,
 *            "date": "2018-11-18 16:11:26",
 *            "id": "2hl6nHtY0"
 *        },
 *         以下是系统消息返回结果（文章驳回 type = 6）
 *        {
 *            "_id": "bSof2jyRa",
 *            "type": "6",
 *            "passiveUser": {
 *                 "_id": "TMCRnMWru",
 *                 "userName": "Kevin",
 *                 "category": [],
 *                 "group": "1",
 *                 "logo": "https://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1546577523237.jpeg",
 *                 "date": "2019-01-04 12:51:31",
 *                 "enable": true,
 *                 "birth": "2019-05-24",
 *                 "id": "TMCRnMWru"
 *            },
 *            "activeAdminUser": { // 因为系统消息是管理员触发的，这里只返回了管理员名，客户端可以直接显示 系统管理员 ，建议不显示用户名
 *                 "_id": "B1OTQRbOW",
 *                 "userName": "doracms"
 *            },
 *            "content": {
 *                 "_id": "F_3IaAMg_",
 *                 "title": "標標本",
 *                 "dismissReason": "你的话太多了1",
 *                 "updateDate": "2019-05-24 10:45:13",
 *                 "date": "2019-05-24 10:45:13",
 *                 "id": "F_3IaAMg_"
 *            },
 *            "__v": 0,
 *            "isRead": false,
 *            "date": "2019-05-24 10:34:47",
 *            "id": "bSof2jyRa"
 *        }
 *    ]
 * }
 * @apiSampleRequest http://localhost:8884/api/v0/siteMessage/getList
 * @apiVersion 1.0.0
 */
router.get('/siteMessage/getList', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.user = req.session.user._id;
  next()
}, SiteMessage.getSiteMessages);



/**
 * @api {get} /api/v0/siteMessage/setHasRead 设置消息为已读
 * @apiDescription 设置消息为已读，包含 赞赏、关注和评论消息；需要登录态
 * @apiName /siteMessage/setHasRead
 * @apiGroup Messages
 * @apiParam {string} ids 消息id,多个id用逗号隔开;传 ids=all 设置所有消息为已读
 * @apiParam {string} type 当ids为all时，传该参数，指定全部设置已读的消息类别
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "设置已读成功",
 *    "server_time": 1542529985218,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/siteMessage/setHasRead?ids=la6aj0hd7
 * @apiVersion 1.0.0
 */
router.get('/siteMessage/setHasRead', siteFunc.checkUserSessionForApi, SiteMessage.setMessageHasRead)


/**
 * @api {get} /api/v0/siteMessage/getSiteMessageOutline 获取私信概要
 * @apiDescription 获取私信概要(私信内容，关注、赞赏、评论、系统消息 未读数量和第一条记录)
 * @apiName /siteMessage/getSiteMessageOutline
 * @apiGroup Messages
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "getSiteMessageOutline",
 *  "server_time": 1543904194731,
 *  "data": {
 *    "first_privateLetter": {
 *    "_id": "yvOk_g1y71",
 *    "user": "yNYHEw3-e",
 *    "notify": {   // 私信的具体内容
 *        "_id": "5mCBWXS-B",
 *        "title": "这是一条私信，不要偷偷打开哟。。。",
 *        "content": "<p>这是一条私信，不要偷偷打开哟。。。</p>",
 *        "adminSender": "4JiWCMhzg",
 *        "type": "1",
 *        "__v": 0,
 *        "date": "2018-11-18 16:13:47",
 *        "id": "5mCBWXS-B"
 *      },
 *    "__v": 0,
 *    "date": "2018-11-18 16:13:47",
 *    "isRead": false,
 *    "id": "yvOk_g1y71"
 *    },
 *    "private_no_read_num": 1,
 *    "no_read_good_num": 1,
 *    "first_good_message": { // 被点赞的第一条消息，客户端可以根据 activeUser 组装消息 "activeUser.userName 给你点赞了"
 *         "_id": "Ogg7mFnjS",
 *         "type": "1",
 *         "activeUser": {
 *             "_id": "zwwdJvLmP",
 *             "userName": "doramart",
 *             "date": "2018-12-04 14:16:34",
 *             "id": "zwwdJvLmP"
 *           },
 *         "passiveUser": "yNYHEw3-e",
 *         "content": "Y1XFYKL52",
 *         "__v": 0,
 *         "isRead": false,
 *         "date": "2018-11-18 16:10:44",
 *         "id": "Ogg7mFnjS"
 *    },
 *    "no_read_follow_num": 1,
 *    "first_follow_message": { // 被关注的第一条消息，客户端可以根据 activeUser 组装消息 "activeUser.userName 关注了你"
 *         "_id": "2hl6nHtY0",
 *         "type": "2",
 *         "activeUser": {
 *             "_id": "zwwdJvLmP",
 *             "userName": "doramart",
 *             "date": "2018-12-04 14:16:34",
 *             "id": "zwwdJvLmP"
 *           },
 *         "passiveUser": "yNYHEw3-e",
 *         "__v": 0,
 *         "isRead": false,
 *         "date": "2018-11-18 16:11:26",
 *         "id": "2hl6nHtY0"
 *    },
 *    "no_read_comment_num": 1,
 *    "first_comment_message": { // 评论的第一条消息，客户端可以根据 activeUser 组装消息 "activeUser.userName 给你评论了"
 *         "_id": "iKGCu4EJj",
 *         "type": "3",
 *         "activeUser": {
 *             "_id": "zwwdJvLmP",
 *             "userName": "doramart",
 *             "date": "2018-12-04 14:16:34",
 *             "id": "zwwdJvLmP"
 *           },
 *         "passiveUser": "yNYHEw3-e",
 *         "content": "Y1XFYKL52",
 *         "__v": 0,
 *         "isRead": false,
 *         "date": "2018-11-18 16:11:21",
 *         "id": "iKGCu4EJj"
 *    },
 *    "no_read_system_num": 1,
 *    "first_system_message": { // 系统消息（约定type>5的都为系统消息），客户端可以根据 activeUser 组装消息 "activeAdminUser.userName 驳回了你的文章"
 *         "_id": "iKGCu4EJj",
 *         "type": "6",
 *         "activeUser": {
 *             "_id": "zwwdJvLmP",
 *             "userName": "doramart",
 *             "date": "2018-12-04 14:16:34",
 *             "id": "zwwdJvLmP"
 *           },
 *         "passiveUser": "yNYHEw3-e",
 *         "content": "Y1XFYKL52",
 *         "__v": 0,
 *         "isRead": false,
 *         "date": "2018-11-18 16:11:21",
 *         "id": "iKGCu4EJj"
 *    } 
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/siteMessage/getSiteMessageOutline?token=xxx
 * @apiVersion 1.0.0
 */
router.get('/siteMessage/getSiteMessageOutline', siteFunc.checkUserSessionForApi, SiteMessage.getSiteMessageOutline)


/**
 * @api {get} /api/v0/masterClass/getClassType 获取大师课类别列表
 * @apiDescription 获取大师课类别列表
 * @apiName /masterClass/getClassType
 * @apiGroup MasterClass
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "getClassType",
 *    "server_time": 1542703718431,
 *    "data": [
 *     {
 *     _id: "SLVcgEtEWl",
 *     name: "商学院",
 *     comments: "商学院",
 *     __v: 0,
 *     date: "2018-11-19 15:20:12",
 *     id: "SLVcgEtEWl"
 *     },
 *     ...
 * ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/masterClass/getClassType
 * @apiVersion 1.0.0
 */
router.get('/masterClass/getClassType', siteFunc.checkUserLoginState, ClassType.getClassTypes);


/**
 * @api {get} /api/v0/masterClass/getClassInfo 获取某课程信息
 * @apiDescription 获取某课程信息
 * @apiName /masterClass/getClassInfo
 * @apiGroup MasterClass
 * @apiParam {string} classId 课程ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "getClassInfos",
 *    "server_time": 1542703718431,
 *    "data":{
 *            "_id": "9cGIP42Mo",
 *            "name": "7 天走进咖啡生活美学",
 *            "author": "Se310wTPO",
 *            "comments": "这是一门给咖啡爱好者的咖啡品选课；这是一门提升审美品味的咖啡美学课；这是一门让生活「慢」下来的修心课。在这里，你可以用咖啡肆意品鉴生活，掌握圈内人必知的咖啡知识，用全新的生活态度与方式理解咖啡之美。",
 *            "__v": 0,
 *            "categories": [
 *                {
 *                 "_id": "ArRhAS1Y_z",
 *                 "name": "人文艺术",
 *                 "date": "2018-11-20 16:48:38",
 *                 "id": "ArRhAS1Y_z"
 *                }
 *            ],
 *            "date": "2018-11-19 16:37:46",
 *            "price": 399,
 *            "sImg": "/upload/images/defaultImg.jpg" // 封面图
 *            "vipPrice": 199,
 *            "unit": "MEC", // 单位
 *            "discount": false,
 *            "priceType": '0', // 0 免费 1 会员免费 2会员付费
 *            "boutique": false,
 *            "type": "0", // 课程类型 0 文字，1、视频，2、音频
 *            "average": 9.8 //平均分
 *            "catalog_num": 10 //课程总章数
 *            "record_num": 300 // 课程订阅/购买/已学 数
 *            "update_catalog_num": 3 // 已更新章节数
 *            "hadReadChapters": 3 // 本人已读章节数（需要传token）
 *            "hadPayClass": false // 本人是否已订阅（需要传token）
 *            "hadComment": false // 本人是否已评论（需要传token）
 *            "recommend": false,
 *            "id": "9cGIP42Mo"
 *        }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/masterClass/getClassInfo?classId=9cGIP42Mo
 * @apiVersion 1.0.0
 */
router.get('/masterClass/getClassInfo', siteFunc.checkUserLoginState, ClassInfo.getOneClassInfo);

/**
 * @api {get} /api/v0/masterClass/getCatalogs 获取课程目录列表
 * @apiDescription 获取课程目录列表
 * @apiName /masterClass/getCatalogs
 * @apiGroup MasterClass
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} searchkey 搜索目录关键字
 * @apiParam {string} classId 课程ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "getClassContents",
 *  "server_time": 1545704279191,
 *  "data": [
 *   {
 *    "_id": "P32d2j_y2",
 *    "free": false,
 *    "audioArr": [],
 *    "videoArr": [
 *       "eece2b7c748f48e4af128910f460f3eb"
 *    ],
 *    "imageArr": [
 *       "http://outin-61822016f93911e8ac9100163e06123c.oss-cn-shanghai.aliyuncs.com/eece2b7c748f48e4af128910f460f3eb/snapshots/daa4718c54cf4740b46b097813453362-00004.jpg?Expires=1546701658&OSSAccessKeyId=LTAIx2xjxmIyFov4&Signature=4kWAUbOfK2bRBRl5S12mV8J59zI%3D"
 *    ],
 *    "date": "2018-12-28 17:49:09",
 *    "size": "19235908",
 *    "duration": "50.8799",
 *    "catalog": "视频第一章",
 *    "catalogIndex": "01",
 *    "id": "P32d2j_y2"
 *   }
 * ...
 *  ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/masterClass/getCatalogs?classId
 * @apiVersion 1.0.0
 */
router.get('/masterClass/getCatalogs', siteFunc.checkUserLoginState, (req, res, next) => {
  req.query.state = '2';
  next();
}, ClassContent.getClassContents);


/**
 * @api {get} /api/v0/masterClass/getList 大师课列表
 * @apiDescription 大师课首页课程列表，带分页
 * @apiName /masterClass/getList
 * @apiGroup MasterClass
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} searchkey 搜索课程关键字
 * @apiParam {string} type 课程类型（1/推荐课程，2/vip专享，3/优惠转，4/精品）
 * @apiParam {string} category 课程类别ID
 * @apiParam {string} creator 查询指定用户开设的课程列表，可传此参数
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "getClassInfos",
 *    "server_time": 1542703718431,
 *    "data": [
 *        {
 *            "_id": "9cGIP42Mo",
 *            "name": "7 天走进咖啡生活美学",
 *            "author": "Se310wTPO",
 *            "comments": "这是一门给咖啡爱好者的咖啡品选课；这是一门提升审美品味的咖啡美学课；这是一门让生活「慢」下来的修心课。在这里，你可以用咖啡肆意品鉴生活，掌握圈内人必知的咖啡知识，用全新的生活态度与方式理解咖啡之美。",
 *            "__v": 0,
 *            "categories": [
 *                {
 *                 "_id": "ArRhAS1Y_z",
 *                 "name": "人文艺术",
 *                 "date": "2018-11-20 16:48:38",
 *                 "id": "ArRhAS1Y_z"
 *                }
 *            ],
 *            "date": "2018-11-19 16:37:46",
 *            "price": 399,
 *            "sImg": "/upload/images/defaultImg.jpg" // 封面图
 *            "vipPrice": 199,
 *            "unit": "MEC", // 单位
 *            "discount": false,
 *            "priceType": '0', // 0 免费 1 会员免费 2会员付费
 *            "boutique": false,
 *            "type": "0", // 课程类型 0 文字，1、视频，2、音频
 *            "average": 9.8 //平均分
 *            "catalog_num": 10 //课程总章数
 *            "record_num": 300 // 课程订阅/购买/已学 数
 *            "update_catalog_num": 3 // 已更新章节数
 *            "hadReadChapters": 3 // 本人已读章节数（需要传token）
 *            "hadPayClass": false // 本人是否已订阅（需要传token）
 *            "hadComment": false // 本人是否已评论（需要传token）
 *            "recommend": false,
 *            "id": "9cGIP42Mo"
 *        },
 *        ...
 *    ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/masterClass/getList?type=1
 * @apiVersion 1.0.0
 */
router.get('/masterClass/getList', siteFunc.checkUserLoginState, (req, res, next) => {
  req.query.state = '2';
  next()
}, ClassInfo.getClassInfos);


/**
 * @api {get} /api/v0/masterClass/getClass 大师课课程内容(详情)
 * @apiDescription 大师课课程内容,返回结果中包含了课程目录
 * @apiName /masterClass/getClass
 * @apiGroup MasterClass
 * @apiParam {string} id 课程id (eq:{id:'gKwEjAGSG'})
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "get data success",
 *    "server_time": 1542702559500,
 *    "data": {
 *        "_id": "gKwEjAGSG",
 *        "catalog": "[第一天]7 天走进咖啡生活美学",
 *        "duration": "100",
 *        "basicInfo": {
 *            "_id": "9cGIP42Mo",
 *            "name": "7 天走进咖啡生活美学",
 *            "author": "Se310wTPO",
 *            "comments": "这是一门给咖啡爱好者的咖啡品选课；这是一门提升审美品味的咖啡美学课；这是一门让生活「慢」下来的修心课。在这里，你可以用咖啡肆意品鉴生活，掌握圈内人必知的咖啡知识，用全新的生活态度与方式理解咖啡之美。",
 *            "__v": 0,
 *            "categories": [
 *                {
 *                 "_id": "ArRhAS1Y_z",
 *                 "name": "人文艺术",
 *                 "comments": "人文艺术",
 *                 "__v": 0,
 *                 "date": "2018-11-19 15:20:12",
 *                 "id": "ArRhAS1Y_z"
 *                }
 *            ],
 *            "date": "2018-11-19 16:37:46",
 *            "price": 399,
 *            "sImg": "/upload/images/defaultImg.jpg" // 封面图
 *            "vipPrice": 199,
 *            "unit": "MEC", // 单位
 *            "discount": false,
 *            "vip": false,
 *            "recommend": false,
 *            "id": "9cGIP42Mo"
 *        },
 *        "simpleComments": [
 *        {
 *          "type": "contents",
 *          "content": "写一个“宅男拯救世界”的故事。我们先寻找一个最短的序列，可以包含  的所有排列。  的所有排列有下面六种：可以看到，  包含了以上所有的排列作为子列，并且不难验证，\n"
 *        },
 *        {
 *          "type": "image",
 *          "content": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/1077205740567007232.jpg"
 *        },
 *        {
 *          "type": "contents",
 *          "content": "\n他是长度最短的拥有这个性质的序列。我们现在考虑对  这个序列问同样的问题，长度最短，并且包含所有  的各种排列的序列有多长呢？数学家把这种序列叫做“超排列”。如果我们仔细思考几分钟，不难发现一种很自然的归纳的构造方法：假设  个元素的超排列长度是  ，我们可以把这个序列“分拆”成若干个长度为  的子列，然后在其中“插入”第  个元素，我们可以得到一个新序列，包含  个元素的所有排列，而且长度只有  。 \n"
 *        }
 *        ],
 *        "author": {
 *            "_id": "Se310wTPO",
 *            "userName": "master6",
 *            "date": "2018-11-20 16:29:19",
 *            "id": "Se310wTPO"
 *        },
 *        "__v": 0,
 *        "date": "2018-11-19 17:41:00",
 *        "id": "gKwEjAGSG"
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/masterClass/getClass
 * @apiVersion 1.0.0
 */
router.get('/masterClass/getClass', siteFunc.checkUserLoginState, ClassContent.getOneClassByParams);


/**
 * @api {get} /api/v0/masterClass/getFeedback 大师课课程反馈
 * @apiDescription 大师课课程反馈,带分页
 * @apiName /masterClass/getFeedback
 * @apiGroup MasterClass
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} classId 课程id
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "操作成功 ClassFeedBack",
 *    "server_time": 1542705243187,
 *    "data": [
 *        {
 *            "_id": "EAiaItyfM",
 *            "classContent": "gKwEjAGSG",
 *            "author": {
 *               "_id": "d_K_O6UIV",
 *               "userName": "master1",
 *               "logo": "/upload/images/defaultlogo.png",
 *               "date": "2018-11-18 20:23:50",
 *               "enable": true,
 *               "id": "d_K_O6UIV"
 *            },
 *            "__v": 0,
 *            "content": "这个课程很不错，老师在线指导，效果很好",
 *            "hasPraise": false,
 *            "praiseNum": 0,
 *            "date": "2 小时前",
 *            "id": "EAiaItyfM"
 *        },
 *        {
 *            "_id": "GnXZT8S_tP",
 *            "classContent": "gKwEjAGSG",
 *            "author": {
 *               "_id": "d_K_O6UIV",
 *               "userName": "master1",
 *               "logo": "/upload/images/defaultlogo.png",
 *               "date": "2018-11-18 20:23:50",
 *               "enable": true,
 *               "id": "d_K_O6UIV"
 *            },
 *            "__v": 0,
 *            "content": "这个课程很不错，老师在线指导，效果很好",
 *            "hasPraise": false,
 *            "praiseNum": 0,
 *            "date": "2 小时前",
 *            "id": "GnXZT8S_tP"
 *        },
 *    ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/masterClass/getFeedback?classId=LSMIgAExV
 * @apiVersion 1.0.0
 */
router.get('/masterClass/getFeedback', ClassScore.getClassScores);

/**
 * @api {post} /api/v0/masterClass/addScore 大师课课程评分
 * @apiDescription 大师课课程评分,需要登录态
 * @apiName /masterClass/addScore
 * @apiGroup MasterClass
 * @apiParam {string} token 登录返回的鉴权参数
 * @apiParam {float} score 分数
 * @apiParam {string} comments 评论
 * @apiParam {string} class 评分的课程ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": "ClassScore",
 *    "message": {
 *      "id": "2gvCfZwRd"
 *    },
 *    "server_time": 1543065038296,
 *    "data": "save"
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/masterClass/addScore
 * @apiVersion 1.0.0
 */
router.post('/masterClass/addScore', siteFunc.checkUserSessionForApi, ClassScore.addClassScore);

/**
 * @api {post} /api/v0/masterClass/subClass 课程订阅
 * @apiDescription 课程订阅
 * @apiName /masterClass/subClass
 * @apiGroup MasterClass
 * @apiParam {string} class 课程ID
 * @apiParam {string} password 资金密码
 * @apiParam {string} comments 备注(选填)
 * @apiParam {string} token 登录返回的鉴权参数
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": "ClassScore",
 *    "message": {
 *      "id": "2gvCfZwRd",
 *      "average": 4
 *    },
 *    "server_time": 1543065038296,
 *    "data": "save"
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/masterClass/subClass
 * @apiVersion 1.0.0
 */
router.post('/masterClass/subClass', siteFunc.checkUserSessionForApi, ClassPayRecord.addClassPayRecord);


/**
 * @api {post} /api/v0/masterClass/addClassToStudy 加入学习
 * @apiDescription 加入学习
 * @apiName /masterClass/addClassToStudy
 * @apiGroup MasterClass
 * @apiParam {string} class 课程ID
 * @apiParam {string} token 登录返回的鉴权参数
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": "ClassPayRecord",
 *    "message": {
 *    },
 *    "server_time": 1543065038296,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/masterClass/addClassToStudy
 * @apiVersion 1.0.0
 */
router.post('/masterClass/addClassToStudy', siteFunc.checkUserSessionForApi, ClassPayRecord.addClassToStudy);


/**
 * @api {get} /api/v0/masterClass/getMyOrderClass 获取我订阅的课程列表
 * @apiDescription 获取我订阅的课程列表,带分页,需要登录态
 * @apiName /masterClass/getMyOrderClass
 * @apiGroup MasterClass
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} token 登录返回的鉴权参数
 * @apiParam {string} userId 获取指定用户学习的课程，传此参数
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "getClassPayRecords",
 *  "server_time": 1543475998905,
 *  "data": [
 *    {
 *      "_id": "2Zy-uzwkH",
 *      "class": "9cGIP42Mo",
 *      "user": {
 *      "_id": "zwwdJvLmP",
 *      "userName": "doramart",
 *      "date": "2018-11-29 15:19:58",
 *      "id": "zwwdJvLmP"
 *      },
 *      "comments": "这是我购买的第4个课程",
 *      "__v": 0,
 *      "learningProgress": [
 *        
 *      ],
 *      "state": true,
 *      "date": "2018-11-29 14:52:53",
 *      "uuid": "f542b000f3a211e88b97194f2a416fd4",
 *      "money": 199,
 *      "id": "2Zy-uzwkH"
 *    },
 *    ...
 *  ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/masterClass/getMyOrderClass
 * @apiVersion 1.0.0
 */
router.get('/masterClass/getMyOrderClass', siteFunc.checkUserSessionForApi, ClassPayRecord.getClassPayRecords);


/**
 * @api {get} /api/v0/wallet/getWalletBasicInfo 获取钱包首页基础信息
 * @apiDescription 获取钱包首页基础信息，MEC,MVPC,MBT 是否设置了自己账号等；需要登录态
 * @apiName /wallet/getWalletBasicInfo
 * @apiGroup Wallet
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功 getWalletBasicInfo",
 *  "server_time": 1543484196749,
 *  "data": {
 *     "total_MEC_num": 0.1,  // MEC金额
 *     "total_MEC_unit": "MEC",  // MEC单位
 *     "total_MVPC_num": 0,  // MVPC金额
 *     "total_MVPC_unit": "MVPC", // MVPC单位
 *     "total_MBT_num": 0,  // MBT金额
 *     "total_MBT_unit": "MBT", // MBT单位
 *     "hasSetFundPassword": false // 是否已设置支付密码
 *     "authState": false // 是否已身份认证
 *  }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/wallet/getWalletBasicInfo?token=xxx
 * @apiVersion 1.0.0
 */
router.get('/wallet/getWalletBasicInfo', siteFunc.checkUserSessionForApi, BillRecord.getWalletBasicInfo)



/**
 * @api {get} /api/v0/wallet/getMyBill 获取收支明细
 * @apiDescription 获取收支明细；需要登录态
 * @apiName /wallet/getMyBill
 * @apiGroup Wallet
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} billType 账单类型(收入：1，支出：2, 充值：3，提现：4)
 * @apiParam {string} time 时间段(可传：d7/7天,d30/30天,d60/60天,d180/半年,d365/一年,all/全部)
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 * {
 * "status": 200,
 * "message": "getBillRecords",
 * "server_time": 1543321730225,
 * "data": {
 *    "billRecords": [
 *       {
 *      "_id": "MFUAktOIO9",
 *      "state": "0", // 针对提币的状态 0:待审核 1:审核通过 2:审核不通过
 *      "logs": "浏览",
 *      "user": {
 *         "_id": "d_K_O6UIV",
 *         "userName": "master1",
 *         "date": "2018-11-27 20:28:50",
 *         "id": "d_K_O6UIV"
 *         },
 *      "type": "1",
 *      "action": {
 *         "_id": "vl3GKnaw2",
 *         "target_content": "Y1XFYKL52",
 *         "target_message": "",
 *         "target_communityContent": "",
 *         "action": "1",
 *         "user_id": "d_K_O6UIV",
 *         "__v": 0,
 *         "flag": "1",
 *         "updatetime": "2018-11-25 00:15:32",
 *         "datetime": "2018-11-25 00:15:32",
 *         "id": "vl3GKnaw2"
 *         },
 *      "uuid": "2b12d4f0f00411e88a9c817056005436",
 *      "__v": 0,
 *      "date": "2018-11-25 00:15:32",
 *      "unit": "MEC",
 *      "coins": 0.1,
 *      "id": "MFUAktOIO9"
 *       },
 *       ...
 *     ],
 *    "total_coins": 0.2 //条件内总收入/支出
 *   }
 * }
 * @apiSampleRequest http://localhost:8884/api/v0/wallet/getMyBill?billType=1&token=xxx
 * @apiVersion 1.0.0
 */
router.get('/wallet/getMyBill', siteFunc.checkUserSessionForApi, BillRecord.getBillRecords)


/**
 * @api {get} /api/v0/wallet/getBillRankingList 获取收/支榜单
 * @apiDescription 获取收/支榜单
 * @apiName /wallet/getBillRankingList
 * @apiGroup Wallet
 * @apiParam {string} current 当前页码
 * @apiParam {string} pageSize 每页记录数
 * @apiParam {string} billType 账单类型(收入：1，支出：2)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "getBillRecords",
 *    "server_time": 1547453810545,
 *    "data": {
 *       "startTime": "2019-01-02",
 *       "endTime": "2019-01-02",
 *       "recordList": [
 *            {
 *              "_id": "-mH5Bgfvj",
 *              "rewardtotalnum": 1841.4916,
 *              "datetime": "2019-01-14 16:16:50",
 *              "id": "-mH5Bgfvj",
 *              "userName": "biangbiang",
 *              "logo": "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1546584231618.jpeg"
 *            }
 *        ]
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/wallet/getBillRankingList?billType=1
 * @apiVersion 1.0.0
 */
router.get('/wallet/getBillRankingList', BillRecord.getBillRankingList)



/**
 * @api {get} /api/v0/wallet/getCoinRecords 获取指定币的所有记录
 * @apiDescription 获取指定币的所有记录，带分页；需要登录态
 * @apiName /wallet/getCoinRecords
 * @apiGroup Wallet
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} time 时间段(可传：d7/7天,d30/30天,d60/60天,d180/半年,d365/一年,all/全部)
 * @apiParam {string} unit 币种单位
 * @apiParam {string} billType 可选(MVPC 默认只获取收入,传 billType=all 获取收入和转账)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功 getCoinRecords",
 *  "server_time": 1543484196749,
 *  这是非METT币的返回结果
 *  "data": [
 *     {
 *      "_id": "MFUAktOIO9",
 *      "state": "0", // 针对提币的状态 0:待审核 1:审核通过 2:审核不通过
 *      "logs": "浏览",
 *      "user": {
 *         "_id": "d_K_O6UIV",
 *         "userName": "master1",
 *         "date": "2018-11-27 20:28:50",
 *         "id": "d_K_O6UIV"
 *         },
 *      "type": "1",
 *      "action": {
 *         "_id": "vl3GKnaw2",
 *         "target_content": "Y1XFYKL52",
 *         "target_message": "",
 *         "target_communityContent": "",
 *         "action": "1",
 *         "user_id": "d_K_O6UIV",
 *         "__v": 0,
 *         "flag": "1",
 *         "updatetime": "2018-11-25 00:15:32",
 *         "datetime": "2018-11-25 00:15:32",
 *         "id": "vl3GKnaw2"
 *         },
 *      "uuid": "2b12d4f0f00411e88a9c817056005436",
 *      "__v": 0,
 *      "date": "2018-11-25 00:15:32",
 *      "unit": "MEC",
 *      "coins": 0.1,
 *      "id": "MFUAktOIO9"
 *       },
 *       ...
 *     ],
 * 
 *    这是MVPC币的返回结果
 *  {
 *    "status": 200,
 *    "message": "getBillRecords",
 *    "server_time": 1547478688927,
 *    "data": [
 *        {
 *         "type": "1",
 *         "coins": 1841.4916,
 *         "date": "2019-01-12 18:43:40",
 *         "logs": "行为贡献激励"
 *        },
 *        {
 *         "type": "2",
 *         "coins": 1841.4916,
 *         "date": "2019-01-12 18:43:40",
 *         "logs": "内容创作激励"
 *        }   
 *        {
 *         "type": "3",
 *         "coins": 1841.4916,
 *         "date": "2019-01-12 18:43:40",
 *         "logs": "浏览激励"
 *        }
 *    ]
 *}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/wallet/getCoinRecords?token=xxx
 * @apiVersion 1.0.0
 */
router.get('/wallet/getCoinRecords', siteFunc.checkUserSessionForApi, BillRecord.getMyCoinsRecords)


/**
 * @api {get} /api/v0/wallet/getBillDetails 获取账单详情
 * @apiDescription 获取账单详情，带分页；需要登录态
 * @apiName /wallet/getBillDetails
 * @apiGroup Wallet
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiParam {string} id 记录ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "操作成功 getBillDetails",
 *  "server_time": 1543484196749,

 * @apiSampleRequest http://localhost:8884/api/v0/wallet/getBillDetails?token=xxx
 * @apiVersion 1.0.0
 */
router.get('/wallet/getBillDetails', siteFunc.checkUserSessionForApi, BillRecord.getBillDetails)


/**
 * @api {post} /api/v0/wallet/reSetFunPassword 重置/设置 资金密码
 * @apiDescription 重置/设置 资金密码,需要登录态，短信验证码有效期2分钟
 * @apiName /wallet/reSetFunPassword
 * @apiGroup Wallet
 * @apiParam {string} token 登录返回的鉴权参数
 * @apiParam {string} type (1:通过手机验证码方式，2:通过邮箱验证码方式)
 * @apiParam {string} messageCode 验证码
 * @apiParam {string} password 资金密码
 * @apiParam {string} confirmPassword 确认资金密码
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "Reset/Modify fun password success",
 *    "server_time": 1543140202776,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/wallet/reSetFunPassword
 * @apiVersion 1.0.0
 */
router.post('/wallet/reSetFunPassword', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.messageType = '2';
  next();
}, User.reSetFunPassword);

/**
 * @api {post} /api/v0/wallet/modifyFunPassword 修改资金密码
 * @apiDescription 修改资金密码,需要登录态
 * @apiName /wallet/modifyFunPassword
 * @apiGroup Wallet
 * @apiParam {string} token 登录返回的鉴权参数
 * @apiParam {string} oldPassword 原资金密码
 * @apiParam {string} password 新资金密码
 * @apiParam {string} confirmPassword 确认新资金密码
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "Reset/Modify fun password success",
 *    "server_time": 1543140202776,
 *    "data": {}
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/wallet/modifyFunPassword
 * @apiVersion 1.0.0
 */
router.post('/wallet/modifyFunPassword', siteFunc.checkUserSessionForApi, (req, res, next) => {
  req.query.messageType = '3';
  next();
}, User.reSetFunPassword);



/**
 * @api {get} /api/v0/wallet/getWalletList 获取我的钱包地址
 * @apiDescription 获取我的钱包地址；需要登录态
 * @apiName /wallet/getWalletList
 * @apiGroup Wallet
 * @apiParam {string} token 登录时返回的参数鉴权
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "WalletAddress",
 *  "server_time": 1543150128594,
 *  "data": [
 *    {
 *    "_id": "yhiiNOFc2",
 *    "user": "zwwdJvLmP",
 *    "wallet": "0x2ed28eDIbK206Bf008C582f5187E4F967e6214e6",
 *    "comments": "这是我的第一个钱包",
 *    "__v": 0,
 *    "date": "2018-11-25 20:47:35",
 *    "id": "yhiiNOFc2"
 *    }
 *    ...
 *  ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/wallet/getWalletList?token=xxx
 * @apiVersion 1.0.0
 */
router.get('/wallet/getWalletList', siteFunc.checkUserSessionForApi, WalletAddress.getWalletAddress)


/**
 * @api {post} /api/v0/wallet/addOneWallet 新增钱包地址
 * @apiDescription 新增钱包地址,需要登录态
 * @apiName /wallet/addOneWallet
 * @apiGroup Wallet
 * @apiParam {string} token 登录返回的鉴权参数
 * @apiParam {string} wallet 钱包地址
 * @apiParam {string} comments 钱包备注
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "WalletAddress",
 *    "server_time": 1543150055651,
 *    "data": {
 *     "id": "yhiiNOFc2"
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/wallet/addOneWallet
 * @apiVersion 1.0.0
 */
router.post('/wallet/addOneWallet', siteFunc.checkUserSessionForApi, WalletAddress.addWalletAddress);

/**
 * @api {post} /api/v0/wallet/updateWallet 修改钱包地址
 * @apiDescription 修改钱包地址,需要登录态
 * @apiName /wallet/updateWallet
 * @apiGroup Wallet
 * @apiParam {string} token 登录返回的鉴权参数
 * @apiParam {string} id 指定钱包ID
 * @apiParam {float} wallet 钱包地址
 * @apiParam {string} comments 钱包备注
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "WalletAddress",
 *    "server_time": 1543150055651,
 *    "data": {
 *     "id": "yhiiNOFc2"
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/wallet/updateWallet
 * @apiVersion 1.0.0
 */
router.post('/wallet/updateWallet', siteFunc.checkUserSessionForApi, WalletAddress.updateWalletAddress);


/**
 * @api {get} /api/v0/wallet/delWalletAddress 删除钱包地址
 * @apiDescription 删除钱包地址,需要登录态
 * @apiName /wallet/delWalletAddress
 * @apiGroup Wallet
 * @apiParam {string} token 登录返回的鉴权参数
 * @apiParam {string} id 指定钱包ID
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "delWalletAddress",
 *    "server_time": 1543150055651,
 *    "data": {
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/wallet/delWalletAddress
 * @apiVersion 1.0.0
 */
router.get('/wallet/delWalletAddress', siteFunc.checkUserSessionForApi, WalletAddress.delWalletAddress);



/**
 * @api {get} /api/v0/wallet/getRechargeAmount 获取充值价格列表
 * @apiDescription 获取充值价格列表和默认值,需要登录态
 * @apiName /wallet/getRechargeAmount
 * @apiGroup Wallet
 * @apiParam {string} token 登录返回的鉴权参数
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "status": 200,
 *  "message": "getResetAmounts",
 *  "server_time": 1543568477118,
 *  "data": [
 *    {
 *     "_id": "MYYxtCNXn",
 *     "unit": "MEC",  // 单位
 *     "__v": 0,
 *     "date": "2018-11-30 16:56:34",
 *     "default": false, // 是否默认选中
 *     "money": 198,  // 金额
 *     "id": "MYYxtCNXn"
 *    },
 *    ...
 *  ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/wallet/getRechargeAmount
 * @apiVersion 1.0.0
 */
router.get('/wallet/getRechargeAmount', siteFunc.checkUserSessionForApi, ResetAmount.getResetAmounts)


/**
 * @api {get} /api/v0/wallet/getCashGas 获取旷工费
 * @apiDescription 获取旷工费,需要登录态
 * @apiName /wallet/getCashGas
 * @apiGroup Wallet
 * @apiParam {string} token 登录返回的鉴权参数
 * @apiParam {string} coins 币数
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 * {
 *   "status": 200,
 *   "message": "get data success",
 *   "server_time": 1545479477817,
 *   "data": 
 *   {
 *     "gas" : "0.000029999999999999997 // 所需旷工费"
 *   }
 * }
 * @apiSampleRequest http://localhost:8884/api/v0/wallet/getCashGas
 * @apiVersion 1.0.0
 */
router.get('/wallet/getCashGas', siteFunc.checkUserSessionForApi, BillRecord.getCashGas)


/**
 * @api {post} /api/v0/vip/openAccount 开通VIP会员
 * @apiDescription 开通VIP会员
 * @apiName /vip/openAccount
 * @apiGroup VIP
 * @apiParam {string} token 登录返回的鉴权参数
 * @apiParam {string} password 资金密码
 * @apiParam {string} setMeal 指定会员套餐ID
 * @apiParam {string} comments 开通备注(非必填)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "购买会员 成功",
 *    "server_time": 1543150055651,
 *    "data": {
 *     "leftCoins": 9988
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/vip/openAccount
 * @apiVersion 1.0.0
 */
router.post('/vip/openAccount', siteFunc.checkUserSessionForApi, OpenVipRecord.addOpenVipRecord);

/**
 * @api {get} /api/v0/vip/getSetMeal 获取VIP套餐列表
 * @apiDescription 获取VIP套餐列表
 * @apiName /vip/getSetMeal
 * @apiGroup VIP
 * @apiParam {string} token 登录返回的鉴权参数
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "getVipSetMeals",
 *    "server_time": 1546160983360,
 *    "data": [
 *        {
 *            "_id": "S-GgNzIc8",
 *            "time": "12", // 套餐时长
 *            "comments": "12个月40MEC",
 *            "__v": 0,
 *            "date": "2018-12-30 17:00:36", 
 *            "default": true, // 是否默认/推荐
 *            "state": true,  
 *            "unit": "MEC", // 套餐单位
 *            "coins": 40, // 套餐价格
 *            "id": "S-GgNzIc8"
 *        },
 *        {
 *            "_id": "eMjvA-G1G",
 *            "time": "3",
 *            "comments": "3个月12MEC",
 *            "__v": 0,
 *            "date": "2018-12-30 16:59:39",
 *            "default": false,
 *            "state": true,
 *            "unit": "MEC",
 *            "coins": 12,
 *            "id": "eMjvA-G1G"
 *        },
 *        {
 *            "_id": "XGT2f6gjn",
 *            "time": "1",
 *            "comments": "5MEC",
 *            "__v": 0,
 *            "date": "2018-12-30 16:49:07",
 *            "default": false,
 *            "state": true,
 *            "unit": "MEC",
 *            "coins": 5,
 *            "id": "XGT2f6gjn"
 *        }
 *    ]
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/vip/getSetMeal
 * @apiVersion 1.0.0
 */
router.get('/vip/getSetMeal', siteFunc.checkUserSessionForApi, VipSetMeal.getVipSetMeals);


/**
 * @api {get} /api/v0/vip/getVipInfo 获取用户的vip信息
 * @apiDescription 获取用户的vip信息
 * @apiName /vip/getVipInfo
 * @apiGroup VIP
 * @apiParam {string} token 登录返回的鉴权参数
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "getVipInfo success",
 *    "server_time": 1546170980567,
 *    "data": {
 *       "isVip": true,
 *       "endTime": "2019-3-30 17:44:31"
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/vip/getVipInfo
 * @apiVersion 1.0.0
 */
router.get('/vip/getVipInfo', siteFunc.checkUserSessionForApi, User.getVipInfo);


router.get('/system/getHelp', HelpCenter.getHelpCenters);


/**
 * @api {get} /api/v0/system/getAppVersion 获取APP版本信息
 * @apiDescription 获取APP版本信息
 * @apiName /system/getAppVersion
 * @apiGroup System
 * @apiParam {string} versionCode 版本号(传字符串类型整数)
 * @apiParam {string} client 客户端类型(0:安卓，1:ios)
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "get versionData success",
 *    "server_time": 1548049855934,
 *    "data": {
 *         "_id": "H158NRt7Q",
 *         "description": "新增自动更新1",
 *         "title": "有更新啦1",
 *         "url": "/upload/images/apk20180717192615.apk",
 *         "version": "21",
 *         "versionName": "1.1",
 *         "date": "2018-07-16 16:16:17",
 *         "forcibly": true,
 *         "id": "H158NRt7Q"
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/system/getAppVersion
 * @apiVersion 1.0.0
 */
router.get('/system/getAppVersion', VersionManage.getVersionManages)


/**
 * @api {get} /api/v0/system/getAppSwitch 获取APP系统开关
 * @apiDescription 获取APP系统开关
 * @apiName /system/getAppSwitch
 * @apiParam {string} client 客户端类型(0:安卓，1:ios)
 * @apiGroup System
 * @apiSuccess {json} result
 * @apiSuccessExample {json} Success-Response:
 *{
 *    "status": 200,
 *    "message": "getAppSwitch",
 *    "server_time": 1549441087075,
 *    "data": {
 *        "hideVip": true,
 *        "hideWallet": true,
 *        "hideClassFeature": true
 *        "hideClassPrice": true
 *        "hideMBT": true
 *        "hideMPVC": true
 *        "hideEcologyFriend": true
 *        "allowUserPostContent": true  // 允许普通用户发布文章
 *        "allowUserCreateCommunity": true  // 允许普通用户创建社群
 *    }
 *}
 * @apiSampleRequest http://localhost:8884/api/v0/system/getAppSwitch
 * @apiVersion 1.0.0
 */
router.get('/system/getAppSwitch', SystemConfig.getAppSwitch)


// router.get('/renderTypes', async (req, res, next) => {

//   var specialTypes = [
//     [["生活"], ["美食", "旅行", "生活", "家具", "汽车"]],
//     [["人文"], ["历史", "文学", "哲学"]],
//     [["文化"], ["艺术", "文化", "绘画", "摄影", "阅读"]],
//     [["娱乐"], ["游戏", "音乐", "动漫", "电影"]],
//     [["运动"], ["体育", "健康", "篮球", "足球", "户外运动"]],
//     [["商业"], ["金融", "财务", "投资", "房地产", "经济学", "保险", "创业", "市场营销", "会计", "商业"]],
//     [["健康"], ["医学", "食品安全", "医疗"]],
//     [["法律"], ["法律", "律师"]],
//     [["教育"], ["教育", "儿童教育", "职场", "职场规划"]],
//     [["心理学"], ["心理学"]],
//     [["情感"], ["情感", "亲密关系"]],
//     [["自然科学"], ["物理学", "化学", "天文学", "地球科学", "生物学", "博物学", "环境科学", "材料科学", "数学"]],
//     [["社会科学"], ["政治学", "人文学", "社会学", "教育学", "新闻学", "语言学", "地理学", "考古学"]],
//     [["工程学"], ["建筑", "土木", "交通", "电气工程", "机械", "能源", "航空航天", "船舶与海洋工程"]],
//     [["科技"], ["科技", "消费电子产品"]],
//     [["互联网"], ["互联网"]]
//   ];

//   for (let m = 0; m < specialTypes.length; m++) {
//     let targetType = specialTypes[m]
//     let parentTypeName = targetType[0][0];
//     let childTypeArr = targetType[1];
//     let typeParentObj = {
//       comments: "humanity",
//       contentTemp: "N1JIyGBne",
//       defaultUrl: "humanity",
//       enable: true,
//       keywords: parentTypeName,
//       name: parentTypeName,
//       parentId: "0",
//       sortId: 1,
//       type: "1"
//     }

//     const newContentCategory = new ContentCategoryModel(typeParentObj);
//     let cateObj = await newContentCategory.save();
//     // 更新sortPath defaultUrl
//     let newQuery = {};
//     newQuery.sortPath = '0,' + cateObj._id
//     await ContentCategoryModel.findOneAndUpdate({ _id: cateObj._id }, { $set: newQuery });

//     // let currentItem = await ContentCategoryModel.findOne({ _id: cateObj._id });
//     for (let n = 0; n < childTypeArr.length; n++) {
//       let childItem = childTypeArr[n];
//       let typeChildrenObj = {
//         comments: "humanity",
//         contentTemp: "",
//         defaultUrl: "humanity",
//         enable: true,
//         keywords: childItem,
//         name: childItem,
//         parentId: cateObj._id,
//         sortId: n + 1,
//         type: "1"
//       }

//       const newChildContentCategory = new ContentCategoryModel(typeChildrenObj);
//       let cateChildObj = await newChildContentCategory.save();
//       // 更新sortPath defaultUrl
//       let newQuery = {};
//       let parentObj = await ContentCategoryModel.findOne({ '_id': cateObj._id }, 'sortPath defaultUrl');
//       newQuery.sortPath = parentObj.sortPath + "," + cateChildObj._id;
//       newQuery.defaultUrl = parentObj.defaultUrl + '/' + typeChildrenObj.defaultUrl
//       await ContentCategoryModel.findOneAndUpdate({ _id: cateChildObj._id }, { $set: newQuery });

//     }
//   }



//   res.send({
//     status: 200
//   })
// })

// router.get('/renderTags', async (req, res, next) => {
//   // let tagsArr = ["游戏", "运动", "互联网", "艺术", "阅读", "美食", "动漫", "汽车", "生活方式", "教育", "摄影", "历史", "文化", "旅行", "职业发展", "经济学", "足球", "篮球", "投资", "音乐", "电影", "法律", "自燃科学", "设计", "创业", "健康", "商业", "体育", "科技", "化学", "物理学", "生物学", "金融"];
//   // let tagsArr = ["职场成长", "培训考试", "人文艺术", "互联网", "科技科普", "心理情感", "健康血糖", "文学小说", "生活研修", "商学院"];
//   let tagsArr = ["推荐社群", "家具装饰", "地域", "健身", "艺术", "搞笑", "游戏", "教育", "体育", "兴趣", "摄影", "美食", "科学", "人文&社会", "美妆&时尚", "科技", "音乐", "亲子", "宠物", "娱乐明星", "文学", "情感", "影视", "资源", "旅行", "区块链", "动漫"];
//   for (const targetTag of tagsArr) {
//     let tagObj = {
//       name: targetTag,
//       sImg: '/upload/images/img20181115091608.png',
//       comments: targetTag
//     }
//     let myTag = new CommunityTagModel(tagObj);
//     await myTag.save();
//   }
//   res.send({
//     status: 200
//   })
// })

// 手动充值
// router.get('/user/askCashShou', CurrencyApproval.askCashShou);

// 推送测试
// router.get('/jsPush', siteFunc.pushTest);
// router.get('/checkGoogleAuth', User.checkGoogleAuth);

module.exports = router