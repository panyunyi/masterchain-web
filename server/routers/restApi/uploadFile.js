/**
 * Created by Administrator on 2018年12月06日09:31:07
 * 系统支持功能
 */
const express = require('express');
const router = express.Router();
router.caseSensitive = true;
router.strict = true
//文件上传类
const formidable = require('formidable');
const util = require('util');
const fs = require('fs');
const Core = require('@alicloud/pop-core');
var RPCClient = require('@alicloud/pop-core').RPCClient;
const axios = require('axios');

const mime = require('../../../utils/mime').types;
const MTS = require('../../../utils/middleware/aliyunSignature');
const service = require('../../../utils/service');
const authToken = require('../../../utils/authToken');
const {
    ClassContent,
    ClassPayRecord
} = require('../../lib/controller');


//站点配置
const settings = require("../../../configs/settings");
const siteFunc = require('../../../utils/siteFunc')

const _ = require('lodash');
let checkPathNum = 0;
var OSS = require('ali-oss');
var client = new OSS({
    region: settings.region,
    accessKeyId: settings.accessKeyId,
    accessKeySecret: settings.accessKeySecret
});

var ali_oss = {
    bucket: settings.bucket,
    endPoint: settings.endPoint,
}

// 初始化
var videoOnDemandClient = new Core({
    accessKeyId: settings.accessKeyId,
    secretAccessKey: settings.accessKeySecret,
    endpoint: 'https://sts.aliyuncs.com',
    apiVersion: '2015-04-01'
});


let confirmPath = (path) => {
    return new Promise(function (resolve, reject) {
        let waitCheck = () => {
            setTimeout(() => {
                if (!fs.existsSync(path)) {
                    if (checkPathNum == 3) {
                        resolve(fs.existsSync(path));
                    } else {
                        checkPathNum++;
                        waitCheck();
                    }
                } else {
                    resolve(fs.existsSync(path));
                }
            }, 200);
        }
        waitCheck();
    })
}

let checkFilePath = async function (path) {
    checkPathNum = 0;
    return await confirmPath(path);
};

let updateFileForApi = function (fileType, imgKey, localFile) {
    return new Promise(async (resolve, reject) => {

        let ossPath = '/';
        if (fileType == 'images') {
            ossPath = 'upload/images/' + imgKey
        } else if (fileType == 'videos') {
            ossPath = 'upload/videos/' + imgKey;
        } else if (fileType == 'audios') {
            ossPath = 'upload/audios/' + imgKey;
        } else if (fileType == 'apks') {
            ossPath = 'upload/apks/' + imgKey
        }

        try {
            if (process.env.NODE_ENV == 'production') {
                client.useBucket(ali_oss.bucket);
                var result = await client.put(ossPath, localFile);
                // console.log('- upload-result--', result);
                let targetUrl = result.url;
                if (targetUrl.indexOf('http://') >= 0) {
                    targetUrl = targetUrl.replace('http://', 'https://');
                }
                resolve(targetUrl);
            } else {
                resolve('/' + ossPath);
            }
        } catch (error) {
            reject(error);
        }
    })
}

// let snapShot = function (videoId) {

// }

router.post('/files', siteFunc.checkUserSessionForApi, function (req, res, next) {

    let form = new formidable.IncomingForm();
    console.log('start upload');
    //存放目录
    let updatePath = "public/upload/images/";
    let updateVideoPath = "public/upload/videos/";
    let updateAudioPath = "public/upload/audios/";
    let updateApkPath = "public/upload/apks/";
    let newFileName = "",
        fileType = '';
    let type = '0' // 0图片 1:视频 2:apk
    let uploadType = req.query.type;
    form.encoding = 'utf-8';
    form.keepExtensions = true;
    form.uploadDir = updatePath;

    if (uploadType == 'appPackage') {
        form.maxFileSize = 50 * 1024 * 1024; // 最大100M
    } else {
        form.maxFileSize = 150 * 1024 * 1024; // 最大5M
    }

    var files = [];
    form.on('file', function (filed, file) {
        files.push([filed, file]);
    })

    form.parse(req, async function (err, fields, files) {

        try {

            if (err) {
                console.log(err);
            }
            // console.log(fields);
            // console.log(files);
            // await siteFunc.checkPostToken(req, res, fields.token);
            if (!files.file) {
                throw new siteFunc.UserException(res.__('label_uploader_largeFile'));
            }
            //校验文件的合法性
            let realFileType = service.getFileMimeType(files.file.path);
            console.log('---realFileType-', realFileType);
            let contentType = mime[realFileType.fileType] || 'unknown';
            if (contentType == 'unknown') {
                throw new siteFunc.UserException(settings.system_error_imageType);
            }

            let typeKey = "others";
            let fileNameArr = files.file.name.split('.');
            let thisType = fileNameArr[fileNameArr.length - 1];
            let ms = (new Date()).getTime().toString();
            // console.log('--realFileType.fileType---', realFileType.fileType)
            if (realFileType.fileType == 'jpg' || realFileType.fileType == 'jpeg' || realFileType.fileType == 'png' || realFileType.fileType == 'gif') {
                fileType = 'images';
            } else if (realFileType.fileType == 'ogg' ||
                realFileType.fileType == 'mp4' ||
                realFileType.fileType == 'avi' ||
                realFileType.fileType == 'webm' ||
                realFileType.fileType == 'flv') {
                fileType = 'videos';
            } else if (realFileType.fileType == 'wmv' ||
                realFileType.fileType == 'mp3' ||
                realFileType.fileType == 'ogg' ||
                realFileType.fileType == 'aac') {
                fileType = 'audios';
            } else if (realFileType.fileType == 'zip') {
                if (uploadType == 'appPackage') {
                    fileType = 'apks';
                }
            }

            if (fileType == 'images') {
                typeKey = "img";
                type = '0';
            } else if (fileType == 'videos') {
                typeKey = "video";
                type = '1';
            } else if (fileType == 'apks') {
                typeKey = "apk";
                type = '2';
            } else if (fileType == 'audios') {
                typeKey = "audio";
                type = '3';
            }

            newFileName = typeKey + ms + "." + thisType;

            if (fileType == 'images') {
                fs.renameSync(files.file.path, updatePath + newFileName)
            } else if (fileType == 'videos') {
                fs.renameSync(files.file.path, updateVideoPath + newFileName)
            } else if (fileType == 'audios') {
                fs.renameSync(files.file.path, updateAudioPath + newFileName)
            } else if (fileType == 'apks') {
                fs.renameSync(files.file.path, updateApkPath + newFileName)
            }

            let addPath = process.cwd() + '/public/upload/images/' + newFileName;
            if (fileType == 'videos') {
                addPath = process.cwd() + '/public/upload/videos/' + newFileName;
            } else if (fileType == 'audios') {
                addPath = process.cwd() + '/public/upload/audios/' + newFileName;
            } else if (fileType == 'apks') {
                addPath = process.cwd() + '/public/upload/apks/' + newFileName;
            }

            if (await checkFilePath(addPath)) {

                let fileInfo = await updateFileForApi(fileType, newFileName, addPath);

                if (!_.isEmpty(fileInfo)) {

                    console.log('fileInfo', fileInfo);

                    // 删除本地文件
                    if (process.env.NODE_ENV == 'production') {
                        fs.unlinkSync(addPath);
                    }

                    if (fileType == 'videos') {
                        // 提交截图
                        console.log('开始提交截图作业！');
                        MTS.videoShot('upload/videos/', newFileName);
                    }

                    let renderData = {
                        path: fileInfo,
                        type
                    }
                    // 添加视频缩略图地址
                    if (type == '1') {
                        renderData.thumbnail = siteFunc.getVideoImgByLink(fileInfo);
                    }

                    res.send(siteFunc.renderApiData(req, res, 200, 'get data success', renderData, 'save'));
                }

            } else {
                throw new siteFunc.UserException(settings.system_error_upload);
            }
        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'ossUpload'))
        }

    });

});

router.get('/video/askScreenshot', authToken, (req, res) => {

    try {
        let fileName = req.query.fileName;
        MTS.videoShot('upload/videos/', fileName);
        res.send({
            status: 200,
            message: error.message
        });
    } catch (error) {
        res.send({
            status: 500,
            message: error
        });
    }

})

const AliCloudVideo = require('ali-cloud-video')

const ali = new AliCloudVideo({
    AccessKeyId: settings.accessKeyId,
    AccessKeySecret: settings.accessKeySecret
})

// 获取上传前的上传凭证
router.get('/getUploadAuth', siteFunc.checkUserSessionForApi, (req, res, next) => {
    //设置参数
    var params = {
        userId: req.session.user._id,
        TemplateGroupId: '55db2ceb56792dffb979eea6db33dd9d'
    }

    ali.getUploadAuth(params, (err, result) => {
        if (err) console.log(err);
        console.log(result)
        res.send(result);
    })
})

// 刷新上传凭证
router.get('/reFreshUploadAuth', siteFunc.checkUserSessionForApi, (req, res, next) => {
    //设置参数
    var videoId = req.query.videoId;
    if (!videoId) {
        throw new siteFunc.UserException(settings.system_error_upload);
    }
    ali.refreshUploadAuth(videoId, (err, result) => {
        if (err) console.log(err);
        console.log(result)
        res.send(result);
    })
})



router.get('/getStsAuth', async (req, res, next) => {
    // 管理员可以直接获取
    if (req.query.useClient == '0' && req.session.adminUserInfo) {
        next()
    } else {
        await siteFunc.checkUserLoginState(req, res, next);
    }
}, async (req, res, next) => {
    if (req.query.useClient == '0' && req.session.adminUserInfo) {
        next()
    } else {
        try {
            let videoId = req.query.videoId;
            let queryObj = {
                $or: [{
                    videoArr: videoId
                }, {
                    audioArr: videoId
                }]
            };
            if (videoId) {
                // console.log('-videoId---', videoId)
                let targetClass = await ClassContent.getOneClassInfo(queryObj)

                // 没有权限
                if (_.isEmpty(targetClass)) {
                    // console.log('---222--')
                    throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
                    // next();
                } else {

                    if (req.session.adminUserInfo) {
                        next();
                    } else {
                        let targetUser = req.session.user;

                        if (!_.isEmpty(targetUser)) {
                            // 自己的课程不用限制
                            if (targetClass.author._id != req.session.user._id) {

                                // 还未发布的课程他人不能访问
                                if (targetClass.state != '2') {
                                    throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                                }

                                // 会员免费
                                if (targetClass.basicInfo.priceType == '1') {
                                    if (!targetUser.vip && !targetClass.free) {
                                        throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                                    }
                                }

                                // 会员付费
                                if (targetClass.basicInfo.priceType == '2') {

                                    let classRecord = await ClassPayRecord.getOneClassPayRecords({
                                        user: req.session.user._id,
                                        class: targetClass.basicInfo._id
                                    });

                                    if (!targetClass.free && _.isEmpty(classRecord)) {
                                        throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                                    }

                                }
                                console.log('符合用户角色要求');
                                next();
                            } else {
                                console.log('自己的课程');
                                next();
                            }
                        } else {
                            // console.log('--targetClass--', targetClass)
                            if (!targetClass.free && targetClass.basicInfo.priceType != '0') {
                                throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                            }
                            next();
                        }
                    }

                }

            } else {
                throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
            }

        } catch (error) {
            res.send({
                status: 500,
                message: error.message
            });
        }
    }

}, async (req, res, next) => {



    var params = {
        "RoleArn": "acs:ram::5030742681317628:role/vod",
        "RoleSessionName": "ECSAdmin",
        "DurationSeconds": 3600
    }

    var requestOption = {
        method: 'POST'
    };

    videoOnDemandClient.request('AssumeRole', params, requestOption).then((result) => {
        // console.log(result);
        res.send(result);
    }, (ex) => {
        console.log(ex);
        res.send({
            status: 500,
            message: '获取播放凭证失败，请稍后重试！'
        });
    })


})


module.exports = router;