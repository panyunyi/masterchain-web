/**
 * 
 * 用户相关界面
 * 
 */
const express = require('express')
const router = express.Router()
router.caseSensitive = true
router.strict = true
const fs = require('fs')
const path = require('path')
const {
    authSession
} = require('../../utils');
const generalFun = require("../lib/utils/generalFun");
const settings = require('../../configs/settings');
const {
    AdminUser,
    ContentCategory,
    Content,
    ContentTag,
    User,
    Message,
    SystemConfig,
    UserNotify,
    Ads
} = require('../lib/controller');
const moment = require('moment');
const shortid = require('shortid');
const _ = require('lodash')
const ContentBiz = require('../lib/modelBiz/ContentBiz')
const {
    siteFunc
} = require('../../utils');


function checkUserSessionForPage(req, res, next) {
    if (!_.isEmpty(req.session.user)) {
        next()
    } else {
        res.redirect('/users/login');
    }
}


//用户登录
router.get('/login', function (req, res, next) {
    if (req.session.user) {
        res.redirect("/");
    } else {
        req.query.title = '用户登录';
        req.query.tempPage = 'users/userLogin.html';
        next()
    }
}, generalFun.getDataForUserLoginAndReg);


//用户注册
router.get('/reg', function (req, res, next) {
    if (req.session.user) {
        res.redirect("/");
    } else {
        req.query.title = '用户注册';
        req.query.tempPage = 'users/userReg.html';
        next()
    }
}, generalFun.getDataForUserReg);


//用户中心
router.get('/userCenter', checkUserSessionForPage, (req, res, next) => {
    req.query.title = "用户中心";
    req.query.tempPage = 'users/userCenter.html';
    next()
}, generalFun.getDataForUserCenter);

//用户中心
router.get('/personInfo', checkUserSessionForPage, (req, res, next) => {
    req.query.title = "编辑用户信息";
    req.query.tempPage = 'users/personInfo.html';
    next()
}, generalFun.getDataForUserCenter);

router.get("/addCourse.html", checkUserSessionForPage,
    generalFun.getDataForAddClass
);

router.get("/editCourseInfo/:id", checkUserSessionForPage, (req, res, next) => {
    let classId = req.params.id;
    if (!shortid.isValid(classId)) {
        res.redirect("/users/userCenter");
    } else {
        req.query.classId = classId;
        req.query.title = "编辑课程信息";
        next()
    }
}, generalFun.getDataForAddClassInfo);


router.get("/addCourseInfo.html", checkUserSessionForPage,
    generalFun.getDataForAddClassInfo
);

router.get("/classPreview.html", checkUserSessionForPage, (req, res, next) => {
    next();
}, generalFun.getDataForPlayerShow);

router.get("/applyCreation.html", checkUserSessionForPage,
    generalFun.getDataForApplyCreation
);

//忘记密码
router.get('/forgotPsd', (req, res, next) => {
    if (req.session.user) {
        res.redirect('/');
    } else {
        next();
    }
}, (req, res, next) => {
    req.query.title = "忘记密码";
    req.query.tempPage = 'users/userForgotPsd.html';
    next()
}, generalFun.getDataForForgotPsd);

//忘记密码通过邮箱
router.get('/forgotPsdByEmail', (req, res, next) => {
    if (req.session.user) {
        res.redirect('/');
    } else {
        next();
    }
}, (req, res, next) => {
    req.query.title = "忘记密码";
    req.query.tempPage = 'users/userForgotPsdByEmail.html';
    next()
}, generalFun.getDataForForgotPsd);


// 用户相关主界面
router.get('/userTopics/:id', checkUserSessionForPage, (req, res, next) => {
    let contentId = req.params.id;
    if (!shortid.isValid(contentId)) {
        res.redirect("/users/userCenter");
    } else {
        req.query.specialId = contentId;
        req.query.title = "我的专题";
        req.query.tempPage = 'users/userTopicList.html';
        next()
    }

}, generalFun.getDataForUserSpecialList);

// 用户投稿主界面
router.get('/userAddContent', checkUserSessionForPage, (req, res, next) => {
    req.query.title = "创作";
    req.query.tempPage = 'users/userAddContent.html';
    req.query.contentType = 'normal';
    next()
}, generalFun.getDataForUserAddContent);

router.get('/editContent/:id', checkUserSessionForPage, async (req, res, next) => {

    let contentId = req.params.id;
    if (!shortid.isValid(contentId)) {
        res.redirect("/users/userCenter");
    } else {
        let contentInfo = await ContentBiz.getOneContentByParams({
            _id: contentId,
            uAuthor: req.session.user._id,
            state: '0'
        });
        if (!_.isEmpty(contentInfo)) {
            req.query.title = "编辑创作";
            req.query.contentId = contentId;
            req.query.contentType = contentInfo.type == '1' ? 'normal' : 'special';
            req.query.tempPage = 'users/userAddContent.html';
            next()
        } else {
            res.redirect("/users/userCenter");
        }

    }

}, generalFun.getDataForUserAddContent);

router.get('/addSpecialTopic', checkUserSessionForPage, (req, res, next) => {
    req.query.title = "发布专题";
    req.query.tempPage = 'users/userAddContent.html';
    req.query.contentType = 'special';
    next()
}, generalFun.getDataForUserAddContent);

router.get('/identityAuth', checkUserSessionForPage, (req, res, next) => {
    req.query.title = "大师认证";
    req.query.tempPage = 'users/identityAuth.html';
    req.query.step = '1';
    next()
}, siteFunc.checkIdentifyStep, generalFun.getDataForIdentityAuth);

router.get('/identityFullInfo', checkUserSessionForPage, (req, res, next) => {
    req.query.title = "大师认证完善资料";
    req.query.tempPage = 'users/identityAuthStep2.html';
    req.query.step = '2';
    next()
}, siteFunc.checkIdentifyStep, generalFun.getDataForIdentityAuth);

router.get('/identitySuccess', checkUserSessionForPage, (req, res, next) => {
    req.query.title = "大师认证提交成功";
    req.query.tempPage = 'users/identityAuthSuccess.html';
    req.query.step = '3';
    next()
}, siteFunc.checkIdentifyStep, generalFun.getDataForIdentityAuth);


// 找回密码
// router.get('/confirmEmail', generalFun.getDataForResetPsdPage)


//点击找回密码链接跳转页面
// router.get('/reset_pass', User.reSetPass);


module.exports = router;