const express = require("express");
const router = express.Router();
router.strict = true;
router.caseSensitive = true;
const fs = require("fs");
const path = require("path");
const _ = require("lodash");
const generalFun = require("../lib/utils/generalFun");
const {
  ContentCategory,
  Content,
  SystemConfig,
  User
} = require("../lib/controller");
const moment = require("moment");
const shortid = require("shortid");
const validator = require("validator");
var schedule = require("node-schedule");
const HelpCenterBiz = require('../lib/modelBiz/HelpCenterBiz');

//配置站点地图和robots抓取
router.get("/sitemap.xml", (req, res, next) => {
  SystemConfig.getConfigsByFiles("siteDomain")
    .then(result => {
      let root_path = result[0].siteDomain;
      let priority = 0.8;
      let freq = "weekly";
      let lastMod = moment().format("YYYY-MM-DD");
      let xml =
        '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
      xml += "<url>";
      xml += "<loc>" + root_path + "</loc>";
      xml += "<changefreq>daily</changefreq>";
      xml += "<lastmod>" + lastMod + "</lastmod>";
      xml += "<priority>" + 1 + "</priority>";
      xml += "</url>";

      req.query.catefiles = "defaultUrl";
      req.query.contentfiles = "title";
      ContentCategory.getAllCategories(req, res)
        .then(cates => {
          cates.forEach(function (cate) {
            xml += "<url>";
            xml +=
              "<loc>" +
              root_path +
              "/" +
              cate.defaultUrl +
              "___" +
              cate._id +
              "</loc>";
            xml += "<changefreq>weekly</changefreq>";
            xml += "<lastmod>" + lastMod + "</lastmod>";
            xml += "<priority>0.8</priority>";
            xml += "</url>";
          });
          return Content.getAllContens(req, res);
        })
        .then(contentLists => {
          contentLists.forEach(function (post) {
            xml += "<url>";
            xml += "<loc>" + root_path + "/details/" + post._id + ".html</loc>";
            xml += "<changefreq>weekly</changefreq>";
            xml += "<lastmod>" + lastMod + "</lastmod>";
            xml += "<priority>0.5</priority>";
            xml += "</url>";
          });
          xml += "</urlset>";
          res.end(xml);
        })
        .catch(err => {
          res.send({
            status: 500,
            message: err.message
          });
        });
    })
    .catch(err => {
      console.log("请先配置站点域名:", err);
    });
});

router.get("/sitemap.html", generalFun.getDataForSiteMap);

router.get(
  ["/", "/zh-CN", "/zh-TW"],
  (req, res, next) => {
    next();
  },
  generalFun.getDataForIndexPage
);

router.get(["/hot.html"],
  (req, res, next) => {
    next();
  },
  generalFun.getDataForHotPage
);

router.get(["/discover.html"],
  (req, res, next) => {
    next();
  },
  generalFun.getDataForDiscoverPage
);

router.get(["/masters.html"],
  (req, res, next) => {
    next();
  },
  generalFun.getDataForMasterPage
);

router.get(["/topic.html"],
  (req, res, next) => {
    next();
  },
  generalFun.getDataForTopicPage
);

// 专题列表
router.get(["/topicList.html"],
  (req, res, next) => {
    next();
  },
  generalFun.getDataForTopicListPage
);

// 大师课首页
router.get("/masterCourse.html",
  (req, res, next) => {
    next();
  },
  generalFun.getDataForMasterClass
);

// app下载首页
router.get("/download.html",
  (req, res, next) => {
    req.query.tempPage = 'download.html';
    next();
  },
  generalFun.getDataForDownload
);

// 移动端下载
router.get("/mb/download.html", (req, res, next) => {
  req.query.tempPage = 'mobile/download.html';
  next();
}, generalFun.getDataForDownload);

// 帮助中心web
router.get(["/help.html", "/help_:lang.html"],
  (req, res, next) => {
    req.query.helpType = '0';
    next();
  },
  generalFun.getDataForWebHelpCenter
);

// 帮助中心
router.get(["/helpCenter.html", "/helpCenter_:lang.html"],
  (req, res, next) => {
    req.query.helpType = '0';
    next();
  },
  generalFun.getDataForHelpCenter
);

// 签到规则
router.get(["/attendanceRules.html", "/attendanceRules_:lang.html"],
  (req, res, next) => {
    req.query.helpType = '9';
    next();
  },
  generalFun.getDataForHelpCenter
);

// 隐私申明
router.get(["/privacyStatement.html", "/privacyStatement_:lang.html"],
  (req, res, next) => {
    req.query.helpType = '2';
    next();
  },
  generalFun.getDataForWebHelpCenter
);

// 关于我们
router.get(["/aboutUs.html", "/aboutUs_:lang.html"],
  (req, res, next) => {
    req.query.helpType = '3';
    next();
  },
  generalFun.getDataForWebHelpCenter
);

// 合作伙伴
router.get(["/partner.html", "/partner_:lang.html"],
  (req, res, next) => {
    req.query.helpType = '4';
    next();
  },
  generalFun.getDataForWebHelpCenter
);

// 商务合作
// router.get(["/businessCooperation.html", "/businessCooperation_:lang.html"],
//   async (req, res) => {
//     let helpLang = req.params.lang == 'zh-TW' ? '3' : '1';
//     const helpModel = await HelpCenterBiz.getHelpByType('5', helpLang);
//     res.send(helpModel.comments);
//   }
// );
router.get(["/businessCooperation.html", "/businessCooperation_:lang.html"],
  (req, res, next) => {
    req.query.helpType = '5';
    next();
  },
  generalFun.getDataForWebHelpCenter
);

// 我要投稿
// router.get(["/contribute.html", "/contribute_:lang.html"],
//   async (req, res) => {
//     let helpLang = req.params.lang == 'zh-TW' ? '3' : '1';
//     const helpModel = await HelpCenterBiz.getHelpByType('6', helpLang);
//     res.send(helpModel.comments);
//   }
// );

router.get(["/contribute.html", "/contribute_:lang.html"],
  (req, res, next) => {
    req.query.helpType = '6';
    next();
  },
  generalFun.getDataForWebHelpCenter
);

// 服务条款
router.get(["/termsOfService.html", "/termsOfService_:lang.html"],
  (req, res, next) => {
    req.query.helpType = '7';
    next();
  },
  generalFun.getDataForWebHelpCenter
);

// 隐私条款
router.get(["/disclaimer.html", "/disclaimer_:lang.html"],
  (req, res, next) => {
    req.query.helpType = '8';
    next();
  },
  generalFun.getDataForWebHelpCenter
);

// 用户协议
router.get(["/userAgreement.html", "/userAgreement_:lang.html"],
  (req, res, next) => {
    req.query.helpType = '10';
    next();
  },
  generalFun.getDataForWebHelpCenter
);

// 隐私服务政策
router.get(["/privacyServicePolicy.html", "/privacyServicePolicy_:lang.html"],
  (req, res, next) => {
    req.query.helpType = '11';
    next();
  },
  generalFun.getDataForWebHelpCenter
);

// 钱包帮助中心
router.get(["/walletHelpCenter.html", "/walletHelpCenter_:lang.html"],
  (req, res, next) => {
    req.query.helpType = '1';
    next();
  },
  generalFun.getDataForHelpCenter
);

// 钱包帮助中心(android)
router.get(["/androidWalletHelpCenter.html", "/androidWalletHelpCenter_:lang.html"],
  (req, res, next) => {
    req.query.helpType = '12';
    next();
  },
  generalFun.getDataForHelpCenter
);


// 邀请注册
router.get("/invitation.html",
  async (req, res, next) => {
      let uid = req.query.uid;
      if (!shortid.isValid(uid)) {
        res.redirect("/");
      } else {
        try {
          let targetUser = await User.getOneUserByParams({
            _id: uid
          });
          if (_.isEmpty(targetUser)) {
            res.redirect("/");
          } else {
            next();
          }
        } catch (error) {
          res.redirect("/");
        }
      }
    },
    generalFun.getDataForInvitation
);

router.get(
  "/page/:current.html",
  (req, res, next) => {
    req.query.current = req.params.current;
    next();
  },
  generalFun.getDataForIndexPage
);

// 内容详情入口
router.get(
  "/details/:id.html",
  (req, res, next) => {
    let contentId = req.params.id;
    if (contentId) {
      if (!shortid.isValid(contentId)) {
        res.redirect("/");
      } else {
        req.query.id = contentId;
        next();
      }
    } else {
      res.redirect("/");
    }
  },
  generalFun.getDataForContentDetails
);

// 课程详情入口
router.get("/classDetails/:id.html",
  (req, res, next) => {
    let contentId = req.params.id;
    if (contentId) {
      if (!shortid.isValid(contentId)) {
        res.redirect("/");
      } else {
        req.query.classId = contentId;
        next();
      }
    } else {
      res.redirect("/");
    }
  },
  generalFun.getDataForClassDetails
);

// 类别入口
// router.get(
//   [
//     "/:cate1?___:typeId?",
//     "/:cate1?___:typeId?/:current.html?",
//     "/:cate0/:cate1?___:typeId?",
//     "/:cate0/:cate1?___:typeId?/:current.html?"
//   ],
//   (req, res, next) => {
//     let typeId = req.params.typeId;
//     let cate1 = req.params.cate1;
//     let current = req.params.current;
//     if (typeId) {
//       if (!shortid.isValid(typeId)) {
//         res.redirect("/");
//       } else {
//         req.query.typeId = typeId;
//         if (current) {
//           if (validator.isNumeric(current)) {
//             req.query.current = current;
//             next();
//           } else {
//             res.redirect("/");
//           }
//         } else {
//           next();
//         }
//       }
//     } else {
//       res.redirect("/");
//     }
//   },
//   generalFun.getDataForCatePage
// );


router.get(
  ["/keyword/:keyword", "/keyword/:keyword/:current.html"],
  (req, res, next) => {
    let keyword = req.params.keyword;
    let current = req.params.current;
    if (keyword) {
      req.query.keyword = keyword;
      if (current) {
        if (validator.isNumeric(current)) {
          req.query.current = current;
          next();
        } else {
          res.redirect("/");
        }
      } else {
        next();
      }
    } else {
      res.redirect("/");
    }
  },
  generalFun.getDataForSearchPage
);

router.get(
  ["/search/:searchkey", "/search/:searchkey/:current.html"],
  (req, res, next) => {
    let searchkey = req.params.searchkey;
    let current = req.params.current;
    if (searchkey) {
      req.query.searchkey = searchkey;
      if (current) {
        if (validator.isNumeric(current)) {
          req.query.current = current;
          next();
        } else {
          res.redirect("/");
        }
      } else {
        next();
      }
    } else {
      res.redirect("/");
    }
  },
  generalFun.getDataForSearchPage
);

router.get(
  ["/tag/:tagName", "/tag/:tagName/:current.html"],
  (req, res, next) => {
    let tagName = req.params.tagName;
    let current = req.params.current;
    if (tagName) {
      req.query.tagName = tagName;
      if (current) {
        if (validator.isNumeric(current)) {
          req.query.current = current;
          next();
        } else {
          res.redirect("/");
        }
      } else {
        next();
      }
    } else {
      res.redirect("/");
    }
  },
  generalFun.getDataForSearchPage
);

// 管理员登录
router.get(
  "/dr-admin",
  (req, res, next) => {
    if (req.session.adminlogined) {
      res.redirect("/manage");
    } else {
      next();
    }
  },
  generalFun.getDataForAdminUserLogin
);
module.exports = router;