/**
 * Created by Administrator on 2018/11/23.
 * 用户行为记录
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');
var Content = require('./Content');
var CommunityContent = require('./CommunityContent');
var Message = require('./Message');
var ClassInfo = require('./ClassInfo');


var UserActionHisSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    target_content: {
        type: Schema.Types.Mixed
    }, // 发布普通文章或专题
    status: {
        type: Number,
        default: 1
    }, // 针对邀请用户的状态
    action: String, // 行为类别
    datetime: {
        type: Date,
        default: Date.now
    }, // 创作
    updatetime: {
        type: Date,
        default: Date.now
    }, // 更新时间
    userid: {
        type: String
    }, // 成员
    // flag: { type: String, default: '1' } // 是否有效 1有效 0无效
});

UserActionHisSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
UserActionHisSchema.set('toObject', {
    getters: true,
    virtuals: true
});

UserActionHisSchema.path('datetime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});
UserActionHisSchema.path('updatetime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var UserActionHis = mongoose.model("UserActionHis", UserActionHisSchema);

module.exports = UserActionHis;