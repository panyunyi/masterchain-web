/**
 * Created by Administrator on 2015/4/15.
 * 意见反馈对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');
var AdminUser = require('./AdminUser');


var FeedBackSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    name: String,
    phoneNum: String, // 联系方式
    state: {
        type: Boolean,
        default: false
    }, // 反馈状态
    time: {
        type: Date,
        default: Date.now
    }, //反馈时间
    updateTime: {
        type: Date,
        default: Date.now
    },
    user: {
        type: String,
        ref: 'User'
    }, // 反馈人
    updateUser: {
        type: String,
        ref: 'AdminUser'
    }, // 审核操作人
    comments: String // 反馈内容
});

FeedBackSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
FeedBackSchema.set('toObject', {
    getters: true,
    virtuals: true
});

FeedBackSchema.path('time').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});
FeedBackSchema.path('updateTime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var FeedBack = mongoose.model("FeedBack", FeedBackSchema);

module.exports = FeedBack;