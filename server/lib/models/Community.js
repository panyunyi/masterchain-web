/**
 * Created by Administrator on 2015/4/15.
 * 社群对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');
var CommunityTag = require('./CommunityTag');

var CommunitySchema = new Schema({
    _id: {
        type: String,

        'default': shortid.generate
    },
    name: String, // 社群名称
    sImg: {
        type: String,
        default: "/upload/images/defaultImg.jpg"
    }, // 社群logo
    date: {
        type: Date,
        default: Date.now
    }, // 创建时间
    recommend: {
        type: Boolean,
        default: false
    }, // 是否推荐
    tags: [{
        type: String,
        ref: 'CommunityTag'
    }], // 社群标签
    creator: {
        type: String,
        ref: 'User'
    }, // 创建者
    comments: String,
    open: {
        type: String,
        default: '0'
    }, // 1公开 0私有
    needAuth: {
        type: String,
        default: '0'
    }, // 0不需要认证 1需要认证
    questions: {
        type: Schema.Types.Mixed
    }, // 进群问题和答案{'question':'你喜欢的宠物?','answer':'海豚'}
    state: {
        type: String,
        default: '0'
    }, // 0不显示 1显示
});

CommunitySchema.index({
    tags: 1,
    creator: 1
}); // 添加索引

CommunitySchema.set('toJSON', {
    getters: true,
    virtuals: true
});
CommunitySchema.set('toObject', {
    getters: true,
    virtuals: true
});

CommunitySchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var Community = mongoose.model("Community", CommunitySchema);

module.exports = Community;