/**
 * Created by Administrator on 2018/11/29.
 * 课程订阅 记录
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');
const uuidv1 = require('uuid/v1');
var ClassInfo = require('./ClassInfo');
var ClassContent = require('./ClassContent');

var ClassPayRecordSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    class: { type: String, ref: 'ClassInfo' }, // 付款课程Id
    money: { type: Number, default: 0 }, // 付款额度
    uuid: { type: String, default: uuidv1().split('-').join('') },
    date: { type: Date, default: Date.now },
    user: { type: String, ref: 'User' }, // 付款者
    state: { type: Boolean, default: false }, // 付款状态
    learningProgress: [{ type: String, ref: "ClassContent" }], // 学习进度，已学章节
    comments: String // 备注
});


ClassPayRecordSchema.index({ user: 1 });

ClassPayRecordSchema.set('toJSON', { getters: true, virtuals: true });
ClassPayRecordSchema.set('toObject', { getters: true, virtuals: true });

ClassPayRecordSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var ClassPayRecord = mongoose.model("ClassPayRecord", ClassPayRecordSchema);

module.exports = ClassPayRecord;

