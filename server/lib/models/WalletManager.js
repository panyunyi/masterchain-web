/**
 * Created by Administrator 2018年11月25日14:41:24.
 * 给用户随机分配的钱包地址对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
const User = require('./User')
var WalletManagerSchema = new Schema({
    _id: Schema.Types.ObjectId,
    walletaddress: String,
    walletpath: String,
    password: { type: String },
    createtime: { type: Date, default: Date.now },
    updatetime: { type: Date, default: Date.now },
    userid: { type: String, ref: 'User' }
});

WalletManagerSchema.set('toJSON', { getters: true, virtuals: true });
WalletManagerSchema.set('toObject', { getters: true, virtuals: true });

WalletManagerSchema.path('createtime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});
WalletManagerSchema.path('updatetime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var WalletManager = mongoose.model("WalletManager", WalletManagerSchema);

module.exports = WalletManager;

