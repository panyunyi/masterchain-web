/**
 * Created by Administrator on 2015/4/15.
 * 收支记录
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');
var UserActionHis = require('./UserActionHis');
var Content = require('./Content');
var Special = require('./Special');
var Community = require('./Community');
var CommunityContent = require('./CommunityContent');
var CommunityMessage = require('./CommunityMessage');
var Message = require('./Message');
var ClassInfo = require('./ClassInfo');
const uuidv1 = require('uuid/v1');


var BillRecordSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    type: String, // 操作类型
    action: {
        type: String,
        ref: "UserActionHis"
    }, // 用户操作
    coins: {
        type: Number,
        default: 0
    },
    unit: {
        type: String,
        enum: ['MEC', 'MVPC', 'MBT']
    }, // 价格单位 MEC/MVPC/MBT
    date: {
        type: Date,
        default: Date.now
    },
    updatetime: { // 审核时间
        type: Date
    },
    user: {
        type: String,
        ref: 'User'
    }, // 成员
    logs: String, // 账单备注
    transferInfo: {
        type: Schema.Types.Mixed
    }, // 转账流水号
    iapComments: String, // iap内购备注
    orderId: String, // 订单号
    target_class: {
        type: String,
        ref: "ClassInfo"
    }, // 发布普通文章或专题
    target_content: {
        type: String,
        ref: "Content"
    }, // 发布普通文章或专题
    target_message: {
        type: String,
        ref: "Message"
    }, // 发表评论
    target_communityContent: {
        type: String,
        ref: "CommunityContent"
    }, // 发布社群帖子
    target_communityMessage: {
        type: String,
        ref: "CommunityMessage"
    }, // 发布社群帖子
    target_user: {
        type: String,
        ref: 'User'
    }, // 被邀请的人，关注大师
    target_community: {
        type: String,
        ref: 'Community'
    }, // 关注社群
    target_special: {
        type: String,
        ref: 'Special'
    }, // 关注专题
    uuid: {
        type: String,
        default: uuidv1().split('-').join('')
    },
    state: {
        type: String,
        default: '0'
    }, // 针对提币审核状态, 0为待审核 1为审核通过 2为审核不通过
    // 针对邀请 0邀请成功但不计算币，1邀请成功算币
    dismissReason: String, // 驳回原因(针对审核不通过)

});

BillRecordSchema.index({
    target_communityContent: 1,
    target_content: 1,
    type: 1,
    user: 1,
    unit: 1,
}); // 添加索引

BillRecordSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
BillRecordSchema.set('toObject', {
    getters: true,
    virtuals: true
});

BillRecordSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

BillRecordSchema.path('updatetime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var BillRecord = mongoose.model("BillRecord", BillRecordSchema);

module.exports = BillRecord;