/**
 * Created by Administrator on 2015/4/15.
 * VIP套餐对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');


var VipSetMealSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    time: String, // 套餐时间（默认月数）
    coins: { type: Number, default: 0 },
    unit: { type: String, default: 'MEC', enum: ['MEC', 'MVPC', 'MBT'] }, // 价格单位 MEC/MVPC/MBT
    state: { type: Boolean, default: false }, // 是否有效
    default: { type: Boolean, default: false }, // 默认选择
    date: { type: Date, default: Date.now },
    comments: String
});

VipSetMealSchema.set('toJSON', { getters: true, virtuals: true });
VipSetMealSchema.set('toObject', { getters: true, virtuals: true });

VipSetMealSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var VipSetMeal = mongoose.model("VipSetMeal", VipSetMealSchema);

module.exports = VipSetMeal;

