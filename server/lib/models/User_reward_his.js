/**
 * Created by Administrator on 2015/4/15.
 * 用户奖励信息快照
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');


var UserRewardHisSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    userid: String, // 用户ID
    datetime: {
        type: Date,
        default: Date.now
    },
    EV: Number, //用户奖励时EV值
    MI: Number, //用户奖励时MI值
    rewardcontentmstt: {
        type: String,
    }, //内容奖励MSTT
    rewardactionmstt: {
        type: String,
    }, //行为奖励MSTT
    rewardreviewermstt: {
        type: String,
    }, //审核奖励MSTT
    rewardtotalmstt: {
        type: String,
    }, //总奖励MSTT
    rewardcontentnum: {
        type: Number,
    }, //内容奖励MSTT
    rewardactionnum: {
        type: Number,
    }, //行为奖励MSTT
    rewardreviewernum: {
        type: Number,
    }, //审核奖励MSTT
    rewardtotalnum: {
        type: Number,
    }, //总奖励MSTT
    rewardid: Schema.Types.ObjectId, //审核奖励MSTT
});

UserRewardHisSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
UserRewardHisSchema.set('toObject', {
    getters: true,
    virtuals: true
});

UserRewardHisSchema.path('datetime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});



var UserRewardHis = mongoose.model("UserRewardHis", UserRewardHisSchema);

module.exports = UserRewardHis;