/**
 * Created by Administrator on 2018/11/24.
 * 课程评分模块
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');
var ClassInfo = require('./ClassInfo');


var ClassScoreSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    score: { type: Number, default: 0.0 },
    date: { type: Date, default: Date.now },
    class: { type: String, ref: 'ClassInfo' }, // 关联的课程
    user: { type: String, ref: 'User' }, // 评论人员
    hadScore: { type: Boolean, default: false }, // 评论人员
    comments: String // 评论
});

ClassScoreSchema.index({ class: 1 });

ClassScoreSchema.set('toJSON', { getters: true, virtuals: true });
ClassScoreSchema.set('toObject', { getters: true, virtuals: true });

ClassScoreSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var ClassScore = mongoose.model("ClassScore", ClassScoreSchema);

module.exports = ClassScore;

