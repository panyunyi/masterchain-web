/**
 * Created by Administrator on 2015/4/15.
 * 用户信息快照
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')


var UserMipHisSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    user_id: String, //用户id
    datetime: { type: Date, default: Date.now }, // 指数计算时间
    MP: Schema.Types.Decimal128,//MP值
    MI: Schema.Types.Decimal128 //MI值
});

UserMipHisSchema.set('toJSON', { getters: true, virtuals: true });
UserMipHisSchema.set('toObject', { getters: true, virtuals: true });

UserMipHisSchema.path('datetime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var UserMipHis = mongoose.model("UserMipHis", UserMipHisSchema);

module.exports = UserMipHis;

