/**
 * Created by Administrator on 2015/4/15.
 * 大师类别对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
// var User = require('./User');


var MasterCategorySchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    name: String,
    date: {
        type: Date,
        default: Date.now
    },
    sImg: {
        type: String
    },
    sort: {
        type: Number,
        default: 0
    },
    comments: String
});

MasterCategorySchema.set('toJSON', {
    getters: true,
    virtuals: true
});
MasterCategorySchema.set('toObject', {
    getters: true,
    virtuals: true
});

MasterCategorySchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var MasterCategory = mongoose.model("MasterCategory", MasterCategorySchema);

module.exports = MasterCategory;