/**
 * Created by Administrator on 2015/4/15.
 * 用户EV快照
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');


var User_ev_hisSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    user_id: String, // 用户ID
    datetime: { type: Date, default: Date.now }, //指数计算时间
    EV: Schema.Types.Decimal128 // EV 值
});

User_ev_hisSchema.set('toJSON', { getters: true, virtuals: true });
User_ev_hisSchema.set('toObject', { getters: true, virtuals: true });

User_ev_hisSchema.path('datetime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var User_ev_his = mongoose.model("User_ev_his", User_ev_hisSchema);

module.exports = User_ev_his;

