/**
 * Created by Administrator on 2017/5/19.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var shortid = require('shortid');
var moment = require('moment')
var User = require('./User');
var MasterCategory = require('./MasterCategory');
var ContentCategory = require('./ContentCategory');
var Community = require('./Community');
var CommunityContent = require('./CommunityContent');
var CommunityMessage = require('./CommunityMessage');
var WalletAddress = require('./WalletAddress');
var Content = require('./Content');
var Message = require('./Message');
var Special = require('./Special');
var ContentTag = require('./ContentTag');


var UserSchema = new Schema({
    _id: {
        type: String,

        'default': shortid.generate
    },
    enable: {
        type: Boolean,
        default: true
    }, //用户是否有效
    creativeRight: {
        type: Boolean,
        default: false
    }, //用户是否有创作权限
    authState: {
        type: Boolean,
        default: false
    }, //用户是已进行身份认证
    masterAuthState: {
        type: Boolean,
        default: false
    }, //用户是已进行大师认证
    vip: {
        type: Boolean,
        default: false
    }, //用户是否是VIP用户
    name: String,
    userName: String,
    password: String,
    fundPassword: String, // 资金密码 6位数字加密
    email: String,
    qq: Number,
    phoneNum: String,
    countryCode: {
        type: String
    }, // 手机号前国家代码
    idNo: Number,
    idType: {
        type: String,
        default: '1'
    }, // 证件类型 1为身份证
    comments: {
        type: String,
        default: ""
    },
    introduction: {
        type: String,
        default: ""
    }, // 个人简介
    position: String, // 职位
    profession: String, // 职业
    industry: String, // 行业
    experience: String, // 教育经历
    company: String, // 大学或公司
    website: String, // 个人站点
    date: {
        type: Date,
        default: Date.now
    },
    logo: {
        type: String,
        default: "https://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1547021000228.png"
    },
    group: {
        type: String,
        default: "0"
    }, // 0 普通用户 1 大师
    province: String, // 所在省份
    city: String, // 所在城市
    birth: {
        type: Date,
        default: new Date('1770-01-01')
    }, // 出生年月日 2018-03-21
    gender: {
        type: String,
        default: '0'
    }, // 性别 0男 1女
    despises: [{
        type: String,
        ref: 'Content'
    }], // 👎文章或帖子
    despiseMessage: [{
        type: String,
        ref: 'Message'
    }], // 👎评论
    despiseCommunityContent: [{
        type: String,
        ref: 'CommunityContent'
    }], // 👎评论
    despiseCommunityMessage: [{
        type: String,
        ref: 'CommunityMessage'
    }], // 👎评论
    favorites: [{
        type: String,
        ref: 'Content'
    }], // 收藏文章或帖子
    favoriteCommunityContent: [{
        type: String,
        ref: 'CommunityContent'
    }], // 收藏社群内容
    praiseContents: [{
        type: String,
        ref: 'Content'
    }], // 点赞的文章或帖子
    praiseMessages: [{
        type: String,
        ref: 'Message'
    }], // 点赞的评论
    praiseCommunityContent: [{
        type: String,
        ref: 'CommunityContent'
    }], // 点赞的社群帖子
    praiseCommunityMessage: [{
        type: String,
        ref: 'CommunityMessage'
    }], // 点赞的社群评论
    walletAddress: {
        type: String
    }, // 系统分配的钱包
    walletAddressPassword: {
        type: String
    }, // 系统分配的钱包密码
    category: [{
        type: String,
        ref: 'MasterCategory'
    }], // 大师类别
    followers: [{
        type: String,
        ref: 'User'
    }], // 关注我的大师
    watchers: [{
        type: String,
        ref: 'User'
    }], // 我关注的大师
    watchSpecials: [{
        type: String,
        ref: 'Special'
    }], // 我关注的专题
    watchCommunity: [{
        type: String,
        ref: 'Community'
    }], // 我关注的社群
    watchCommunityState: [{
        accessDate: {
            type: String,
            // default: new Date,
            // get: v => moment(v).utcOffset(8).format('YYYY-MM-DD HH:mm:ss')
        },
        joinDate: {
            type: String,
            // default: new Date,
            // get: v => moment(v).utcOffset(8).format('YYYY-MM-DD HH:mm:ss')
        },
        refuseDate: {
            type: String,
            // default: new Date,
            // get: v => moment(v).utcOffset(8).format('YYYY-MM-DD HH:mm:ss')
        },
        community: {
            type: String,
            ref: 'Community'
        },
        state: {
            type: String,
            default: '1'
        },
        questions: {
            type: Schema.Types.Mixed
        },
    }], // 关注社群加入的状态 0:待审核 1:已加入 2:拒绝加入
    lastCommunitySpeach: {
        type: Date
    }, // 最后发言时间
    watchTags: [{
        type: String,
        ref: 'ContentTag'
    }], // 我关注的标签
    retrieve_time: {
        type: Number
    }, // 用户发送激活请求的时间
    loginActive: {
        type: Boolean,
        default: false
    }, // 首次登录
    deviceId: String, // 针对游客的设备id
});

UserSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
UserSchema.set('toObject', {
    getters: true,
    virtuals: true
});

UserSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

UserSchema.path('lastCommunitySpeach').get(function (v) {
    if (v) {
        return moment(v).format("YYYY-MM-DD HH:mm:ss");
    } else {
        return '';
    }
});

UserSchema.path('birth').get(function (v) {
    return moment(v).format("YYYY-MM-DD");
});

var User = mongoose.model("User", UserSchema);


module.exports = User;