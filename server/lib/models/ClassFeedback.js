/**
 * Created by Administrator on 2015/4/15.
 *  课程反馈
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var settings = require('../../../configs/settings');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');
var ClassInfo = require('./ClassInfo');

var ClassFeedbackSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    classContent: {
        type: String,
        ref: 'ClassInfo'
    }, // 反馈对应的内容ID
    author: {
        type: String,
        ref: 'User'
    },
    replyAuthor: {
        type: String,
        ref: 'User'
    },
    relationMsgId: String, // 关联的反馈Id
    date: { type: Date, default: Date.now }, // 反馈时间
    praiseNum: { type: Number, default: 0 }, // 被赞次数
    hasPraise: { type: Boolean, default: false }, //  当前是否已被点赞
    praiseMembers: String, // 点赞用户id集合
    content: { type: String, default: "输入评论内容..." }// 反馈内容
});


ClassFeedbackSchema.set('toJSON', { getters: true, virtuals: true });
ClassFeedbackSchema.set('toObject', { getters: true, virtuals: true });

ClassFeedbackSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var ClassFeedback = mongoose.model("ClassFeedback", ClassFeedbackSchema);

module.exports = ClassFeedback;

