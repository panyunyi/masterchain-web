/**
 * Created by Administrator on 2015/4/15.
 * MSTT 快照
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')


var Mstt_bookSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    reward_MSTT: String, // 新增内容奖励MSTT
    date: { type: Date, default: Date.now },
    reward_action: String //新增行为奖励MSTT
});

Mstt_bookSchema.set('toJSON', { getters: true, virtuals: true });
Mstt_bookSchema.set('toObject', { getters: true, virtuals: true });

Mstt_bookSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var Mstt_book = mongoose.model("Mstt_book", Mstt_bookSchema);

module.exports = Mstt_book;

