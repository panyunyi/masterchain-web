/**
 * Created by Administrator on 2015/4/15.
 * 专题对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');
var AdminUser = require('./AdminUser');
var ContentCategory = require('./ContentCategory');


var SpecialSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    name: String,
    sImg: {
        type: String,
        default: "/upload/images/defaultImg.jpg"
    },
    date: {
        type: Date,
        default: Date.now
    },
    category: [{
        type: String,
        ref: 'ContentCategory'
    }], // 所属分类
    creator: {
        type: String,
        ref: "User"
    }, // 创建者
    adminCreator: {
        type: String,
        ref: "AdminUser"
    }, // 管理员创建者
    approver: {
        type: String,
        ref: "AdminUser"
    }, // 审核人
    comments: String,
    state: {
        type: String,
        default: '0'
    }, // 0不显示 1显示
});

SpecialSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
SpecialSchema.set('toObject', {
    getters: true,
    virtuals: true
});

SpecialSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var Special = mongoose.model("Special", SpecialSchema);

module.exports = Special;