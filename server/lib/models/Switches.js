/**
 * Created by Administrator on 2017/4/15.
 * 广告管理
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')

var User = require('./User');
var SwitchesSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    // open: {
    //     type: Boolean,
    //     default: false
    // },
    commentOrReply: {
        type: Boolean,
        default: false
    }, // 评论或回复我
    follow: {
        type: Boolean,
        default: false
    }, // 关注我
    followCommunity: {
        type: Boolean,
        default: false
    }, // 关注了我的社群
    followSpecial: {
        type: Boolean,
        default: false
    }, // 关注了我的专题
    thumbsUpComments: {
        type: Boolean,
        default: false
    }, // 赞同回答
    thumbsUpContent: {
        type: Boolean,
        default: false
    }, // 点赞文章
    appreciateContent: {
        type: Boolean,
        default: false
    }, // 打赏文章
    subscribeClass: {
        type: Boolean,
        default: false
    }, // 购买课程
    followMasterClass: {
        type: Boolean,
        default: false
    }, // 关注的大师发布了课程
    followMasterContent: {
        type: Boolean,
        default: false
    }, // 关注的大师发布了新文章

    date: {
        type: Date,
        default: Date.now
    },
    user: {
        type: String,
        ref: 'User'
    }, // 关联用户
    comments: String, // 描述
});



SwitchesSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
SwitchesSchema.set('toObject', {
    getters: true,
    virtuals: true
});

SwitchesSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var Switches = mongoose.model("Switches", SwitchesSchema);
module.exports = Switches;