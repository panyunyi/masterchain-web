/**
 * Created by Administrator on 2015/4/15.
 * 审核快照
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')



var Review_hisSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    creative: String, // 创作内容
    datetime: { type: Date, default: Date.now }, // 审核时间
    reviewer: String, // 审核者
});

Review_hisSchema.set('toJSON', { getters: true, virtuals: true });
Review_hisSchema.set('toObject', { getters: true, virtuals: true });

Review_hisSchema.path('datetime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var Review_his = mongoose.model("Review_his", Review_hisSchema);

module.exports = Review_his;

