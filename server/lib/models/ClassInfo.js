/**
 * Created by Administrator on 2015/4/15.
 * 大师课基础信息
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');
var ClassType = require('./ClassType');


var ClassInfoSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    name: String, // 课程名
    buyTips: String, // 购买须知
    type: {
        type: String,
        default: "0"
    }, // 课程类型 0 文字，1、视频，2、音频
    sImg: {
        type: String,
        default: "/upload/images/defaultImg.jpg"
    }, // 课程封面
    recommend: {
        type: Boolean,
        default: false
    }, // 推荐
    discount: {
        type: Boolean,
        default: false
    }, // 是否优惠
    boutique: {
        type: Boolean,
        default: false
    }, // 是否是精品课程
    author: {
        type: String,
        ref: 'User'
    }, // 关联作者
    catalog_num: {
        type: Number,
        default: 0
    }, // 课程总章数
    priceType: {
        type: String,
        default: '0'
    }, // 0 免费 1 会员免费 2会员付费
    price: {
        type: Number,
        default: 0
    }, // price
    vipPrice: {
        type: Number,
        default: 0
    }, // vip price
    unit: {
        type: String,
        enum: ['MEC', 'MVPC', 'MBT']
    }, // 价格单位 MEC/MVPC/MBT
    date: {
        type: Date,
        default: Date.now
    },
    updateDate: {
        type: Date,
        default: Date.now
    }, // 更新日期
    auditDate: {
        type: Date,
        default: Date.now
    }, // 审核日期
    categories: [{
        type: String,
        ref: 'ClassType'
    }], //课程类别
    comments: String, // 课程简介
    state: {
        type: String,
        default: '0'
    }, // 0草稿 1待审核 2审核通过
});

ClassInfoSchema.index({
    author: 1
});

ClassInfoSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
ClassInfoSchema.set('toObject', {
    getters: true,
    virtuals: true
});

ClassInfoSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});
ClassInfoSchema.path('updateDate').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});
ClassInfoSchema.path('auditDate').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});
var ClassInfo = mongoose.model("ClassInfo", ClassInfoSchema);

module.exports = ClassInfo;