/**
 * Created by Administrator on 2015/4/15.
 * 身份认证对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');


var IdentityAuthenticationSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    name: String,
    type: String, // 认证类型  1：身份认证  2：大师认证
    cardType: String, // 0 居民身份证 1护照 2其他有效证件
    areaType: String, // 0 大陆  1港澳 2台湾 3外籍
    idCardNo: String, // 证件号码
    cardFront: String, // 证件照正面图片
    cardBack: String, // 证件照反面图片
    comments: String, // 备注
    // 以下为大师认证相关
    authStep: {
        type: String,
        default: '0'
    }, // 针对大师认证的步骤记录
    masterCategory: '',
    diploma_label: '',
    diploma: {
        type: Schema.Types.Mixed
    }, // 学历证书
    professionalCertificate_label: '', // 专业证书
    professionalCertificate: {
        type: Schema.Types.Mixed
    }, // 专业证书
    otherCertificate_label: '', // 其它证书
    otherCertificate: {
        type: Schema.Types.Mixed
    }, // 其它证书
    pastExperience: {
        type: Schema.Types.Mixed
    }, // 过往经历
    pastArticles: {
        type: Schema.Types.Mixed
    }, // 过往文章
    publishedBooks: {
        type: Schema.Types.Mixed
    }, // 出刊书籍
    uploadAttachments: {
        type: Schema.Types.Mixed
    }, // 附件上传
    date: {
        type: Date,
        default: Date.now
    },
    updatetime: {
        type: Date,
        default: Date.now
    },
    user: {
        type: String,
        ref: 'User'
    }, // 申请人
    state: {
        type: String,
        default: '0'
    }, // 审核状态 0:待审核 1:审核通过 2:审核不通过
    dismissReason: String, // 驳回原因(针对审核不通过)
    approver: {
        type: String,
        ref: 'AdminUser'
    }, // 审批人
});

IdentityAuthenticationSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
IdentityAuthenticationSchema.set('toObject', {
    getters: true,
    virtuals: true
});

IdentityAuthenticationSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});
IdentityAuthenticationSchema.path('updatetime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var IdentityAuthentication = mongoose.model("IdentityAuthentication", IdentityAuthenticationSchema);

module.exports = IdentityAuthentication;