/**
 * Created by Administrator on 2015/4/15.
 * 交易信息
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')



var TransactionSchema = new Schema({
    _id: Schema.Types.ObjectId,
    hash: String,
    nonce: Number,
    blockHash: String,
    blockNumber: Number,
    transactionIndex: Number,
    from: String,
    to: String,
    value: Number,
    gasPrice: Number,
    gas: Number,
    input: String,
    creates: String,
    publicKey: String,
    raw: String,
    r: String,
    s: String,
    v: Number,
    commission: Number,
    gasUsed: Number,
    valueMstt: Number,
    nonceRaw: String,
    valueRaw: String,
    gasPriceRaw: String,
    gasRaw: String,
    blockNumberRaw: String,
    transactionIndexRaw: String,

});

TransactionSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
TransactionSchema.set('toObject', {
    getters: true,
    virtuals: true
});


var Transaction = mongoose.model("Transaction", TransactionSchema);

module.exports = Transaction;