/**
 * Created by Administrator on 2015/4/15.
 * 创作权限对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');
var ContentCategory = require('./ContentCategory');


var CreativeRightSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    newCateName: String, //申请类别名称
    category: { type: String, ref: 'ContentCategory' }, //申请类别大类
    type: { type: String, default: '0' }, // 0、创作权限 
    date: { type: Date, default: Date.now },
    updatetime: { type: Date, default: Date.now },
    user: { type: String, ref: 'User' }, // 申请人
    state: { type: String, default: '0' }, // 审核状态 0:待审核 1:审核通过 2:审核不通过
    dismissReason: String, // 驳回原因(针对审核不通过)
    approver: { type: String, ref: 'AdminUser' }, // 审批人
    comments: String // 申请原因
});

CreativeRightSchema.set('toJSON', { getters: true, virtuals: true });
CreativeRightSchema.set('toObject', { getters: true, virtuals: true });

CreativeRightSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});
CreativeRightSchema.path('updatetime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var CreativeRight = mongoose.model("CreativeRight", CreativeRightSchema);

module.exports = CreativeRight;

