/**
 * Created by Administrator on 2015/4/15.
 * 举报审核对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');
var AdminUser = require('./AdminUser');
const uuidv1 = require('uuid/v1');
var Content = require('./Content');
var CommunityContent = require('./CommunityContent');
var Message = require('./Message');
var CommunityMessage = require('./CommunityMessage');
var UserActionHis = require('./UserActionHis');

var ReportRecordSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    time: { type: Date, default: Date.now },
    updatetime: { type: Date, default: Date.now },
    user: { type: String, ref: 'User' }, // 成员
    approver: { type: String, ref: 'AdminUser' }, // 审批人
    comments: String,
    type: String, // 0普通帖子或专题  1、评论  2、社群帖子
    target_communityContent: { type: String, ref: "CommunityContent" }, // 社群帖子
    target_content: { type: String, ref: "Content" }, // 普通文章或专题
    target_message: { type: String, ref: "Message" }, // 评论
    target_communityMessage: { type: String, ref: "CommunityMessage" }, // 社群评论
    userAction: { type: String, ref: "UserActionHis" }, // 关联用户行为
    uuid: { type: String, default: uuidv1().split('-').join('') },
    state: { type: String, default: '0' }, // 审核状态  0为待审核 1为审核通过 2为审核不通过
    dismissReason: String, // 驳回原因(针对审核不通过)

});

ReportRecordSchema.set('toJSON', { getters: true, virtuals: true });
ReportRecordSchema.set('toObject', { getters: true, virtuals: true });

ReportRecordSchema.path('time').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});
ReportRecordSchema.path('updatetime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var ReportRecord = mongoose.model("ReportRecord", ReportRecordSchema);

module.exports = ReportRecord;

