/**
 * Created by Administrator on 2015/4/15.
 * 充值金额对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');


var ResetAmountSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    money: { type: Number, default: 0 },
    default: { type: Boolean, default: false },
    unit: { type: String, enum: ['MEC', 'MVPC', 'MBT'] }, // 价格单位 MEC/MVPC/MBT
    date: { type: Date, default: Date.now }
});

ResetAmountSchema.set('toJSON', { getters: true, virtuals: true });
ResetAmountSchema.set('toObject', { getters: true, virtuals: true });

ResetAmountSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var ResetAmount = mongoose.model("ResetAmount", ResetAmountSchema);

module.exports = ResetAmount;

