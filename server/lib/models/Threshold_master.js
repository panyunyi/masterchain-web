/**
 * Created by Administrator on 2015/4/15.
 * 阈值快照	
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')


var Threshold_masterSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    threshold: Schema.Types.Decimal128, //MP计算用阈值
    date: { type: Date, default: Date.now }
});

Threshold_masterSchema.set('toJSON', { getters: true, virtuals: true });
Threshold_masterSchema.set('toObject', { getters: true, virtuals: true });

Threshold_masterSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var Threshold_master = mongoose.model("Threshold_master", Threshold_masterSchema);

module.exports = Threshold_master;

