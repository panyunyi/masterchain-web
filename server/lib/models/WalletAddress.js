/**
 * Created by Administrator 2018年11月25日14:41:24.
 * 钱包地址对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
const User = require('./User')
var WalletAddressSchema = new Schema({
    _id: {
        type: String,

        'default': shortid.generate
    },
    wallet: String,
    user: { type: String, ref: 'User' },
    date: { type: Date, default: Date.now },
    comments: String
});

WalletAddressSchema.index({ wallet: 1 }); // 添加索引

WalletAddressSchema.set('toJSON', { getters: true, virtuals: true });
WalletAddressSchema.set('toObject', { getters: true, virtuals: true });

WalletAddressSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var WalletAddress = mongoose.model("WalletAddress", WalletAddressSchema);

module.exports = WalletAddress;

