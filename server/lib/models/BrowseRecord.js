/**
 * Created by Administrator on 2019年03月13日19:03:23.
 * 浏览记录对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var User = require('./User');
var AdminUser = require('./AdminUser');
var Content = require('./Content');
var moment = require('moment')

var BrowseRecordSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    target: {
        type: String,
        ref: 'Content'
    }, // 目标的ID
    user: {
        type: String,
        ref: 'User'
    }, // 浏览者
    date: {
        type: Date,
        default: Date.now
    }
});


BrowseRecordSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
BrowseRecordSchema.set('toObject', {
    getters: true,
    virtuals: true
});

BrowseRecordSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var BrowseRecord = mongoose.model("BrowseRecord", BrowseRecordSchema);

module.exports = BrowseRecord;