/**
 * Created by Administrator on 2015/4/15.
 * 大师课程内容
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');
var ClassInfo = require('./ClassInfo');


var ClassContentSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    catalogIndex: {
        type: String,
        default: '0'
    }, // 目录索引
    catalog: {
        type: String,
        default: ''
    }, // 目录名称
    duration: {
        type: String,
        default: '0.01'
    }, // 时长
    size: {
        type: String,
        default: '0.01'
    }, // 文件大小
    basicInfo: {
        type: String,
        ref: "ClassInfo"
    }, // 基础信息
    date: {
        type: Date,
        default: Date.now
    }, // 发布日期
    updateDate: {
        type: Date,
        default: Date.now
    }, // 更新日期
    auditDate: {
        type: Date,
        default: Date.now
    }, // 审核日期
    author: {
        type: String,
        ref: 'User'
    }, // 作者
    appShowType: {
        type: String,
        default: '2'
    }, // app端排版格式 2图文 3视频 4音频
    simpleComments: String, //带格式的纯文本
    imageArr: [{
        type: String
    }], // 媒体集合（图片）
    videoArr: [{
        type: String
    }], // 媒体集合（影片）
    audioArr: [{
        type: String
    }], // 媒体集合（音频）
    description: String, // 课程简介
    comments: String, // 课程内容
    state: {
        type: String,
        default: '0'
    }, // 0草稿 1待审核 2审核通过
    free: {
        type: Boolean,
        default: false
    } // 此章节是否免费
});

ClassContentSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
ClassContentSchema.set('toObject', {
    getters: true,
    virtuals: true
});

ClassContentSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});
ClassContentSchema.path('updateDate').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});
ClassContentSchema.path('auditDate').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var ClassContent = mongoose.model("ClassContent", ClassContentSchema);

module.exports = ClassContent;