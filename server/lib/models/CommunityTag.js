/**
 * Created by Administrator on 2015/4/15.
 * 社群标签对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')

var CommunityTagSchema = new Schema({
    _id: {
        type: String,

        'default': shortid.generate
    },
    name: String,
    sImg: {
        type: String,
        default: "/upload/images/defaultImg.jpg"
    }, // 文章小图
    date: {
        type: Date,
        default: Date.now
    },
    sort: {
        type: Number,
        default: 0
    },
    comments: String
});

CommunityTagSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
CommunityTagSchema.set('toObject', {
    getters: true,
    virtuals: true
});

CommunityTagSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var CommunityTag = mongoose.model("CommunityTag", CommunityTagSchema);

module.exports = CommunityTag;