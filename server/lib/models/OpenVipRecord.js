/**
 * Created by Administrator on 2015/4/15.
 * VIP开通记录对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');
var VipSetMeal = require('./VipSetMeal');


var OpenVipRecordSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    time: Number, // 月数
    type: {
        type: String,
        default: '0'
    }, //0：默认购买  1：延期
    setMeal: {
        type: String,
        ref: 'VipSetMeal'
    }, // 套餐
    unit: {
        type: String,
        enum: ['MEC', 'MVPC', 'MBT']
    }, // 价格单位 MEC/MVPC/MBT
    date: {
        type: Date,
        default: Date.now
    },
    user: {
        type: String,
        ref: 'User'
    }, // 开通者
    comments: String // 开通详情
});

OpenVipRecordSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
OpenVipRecordSchema.set('toObject', {
    getters: true,
    virtuals: true
});

OpenVipRecordSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var OpenVipRecord = mongoose.model("OpenVipRecord", OpenVipRecordSchema);

module.exports = OpenVipRecord;