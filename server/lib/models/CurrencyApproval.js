/**
 * Created by Administrator on 2015/4/15.
 * 提币审批对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');
var AdminUser = require('./AdminUser');
var WalletAddress = require('./WalletAddress');
var BillRecord = require('./BillRecord');
const uuidv1 = require('uuid/v1');


var CurrencyApprovalSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },

    coins: Number, // 提币数额
    unit: { type: String, enum: ['MEC', 'MVPC', 'MBT'] }, // 价格单位 MEC/MVPC/MBT
    date: { type: Date, default: Date.now },
    updatetime: { type: Date, default: Date.now },
    billRecord: { type: String, ref: 'BillRecord' }, // 绑定的账单记录
    user: { type: String, ref: 'User' }, // 发起人
    approver: { type: String, ref: 'AdminUser' }, // 审批人
    comments: String,
    walletAddress: { type: String, ref: 'WalletAddress' }, //提币钱包
    state: { type: String, default: '0' }, // 审核状态  0为待审核 1为审核通过 2为审核不通过
    dismissReason: String, // 驳回原因(针对审核不通过)
    uuid: { type: String, default: uuidv1().split('-').join('') }
});

CurrencyApprovalSchema.set('toJSON', { getters: true, virtuals: true });
CurrencyApprovalSchema.set('toObject', { getters: true, virtuals: true });

CurrencyApprovalSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var CurrencyApproval = mongoose.model("CurrencyApproval", CurrencyApprovalSchema);

module.exports = CurrencyApproval;

