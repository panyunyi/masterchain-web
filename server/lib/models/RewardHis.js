/**
 * Created by Administrator on 2015/4/15.
 * 分配结果
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')


var RewardHisSchema = new Schema({
    _id: Schema.Types.ObjectId,
    mi: Schema.Types.Decimal128, //分配时总mi
    date: {
        type: Date,
        default: Date.now
    },
    datetime: {
        type: Date,
        default: Date.now
    }, // 运行时间
    datapointtime: {
        type: Date,
        default: Date.now
    }, // 运行时间
    starttime: {
        type: Date,
        default: Date.now
    }, // 运行时间
    endtime: {
        type: Date,
        default: Date.now
    }, // 运行时间
    ev: Schema.Types.Decimal128, // 分配时总ev
    mstt: Schema.Types.Decimal128, // 分配总量mstt
    result: Number // 分配结果
});

RewardHisSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
RewardHisSchema.set('toObject', {
    getters: true,
    virtuals: true
});

RewardHisSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});
RewardHisSchema.path('datetime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});
RewardHisSchema.path('datapointtime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});
RewardHisSchema.path('starttime').get(function (v) {
    return moment(v).format("YYYY-MM-DD");
});
RewardHisSchema.path('endtime').get(function (v) {
    return moment(v).format("YYYY-MM-DD");
});

var RewardHis = mongoose.model("RewardHis", RewardHisSchema);

module.exports = RewardHis;