/**
 * Created by Administrator on 2015/4/15.
 * 大师课类别维护
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')


var ClassTypeSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    name: String,
    date: {
        type: Date,
        default: Date.now
    },
    sort: {
        type: Number,
        default: 0
    },
    comments: String
});

ClassTypeSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
ClassTypeSchema.set('toObject', {
    getters: true,
    virtuals: true
});

ClassTypeSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var ClassType = mongoose.model("ClassType", ClassTypeSchema);

module.exports = ClassType;