/**
 * Created by Administrator on 2015/4/15.
 * 大师类别对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')

var IntegralonfigSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    login_first: {
        type: Number,
        default: 0
    }, // 首次登陆
    register: {
        type: Number,
        default: 0
    }, // 用户注册
    browse: {
        type: Number,
        default: 0
    }, // 浏览
    browse_limit: {
        type: Number,
        default: 0
    }, // 浏览限制
    give_thumbs_up: {
        type: Number,
        default: 0
    }, // 点赞
    give_thumbs_up_limit: {
        type: Number,
        default: 0
    }, // 点赞限制
    comment: {
        type: Number,
        default: 0
    }, // 评论
    comment_limit: {
        type: Number,
        default: 0
    }, // 评论限制
    community_comment: {
        type: Number,
        default: 0
    }, // 社群评论
    community_comment_limit: {
        type: Number,
        default: 0
    }, // 社群评论限制
    appreciate: {
        type: Number,
        default: 0
    }, // 打赏
    appreciate_limit: {
        type: Number,
        default: 0
    }, // 打赏限制
    appreciate_in: {
        type: Number,
        default: 0
    }, // 打赏收入
    appreciate_out: {
        type: Number,
        default: 0
    }, // 打赏支出
    collections: {
        type: Number,
        default: 0
    }, // 收藏
    collections_limit: {
        type: Number,
        default: 0
    }, // 收藏限制
    follow: {
        type: Number,
        default: 0
    }, // 关注
    follow_limit: {
        type: Number,
        default: 0
    }, // 关注限制
    forward: {
        type: Number,
        default: 0
    }, // 转发
    forward_limit: {
        type: Number,
        default: 0
    }, // 转发限制
    community_building: {
        type: Number,
        default: 0
    }, // 社群建设
    community_building_limit: {
        type: Number,
        default: 0
    }, // 社群建设限制
    invitation: {
        type: Number,
        default: 0
    }, // 邀请
    report: {
        type: Number,
        default: 0
    }, // 举报
    review: {
        type: Number,
        default: 0
    }, // 审核
    creat_content: {
        type: Number,
        default: 0
    }, // 内容创作
    creat_identity: {
        type: Number,
        default: 0
    }, // 实名认证
    shop_send: {
        type: Number,
        default: 0
    }, // 商城会员发放
    transfer_coins: {
        type: Number,
        default: 0
    }, // 提币
    recharge: {
        type: Number,
        default: 0
    }, // 充值
    subscribe: {
        type: Number,
        default: 0
    }, // 课程订购
    buyVip: {
        type: Number,
        default: 0
    }, // 购买会员
    signIn: {
        type: Number,
        default: 0
    }, // 签到
    signIn_continuity: {
        type: Number,
        default: 0
    }, // 连续签到7天
    brithday: {
        type: Number,
        default: 0
    }, // 生日
    date: {
        type: Date,
        default: Date.now
    },

});

IntegralonfigSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
IntegralonfigSchema.set('toObject', {
    getters: true,
    virtuals: true
});

IntegralonfigSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var Integralonfig = mongoose.model("Integralonfig", IntegralonfigSchema);

module.exports = Integralonfig;