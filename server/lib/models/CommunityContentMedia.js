/**
 * Created by Administrator on 2018/11/25.
 * 社群帖子媒体
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var CommunityContent = require('./CommunityContent');



var CommunityContentMediaSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    link: String, // 媒体链接
    date: { type: Date, default: Date.now },
    content: { type: String, ref: 'CommunityContent' }, // 关联帖子
    type: String, // 帖子类型（0:图片，1:视频）
    videoImg: String // 视频缩略图
});

CommunityContentMediaSchema.index({ content: 1 }); // 添加索引

CommunityContentMediaSchema.set('toJSON', { getters: true, virtuals: true });
CommunityContentMediaSchema.set('toObject', { getters: true, virtuals: true });

CommunityContentMediaSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var CommunityContentMedia = mongoose.model("CommunityContentMedia", CommunityContentMediaSchema);

module.exports = CommunityContentMedia;

