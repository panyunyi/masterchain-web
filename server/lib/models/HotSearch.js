/**
 * Created by Administrator on 2015/4/15.
 * 热搜关键词对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')


var HotSearchSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    word: String,
    date: {
        type: Date,
        default: Date.now
    },
    updatetime: {
        type: Date,
        default: Date.now
    },
    frequency: { // 频率
        type: Number,
        default: 0
    }
});

HotSearchSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
HotSearchSchema.set('toObject', {
    getters: true,
    virtuals: true
});

HotSearchSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});
HotSearchSchema.path('updatetime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var HotSearch = mongoose.model("HotSearch", HotSearchSchema);

module.exports = HotSearch;