/**
 * Created by Administrator on 2018/11/25.
 * 社群帖子
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var Community = require('./Community');
var CommunityContentMedia = require('./CommunityContentMedia');
var User = require('./User');


var CommunityContentSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    state: {
        type: String,
        // default: true
    }, // 是否显示 1.显示 0.不显示
    date: {
        type: Date,
        default: Date.now
    },
    community: {
        type: String,
        ref: 'Community'
    }, // 关联社群
    user: {
        type: String,
        ref: 'User'
    }, // 发帖人
    type: {
        type: String,
        enum: ['0', '1']
    }, // 0 普通帖子 1 公告
    comments: String, // 帖子内容
    thumbnail: String, // 帖子缩略图
    mediaArr: [{
        type: String,
        ref: 'CommunityContentMedia'
    }] // 媒体集合（图片，视频等）
});

CommunityContentSchema.index({
    community: 1
}); // 添加索引

CommunityContentSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
CommunityContentSchema.set('toObject', {
    getters: true,
    virtuals: true
});

CommunityContentSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var CommunityContent = mongoose.model("CommunityContent", CommunityContentSchema);

module.exports = CommunityContent;