/**
 * Created by Administrator on 2018/1/31.
 *  支付宝流水表
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');

var PaymentLogSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    type: String,
    date: { type: Date, default: Date.now },
    gmtCreate: { type: Date, default: Date.now }, //交易创建时间
    gmtPayment: { type: Date, default: Date.now }, //交易付款时间
    gmtRefund: { type: Date, default: Date.now }, //交易退款时间	
    gmtClose: { type: Date, default: Date.now }, //交易结束时间	
    user: { type: String, ref: 'User' },
    totalAmount: { type: Number }, // 转账钱数
    tradeNo: { type: String }, // 支付宝交易号
    outTradeNo: { type: String }, // 网站订单号
    tradeStatus: { type: String }, // 支付状态
    productCode: { type: String }, // 产品编号
    subject: { type: String }, //商品的标题
    body: { type: String }, //交易的具体描述信息
    state: { type: Boolean, default: false }, //支付状态
    sendCoinState: { type: Boolean, default: false }, //是否完成会员推广奖励的发放
});


PaymentLogSchema.set('toJSON', { getters: true, virtuals: true });
PaymentLogSchema.set('toObject', { getters: true, virtuals: true });

PaymentLogSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD");
});

var PaymentLog = mongoose.model("PaymentLog", PaymentLogSchema);

module.exports = PaymentLog;

