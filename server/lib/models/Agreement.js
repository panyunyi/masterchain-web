/**
 * Created by Administrator on 2015/4/15.
 * 协议对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var AdminUser = require('./AdminUser');


var AgreementSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    name: String,
    state: { type: Boolean, default: false }, // 启用或禁用
    time: { type: Date, default: Date.now },
    updateTime: { type: Date, default: Date.now }, // 更新时间
    user: { type: String, ref: 'AdminUser' }, // 编辑者
    comments: String
});

AgreementSchema.set('toJSON', { getters: true, virtuals: true });
AgreementSchema.set('toObject', { getters: true, virtuals: true });

AgreementSchema.path('time').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

AgreementSchema.path('updateTime').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var Agreement = mongoose.model("Agreement", AgreementSchema);

module.exports = Agreement;

