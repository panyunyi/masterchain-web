const mongoose = require('mongoose');
const settings = require('../../../configs/settings');

mongoose.connect(settings.mongo_connection_uri, {
    useMongoClient: true
});

mongoose.Promise = global.Promise;
const db = mongoose.connection;

db.once('open', () => {
    console.log('connect mongodb success')
})

db.on('error', function (error) {
    console.error('Error in MongoDb connection: ' + error);
    mongoose.disconnect();
});

db.on('close', function () {
    console.log('数据库断开，重新连接数据库');
});


exports.AdminUser = require('./AdminUser');
exports.User = require('./User');
exports.AdminGroup = require('./AdminGroup');
exports.AdminResource = require('./AdminResource');
exports.ContentCategory = require('./ContentCategory');
exports.Content = require('./Content');
exports.ContentTag = require('./ContentTag');
exports.Message = require('./Message');
exports.UserNotify = require('./UserNotify');
exports.Notify = require('./Notify');
exports.SystemConfig = require('./SystemConfig');
exports.DataOptionLog = require('./DataOptionLog');
exports.SystemOptionLog = require('./SystemOptionLog');
exports.Ads = require('./Ads');
exports.AdsItems = require('./AdsItems');
exports.ContentTemplate = require('./ContentTemplate');
exports.TemplateItems = require('./TemplateItems');
exports.Community = require('./Community');
exports.CommunityTag = require('./CommunityTag');
exports.CommunityContentMedia = require('./CommunityContentMedia');
exports.MasterCategory = require('./MasterCategory');
exports.SiteMessage = require('./SiteMessage');
exports.ClassType = require('./ClassType');
exports.ClassInfo = require('./ClassInfo');
exports.ClassContent = require('./ClassContent');
exports.ClassFeedback = require('./ClassFeedback');
exports.CommunityContent = require('./CommunityContent');
exports.UserActionHis = require('./UserActionHis');
exports.ClassScore = require('./ClassScore');
exports.BillRecord = require('./BillRecord');
exports.WalletAddress = require('./WalletAddress');
exports.Mstt_book = require('./Mstt_book');
exports.RewardHis = require('./RewardHis');
exports.Review_his = require('./Review_his');
exports.User_mi_mp_his = require('./User_mi_mp_his');
exports.User_ev_his = require('./User_ev_his');
exports.User_reward_his = require('./User_reward_his');
exports.Threshold_master = require('./Threshold_master');
exports.Action_limit_master = require('./Action_limit_master');
exports.Weight_master = require('./Weight_master');
exports.ClassPayRecord = require('./ClassPayRecord');
exports.ResetAmount = require('./ResetAmount');
exports.PaymentLog = require('./PaymentLog');
exports.Agreement = require('./Agreement');
exports.HelpCenter = require('./HelpCenter');
exports.FeedBack = require('./FeedBack');
exports.CurrencyApproval = require('./CurrencyApproval');
exports.CreativeRight = require('./CreativeRight');
exports.ReportRecord = require('./ReportRecord');
exports.WalletManager = require('./WalletManager');
exports.IdentityAuthentication = require('./IdentityAuthentication');
exports.SignIn = require('./SignIn');
exports.Special = require('./Special');
exports.OpenVipRecord = require('./OpenVipRecord');
exports.VipSetMeal = require('./VipSetMeal');
exports.CommunityMessage = require('./CommunityMessage');
exports.Transaction = require('./Transaction');
exports.HotSearch = require('./HotSearch');
exports.VersionManage = require('./VersionManage');
exports.BrowseRecord = require('./BrowseRecord');
exports.Integralonfig = require('./Integralonfig');
exports.Switches = require('./Switches');
exports.ContentRewardHis = require('./ContentRewardHis');
//DoraModelEnd