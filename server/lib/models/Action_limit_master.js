/**
 * Created by Administrator on 2015/4/15.
 * 行为上限快照	
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');


var Action_limit_masterSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    view: Schema.Types.Decimal128, // 浏览
    good: Schema.Types.Decimal128, // 点赞
    comment: Schema.Types.Decimal128, // 评论
    populace: Schema.Types.Decimal128, // 赞赏
    follow: Schema.Types.Decimal128, // 关注
    transmit: Schema.Types.Decimal128, // 转发
    group: Schema.Types.Decimal128, // 社群建设
    invite: Schema.Types.Decimal128, // 邀请
    report: Schema.Types.Decimal128, // 举报
    date: { type: Date, default: Date.now },
});

Action_limit_masterSchema.set('toJSON', { getters: true, virtuals: true });
Action_limit_masterSchema.set('toObject', { getters: true, virtuals: true });

Action_limit_masterSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD HH:mm:ss");
});

var Action_limit_master = mongoose.model("Action_limit_master", Action_limit_masterSchema);

module.exports = Action_limit_master;

