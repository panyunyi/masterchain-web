/**
 * Created by Administrator on 2015/4/15.
 * 文章获取奖励历史
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment')
var Content = require('./Content');

var ContentRewardHisSchema = new Schema({
    _id: Schema.Types.ObjectId,
    content: {
        type: String,
        ref: 'Content'
    },
    rewardcontentmstt: String,
    rewardreviewermstt: String,
    rewardperreviewermstt: String,
    rewardtotalmstt: String,
    ev: String,
    review: Array,
    author: String,
    datetime: {
        type: Date
    },
    rewardid: Schema.Types.ObjectId,
    evnum: Number,
    rewardcontentnum: Number,
    rewardreviewernum: Number,
    rewardperreviewernum: Number,
    rewardtotalnum: Number

});

ContentRewardHisSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
ContentRewardHisSchema.set('toObject', {
    getters: true,
    virtuals: true
});

ContentRewardHisSchema.path('datetime').get(function (v) {
    return moment(v).format("YYYY/MM/DD");
});

var ContentRewardHis = mongoose.model("ContentRewardHis", ContentRewardHisSchema);

module.exports = ContentRewardHis;