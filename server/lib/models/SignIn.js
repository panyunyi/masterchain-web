/**
 * Created by Administrator on 2015/4/15.
 * 签到对象
 */
var mongoose = require('mongoose');
var shortid = require('shortid');
var Schema = mongoose.Schema;
var moment = require('moment')
var User = require('./User');


var SignInSchema = new Schema({
    _id: {
        type: String,
        'default': shortid.generate
    },
    date: {
        type: Date,
        default: Date.now
    },
    flag: {
        type: Boolean
    }, //是否是起始位置
    state: {
        type: String,
        default: '0'
    }, //1、发放了累计奖励
    user: {
        type: String,
        ref: 'User'
    } // 签到者
});

SignInSchema.index({
    user: 1
}); // 添加索引

SignInSchema.set('toJSON', {
    getters: true,
    virtuals: true
});
SignInSchema.set('toObject', {
    getters: true,
    virtuals: true
});

SignInSchema.path('date').get(function (v) {
    return moment(v).format("YYYY-MM-DD");
});

var SignIn = mongoose.model("SignIn", SignInSchema);

module.exports = SignIn;