const contentModel = require('../models/Content')
const {
    siteFunc
} = require('../../../utils');

exports.getSpecialContentsByCategoryId = async (id) => {
    const contents = await contentModel
        .find({
            categories: id,
            type: 2,
            state: '2'
        })
        .sort({
            date: -1
        })
        .populate([{
            path: 'uAuthor',
            select: siteFunc.getAuthUserFields('session'),
        }])
        .exec();

    return {
        docs: contents,
    };

}

exports.getOneContentByParams = async (params = {}) => {
    const content = await contentModel.findOne(params);
    return content;
}