/**
 *
 * @author    Jerry Bendy
 * @since     2019-01-05
 */

const helpCenterModel = require('../models/HelpCenter')

exports.getHelpByType = async (type, helpLang = '1') => {
    return await helpCenterModel
        .findOne({
            type,
            lang: helpLang,
            state: true,
        })
        .exec();
}