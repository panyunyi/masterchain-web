// 专题
const SpecialModel = require('../models/Special')
const UserModel = require('../models/User')
const ContentModel = require('../models/Content')
const {
    siteFunc
} = require('../../../utils');
const _ = require('lodash');

function renderSpecialItem(userId = '', specialList) {

    return new Promise(async (resolve, reject) => {
        try {
            let newSpecialList = JSON.parse(JSON.stringify(specialList));
            let userInfo = {};
            if (userId) {
                userInfo = await UserModel.findOne({
                    _id: userId
                }, siteFunc.getAuthUserFields('session'));
            }
            for (const newSpecial of newSpecialList) {

                let watchState = false
                let watchNum = await UserModel.count({
                    watchSpecials: newSpecial._id
                });
                newSpecial.watch_num = watchNum;
                if (!_.isEmpty(userInfo) && userInfo.watchSpecials && (userInfo.watchSpecials).indexOf(newSpecial._id) >= 0) {
                    watchState = true;
                }
                newSpecial.had_watched = watchState;

                let content_num = await ContentModel.count({
                    category: newSpecial._id,
                    state: '2'
                });
                newSpecial.total_contentNum = content_num;
            }
            resolve(newSpecialList);
        } catch (error) {
            console.log('---error--', error)
            resolve({})
        }
    })
}



exports.getSpecialList = async () => {
    const contents = await SpecialModel
        .find({})
        .sort({
            date: -1
        })
        .limit(10)
        .populate('category')
        .exec();

    if (userInfo) {
        contents = await renderSpecialItem(userInfo._id, contents);
    }


    return {
        docs: contents,
    };

}

exports.getSpecialById = async (id = '', userInfo = {}) => {
    let contents = await SpecialModel
        .find({
            _id: id
        })
        .sort({
            date: -1
        })
        .populate([{
            path: 'creator',
            select: 'name userName _id'
        }, {
            path: 'category',
            select: 'name _id'
        }])
        .exec();

    if (userInfo) {
        contents = await renderSpecialItem(userInfo._id, contents);
    }

    return contents[0];
}

exports.getSpecialByUserId = async (userId = '') => {
    // console.log('--userId--', userId)
    const contents = await SpecialModel
        .find({
            creator: userId
        })
        .sort({
            date: -1
        })
        .populate('category')
        .exec();

    return {
        docs: contents,
    };
}