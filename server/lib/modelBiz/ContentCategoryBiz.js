const ContentCategoryModel = require('../models/ContentCategory')



exports.getContentCategoryById = async (id) => {


    const result = await ContentCategoryModel
        .findOne({
            _id: id,
        })
        // .populate([{
        // }])
        .exec();

    return result;
}
