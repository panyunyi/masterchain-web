/**
 ** 定义获取前端数据入口
 * 
 */
// [documentList] 约定获取文档列表
const mainCtrl = require('./mainCtrl');


let generalFun = {

    async getDataForIndexPage(req, res, next) {
        req.query.tempPage = 'index.html';
        req.query.modules = [
            // { action: 'get_document_list', params: { current: req.query.current } },
            {
                action: 'get_document_rec_list'
            },
            // { action: 'get_document_hot_list' },
            {
                action: 'get_site_ads_list'
            },
            // { action: 'get_new_message_list' },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForHotPage(req, res, next) {
        req.query.tempPage = 'indexHot.html';
        req.query.modules = [
            // { action: 'get_document_list', params: { current: req.query.current } },
            {
                action: 'get_hot_page_data'
            },
            {
                action: 'get_document_hot_list'
            },
            {
                action: 'get_site_ads_list'
            },
            // { action: 'get_new_message_list' },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    // 发现社群
    async getDataForDiscoverPage(req, res, next) {
        req.query.tempPage = 'indexDiscover.html';
        req.query.modules = [{
                action: 'get_discover_page_data'
            }, {
                action: 'get_community_list'
            },
            {
                action: 'get_community_tags_list'
            },
            {
                action: 'get_community_content_list'
            },
            {
                action: 'get_site_ads_list'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },
    // 发现大师
    async getDataForMasterPage(req, res, next) {
        req.query.tempPage = 'indexMasters.html';
        req.query.modules = [{
                action: 'get_masters_page_data'
            }, {
                action: 'get_document_master_list'
            },
            {
                action: 'get_master_category_list'
            },
            {
                action: 'get_site_ads_list'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForTopicPage(req, res, next) {
        req.query.tempPage = 'indexTopic.html';
        req.query.modules = [{
                action: 'get_topic_page_data'
            }, {
                action: 'get_special_list'
            },
            {
                action: 'get_site_ads_list'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForTopicListPage(req, res, next) {
        req.query.tempPage = 'topicList.html';
        req.query.modules = [{
                action: 'get_special_content_list'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForMasterClass(req, res, next) {
        req.query.tempPage = 'masterClass.html';
        req.query.modules = [{
                action: 'get_class_list'
            },
            {
                action: 'get_site_ads_list'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForAddClass(req, res, next) {
        req.query.tempPage = 'users/userAddMasterCourse.html';
        req.query.modules = [{
                action: 'get_add_class'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForAddClassInfo(req, res, next) {
        req.query.tempPage = 'users/userAddCourseInfo.html';
        req.query.modules = [{
                action: 'get_add_classInfo'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForApplyCreation(req, res, next) {
        req.query.tempPage = 'users/applyCreation.html';
        req.query.modules = [{
                action: 'get_applyCreation'
            },
            {
                action: 'get_category_list'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForPlayerShow(req, res, next) {
        req.query.tempPage = 'users/aliplayer.html';
        req.query.modules = [{
            action: 'get_player_info'
        }, {
            action: 'get_site_info',
            params: {
                modal: 'simple'
            }
        }];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForDownload(req, res, next) {
        req.query.modules = [{
                action: 'get_download_info'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForHelpCenter(req, res, next) {
        req.query.tempPage = 'helpCenter.html';
        req.query.modules = [{
                action: 'get_helpCenter_info'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForInvitation(req, res, next) {
        req.query.tempPage = 'invitation.html';
        req.query.modules = [{
                action: 'get_invitation_info'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForWebHelpCenter(req, res, next) {
        req.query.tempPage = 'help.html';
        req.query.modules = [{
                action: 'get_web_helpCenter_info'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForCatePage(req, res, next) {
        req.query.modules = [
            // { action: 'get_category_list' },
            {
                action: 'get_site_ads_list'
            },
            // { action: 'get_category_list_byContentId' },
            {
                action: 'get_document_list',
                params: {
                    current: req.query.current
                }
            },
            // { action: 'get_document_hot_list' },
            // { action: 'get_document_rec_list' },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForSearchPage(req, res, next) {
        req.query.tempPage = 'public/searchResult.html';
        req.query.modules = [{
                action: 'get_site_ads_list'
            },
            {
                action: 'get_document_list',
                params: {
                    current: req.query.current
                }
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForContentDetails(req, res, next) {
        // console.log('---111--');
        req.query.modules = [
            // { action: 'get_category_list' },
            // { action: 'get_category_list_byContentId', params: { contentId: req.params.id } },
            {
                action: 'get_content_detail'
            },
            // { action: 'get_random_content' },
            // { action: 'get_document_new_list' },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForClassDetails(req, res, next) {
        // console.log('---111--');
        req.query.tempPage = '2-stage-default/classDetail.html';
        req.query.modules = [
            // { action: 'get_category_list' },
            // { action: 'get_category_list_byContentId', params: { contentId: req.params.id } },
            {
                action: 'get_class_detail'
            },
            // { action: 'get_random_content' },
            // { action: 'get_document_new_list' },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForSiteMap(req, res, next) {
        req.query.tempPage = 'sitemap.html';
        req.query.modules = [{
                action: 'get_sitemap_list',
                params: {
                    contentfiles: 'title'
                }
            },
            {
                action: 'get_category_list'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple',
                    title: '站点地图'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForErr() {
        req.query.tempPage = 'public/do' + req.query.errNo + '.html';
        req.query.modules = [{
            action: 'get_error_info_' + req.query.errNo
        }];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForUserLoginAndReg(req, res, next) {
        req.query.modules = [{
                action: 'get_login_info'
            },
            {
                action: 'get_category_list'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForUserReg(req, res, next) {
        req.query.modules = [{
                action: 'get_reg_info'
            },
            {
                action: 'get_category_list'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForResetPsdPage(req, res, next) {
        req.query.tempPage = 'users/userConfirmEmail.html';
        req.query.modules = [{
                action: 'get_category_list'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForUserCenter(req, res, next) {
        req.query.modules = [{
                action: 'get_user_basic_info'
            },
            {
                action: 'get_special_class_list'
            },
            {
                action: 'get_tag_list'
            },
            {
                action: 'get_identity_state',
                params: {
                    type: '2'
                }
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForUserSpecialList(req, res, next) {
        req.query.modules = [{
                action: 'get_user_basic_info'
            },
            {
                action: 'get_special_info'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForUserAddContent(req, res, next) {
        req.query.modules = [{
                action: 'get_special_class_list'
            },
            {
                action: 'get_tag_list'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            },
            {
                action: 'get_add_content_info'
            },
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForIdentityAuth(req, res, next) {
        req.query.modules = [{
                action: 'get_identityAuth_info'
            },
            {
                action: 'get_master_category_list'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            },
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForForgotPsd(req, res, next) {
        req.query.modules = [{
                action: 'get_forgotPsd_info'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForUserReply(req, res, next) {
        req.query.tempPage = 'users/userReplies.html';
        req.query.modules = [{
            action: 'get_user_replies_list',
            params: {
                user: req.session.user._id
            }
        }];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForUserNotice(req, res, next) {
        req.query.tempPage = 'users/userNotice.html';
        req.query.modules = [{
            action: 'get_user_notice_list',
            params: {
                user: req.session.user._id
            }
        }];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForAdminUserLogin(req, res, next) {
        req.query.tempPage = 'adminUserLogin.html';
        req.query.modules = [{
                action: 'get_category_list'
            },
            {
                action: 'get_adminlogin_Info'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForIcoInfoList(req, res, next) {
        req.query.modules = [{
                action: 'get_ico_info_list'
            },
            {
                action: 'get_category_list'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

    async getDataForIcoDetails(req, res, next) {
        req.query.modules = [{
                action: 'get_ico_details'
            },
            {
                action: 'get_category_list'
            },
            {
                action: 'get_site_info',
                params: {
                    modal: 'simple'
                }
            }
        ];
        await mainCtrl.getPageData(req, res, next);
    },

}



module.exports = generalFun;