/**
 ** 定义获取前端数据入口
 * 
 */
// [documentList] 约定获取文档列表
const {
    ContentCategory,
    Content,
    ContentTag,
    User,
    Message,
    SystemConfig,
    UserNotify,
    Ads,
    ContentTemplate,
    CommunityContent,
    Community,
    CommunityTag,
    ClassInfo,
    HelpCenter,
    Special,
    MasterCategory,
    VersionManage,
    IdentityAuthentication
} = require('../controller');
const settings = require("../../../configs/settings");
const logUtil = require("../../../utils/middleware/logUtil");
const siteFunc = require("../../../utils/siteFunc");
const _ = require('lodash');
const ContentBiz = require('../modelBiz/ContentBiz')
const ContentCategoryBiz = require('../modelBiz/ContentCategoryBiz')
const SpecialBiz = require('../modelBiz/SpecialBiz')

let mainCtrl = {

    // 获取页面基础信息
    async getSiteInfo(req, res, next) {
        let configs = await SystemConfig.getSystemConfigs(req, res, next);
        const {
            siteName,
            siteDiscription,
            siteKeywords,
            siteAltKeywords,
            ogTitle,
            allowUserPostContent
        } = configs[0] || [];
        // 查询是否已发布过课程
        let myClasses = await mainCtrl.getMasterClassByUserId(req, res, next);
        let {
            title,
            des,
            keywords
        } = req.query;
        let pageTitle = title ? (title + ' | ' + siteName) : siteName;
        let discription = des ? des : siteDiscription;
        let key = keywords ? keywords : siteKeywords;
        let altkey = siteAltKeywords || '';
        return {
            title: pageTitle,
            desTitle: title ? (title + ' | ' + ogTitle) : ogTitle,
            discription,
            key,
            altkey,
            configs: configs[0] || [],
            version: 'v2.1.1',
            lang: req.session.locale,
            haveClass: myClasses.length > 0 ? '1' : '0',
            allowUserPostContent
        }
    },


    // 获取分类信息
    async getCategoryList(req, res, next) {
        req.query.enable = true;
        return await ContentCategory.getContentCategories(req, res, next);
    },

    // 获取单个分类信息
    async getCategoryInfoById(req, res, next) {
        return await ContentCategory.getCategoryInfoById(req, res, next);
    },

    // 获取文档获取当前父子类别信息
    async getCategoryByContentId(req, res, next) {
        return await ContentCategory.getCurrentCategoriesById(req, res, next);
    },

    // 获取文档列表
    async getDocumentList(req, res, next) {
        // !req.query.type && (req.query.type = '1');
        return await Content.getContents(req, res, next);
    },

    // 获取站点地图列表
    async getSiteMapList(req, res, next) {
        return await Content.getAllContens(req, res, next);
    },

    // 获取随机文档列表
    async getRandomDocumentList(req, res, next) {
        return await Content.getRadomContents(req, res, next);
    },

    // 获取单个文档
    async getOneDocument(req, res, next) {
        // req.query.state = '2';
        return await Content.getOneContent(req, res, next);
    },

    // 获取单个课程信息
    async getOneClassInfo(req, res, next) {
        // req.query.state = true;
        return await ClassInfo.getOneClassInfo(req, res, next);
    },

    // 获取留言列表
    async getMessageList(req, res, next) {
        return await Message.getMessages(req, res, next);
    },

    // 获取用户消息
    async getUserNoticeList(req, res, next) {
        return await UserNotify.getUserNotifys(req, res, next);
    },

    // 获取用户列表
    async getUserList(req, res, next) {
        return await User.getUsers(req, res, next);
    },

    // 获取标签数据
    async getTagList(req, res, next) {
        return await ContentTag.getContentTags(req, res, next);
    },

    // 获取广告数据
    async getAdsList(req, res, next) {
        req.query.state = true;
        return await Ads.getAds(req, res, next);
    },

    // 获取当前模板信息
    async getCurrentTempInfo(req, res, next) {
        return await ContentTemplate.getCurrentTempInfo(req, res, next)
    },

    // 获取社群内容列表
    async getCommunityContents(req, res, next) {
        return await CommunityContent.getCommunityContents(req, res, next);
    },
    // 获取社群列表
    async getCommunities(req, res, next) {
        return await Community.getCommunity(req, res, next);
    },
    // 获取社群标签列表
    async getCommunityTags(req, res, next) {
        return await CommunityTag.getCommunityTags(req, res, next);
    },

    // 获取专题列表
    // async getSpecials(req, res, next) {
    //     return await ContentCategory.getContentCategories(req, res, next);
    // },

    // 获取大师课列表
    async getMasterClass(req, res, next) {
        req.query.state = '2';
        req.query.type = '1'
        return await ClassInfo.getClassInfos(req, res, next);
    },

    // 获取指定大师课列表
    async getMasterClassByUserId(req, res, next) {
        if (req.session.user) {
            req.query.creator = req.session.user._id;
            return await ClassInfo.getClassInfos(req, res, next);
        } else {
            return [];
        }
    },

    // 获取帮助内容
    async getHelpInfo(req, res, next) {
        if (req.params.lang == 'zh-TW') {
            req.query.helpLang = '3'
        }
        return await HelpCenter.getHelpCenters(req, res, next);
    },

    // 获取类别或文档详情模板文件
    getCateOrDetailTemp(defaultTempItems, contentTemp = '2-stage-default', type) {
        let fileName = "contentList.html",
            currentPath = "";
        if (type == 'detail') {
            fileName = "detail.html";
        }
        if (!_.isEmpty(contentTemp)) {
            currentPath = contentTemp.forder + "/" + fileName;
        } else {
            let defaultItem = _.filter(defaultTempItems, (temp) => {
                return temp.isDefault;
            })
            currentPath = defaultItem[0].forder + "/" + fileName;
        }
        return currentPath;
    },

    async getPageData(req, res, next) {

        let _this = this;
        req.query.useClient = '1'; // 标记来自pc端
        let pageData = {
            pageType: 'index'
        },
            modules = req.query.modules;

        try {
            for (let md of modules) {
                req.query = Object.assign({}, req.query, md.params);
                if (md.action.indexOf('get_document') > -1) {
                    let queryParams = {},
                        documentKey = 'documentList';
                    if (md.action == 'get_document_hot_list') {
                        queryParams = {
                            sortby: 'clickNum',
                            current: 1,
                        };
                        documentKey = 'hotItemListData';
                    } else if (md.action == 'get_document_new_list') {
                        queryParams = {
                            model: 'simple'
                        };
                        documentKey = 'newItemListData';
                    } else if (md.action == 'get_document_rec_list') {
                        queryParams = {
                            isTop: 1,
                            current: 1,
                            // type: '1'
                        };
                        documentKey = 'reCommendListData';
                    } else if (md.action == 'get_document_master_list') {
                        queryParams = {
                            current: 1,
                            type: '1'
                        };
                        documentKey = 'masterContentListData';
                    }
                    // console.log('---req.query---', req.query)
                    Object.assign(req.query, queryParams);
                    req.query.state = '2';
                    pageData[documentKey] = await mainCtrl.getDocumentList(req, res, next);
                } else if (md.action == 'get_special_list') {
                    pageData.specialList = await Special.getSpecials(req, res);;
                } else if (md.action == 'get_community_content_list') {
                    pageData.communityContentList = await mainCtrl.getCommunityContents(req, res, next);
                } else if (md.action == 'get_community_list') {
                    pageData.communityList = await mainCtrl.getCommunities(req, res, next);
                } else if (md.action == 'get_community_tags_list') {
                    pageData.communityTagsList = await mainCtrl.getCommunityTags(req, res, next);
                } else if (md.action == 'get_category_list') {
                    pageData.cateTypes = await mainCtrl.getCategoryList(req, res, next);
                } else if (md.action == 'get_special_class_list') {
                    pageData.specials = await SpecialBiz.getSpecialByUserId(req.session.user._id);
                } else if (md.action == 'get_category_list_byContentId') {
                    pageData.currentCateList = await mainCtrl.getCategoryByContentId(req, res, next);
                } else if (md.action == 'get_tag_list') {
                    req.query.pageSize = 100; // 修复标签只显示前 10 条的问题（假定标签数不大于 10e8）
                    pageData.tagList = await mainCtrl.getTagList(req, res, next);
                } else if (md.action == 'get_content_detail') {
                    pageData.pageType = 'detail';
                    let docInfo = await mainCtrl.getOneDocument(req, res, next);
                    // 查询不到文档
                    if (_.isEmpty(docInfo)) {
                        continue;
                    } else {
                        pageData.documentInfo = docInfo;
                        const {
                            title,
                            discription
                        } = pageData.documentInfo;
                        req.query.title = title;
                        req.query.des = discription;

                        if (!_.isEmpty(docInfo.tags)) {
                            req.query.tags = docInfo.tags;
                            req.query.contentId = docInfo._id;
                            pageData.recList = await Content.getRecContents(req, res, next);
                        }
                    }
                } else if (md.action == 'get_class_detail') {
                    pageData.pageType = 'classDetail';
                    let docInfo = await mainCtrl.getOneClassInfo(req, res, next);
                    // 查询不到文档
                    if (_.isEmpty(docInfo)) {
                        continue;
                    } else {
                        pageData.classInfo = docInfo;
                        const {
                            name,
                            comments
                        } = pageData.classInfo;
                        req.query.title = name;
                        req.query.des = comments;
                    }
                } else if (md.action == 'get_random_content') {
                    pageData.randomListData = await mainCtrl.getRandomDocumentList(req, res, next);
                } else if (md.action == 'get_content_messages') {
                    pageData.messageList = await mainCtrl.getMessageList(req, res, next);
                } else if (md.action == 'get_user_replies_list') {
                    pageData.pageType = 'replies';
                    pageData.replyMessageList = await mainCtrl.getMessageList(req, res, next);
                } else if (md.action == 'get_new_message_list') {
                    req.query.pageSize = 5;
                    pageData.newMessageList = await mainCtrl.getMessageList(req, res, next);
                } else if (md.action == 'get_user_notice_list') {
                    pageData.pageType = 'notifies';
                    pageData.userNoticeList = await mainCtrl.getUserNoticeList(req, res, next);
                } else if (md.action == 'get_sitemap_list') {
                    pageData.siteMapList = await mainCtrl.getSiteMapList(req, res, next);
                } else if (md.action == 'get_error_info_404' || md.action == 'get_error_info_500') {
                    pageData.pageType = 'error';
                    pageData.errInfo = req.query.message;
                } else if (md.action == 'get_site_info') {
                    pageData.siteInfo = await mainCtrl.getSiteInfo(req, res, next);
                } else if (md.action == 'get_site_ads_list') {
                    req.query.current = 1;
                    pageData.adsList = await mainCtrl.getAdsList(req, res, next);
                } else if (md.action == 'get_adminlogin_Info') {
                    pageData.pageType = 'adminlogin';
                } else if (md.action == 'get_login_info') {
                    pageData.pageType = 'login';
                } else if (md.action == 'get_reg_info') {
                    pageData.pageType = 'register';
                } else if (md.action == 'get_user_basic_info') {
                    pageData.pageType = 'userCenter';
                } else if (md.action == 'get_class_list') {
                    pageData.pageType = 'class';
                    pageData.masterClass = await mainCtrl.getMasterClass(req, res, next);
                } else if (md.action == 'get_download_info') {
                    pageData.pageType = 'download';
                    let versionInfo = await VersionManage.getConfigsByFiles({
                        client: '0'
                    });
                    if (!_.isEmpty(versionInfo)) {
                        pageData.apkPath = versionInfo[0].url;
                    }
                } else if (md.action == 'get_helpCenter_info' || md.action == 'get_web_helpCenter_info') {
                    pageData.pageType = md.action == 'get_helpCenter_info' ? 'helpCenter' : 'help';
                    if (req.query.helpType) {
                        let targetHelp = await mainCtrl.getHelpInfo(req, res, next);
                        if (!_.isEmpty(targetHelp) && targetHelp.length > 0) {
                            pageData.helpInfo = targetHelp[0];
                        }
                    }
                } else if (md.action == 'get_special_content_list') {
                    pageData.pageType = 'topicList'
                    let contentQuery = {
                        topicId: req.query.id,
                        type: 2,
                        state: '2'
                    }
                    Object.assign(req.query, contentQuery);
                    // pageData.specialContentList = await ContentBiz.getSpecialContentsByCategoryId(req.query.id)
                    pageData.specialContentList = await mainCtrl.getDocumentList(req, res, next);
                    pageData.contentCategory = await SpecialBiz.getSpecialById(req.query.id, req.session.user)
                    // console.log(pageData.contentCategory);
                } else if (md.action == 'get_special_info') {
                    pageData.specialInfo = await SpecialBiz.getSpecialById(req.query.specialId, req.session.user)
                } else if (md.action == 'get_add_class') {
                    pageData.pageType = 'newBuiltCourse';
                    pageData.myClassList = await mainCtrl.getMasterClassByUserId(req, res, next);
                } else if (md.action == 'get_add_classInfo') {
                    pageData.pageType = 'newBuiltCourse'
                    if (req.query.classId) {
                        pageData.editClassId = req.query.classId;
                        let targetClass = await ClassInfo.getOneClassInfo(req, res);
                        if (!_.isEmpty(targetClass)) {
                            pageData.editClassInfo = targetClass;
                            // 不是草稿不允许编辑
                            if (targetClass.state != '0') {
                                throw new siteFunc.UserException(res.__('label_page_no_power_content'));
                            }
                        }
                    }
                } else if (md.action == 'get_applyCreation') {
                    pageData.pageType = 'applyCreation'
                } else if (md.action == 'get_forgotPsd_info') {
                    pageData.pageType = 'forgotPsd'
                } else if (md.action == 'get_player_info') {
                    pageData.pageType = 'aliplayer';
                } else if (md.action == 'get_add_content_info') {
                    pageData.pageType = req.query.contentType;
                    if (req.query.contentId) {
                        pageData.contentId = req.query.contentId;
                    }
                } else if (md.action == 'get_invitation_info') {
                    pageData.pageType = "invitation";
                } else if (md.action == 'get_identityAuth_info') {
                    pageData.pageType = "identityAuth";
                } else if (md.action == 'get_identity_state') {
                    pageData.masterAuthState = await IdentityAuthentication.getIdentityAuthenticationInfo(req, res, next);
                } else if (md.action == 'get_master_category_list') {
                    pageData.masterCategories = await MasterCategory.getMasterCategorys(req, res);
                } else if (md.action == 'get_topic_page_data') {
                    pageData.pageType = "topic";
                } else if (md.action == 'get_discover_page_data') {
                    pageData.pageType = "discover";
                } else if (md.action == 'get_masters_page_data') {
                    pageData.pageType = "masters";
                } else if (md.action == 'get_hot_page_data') {
                    pageData.pageType = "hot";
                }
            }

            let defaultTemp = await mainCtrl.getCurrentTempInfo(req, res, next);
            // 登录态
            // console.log('----req.session.user----', req.session.user)
            pageData.userInfo = req.session.user ? await User.getOneUserByParams({
                _id: req.session.user._id
            }) : {};
            pageData.logined = req.session.logined;
            // 静态目录
            pageData.staticforder = defaultTemp.data.alias;
            // 当前类别
            if (req.query.typeId) {
                pageData.cateInfo = await mainCtrl.getCategoryInfoById(req, res, next);
                if (req.query.isIndex) {
                    pageData.pageType = "index";
                    !_.isEmpty(pageData.siteInfo) && (pageData.siteInfo.title = pageData.siteInfo.title)
                } else {
                    pageData.pageType = "cate";
                    let cateName = _.isEmpty(pageData.cateInfo) ? '' : ('|' + pageData.cateInfo.name);
                    !_.isEmpty(pageData.siteInfo) && (pageData.siteInfo.title = pageData.siteInfo.title + cateName);
                }
            }

            if (req.query.searchkey) {
                pageData.pageType = "search";
                pageData.targetSearchKey = req.query.searchkey;
            }

            if (req.query.tagName) {
                pageData.pageType = "tag";
                pageData.targetTagName = req.query.tagName;
            }
            // console.log('---pageData.pageType---', pageData.pageType)
            // 针对分类页和内容详情页动态添加meta
            let defaultTempItems = defaultTemp.data.items;
            if (!_.isEmpty(pageData.siteInfo)) {
                let siteDomain = pageData.siteInfo.configs.siteDomain;
                let ogUrl = siteDomain;
                let ogImg = siteDomain + "/themes/" + defaultTemp.data.alias + "/images/mobile_logo2.jpeg"
                if (pageData.pageType == 'cate') {
                    if (!_.isEmpty(pageData.cateInfo)) {
                        let {
                            defaultUrl,
                            _id,
                            contentTemp
                        } = pageData.cateInfo
                        ogUrl = siteDomain + '/' + defaultUrl + '___' + _id;
                        req.query.tempPage = mainCtrl.getCateOrDetailTemp(defaultTempItems, contentTemp, 'cate');
                    }
                } else if (pageData.pageType == 'detail') {
                    if (!_.isEmpty(pageData.documentInfo)) {
                        ogUrl = siteDomain + '/details/' + pageData.documentInfo._id + '.html';
                        if (pageData.documentInfo.sImg && (pageData.documentInfo.sImg).indexOf('defaultImg.jpg') < 0) {
                            ogImg = pageData.documentInfo.sImg;
                        }
                        // console.log('---222-', pageData.documentInfo.categories.length)
                        // let parentCateTemp = (pageData.documentInfo.categories && pageData.documentInfo.categories[0]) ? pageData.documentInfo.categories[0].contentTemp : '';
                        // console.log('---333-')
                        req.query.tempPage = mainCtrl.getCateOrDetailTemp(defaultTempItems, '', 'detail');
                    } else {
                        // throw new siteFunc.UserException(404);
                        // req.query.infoContent = '您暂无权限访问该文章';
                        // siteFunc.directToErrorPage(req, res, next);
                        throw new siteFunc.UserException(res.__('label_page_no_power_content'));
                    }
                }
                pageData.ogData = {
                    url: ogUrl,
                    img: ogImg
                };
            }

            // 读取国际化文件信息
            let localKeys = await siteFunc.getSiteLocalKeys(req.session.locale, res);
            pageData.lk = localKeys.renderKeys;
            pageData.lsk = JSON.stringify(localKeys.sysKeys);
            // console.log('==masterContentListData===', pageData.masterContentListData)
            // console.log('==masterCategories===', pageData.masterCategories)
            // console.log('==defaultTemp===', defaultTemp)
            res.render(settings.SYSTEMTEMPFORDER + defaultTemp.data.alias + '/' + req.query.tempPage, pageData);
        } catch (error) {
            logUtil.error(error, req);
            if (error.message == '404' || error.message == '505') {
                next();
            } else {
                req.query.message = error.message;
                await siteFunc.directToErrorPage(req, res, next);
            }
        }
    }

}



module.exports = mainCtrl;