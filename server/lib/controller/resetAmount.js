const BaseComponent = require('../prototype/baseComponent');
const ResetAmountModel = require("../models").ResetAmount;
const formidable = require('formidable');
const { service, validatorUtil, siteFunc } = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }

    if (!validator.isFloat((fields.money).toString())) {
        errMsg = res.__("validate_error_params");
    }

    if (!validator.isLength(fields.unit, 1, 12)) {
        errMsg = res.__("validate_rangelength", { min: 1, max: 12, label: res.__("label_tag_name") });
    }

    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class ResetAmount {
    constructor() {
        // super()
    }
    async getResetAmounts(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey, queryObj = {};
            let useClient = req.query.useClient;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = { $regex: reKey }
            }

            const resetAmounts = await ResetAmountModel.find(queryObj).sort({ date: -1 }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize));
            const totalItems = await ResetAmountModel.count(queryObj);

            let resetAmountData = {
                docs: resetAmounts,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderResetAmountData = siteFunc.renderApiData(req, res, 200, 'ResetAmount', resetAmountData);
            if (modules && modules.length > 0) {
                return renderResetAmountData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getResetAmounts', resetAmounts));
                } else {
                    res.send(renderResetAmountData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async addResetAmount(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);

                const tagObj = {
                    money: fields.money,
                    default: fields.default,
                    unit: fields.unit
                }

                const newResetAmount = new ResetAmountModel(tagObj);

                await newResetAmount.save();

                res.send(siteFunc.renderApiData(req, res, 200, 'ResetAmount', { id: newResetAmount._id }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateResetAmount(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
                const userObj = {
                    money: fields.money,
                    default: fields.default,
                    unit: fields.unit
                }
                const item_id = fields._id;

                await ResetAmountModel.findOneAndUpdate({ _id: item_id }, { $set: userObj });
                res.send(siteFunc.renderApiData(req, res, 200, 'ResetAmount', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delResetAmount(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await ResetAmountModel.remove({ _id: req.query.ids });
            res.send(siteFunc.renderApiData(req, res, 200, 'ResetAmount', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new ResetAmount();