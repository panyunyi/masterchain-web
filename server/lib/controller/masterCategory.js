const BaseComponent = require('../prototype/baseComponent');
const MasterCategoryModel = require("../models").MasterCategory;
const UserModel = require("../models").User;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    if (!validatorUtil.isRegularCharacter(fields.name)) {
        errMsg = res.__("validate_error_field", {
            label: res.__("label_tag_name")
        });
    }
    if (!validator.isLength(fields.name, 1, 12)) {
        errMsg = res.__("validate_rangelength", {
            min: 1,
            max: 12,
            label: res.__("label_tag_name")
        });
    }
    if (!validator.isLength(fields.comments, 2, 30)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 30,
            label: res.__("label_comments")
        });
    }
    if (!fields.sImg) {
        errMsg = res.__("validate_userContent_sImg");
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

function renderCategoryList(categoryList) {

    return new Promise(async (resolve, reject) => {
        try {
            let newCateList = JSON.parse(JSON.stringify(categoryList));
            for (let categoryItem of newCateList) {
                let categoryId = categoryItem._id;
                let targetMaters = await UserModel.find({
                    group: '1',
                    category: categoryId
                }, 'userName id _id');

                let followNum = targetMaters.length,
                    followUserName = [];
                for (const master of targetMaters) {
                    followUserName.push(master.userName);
                }

                let userNameStr = followUserName.join(',');
                if (followUserName.length > 3) {
                    userNameStr = (followUserName.splice(0, 3)).join(',') + '...';
                } else {
                    userNameStr = (followUserName.splice(0, 3)).join(',');
                }
                categoryItem.follow_num = followNum;
                categoryItem.follow_users = userNameStr;
            }
            resolve(newCateList);
        } catch (error) {
            resolve([]);
        }
    })
}

class MasterCategory {
    constructor() {
        // super()
    }
    async getMasterCategorys(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {};
            let useClient = req.query.useClient;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = {
                    $regex: reKey
                }
            }
            // console.log('--MasterCategoryModel---', MasterCategoryModel)
            let masterCategorys = await MasterCategoryModel.find(queryObj).sort({
                sort: 1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize));
            const totalItems = await MasterCategoryModel.count(queryObj);

            masterCategorys = await renderCategoryList(masterCategorys);

            let masterCategoryData = {
                docs: masterCategorys,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderMasterCategoryData = siteFunc.renderApiData(req, res, 200, 'MasterCategory', masterCategoryData);
            if (modules && modules.length > 0) {
                return renderMasterCategoryData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getMasterCategorys', masterCategorys));
                } else {
                    res.send(renderMasterCategoryData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async addMasterCategory(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
            } catch (err) {
                console.log(err.message, err);
                res.send(siteFunc.renderApiErr(req, res, 500, err, 'checkform'));
            }

            const tagObj = {
                name: fields.name,
                sImg: fields.sImg,
                comments: fields.comments
            }

            const newMasterCategory = new MasterCategoryModel(tagObj);
            try {
                await newMasterCategory.save();

                res.send(siteFunc.renderApiData(req, res, 200, 'MasterCategory', {
                    id: newMasterCategory._id
                }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateMasterCategory(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
            } catch (err) {
                console.log(err.message, err);
                res.send(siteFunc.renderApiErr(req, res, 500, err, 'checkform'));
            }

            const userObj = {
                name: fields.name,
                sImg: fields.sImg,
                comments: fields.comments
            }
            const item_id = fields._id;
            try {
                await MasterCategoryModel.findOneAndUpdate({
                    _id: item_id
                }, {
                    $set: userObj
                });
                res.send(siteFunc.renderApiData(req, res, 200, 'MasterCategory', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delMasterCategory(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await MasterCategoryModel.remove({
                _id: req.query.ids
            });
            res.send(siteFunc.renderApiData(req, res, 200, 'MasterCategory', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new MasterCategory();