const BaseComponent = require('../prototype/baseComponent');
const ClassInfoModel = require("../models").ClassInfo;
const ClassScoreModel = require("../models").ClassScore;
const ClassContentModel = require("../models").ClassContent;
const ClassPayRecordModel = require("../models").ClassPayRecord;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')
const xss = require("xss");

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }

    if (!validatorUtil.isRegularCharacter(fields.name)) {
        errMsg = res.__("validate_error_field", {
            label: res.__("label_tag_name")
        });
    }

    if (!validator.isLength(fields.name, 1, 12)) {
        errMsg = res.__("validate_rangelength", {
            min: 1,
            max: 12,
            label: res.__("label_tag_name")
        });
    }

    if (fields.buyTips && !validatorUtil.isRegularCharacter(fields.buyTips)) {
        errMsg = res.__("validate_error_field", {
            label: res.__("classInfo_label_buyTips")
        });
    }

    if (fields.buyTips && !validator.isLength(fields.buyTips, 1, 200)) {
        errMsg = res.__("validate_rangelength", {
            min: 1,
            max: 200,
            label: res.__("classInfo_label_buyTips")
        });
    }

    if (!validator.isLength(fields.comments, 2, 500)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 500,
            label: res.__("label_comments")
        });
    }


    // if (!fields.unit) {
    //     errMsg = res.__('validate_inputCorrect', { label: res.__('classInfo_label_price_unit') });
    // }

    if (!fields.sImg) {
        errMsg = res.__('validate_uploadNull', {
            label: res.__('classInfo_label_price_sImg')
        });
    }

    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

async function renderClassData(userInfo = {}, classInfoList = []) {

    let newClassInfoList = JSON.parse(JSON.stringify(classInfoList));
    for (const classInfo of newClassInfoList) {
        // 默认未订购
        classInfo.hadPayClass = false;
        classInfo.hadComment = false;
        let classId = classInfo._id;
        let addScoreUserNum = await ClassScoreModel.count({
            class: classId
        });
        let classScores = await ClassScoreModel.aggregate([{
                $match: {
                    class: classId
                }
            },
            {
                $group: {
                    _id: null,
                    total_num: {
                        $sum: "$score"
                    }
                }
            }
        ]);

        // 已更新并审核的章节数
        let update_catalog = await ClassContentModel.find({
            basicInfo: classId,
            state: '2'
        }, 'catalog catalogIndex').sort({
            date: 1
        });
        classInfo.update_catalog_num = update_catalog.length;
        classInfo.update_catalog = update_catalog;

        // console.log('----update_catalog-', update_catalog)

        let totalClassScore = classScores.length > 0 ? classScores[0].total_num : 0;
        classInfo.average = addScoreUserNum > 0 ? (totalClassScore / addScoreUserNum) : 0;
        // 课程订阅数
        let record_num = await ClassPayRecordModel.count({
            class: classId
        });
        classInfo.record_num = record_num;
        // 本人阅读章节
        if (!_.isEmpty(userInfo)) {
            let myOrderClass = await ClassPayRecordModel.findOne({
                class: classId,
                user: userInfo._id
            }, 'learningProgress');
            if (!_.isEmpty(myOrderClass)) {
                classInfo.hadPayClass = true;
                classInfo.hadReadChapters = !_.isEmpty(myOrderClass.learningProgress) ? myOrderClass.learningProgress.length : 0;
            } else {
                classInfo.hadPayClass = false;
                classInfo.hadReadChapters = 1;
            }

            let userClassScoreInfo = await ClassScoreModel.findOne({
                user: userInfo._id,
                class: classId
            });
            // console.log('---userClassScoreInfo----', userClassScoreInfo)
            if (!_.isEmpty(userClassScoreInfo)) {
                classInfo.hadComment = true;
            }

        } else {
            classInfo.hadReadChapters = 1;
        }

    }
    return newClassInfoList;

}

class ClassInfo {
    constructor() {
        // super()
    }
    async getClassInfos(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {};
            let useClient = req.query.useClient;
            let state = req.query.state;
            let type = req.query.type;
            let userInfo = req.session.user || {};
            let creator = req.query.creator;
            let category = req.query.category;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = {
                    $regex: reKey
                }
            }

            if (type == '1') {
                queryObj.recommend = true;
            }

            if (type == '2') {
                queryObj.priceType = '1';
            }

            if (type == '3') {
                queryObj.discount = true;
            }

            if (type == '4') {
                queryObj.boutique = true;
            }

            if (creator) {
                queryObj.author = creator;
            }

            if (state) {
                queryObj.state = state
            } else {
                queryObj.state = {
                    $ne: '0'
                }
            }

            if (category && shortid.isValid(category)) {
                queryObj.categories = category;
            }

            if (!_.isEmpty(userInfo) && userInfo._id == creator && !type) {
                delete queryObj.state;
            }

            const classInfos = await ClassInfoModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'categories',
                select: 'name _id'
            }, {
                path: 'author',
                select: 'name userName logo _id group'
            }]).exec();
            const totalItems = await ClassInfoModel.count(queryObj);

            let renderClassInfos = await renderClassData(userInfo, classInfos);
            let classInfoData = {
                docs: renderClassInfos,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderClassInfoData = siteFunc.renderApiData(req, res, 200, 'ClassInfo', classInfoData);
            if (modules && modules.length > 0) {
                return renderClassInfoData.data;
            } else {
                if (req.query.useClient == '1') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getClassInfos', classInfoData));
                } else if (req.query.useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getClassInfos', renderClassInfos));
                } else {
                    res.send(renderClassInfoData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async getOneClassInfo(req, res, next) {

        try {
            let userInfo = req.session.user || {};
            let classId = req.query.classId;

            let queryObj = {
                _id: classId,
                state: '2'
            };
            let modules = req.query.modules;
            let useClient = req.query.useClient;

            if (!classId) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }

            if (!_.isEmpty(userInfo)) {
                let targetInfo = await ClassInfoModel.count({
                    _id: classId,
                    author: userInfo._id
                })
                if (targetInfo > 0) {
                    delete queryObj.state;
                }
            }

            let classInfo = await ClassInfoModel.findOne(queryObj).populate([{
                path: 'categories',
                select: 'name _id'
            }, {
                path: 'author',
                select: 'name userName logo _id group'
            }]).exec();

            if (!_.isEmpty(classInfo)) {
                let classArr = [];
                classArr.push(classInfo);
                let renderClassInfo = await renderClassData(userInfo, classArr);

                if (modules && modules.length > 0) {
                    return renderClassInfo[0];
                } else {
                    if (useClient == '2') {
                        res.send(siteFunc.renderApiData(req, res, 200, 'get data success', renderClassInfo[0], 'save'))
                    } else {
                        res.send(renderClassInfo[0]);
                    }
                }

            } else {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }

        } catch (error) {
            throw new siteFunc.UserException(error);
        }
    }

    async addClassInfo(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                checkFormData(req, res, fields);

                if (req.session.user.group != '1') {
                    throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                }

                // 校验价格
                if (Number(fields.price) < 0 || !validator.isNumeric(fields.price.toString()) && !validator.isFloat(fields.price.toString())) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                if (Number(fields.vipPrice) < 0 || !validator.isNumeric(fields.vipPrice.toString()) && !validator.isFloat(fields.vipPrice.toString())) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                if (Number(fields.price) < Number(fields.vipPrice)) {
                    throw new siteFunc.UserException(res.__("validate_inputCorrect", {
                        label: res.__('classInfo_label_price')
                    }));
                }

                let currentPriceType = '0';
                if (fields.price == '0') {
                    currentPriceType = '0' // 免费
                } else if (fields.price != '0' && fields.vipPrice == '0') {
                    currentPriceType = '1' // 会员免费
                } else if (fields.price != '0' && fields.vipPrice != '0') {
                    currentPriceType = '2' // 会员付费
                } else {
                    throw new siteFunc.UserException(res.__("validate_inputCorrect", {
                        label: res.__('classInfo_label_price')
                    }));
                }

                const tagObj = {
                    name: fields.name,
                    type: fields.type,
                    state: fields.draft == '1' ? '0' : '1',
                    author: req.session.user._id,
                    buyTips: fields.buyTips,
                    priceType: currentPriceType,
                    price: fields.price,
                    vipPrice: fields.vipPrice,
                    unit: "MEC",
                    sImg: fields.sImg,
                    recommend: fields.recommend,
                    discount: fields.discount,
                    boutique: fields.boutique,
                    categories: fields.categories,
                    comments: xss(fields.comments)
                }

                const newClassInfo = new ClassInfoModel(tagObj);

                await newClassInfo.save();

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('lc_create_masterClass')
                }), {
                    id: newClassInfo._id
                }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateClassInfo(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                checkFormData(req, res, fields);

                let useClient = req.query.useClient;

                if (useClient == '1') {
                    let classCount = await ClassInfoModel.count({
                        author: req.session.user._id,
                        _id: fields._id
                    });
                    if (classCount == 0) {
                        throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
                    }
                }

                if (fields.price < fields.vipPrice) {
                    throw new siteFunc.UserException(res.__("validate_inputCorrect", {
                        label: res.__('classInfo_label_price')
                    }));
                }

                let currentPriceType = '0';
                if (fields.price == 0) {
                    currentPriceType = '0'
                } else if (fields.price != 0 && fields.vipPrice == 0) {
                    currentPriceType = '1'
                } else if (fields.price != 0 && fields.vipPrice != 0) {
                    currentPriceType = '2'
                } else {
                    throw new siteFunc.UserException(res.__("validate_inputCorrect", {
                        label: res.__('classInfo_label_price')
                    }));
                }

                const userObj = {
                    name: fields.name,
                    type: fields.type,
                    // author: req.session.user._id,
                    buyTips: fields.buyTips,
                    priceType: currentPriceType,
                    price: fields.price,
                    vipPrice: fields.vipPrice,
                    unit: "MEC",
                    sImg: fields.sImg,
                    recommend: fields.recommend,
                    discount: fields.discount,
                    boutique: fields.boutique,
                    categories: fields.categories,
                    comments: xss(fields.comments),
                    state: fields.draft == '1' ? '0' : '1'
                }
                const item_id = fields._id;

                if (useClient == '0') {
                    userObj.state = fields.state;
                    userObj.auditDate = new Date()
                } else {
                    userObj.updateDate = new Date()
                }

                await ClassInfoModel.findOneAndUpdate({
                    _id: item_id
                }, {
                    $set: userObj
                });
                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: '大师课信息更新'
                }), {}, 'update'))
            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delClassInfo(req, res, next) {
        try {
            let errMsg = '';
            let userInfo = req.session.user || {}
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }

            if (useClient == '1') {
                let classCount = await ClassInfoModel.count({
                    author: userInfo._id,
                    _id: req.query.ids
                });
                if (classCount == 0) {
                    throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
                }
            }

            await ClassInfoModel.remove({
                _id: req.query.ids
            });

            // 删除课程要删除课程下的所有章节
            await ClassContentModel.remove({
                basicInfo: req.query.ids
            });

            res.send(siteFunc.renderApiData(req, res, 200, 'ClassInfo', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new ClassInfo();