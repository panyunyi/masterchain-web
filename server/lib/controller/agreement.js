const BaseComponent = require('../prototype/baseComponent');
const AgreementModel = require("../models").Agreement;
const formidable = require('formidable');
const { service, validatorUtil, siteFunc } = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    if (!validator.isLength(fields.name, 1, 50)) {
        errMsg = res.__("validate_rangelength", { min: 1, max: 50, label: res.__("label_tag_name") });
    }
    if (!validator.isLength(fields.comments, 2, 2000)) {
        errMsg = res.__("validate_rangelength", { min: 100, max: 2, label: res.__("label_comments") });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class Agreement {
    constructor() {
        // super()
    }
    async getAgreements(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey, queryObj = {};
            let useClient = req.query.useClient;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = { $regex: reKey }
            }

            const agreements = await AgreementModel.find(queryObj).sort({ date: -1 }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'user',
                select: 'name userName _id'
            }]).exec();
            const totalItems = await AgreementModel.count(queryObj);

            let agreementData = {
                docs: agreements,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderAgreementData = siteFunc.renderApiData(req, res, 200, 'Agreement', agreementData);
            if (modules && modules.length > 0) {
                return renderAgreementData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getAgreements', agreements));
                } else {
                    res.send(renderAgreementData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async addAgreement(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);

                const tagObj = {
                    name: fields.name,
                    state: fields.state,
                    user: req.session.adminUserInfo._id,
                    comments: fields.comments
                }

                const newAgreement = new AgreementModel(tagObj);

                await newAgreement.save();

                res.send(siteFunc.renderApiData(req, res, 200, 'Agreement', { id: newAgreement._id }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateAgreement(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
                const userObj = {
                    name: fields.name,
                    state: fields.state,
                    user: req.session.adminUserInfo._id,
                    comments: fields.comments,
                    updateTime: new Date()
                }
                const item_id = fields._id;

                await AgreementModel.findOneAndUpdate({ _id: item_id }, { $set: userObj });
                res.send(siteFunc.renderApiData(req, res, 200, 'Agreement', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delAgreement(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await AgreementModel.remove({ _id: req.query.ids });
            res.send(siteFunc.renderApiData(req, res, 200, 'Agreement', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new Agreement();