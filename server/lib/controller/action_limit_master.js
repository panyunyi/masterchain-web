const BaseComponent = require('../prototype/baseComponent');
const Action_limit_masterModel = require("../models").Action_limit_master;
const formidable = require('formidable');
const { service, validatorUtil, siteFunc } = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    if (!validator.isLength(fields.name, 1, 12)) {
        errMsg = res.__("validate_rangelength", { min: 1, max: 12, label: res.__("label_tag_name") });
    }
    if (!validator.isLength(fields.comments, 2, 30)) {
        errMsg = res.__("validate_rangelength", { min: 2, max: 30, label: res.__("label_comments") });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class Action_limit_master {
    constructor() {
        // super()
    }
    async getAction_limit_masters(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey, queryObj = {};
            let useClient = req.query.useClient;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = { $regex: reKey }
            }

            const action_limit_masters = await Action_limit_masterModel.find(queryObj).sort({ date: -1 }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'users',
                select: 'name userName _id'
            }]).exec();
            const totalItems = await Action_limit_masterModel.count(queryObj);

            let action_limit_masterData = {
                docs: action_limit_masters,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderAction_limit_masterData = siteFunc.renderApiData(req, res, 200, 'Action_limit_master', action_limit_masterData);
            if (modules && modules.length > 0) {
                return renderAction_limit_masterData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getAction_limit_masters', action_limit_masters));
                } else {
                    res.send(renderAction_limit_masterData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async addAction_limit_master(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);

                const tagObj = {
                    name: fields.name,
                    comments: fields.comments
                }

                const newAction_limit_master = new Action_limit_masterModel(tagObj);

                await newAction_limit_master.save();

                res.send(siteFunc.renderApiData(req, res, 200, 'Action_limit_master', { id: newAction_limit_master._id }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateAction_limit_master(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
                const userObj = {
                    name: fields.name,
                    comments: fields.comments
                }
                const item_id = fields._id;

                await Action_limit_masterModel.findOneAndUpdate({ _id: item_id }, { $set: userObj });
                res.send(siteFunc.renderApiData(req, res, 200, 'Action_limit_master', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delAction_limit_master(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await Action_limit_masterModel.remove({ _id: req.query.ids });
            res.send(siteFunc.renderApiData(req, res, 200, 'Action_limit_master', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new Action_limit_master();