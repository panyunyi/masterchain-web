const BaseComponent = require('../prototype/baseComponent');
const ClassScoreModel = require("../models").ClassScore;
const ClassInfoModel = require("../models").ClassInfo;
const ClassPayRecordModel = require("../models").ClassPayRecord;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')
const xss = require("xss");

function checkFormData(req, res, fields) {

    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    if (!validator.isFloat((fields.score).toString())) {
        errMsg = res.__("validate_inputCorrect", {
            label: res.__("classScore_label_score")
        });
    }
    if (!validator.isLength(fields.comments, 2, 100)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 100,
            label: res.__("label_comments")
        });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class ClassScore {
    constructor() {
        // super()
    }
    async getClassScores(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {};
            let useClient = req.query.useClient;
            let classId = req.query.classId;
            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = {
                    $regex: reKey
                }
            }

            if (classId) {
                queryObj.class = classId;
            }

            const classScores = await ClassScoreModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'user',
                select: 'name userName _id logo'
            }, {
                path: 'class',
                select: 'name sImg _id'
            }]).exec();
            const totalItems = await ClassScoreModel.count(queryObj);

            let classScoreData = {
                docs: classScores,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderClassScoreData = siteFunc.renderApiData(req, res, 200, 'ClassScore', classScoreData);
            if (modules && modules.length > 0) {
                return renderClassScoreData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getClassScores', classScores));
                } else {
                    res.send(renderClassScoreData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async addClassScore(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);
                // console.log('---fields--', fields);
                checkFormData(req, res, fields);

                let targetClass = await ClassInfoModel.findOne({
                    _id: fields.class
                });

                if (_.isEmpty(targetClass)) {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }

                // 校验是否有评论权限
                let targetUser = req.session.user;

                if (targetClass.author._id != req.session.user._id) {

                    // 还未发布的课程他人不能访问
                    if (targetClass.state != '2') {
                        throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                    }

                    // 会员免费
                    if (targetClass.priceType == '1') {
                        if (!targetUser.vip) {
                            throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                        }
                    }

                    // 会员付费
                    if (targetClass.priceType == '2') {
                        let classRecord = await ClassPayRecordModel.findOne({
                            user: req.session.user._id,
                            class: targetClass._id
                        });

                        if (_.isEmpty(classRecord)) {
                            throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                        }
                    }
                    console.log('符合用户角色要求,可以评论');
                } else {
                    console.log('自己的课程不能评论');
                    throw new siteFunc.UserException(res.__('user_action_tips_comment_self'));
                }

                const tagObj = {
                    score: fields.score,
                    user: req.session.user._id,
                    comments: xss(fields.comments),
                    class: fields.class,
                    hadScore: true
                }

                const newClassScore = new ClassScoreModel(tagObj);

                await newClassScore.save();

                let classId = fields.class;
                let addScoreUserNum = await ClassScoreModel.count({
                    class: classId
                });
                let classScores = await ClassScoreModel.aggregate([{
                        $match: {
                            class: classId
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            total_num: {
                                $sum: "$score"
                            }
                        }
                    }
                ]);
                let totalClassScore = classScores.length > 0 ? classScores[0].total_num : 0;
                let classInfoAverage = addScoreUserNum > 0 ? (totalClassScore / addScoreUserNum) : 0;

                res.send(siteFunc.renderApiData(req, res, 200, res.__("restful_api_response_success", {
                    label: res.__("user_action_type_comment")
                }), {
                    id: newClassScore._id,
                    average: classInfoAverage
                }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateClassScore(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
                const userObj = {
                    score: fields.score,
                    user: req.session.user._id,
                    comments: xss(fields.comments),
                    class: fields.class,
                    hadScore: true
                }
                const item_id = fields._id;

                await ClassScoreModel.findOneAndUpdate({
                    _id: item_id
                }, {
                    $set: userObj
                });
                res.send(siteFunc.renderApiData(req, res, 200, 'ClassScore', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delClassScore(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await ClassScoreModel.remove({
                _id: req.query.ids
            });
            res.send(siteFunc.renderApiData(req, res, 200, 'ClassScore', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new ClassScore();