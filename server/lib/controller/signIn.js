const BaseComponent = require('../prototype/baseComponent');
const SignInModel = require("../models").SignIn;
const BillRecordModel = require("../models").BillRecord;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')
const settings = require('../../../configs/settings');
const moment = require('moment')

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class SignIn {
    constructor() {
        // super()
    }
    async getSignIns(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {};
            let useClient = req.query.useClient;
            let userInfo = req.session.user;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = {
                    $regex: reKey
                }
            }

            if (userInfo) {
                queryObj.user = userInfo._id;
            }

            const signIns = await SignInModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'user',
                select: 'name userName _id logo'
            }]).exec();
            const totalItems = await SignInModel.count(queryObj);

            let signInData = {
                docs: signIns,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderSignInData = siteFunc.renderApiData(req, res, 200, 'SignIn', signInData);
            if (modules && modules.length > 0) {
                return renderSignInData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getSignIns', signIns));
                } else {
                    res.send(renderSignInData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async getBasicSignInInfo(req, res, next) {
        try {

            let userInfo = req.session.user;
            let cumulative = 0;
            // 判断昨天是否签到
            let thisDate = moment().format('YYYY-MM-DD');
            let lastDate = moment().add(-1, 'days').format('YYYY-MM-DD');
            console.log('-lastDate----', lastDate)
            let thisDaySignRecord = await SignInModel.findOne({
                user: req.session.user._id,
                date: thisDate
            });
            let lastDaySignRecord = await SignInModel.findOne({
                user: req.session.user._id,
                date: lastDate
            });

            // 昨天今天都没签到
            if (_.isEmpty(lastDaySignRecord) && _.isEmpty(thisDaySignRecord)) {
                cumulative = 0;
            } else {
                let rangeTime = siteFunc.getDateStr(-8);
                // console.log('--userInfo----', userInfo)
                // 连续签到天数
                let continuitySignRecords = await SignInModel.find({
                    user: userInfo._id,
                    date: {
                        "$gte": new Date(rangeTime.startTime),
                        "$lte": new Date(rangeTime.endTime)
                    }
                }).sort({
                    date: 1
                });
                let flagTrueIndex = 0;
                continuitySignRecords.map((item, index) => {
                    if (item.flag) {
                        flagTrueIndex = index;
                    }
                });
                // console.log('-continuitySignRecords-----', continuitySignRecords)
                cumulative = continuitySignRecords.length - flagTrueIndex;
            }


            // 累计天数
            let totalSignRecords = await SignInModel.count({
                user: userInfo._id
            });

            // 累计奖励
            let totalBillObj = await BillRecordModel.aggregate([{
                    $match: {
                        type: settings.user_bill_type_signIn,
                        user: userInfo._id
                    }
                },
                {
                    $group: {
                        _id: null,
                        total_num: {
                            $sum: "$coins"
                        }
                    }
                }
            ]);
            let signInIncome = totalBillObj.length > 0 ? totalBillObj[0].total_num : 0;


            // 判断今天是否已签到
            let currentDate = moment().format("YYYY-MM-DD");
            let oldSign = await SignInModel.find({
                user: req.session.user._id,
                date: currentDate
            });
            let todayHadSignIn = false;
            if (!_.isEmpty(oldSign)) {
                todayHadSignIn = true;
            }

            let signWantData = {
                cumulative,
                totalSignRecords,
                signInIncome,
                todayHadSignIn
            }

            res.send(siteFunc.renderApiData(req, res, 200, 'getBasicSignInInfo', signWantData, 'save'));

        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'getBasicSignInInfo'));

        }
    }

    async addSignIn(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                checkFormData(req, res, fields);

                let currentDate = moment().format("YYYY-MM-DD");

                // 判断今天是否已签到
                let oldSign = await SignInModel.find({
                    user: req.session.user._id,
                    date: currentDate
                });
                if (!_.isEmpty(oldSign)) {
                    throw new siteFunc.UserException(res.__('label_had_signIn'));
                }

                // 判断昨天是否签到
                let lastDate = moment().add(-1, 'days').format('YYYY-MM-DD');

                let lastDaySignRecord = await SignInModel.findOne({
                    user: req.session.user._id,
                    date: lastDate
                });

                // 如果前一天没有签到，则之前积累的标记作废，从今天算起
                let beginFlag = false;
                console.log('---lastDaySignRecord---', lastDaySignRecord)
                if (_.isEmpty(lastDaySignRecord)) {
                    await SignInModel.updateMany({
                        user: req.session.user._id,
                        flag: true
                    }, {
                        $set: {
                            flag: false
                        }
                    });
                    console.log('---111----')
                    beginFlag = true;
                } else {
                    // 如果前一天已经清算，则也从今天算起
                    if (!lastDaySignRecord.flag && lastDaySignRecord.state == '1') {
                        beginFlag = true;
                    }
                }

                const tagObj = {
                    date: currentDate,
                    user: req.session.user._id,
                    state: '0',
                    flag: beginFlag
                }
                console.log('---tagObj----', tagObj);
                const newSignIn = new SignInModel(tagObj);

                await newSignIn.save();

                // 判断7天内连续签到次数
                let wantCoins;
                let rangeTime = siteFunc.getDateStr(-8);
                let oldSignRecords = await SignInModel.find({
                    user: req.session.user._id,
                    date: {
                        "$gte": new Date(rangeTime.startTime),
                        "$lte": new Date(rangeTime.endTime)
                    }
                }).sort({
                    date: 1
                });
                let flagTrueIndex = 0,
                    recordsIds = [];
                oldSignRecords.map((item, index) => {
                    if (item.flag) {
                        flagTrueIndex = index;
                    }
                    recordsIds.push(item._id);
                });

                let countinuDateNum = oldSignRecords.length - flagTrueIndex;
                if (countinuDateNum == 7) {
                    let configIntegral = await siteFunc.getIntegralonfig();
                    wantCoins = configIntegral.signIn_continuity;
                    // 标记之前的记录
                    await SignInModel.updateMany({
                        _id: {
                            $in: recordsIds
                        }
                    }, {
                        $set: {
                            flag: false
                        }
                    });
                    // 更新今天节点状态为已经累计发放
                    await SignInModel.findOneAndUpdate({
                        user: req.session.user._id,
                        date: currentDate
                    }, {
                        $set: {
                            state: '1'
                        }
                    })
                }

                // 添加行为积分
                await siteFunc.addUserActionHis(req, res, settings.user_bill_type_signIn, {
                    wantCoins
                });

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('user_bill_type_signIn')
                }), {
                    cumulative: countinuDateNum
                }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

}

module.exports = new SignIn();