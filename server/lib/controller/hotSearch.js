const BaseComponent = require('../prototype/baseComponent');
const HotSearchModel = require("../models").HotSearch;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    if (!validator.isLength(fields.name, 1, 12)) {
        errMsg = res.__("validate_rangelength", {
            min: 1,
            max: 12,
            label: res.__("label_tag_name")
        });
    }
    if (!validator.isLength(fields.comments, 2, 30)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 30,
            label: res.__("label_comments")
        });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class HotSearch {
    constructor() {
        // super()
    }
    async getHotSearchs(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {};
            let useClient = req.query.useClient;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.word = {
                    $regex: reKey
                }
            }
            // console.log('----useClient--', useClient);
            let files = useClient != '0' ? 'word id frequency' : null;
            // console.log('----files--', files);cc
            const hotSearchs = await HotSearchModel.find(queryObj, files).sort({
                frequency: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize));
            const totalItems = await HotSearchModel.count(queryObj);

            let hotSearchData = {
                docs: hotSearchs,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderHotSearchData = siteFunc.renderApiData(req, res, 200, 'HotSearch', hotSearchData);
            if (modules && modules.length > 0) {
                return renderHotSearchData.data;
            } else {
                if (useClient == '1') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getHotSearchs', hotSearchs));
                } else if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getHotSearchs', hotSearchs));
                } else {
                    res.send(renderHotSearchData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }


    async delHotSearch(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await HotSearchModel.remove({
                _id: req.query.ids
            });
            res.send(siteFunc.renderApiData(req, res, 200, 'HotSearch', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new HotSearch();