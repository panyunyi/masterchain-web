const BaseComponent = require('../prototype/baseComponent');
const FeedBackModel = require("../models").FeedBack;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    // if (!validator.isLength(fields.name, 1, 50)) {
    //     errMsg = res.__("validate_rangelength", { min: 1, max: 50, label: res.__("label_tag_name") });
    // }
    if (!validator.isLength(fields.comments, 2, 2000)) {
        errMsg = res.__("validate_rangelength", {
            min: 100,
            max: 2,
            label: res.__("label_comments")
        });
    }
    if (!fields.phoneNum || !validatorUtil.checkPhoneNum((fields.phoneNum).toString())) {
        errMsg = res.__("validate_inputCorrect", {
            label: res.__("label_user_phoneNum")
        })
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class FeedBack {
    constructor() {
        // super()
    }
    async getFeedBacks(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {};
            let useClient = req.query.useClient;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = {
                    $regex: reKey
                }
            }

            const feedBacks = await FeedBackModel.find(queryObj).sort({
                time: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'user',
                select: 'name userName _id'
            }]).exec();
            const totalItems = await FeedBackModel.count(queryObj);

            let feedBackData = {
                docs: feedBacks,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderFeedBackData = siteFunc.renderApiData(req, res, 200, 'FeedBack', feedBackData);
            if (modules && modules.length > 0) {
                return renderFeedBackData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getFeedBacks', feedBacks));
                } else {
                    res.send(renderFeedBackData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async addFeedBack(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                checkFormData(req, res, fields);

                const tagObj = {
                    state: false,
                    user: req.session.user._id,
                    phoneNum: fields.phoneNum,
                    comments: fields.comments
                }

                const newFeedBack = new FeedBackModel(tagObj);

                await newFeedBack.save();

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('user_bill_type_feedback')
                }), {
                    id: newFeedBack._id
                }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateFeedBack(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
                const userObj = {
                    // name: fields.name,
                    state: fields.state,
                    updateUser: req.session.adminUserInfo._id,
                    comments: fields.comments,
                    updateTime: new Date()
                }
                const item_id = fields._id;

                await FeedBackModel.findOneAndUpdate({
                    _id: item_id
                }, {
                    $set: userObj
                });
                res.send(siteFunc.renderApiData(req, res, 200, 'FeedBack', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delFeedBack(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await FeedBackModel.remove({
                _id: req.query.ids
            });
            res.send(siteFunc.renderApiData(req, res, 200, 'FeedBack', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new FeedBack();