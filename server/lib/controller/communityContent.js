const BaseComponent = require('../prototype/baseComponent');
const CommunityContentModel = require("../models").CommunityContent;
const UserModel = require("../models").User;
const BillRecordModel = require("../models").BillRecord;
const CommunityModel = require("../models").Community;
const CommunityMessageModel = require("../models").CommunityMessage;
const CommunityContentMediaModel = require("../models").CommunityContentMedia;
const formidable = require('formidable');
const moment = require('moment');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')
const settings = require('../../../configs/settings');
const xss = require("xss");

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }

    if (!fields.type) {
        errMsg = res.__("validate_error_params");
    }

    if (!siteFunc.checkCurrentId(fields.community)) {
        errMsg = res.__("validate_selectNull", {
            label: res.__("community_label_community")
        });
    }
    if (!validator.isLength(fields.comments, 2, 500)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 500,
            label: res.__("label_content_comments")
        });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

function renderCommunityList(userInfo = {}, communityList = [], useClient = '') {

    return new Promise(async (resolve, reject) => {
        try {
            let newCommunityList = JSON.parse(JSON.stringify(communityList));
            // console.log('--newCommunityList---', newCommunityList)
            for (let contentItem of newCommunityList) {
                contentItem.hasPraised = false;
                contentItem.hasFavorite = false;
                contentItem.hasDespise = false;
                contentItem.community.had_watched = false;
                contentItem.community.had_watched_success = false;

                if (!_.isEmpty(userInfo)) {
                    userInfo = await UserModel.findOne({
                        _id: userInfo._id
                    }, siteFunc.getAuthUserFields('session'));

                    // console.log('---userInfo---', userInfo)
                    // 本人是否已点赞
                    if (userInfo.praiseCommunityContent && userInfo.praiseCommunityContent.indexOf(contentItem._id) >= 0) {
                        contentItem.hasPraised = true;
                    }

                    // 本人是否已收藏
                    if (userInfo.favoriteCommunityContent && userInfo.favoriteCommunityContent.indexOf(contentItem._id) >= 0) {
                        contentItem.hasFavorite = true;
                    }

                    // 本人是否已加入
                    if (userInfo.watchCommunity && userInfo.watchCommunity.indexOf(contentItem.community._id) >= 0) {
                        contentItem.community.had_watched = true;
                        let targetCommunity = _.filter(userInfo.watchCommunityState, (item) => {
                            return item.community = contentItem.community._id;
                        })

                        // 是否已成功加入(被管理员接受)
                        if (!_.isEmpty(targetCommunity)) {
                            contentItem.community.had_watched_success = targetCommunity[0].state == '1' ? true : false;
                        } else {
                            contentItem.community.had_watched_success = false;
                        }
                    }

                    // 本人是否已踩
                    if (userInfo.despiseCommunityContent && userInfo.despiseCommunityContent.indexOf(contentItem._id) >= 0) {
                        contentItem.hasDespise = true;
                    }

                    // 本人是否已打赏
                    let rewardInfo = await BillRecordModel.find({
                        user: userInfo._id,
                        type: settings.user_action_type_appreciate_out,
                        target_communityContent: contentItem._id
                    });
                    if (!_.isEmpty(rewardInfo)) {
                        contentItem.hasReworded = true;
                    }

                    // 本人是否已留言
                    let contentMessage = await CommunityMessageModel.find({
                        contentId: contentItem._id,
                        author: userInfo._id
                    });
                    if (!_.isEmpty(contentMessage)) {
                        contentItem.hasComment = true;
                    }

                }

                // 留言总数
                let commentNum = await CommunityMessageModel.count({
                    contentId: contentItem._id
                });
                contentItem.commentNum = commentNum;

                // 点赞总数
                let likeNum = await UserModel.count({
                    praiseCommunityContent: contentItem._id
                });
                contentItem.likeNum = likeNum;

                // 收藏总数
                let favoriteNum = await UserModel.count({
                    favoriteCommunityContent: contentItem._id
                });
                contentItem.favoriteNum = favoriteNum;

                // 踩帖总数
                let despiseNum = await UserModel.count({
                    despiseCommunityContent: contentItem._id
                });
                contentItem.despiseNum = despiseNum;

                // 打赏总金额
                let total_reward_num = await BillRecordModel.aggregate([{
                        $match: {
                            target_communityContent: contentItem._id,
                            type: settings.user_action_type_appreciate_in
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            total_num: {
                                $sum: "$coins"
                            }
                        }
                    }
                ]);

                contentItem.total_reward_num = total_reward_num.length > 0 ? total_reward_num[0].total_num : 0;

            }
            // console.log('---newCommunityList---', newCommunityList)
            resolve(newCommunityList);
        } catch (error) {
            resolve(communityList);
        }
    })

}


class CommunityContent {
    constructor() {
        // super()
    }
    async getCommunityContents(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {
                    state: '1'
                };
            let useClient = req.query.useClient;
            let communityId = req.query.communityId;
            let type = req.query.type || '0';
            let userInfo = req.session.user || {};
            let sortby = req.query.sortby || '1';
            let attention = req.query.attention;
            let allowCommunity = [];

            if (!_.isEmpty(userInfo)) {
                userInfo = await UserModel.findOne({
                    _id: userInfo._id
                }, siteFunc.getAuthUserFields('community'));

                if (!_.isEmpty(userInfo.watchCommunityState)) {
                    allowCommunity = (userInfo.watchCommunityState).map((item, index) => {
                        if (item.state == '1') {
                            return item.community
                        }
                    })
                }

            }

            // 只查询已审核的社群
            let visibleCommunity = await CommunityModel.find({
                state: '1',
                open: '1'
            }, '_id id');
            let currentCommunityArr = visibleCommunity.map((item, index) => {
                return item._id;
            })
            if (!_.isEmpty(currentCommunityArr)) {
                queryObj.community = {
                    $in: currentCommunityArr
                }
            }
            let defaultPopulate = [{
                path: 'user',
                select: 'name userName _id logo group praiseCommunityContent watchCommunity watchCommunityState'
            }, {
                path: 'community',
                populate: [{
                    path: 'tags',
                    select: 'name _id'
                }, {
                    path: 'creator',
                    select: 'userName _id group'
                }]
            }, {
                path: 'mediaArr',
            }];

            if (sortby == '2') {
                defaultPopulate = [{
                    path: 'user',
                    select: 'name userName _id logo group praiseCommunityContent watchCommunity watchCommunityState',
                    options: {
                        sort: {
                            'praiseCommunityContent': 1
                        }
                    }
                }, {
                    path: 'community',
                    populate: [{
                        path: 'tags',
                        select: 'name _id'
                    }, {
                        path: 'creator',
                        select: 'userName _id group'
                    }]
                }, {
                    path: 'mediaArr',
                }];

                if (!_.isEmpty(userInfo)) {

                    if (attention == '1') {
                        queryObj.community = {
                            $in: allowCommunity || []
                        }
                    } else {
                        if (!_.isEmpty(userInfo.watchCommunity) && userInfo.watchCommunity.length > 6) {
                            queryObj.community = {
                                $in: userInfo.watchCommunity
                            }
                        }
                    }

                }

            } else {

                if (!_.isEmpty(userInfo) && attention == '1') {

                    queryObj.community = {
                        $in: allowCommunity || []
                    }

                }

            }


            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.comments = {
                    $regex: reKey
                }
            }

            if (communityId) {
                queryObj.community = communityId;
            }

            if (type) {
                queryObj.type = type;
            }

            // console.log('--queryObj--', queryObj);

            const communityContents = await CommunityContentModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate(defaultPopulate).exec();

            const totalItems = await CommunityContentModel.count(queryObj);

            let newCommunityList = await renderCommunityList(userInfo, communityContents, useClient);

            let communityContentData = {
                docs: newCommunityList,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };

            let renderCommunityContentData = siteFunc.renderApiData(req, res, 200, 'CommunityContent', communityContentData);

            if (modules && modules.length > 0) {
                return renderCommunityContentData.data;
            } else {
                if (req.query.useClient == '1') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getCommunityContents', communityContentData));
                } else if (req.query.useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getCommunityContents', newCommunityList));
                } else {
                    res.send(renderCommunityContentData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async updateCommunityContent(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                let errMsg = '';
                if (!siteFunc.checkCurrentId(fields._id)) {
                    errMsg = res.__("validate_error_params");
                }

                if (!validator.isLength(fields.comments, 2, 500)) {
                    errMsg = res.__("validate_rangelength", {
                        min: 2,
                        max: 500,
                        label: res.__("label_content_comments")
                    });
                }

                if (errMsg) {
                    throw new siteFunc.UserException(errMsg);
                }

                const userObj = {
                    comments: xss(fields.comments)
                }
                const item_id = fields._id;

                await CommunityContentModel.findOneAndUpdate({
                    _id: item_id
                }, {
                    $set: userObj
                });

                res.send(siteFunc.renderApiData(req, res, 200, 'updateCommunityContent', {}, 'options'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }


    async addCommunityContent(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                checkFormData(req, res, fields);

                let targetUser = await UserModel.findOne({
                    _id: req.session.user._id
                }, siteFunc.getAuthUserFields('session'));

                // 校验社群是否存在
                let targetCommunity = await CommunityModel.findOne({
                    _id: fields.community
                });
                if (_.isEmpty(targetCommunity)) {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }

                // 没有加入社群不能发表
                if ((!targetUser.watchCommunity || targetUser.watchCommunity.indexOf(fields.community) < 0) && (targetCommunity.creator != targetUser._id)) {
                    throw new siteFunc.UserException(res.__("community_label_add_first"));
                }

                if (fields.type == '1' && targetCommunity.creator != targetUser._id) {
                    throw new siteFunc.UserException(res.__("community_label_only_creator"));
                }

                const tagObj = {
                    user: req.session.user._id,
                    community: fields.community,
                    thumbnail: fields.thumbnail,
                    type: fields.type,
                    state: '1',
                    comments: xss(fields.comments)
                }

                const newCommunityContent = new CommunityContentModel(tagObj);

                let targetCommunityContent = await newCommunityContent.save();

                if (fields.mediaArr) { // 可以为空
                    let mediaArr = JSON.parse(fields.mediaArr);
                    let renderMediaArr = [];
                    for (const mediaItem of mediaArr) {
                        let mediaObj = {
                            type: mediaItem.type,
                            link: mediaItem.link,
                            content: targetCommunityContent._id
                        }
                        // 生成缩略图字段
                        if (mediaItem.type == '1' && mediaItem.link) {
                            mediaObj.videoImg = siteFunc.getVideoImgByLink(mediaItem.link);
                        }
                        let targetMediaItem = new CommunityContentMediaModel(mediaObj);
                        let newMediaItem = await targetMediaItem.save();
                        renderMediaArr.push(newMediaItem._id);
                    }
                    if (renderMediaArr.length > 0) {
                        await CommunityContentModel.findOneAndUpdate({
                            _id: targetCommunityContent._id
                        }, {
                            $set: {
                                mediaArr: renderMediaArr
                            }
                        });
                    }
                }

                // 一天内社群建设数量不超过5次不添加奖励
                let rangeTime = siteFunc.getDateStr(-1);
                let post_count = await BillRecordModel.count({
                    user: req.session.user._id,
                    type: settings.user_action_type_community_building,
                    date: {
                        "$gte": new Date(rangeTime.startTime),
                        "$lte": new Date(rangeTime.endTime)
                    }
                });

                let configIntegral = await siteFunc.getIntegralonfig();
                if (post_count < configIntegral.community_building_limit) {
                    await siteFunc.addUserActionHis(req, res, settings.user_action_type_community_building, {
                        targetId: targetCommunityContent._id
                    });
                } else {
                    console.log('进入社群建设奖励超出限制，不再发放');
                }

                // 更新最后发言时间
                await UserModel.findOneAndUpdate({
                    _id: targetUser._id
                }, {
                    $set: {
                        lastCommunitySpeach: new Date(),
                    }
                });

                res.send(siteFunc.renderApiData(req, res, 200, 'addCommunityContent', {
                    id: newCommunityContent._id
                }, settings.user_action_type_community_building))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async delCommunityContent(req, res, next) {
        try {

            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.id)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }

            let targetContent = await CommunityContentModel.findOne({
                _id: req.query.id
            }).populate([{
                path: 'community',
                select: 'name _id creator'
            }]).exec();

            if (_.isEmpty(targetContent)) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }

            // console.log('---targetContent---', targetContent);

            if (req.query.useClient == '2' && targetContent.community.creator != req.session.user._id) {
                throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
            }

            // 删除媒体记录
            // let mediaArr = targetContent.mediaArr;
            // for (const mediaItem of mediaArr) {
            //     await CommunityContentMediaModel.remove({
            //         _id: mediaItem
            //     });
            // }

            // await CommunityContentModel.remove({
            //     _id: req.query.id
            // });

            await CommunityContentModel.updateOne({
                _id: req.query.id
            }, {
                state: '0'
            });


            res.send(siteFunc.renderApiData(req, res, 200, 'delCommunityContent', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new CommunityContent();