const BaseComponent = require('../prototype/baseComponent');
const SpecialModel = require("../models").Special;
const UserModel = require("../models").User;
const ContentModel = require("../models").Content;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')
const xss = require("xss");

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }

    if (!validator.isLength(fields.name, 1, 12)) {
        errMsg = res.__("validate_rangelength", {
            min: 1,
            max: 12,
            label: res.__("label_tag_name")
        });
    }
    if (!validator.isLength(fields.comments, 2, 50)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 50,
            label: res.__("label_comments")
        });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

function renderSpecialItem(userId = '', specialList) {

    return new Promise(async (resolve, reject) => {
        try {
            // console.log('----userId---', userId);
            // console.log('----specialList---', specialList);
            let newSpecialList = JSON.parse(JSON.stringify(specialList));
            let userInfo = {};
            if (userId) {
                userInfo = await UserModel.findOne({
                    _id: userId
                }, siteFunc.getAuthUserFields('session'));
            }
            for (const newSpecial of newSpecialList) {

                let watchState = false
                let watchNum = await UserModel.count({
                    watchSpecials: newSpecial._id
                });
                newSpecial.watch_num = watchNum;
                if (!_.isEmpty(userInfo) && userInfo.watchSpecials && (userInfo.watchSpecials).indexOf(newSpecial._id) >= 0) {
                    watchState = true;
                }
                newSpecial.had_watched = watchState;

                let content_num = await ContentModel.count({
                    categories: newSpecial._id,
                    state: '2'
                });
                newSpecial.total_contentNum = content_num;
            }
            resolve(newSpecialList);
        } catch (error) {
            resolve({})
        }
    })
}

class Special {
    constructor() {
        // super()
    }
    async getSpecials(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let searchkey = req.query.searchkey,
                queryObj = {};
            let useClient = req.query.useClient;
            let creator = req.query.creator;
            let userInfo = req.session.user || {};
            let fieldStr = [{
                path: 'creator',
                select: 'name userName _id'
            }, {
                path: 'adminCreator',
                select: 'name userName _id'
            }, {
                path: 'category',
                select: 'name _id'
            }, {
                path: 'approver',
                select: 'name userName _id'
            }];


            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = {
                    $regex: reKey
                }
            }

            if (creator) {
                queryObj.creator = creator;
            }

            if (useClient != '0') {
                queryObj.state = '1';
            }

            if (useClient == '2') {
                fieldStr = [{
                    path: 'creator',
                    select: 'name userName _id'
                }, {
                    path: 'category',
                    select: 'name _id'
                }]
            }

            const specials = await SpecialModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate(fieldStr).exec();
            const totalItems = await SpecialModel.count(queryObj);

            let renderSpecials = await renderSpecialItem(userInfo._id, specials);

            let specialData = {
                docs: renderSpecials,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderSpecialData = siteFunc.renderApiData(req, res, 200, 'Special', specialData);
            if (modules && modules.length > 0) {
                return renderSpecialData.data;
            } else {
                if (req.query.useClient == '1') {
                    res.send(renderSpecialData);
                } else if (req.query.useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getSpecials', renderSpecials));
                } else {
                    res.send(renderSpecialData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }


    async getMyFollowSpecials(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let searchkey = req.query.searchkey;
            let useClient = req.query.useClient;
            let userInfo = req.session.user;
            let userId = req.query.userId;

            let queryObj = {
                state: '1'
            };

            let targetUser = await UserModel.findOne({
                _id: userId
            });
            if (!_.isEmpty(targetUser) && targetUser.watchSpecials) {
                queryObj = {
                    _id: {
                        $in: targetUser.watchSpecials
                    }
                }
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = {
                    $regex: reKey
                }
            }

            const specials = await SpecialModel.find(queryObj, {
                creator: 0,
                approver: 0
            }).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).exec();
            const totalItems = await SpecialModel.count(queryObj);

            let renderSpecials = await renderSpecialItem(userInfo._id, specials);

            let specialData = {
                docs: specials,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderSpecialData = siteFunc.renderApiData(req, res, 200, 'Special', specialData);
            if (modules && modules.length > 0) {
                return renderSpecialData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getSpecials', renderSpecials));
                } else {
                    res.send(renderSpecialData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    // 针对管理员创建专题
    async addSpecial(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                checkFormData(req, res, fields);

                const tagObj = {
                    name: fields.name,
                    sImg: fields.sImg,
                    state: fields.state,
                    category: fields.category,
                    comments: fields.comments,
                    adminCreator: req.session.adminUserInfo._id
                }

                if (fields.creator) {
                    tagObj.creator = fields.creator;
                    delete tagObj.adminCreator;
                }
                // console.log('--tagObj----', tagObj)

                const newSpecial = new SpecialModel(tagObj);

                await newSpecial.save();

                res.send(siteFunc.renderApiData(req, res, 200, 'Special', {
                    id: newSpecial._id
                }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateSpecial(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
            } catch (err) {
                console.log(err.message, err);
                res.send(siteFunc.renderApiErr(req, res, 500, err, 'checkform'));
            }

            const userObj = {
                name: fields.name,
                sImg: fields.sImg,
                state: fields.state,
                category: fields.category,
                comments: fields.comments
            }
            const item_id = fields._id;
            try {
                await SpecialModel.findOneAndUpdate({
                    _id: item_id
                }, {
                    $set: userObj
                });
                res.send(siteFunc.renderApiData(req, res, 200, 'Special', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delSpecial(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await SpecialModel.remove({
                _id: req.query.ids
            });
            res.send(siteFunc.renderApiData(req, res, 200, 'Special', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }



}

module.exports = new Special();