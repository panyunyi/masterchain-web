const BaseComponent = require('../prototype/baseComponent');
const BillRecordModel = require("../models").BillRecord;
const UserModel = require("../models").User;
const User_reward_hisModel = require("../models").User_reward_his;
const RewardHisModel = require("../models").RewardHis;
const TransactionModel = require("../models").Transaction;
const CurrencyApprovalModel = require("../models").CurrencyApproval;
const OpenVipRecordModel = require("../models").OpenVipRecord;
const formidable = require('formidable');

const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')
const uuidv1 = require('uuid/v1');
const settings = require('../../../configs/settings');
const axios = require('axios')

class BillRecord {
    constructor() {
        // super()
    }
    async getBillRecords(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let billType = req.query.billType;
            let type = req.query.type;
            let searchkey = req.query.searchkey;
            let useClient = req.query.useClient;
            let user = req.session.user;
            let queryObj = {};
            if (type) {
                queryObj.type = type;
            }
            let time = req.query.time;

            if (user && useClient != '0') {
                queryObj.user = user._id
            }

            if (billType == '1') { // 收入
                queryObj.coins = {
                    $gte: 0
                };
            } else if (billType == '2') { // 支出
                queryObj.coins = {
                    $lte: 0
                };
            } else if (billType == '3') { // 充值
                queryObj.type = settings.user_bill_type_recharge;
                queryObj.unit = 'MEC';
            } else if (billType == '4') { // 提币
                queryObj.type = settings.user_bill_type_transfer_coins;
                queryObj.unit = 'MVPC';
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i');
                queryObj.uuid = {
                    $regex: reKey
                }
            }

            if (time) {
                let currentTime = 0;
                if (time == 'd7') {
                    currentTime = -7;
                } else if (time == 'd30') {
                    currentTime = -30;
                } else if (time == 'd60') {
                    currentTime = -60;
                } else if (time == 'd180') {
                    currentTime = -180;
                } else if (time == 'd365') {
                    currentTime = -365;
                } else if (time == 'all') {
                    currentTime = -365 * 10;
                } else {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }
                let rangeTime = siteFunc.getDateStr(currentTime);
                queryObj.date = {
                    "$gte": new Date(rangeTime.startTime),
                    "$lte": new Date(rangeTime.endTime)
                }
            }

            let totalBillObj = await BillRecordModel.aggregate([{
                    $match: queryObj
                },
                {
                    $group: {
                        _id: null,
                        total_num: {
                            $sum: "$coins"
                        }
                    }
                }
            ]);

            let totalBill_num = totalBillObj.length > 0 ? totalBillObj[0].total_num : 0;

            let actionPopulateObj = [{
                path: 'user',
                select: 'name userName _id'
            }, {
                path: 'action'
            }];

            if (useClient == '0') {
                actionPopulateObj = [{
                    path: 'user',
                    select: 'name userName _id'
                }, {
                    path: 'action'
                }, {
                    path: 'target_class'
                }, {
                    path: 'target_content'
                }, {
                    path: 'target_message'
                }, {
                    path: 'target_communityContent'
                }, {
                    path: 'target_user'
                }];
            }
            const billRecords = await BillRecordModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate(actionPopulateObj).exec();

            const totalItems = await BillRecordModel.count(queryObj);

            let billRecordData = {
                docs: billRecords,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    total_coins: totalBill_num,
                    searchkey: searchkey || ''
                }
            };
            let renderBillRecordData = siteFunc.renderApiData(req, res, 200, 'BillRecord', billRecordData);
            if (modules && modules.length > 0) {
                return renderBillRecordData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getBillRecords', {
                        billRecords,
                        total_coins: totalBill_num
                    }));
                } else {
                    res.send(renderBillRecordData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async getInvitionRecords(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            // let type = req.query.type || "user_action_type_invitation";
            let searchkey = req.query.searchkey;
            let useClient = req.query.useClient;
            let queryObj = {};

            queryObj.type = "user_action_type_invitation";

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i');
                let targetUserArr = await UserModel.find({
                    userName: {
                        $regex: reKey
                    }
                })

                let userIdsArr = []
                if (!_.isEmpty(targetUserArr) && targetUserArr.length > 0) {
                    userIdsArr = targetUserArr.map((item, index) => {
                        return item._id;
                    })
                }

                // console.log('--userIdsArr--', userIdsArr)

                if (userIdsArr.length > 0) {
                    _.assign(queryObj, {
                        $or: [{
                            user: {
                                $in: userIdsArr
                            }
                        }, {
                            target_user: {
                                $in: userIdsArr
                            }
                        }]
                    })
                }
            }

            let actionPopulateObj = [{
                path: 'user',
                select: 'name userName _id'
            }, {
                path: 'action'
            }, {
                path: 'target_user'
            }];

            // console.log('-queryObj--', queryObj)

            const billRecords = await BillRecordModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate(actionPopulateObj).exec();

            const totalItems = await BillRecordModel.count(queryObj);

            let billRecordData = {
                docs: billRecords,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    // total_coins: totalBill_num,
                    searchkey: searchkey || ''
                }
            };
            let renderBillRecordData = siteFunc.renderApiData(req, res, 200, 'BillRecord', billRecordData);
            if (modules && modules.length > 0) {
                return renderBillRecordData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getBillRecords', {
                        billRecords,
                        // total_coins: totalBill_num
                    }));
                } else {
                    res.send(renderBillRecordData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }



    async getMyCoinsRecords(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let useClient = req.query.useClient;
            let coinUnit = req.query.unit;
            let searchkey = req.query.searchkey;
            let user = req.session.user;
            let time = req.query.time;
            let queryObj = {};
            let rangeTime = ''
            let billType = req.query.billType;

            if (user) {
                queryObj.user = user._id
            }

            if (time) {
                let currentTime = 0;
                if (time == 'd7') {
                    currentTime = -7;
                } else if (time == 'd30') {
                    currentTime = -30;
                } else if (time == 'd60') {
                    currentTime = -60;
                } else if (time == 'd180') {
                    currentTime = -180;
                } else if (time == 'd365') {
                    currentTime = -365;
                } else if (time == 'all') {
                    currentTime = -365 * 10;
                } else {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }
                rangeTime = siteFunc.getDateStr(currentTime);
                if (coinUnit == 'MVPC') {
                    // datetime
                    delete queryObj.user;
                    queryObj.userid = user.walletAddress
                    queryObj.status = 1;
                    queryObj.datetime = {
                        "$gte": new Date(rangeTime.startTime),
                        "$lte": new Date(rangeTime.endTime)
                    }
                } else {
                    queryObj.date = {
                        "$gte": new Date(rangeTime.startTime),
                        "$lte": new Date(rangeTime.endTime)
                    }

                }
            }


            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.uuid = {
                    $regex: reKey
                }
            }

            if (!coinUnit || (coinUnit != 'MEC' && coinUnit != 'MVPC' && coinUnit != 'MBT')) {
                throw new siteFunc.UserException(res.__('validate_inputCorrect', {
                    label: res.__('classInfo_label_price_unit')
                }));
            }

            let totalBill_num = 0,
                billRecords = [],
                totalItems = 0;

            if (coinUnit == 'MEC' || coinUnit == 'MBT') {

                queryObj.unit = coinUnit;
                queryObj.coins = {
                    $ne: 0
                }

                billRecords = await BillRecordModel.find(queryObj).sort({
                    date: -1
                }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                    path: 'user',
                    select: 'name userName _id'
                }, {
                    path: 'action'
                }]).exec();

                totalItems = await BillRecordModel.count(queryObj);

            } else if (coinUnit == 'MVPC') {
                // console.log('----queryObj-----', queryObj)
                let msttBillRecord = [];
                let incomeBillRecords = await User_reward_hisModel.find(queryObj).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).sort({
                    datetime: -1
                });
                // console.log('--incomeBillRecords---', incomeBillRecords)
                totalItems = await User_reward_hisModel.count(queryObj);
                if (!_.isEmpty(incomeBillRecords)) {
                    // incomeBillRecords = JSON.parse(JSON.stringify(incomeBillRecords));
                    for (const income of incomeBillRecords) {
                        let action_coins = income.rewardactionnum;
                        let review_coins = income.rewardreviewernum;
                        let content_coins = income.rewardcontentnum;
                        let total_coins = income.rewardtotalnum;
                        if (action_coins) {
                            msttBillRecord.push({
                                type: '0',
                                coins: action_coins,
                                date: income.datetime,
                                logs: res.__('user_bill_type_action_log')
                            })
                        }
                        if (review_coins) {
                            msttBillRecord.push({
                                type: '1',
                                coins: review_coins,
                                date: income.datetime,
                                logs: res.__('user_action_type_browse')
                            })
                        }
                        if (content_coins) {
                            msttBillRecord.push({
                                type: '2',
                                coins: content_coins,
                                date: income.datetime,
                                logs: res.__('user_bill_type_transfer_log')
                            })
                        }
                    }

                }

                billRecords = msttBillRecord;

            }

            let billRecordData = {
                docs: billRecords,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    total_coins: totalBill_num,
                    totalPage: Math.ceil(totalItems / pageSize),
                    searchkey: searchkey || ''
                }
            };
            let renderBillRecordData = siteFunc.renderApiData(req, res, 200, 'BillRecord', billRecordData);
            if (modules && modules.length > 0) {
                return renderBillRecordData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getBillRecords', billRecords));
                } else {
                    res.send(renderBillRecordData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }


    // 获取收益排行
    async getBillRankingList(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let useClient = req.query.useClient;
            let userInfo = req.session.user;
            let currentData = {};

            let rewardInfo = await RewardHisModel.find({}, '_id datapointtime id starttime endtime').sort({
                datapointtime: -1
            }).limit(1);
            // console.log('---rewardInfo---', rewardInfo)
            let totalRewordObj = []
            if (!_.isEmpty(rewardInfo)) {
                currentData.startTime = rewardInfo[0].starttime;
                currentData.endTime = rewardInfo[0].endtime;
                var mongoose = require('mongoose');
                var rewardid = mongoose.Types.ObjectId((rewardInfo[0]._id));
                totalRewordObj = await User_reward_hisModel.find({
                    rewardid: rewardid
                }, 'userid rewardtotalnum').sort({
                    rewardtotalnum: -1
                }).limit(20);

            }

            let renderBillList = []
            if (!_.isEmpty(totalRewordObj)) {
                renderBillList = JSON.parse(JSON.stringify(totalRewordObj));
                for (const billItem of renderBillList) {
                    let userId = billItem.userid;
                    let targetUser = await UserModel.findOne({
                        walletAddress: userId
                    }, siteFunc.getAuthUserFields);
                    if (!_.isEmpty(targetUser)) {
                        billItem.userName = targetUser.userName;
                        billItem.logo = targetUser.logo;
                        billItem.id = targetUser.id;
                        billItem._id = targetUser.id;
                        delete billItem.userid;
                    }
                }
            }


            currentData.recordList = renderBillList;

            let renderBillRecordData = siteFunc.renderApiData(req, res, 200, 'BillRecord', currentData);
            if (modules && modules.length > 0) {
                return renderBillRecordData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getBillRecords', currentData));
                } else {
                    res.send(renderBillRecordData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }


    async addBillRecord(req, res, params = {}) {

        try {

            let {
                coins,
                logs,
                userId,
                action
            } = params;
            if (!coins || !logs || !userId || !action) {
                throw new siteFunc.UserException(res.__('validate_error_params'));
            }

            const tagObj = {
                coins: params.coins,
                logs: params.logs,
                user: params.userId,
                action: params.action
            }

            const newBillRecord = new BillRecordModel(tagObj);
            await newBillRecord.save();

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
        }

    }

    async delBillRecord(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await BillRecordModel.remove({
                _id: req.query.ids
            });
            res.send(siteFunc.renderApiData(req, res, 200, 'BillRecord', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }

    async getWalletBasicInfo(req, res, next) {

        try {
            let user = req.session.user;


            let onlineMECCoins = await siteFunc.getMECLeftCoins(user.walletAddress);
            let total_MEC_num = onlineMECCoins;

            let onlineMsttCoins = await siteFunc.getMVPCLeftCoins(user.walletAddress);
            let total_MVPC_Bill_Records = await BillRecordModel.aggregate([{
                    $match: {
                        user: user._id,
                        unit: 'MVPC',
                        type: settings.user_bill_type_transfer_coins,
                        state: '0'
                    }
                },
                {
                    $group: {
                        _id: null,
                        total_num: {
                            $sum: "$coins"
                        }
                    }
                }
            ]);

            let forBid_MVPC_num = total_MVPC_Bill_Records.length > 0 ? total_MVPC_Bill_Records[0].total_num : 0;
            let total_MVPC_num = onlineMsttCoins + forBid_MVPC_num;

            let total_MBT_num = await BillRecordModel.aggregate([{
                    $match: {
                        user: user._id,
                        unit: 'MBT'
                    }
                },
                {
                    $group: {
                        _id: null,
                        total_num: {
                            $sum: "$coins"
                        }
                    }
                }
            ]);

            // 判断是否设置了资金密码
            let userInfo = await UserModel.findOne({
                _id: user._id
            }, 'fundPassword');

            let renderData = {
                total_MEC_num: total_MEC_num,
                total_MEC_unit: 'MEC',
                total_MVPC_num: total_MVPC_num,
                total_MVPC_unit: 'MVPC',
                // TODO 临时返回0
                // total_MBT_num: 0,
                total_MBT_num: total_MBT_num.length > 0 ? total_MBT_num[0].total_num : 0,
                total_MBT_unit: 'MBT',
                hasSetFundPassword: (userInfo && userInfo.fundPassword) ? true : false,
                authState: user.authState ? true : false
            }

            res.send(siteFunc.renderApiData(req, res, 200, 'getWalletBasicInfo', renderData, 'getlist'))

        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'save'));
        }

    }

    async getCashGas(req, res, next) {
        try {

            let wantCoin = req.query.coins;

            if (!validator.isNumeric(wantCoin.toString())) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }

            let targetGas = wantCoin * 0.000001;
            res.send(siteFunc.renderApiData(req, res, 200, 'get data success', {
                gas: targetGas
            }, 'getCashGas'));

        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'delete'));
        }
    }

    async getBillDetails(req, res, next) {
        try {

            let targetId = req.query.id;
            let userInfo = req.session.user;

            if (!shortid.isValid(targetId)) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }

            let targetBill = await BillRecordModel.findOne({
                _id: targetId,
                user: userInfo._id
            }, 'transferInfo');

            if (_.isEmpty(targetBill)) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }

            // console.log('---targetBill--', targetBill)

            // 查询交易详情
            let renderInfo = {};
            if (targetBill.transferInfo) { //已通过审核
                let currentTransferInfo = await TransactionModel.findOne({
                    hash: targetBill.transferInfo.hash
                }, 'from to blocknumber commission gasused gasprice gas value');
                renderInfo = currentTransferInfo;

                if (!_.isEmpty(renderInfo)) {
                    renderInfo = JSON.parse(JSON.stringify(renderInfo));
                } else {
                    renderInfo = Object.assign({}, targetBill.transferInfo);
                }

            } else { // 未通过审核或没有状态
                let userApproval = await CurrencyApprovalModel.findOne({
                    billRecord: targetBill._id
                }).populate([{
                    path: 'user',
                    select: 'walletAddress'
                }]);
                renderInfo.from = userApproval.user.walletAddress;
                renderInfo.to = userApproval.walletAddress;
            }

            renderInfo.approvalTime = targetBill.date;
            renderInfo.tradeTime = targetBill.updatetime;
            renderInfo.state = targetBill.state;

            res.send(siteFunc.renderApiData(req, res, 200, 'get data success', renderInfo, 'getCashGas'));

        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'delete'));
        }
    }

    async getInvitationInfo(req, res) {
        try {

            let queryObj = {
                user: req.session.user._id,
                type: settings.user_action_type_invitation
            };

            let invited_num = await BillRecordModel.count(queryObj);

            let totalBillObj = await BillRecordModel.aggregate([{
                    $match: queryObj
                },
                {
                    $group: {
                        _id: null,
                        total_num: {
                            $sum: "$coins"
                        }
                    }
                }
            ]);

            let totalBill_num = totalBillObj.length > 0 ? totalBillObj[0].total_num : 0;

            res.send(siteFunc.renderApiData(req, res, 200, 'getInvitationList', {
                invited_num,
                // TODO 临时返回0
                totalBill_num: totalBill_num
            }));

        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'getInvitationList'));
        }
    }

    async addBillRecordByType(req, res) {

        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {

            try {
                // console.log('---fields----', fields)
                let actionType = fields.actionType;
                let userId = fields.userId;
                let coins = fields.coins;
                let logs = fields.logs;

                await siteFunc.checkAdminPostToken(req, res, fields.token);

                // if (linkToken != 'PXrZrL4ALVFF8Rah2TLorEAVlraAAK') {
                //     throw new siteFunc.UserException(res.__('validate_error_params'));
                // }

                if (!shortid.isValid(userId)) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                if (!validator.isNumeric(coins.toString())) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                if (actionType == '1') {
                    logs = res.__('user_bill_type_commute');
                } else if (actionType == '2') {
                    logs = res.__('user_bill_type_activity_reward');
                } else if (actionType == '3') {
                    logs = res.__('user_bill_type_backwash');
                } else if (actionType == '9') {
                    logs = res.__('user_bill_type_others');
                }

                if (!logs) {
                    // console.log('---222-', logs)
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                let targetUser = await UserModel.findOne({
                    _id: userId
                }, siteFunc.getAuthUserFields('basic'));

                // console.log('---111-', targetUser)
                if (_.isEmpty(targetUser)) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                // 扣币
                if (Number(coins) < 0) {
                    // 获取我的账户余额
                    let totalBill_records = await BillRecordModel.aggregate([{
                            $match: {
                                user: userId,
                                unit: 'MBT'
                            }
                        },
                        {
                            $group: {
                                _id: null,
                                total_num: {
                                    $sum: "$coins"
                                }
                            }
                        }
                    ]);
                    let totalBill_num = totalBill_records.length > 0 ? totalBill_records[0].total_num : 0;
                    // console.log('--MBTCoins----', totalBill_num);
                    if (totalBill_num < Number(coins) * -1) {
                        throw new siteFunc.UserException(res.__("bill_notice_no_coins"));
                    }
                }

                let beRewardRecord = new BillRecordModel({
                    coins: Number(coins),
                    type: 'user_action_type_shop_send',
                    logs: logs,
                    user: targetUser._id,
                    target_user: '',
                    action: '',
                    unit: 'MBT',
                    uuid: uuidv1().split('-').join(''),
                    state: '0'
                })
                await beRewardRecord.save();

                res.send(siteFunc.renderApiData(req, res, 200, 'addBillRecordByType', {}));

            } catch (error) {
                res.send(siteFunc.renderApiErr(req, res, 500, error, 'addBillRecordByType'));
            }
        })

    }

    // 商城获取用户信息 MEC MBT VIP有效期等
    async getShopBasicInfo(req, res, next) {

        try {
            let userId = req.query.userId;

            let user = await UserModel.findOne({
                _id: userId
            }, siteFunc.getAuthUserFields('session'));

            if (_.isEmpty(user)) {
                throw new siteFunc.UserException(res.__('validate_error_params'));
            }

            let onlineMECCoins = await siteFunc.getMECLeftCoins(user.walletAddress);
            let total_MEC_num = onlineMECCoins;

            let onlineMsttCoins = await siteFunc.getMVPCLeftCoins(user.walletAddress);
            let total_MVPC_Bill_Records = await BillRecordModel.aggregate([{
                    $match: {
                        user: user._id,
                        unit: 'MVPC',
                        type: settings.user_bill_type_transfer_coins,
                        state: '0'
                    }
                },
                {
                    $group: {
                        _id: null,
                        total_num: {
                            $sum: "$coins"
                        }
                    }
                }
            ]);

            let forBid_MVPC_num = total_MVPC_Bill_Records.length > 0 ? total_MVPC_Bill_Records[0].total_num : 0;
            let total_MVPC_num = onlineMsttCoins + forBid_MVPC_num;

            let total_MBT_num = await BillRecordModel.aggregate([{
                    $match: {
                        user: user._id,
                        unit: 'MBT'
                    }
                },
                {
                    $group: {
                        _id: null,
                        total_num: {
                            $sum: "$coins"
                        }
                    }
                }
            ]);


            // 会员有效期
            let endTime = await siteFunc.getVipTime(user);

            let renderData = {
                total_MEC_num: total_MEC_num,
                total_MEC_unit: 'MEC',
                total_MVPC_num: total_MVPC_num,
                total_MVPC_unit: 'MVPC',
                total_MBT_num: total_MBT_num.length > 0 ? total_MBT_num[0].total_num : 0,
                total_MBT_unit: 'MBT',
                isVip: user.vip,
                endTime
            }

            res.send(siteFunc.renderApiData(req, res, 200, 'getWalletBasicInfo', renderData, 'getlist'))

        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'save'));
        }

    }


    // 获取商城发放的MBT列表
    async getShopMallMbtList(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let searchkey = req.query.searchkey;

            let queryObj = {
                type: 'user_action_type_shop_send',
                unit: 'MBT'
            };

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.uuid = {
                    $regex: reKey
                }
            }

            const billRecords = await BillRecordModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'user',
                select: 'name userName _id'
            }]).exec();

            const totalItems = await BillRecordModel.count(queryObj);

            let billRecordData = {
                docs: billRecords,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderBillRecordData = siteFunc.renderApiData(req, res, 200, 'BillRecord', billRecordData);
            if (modules && modules.length > 0) {
                return renderBillRecordData.data;
            } else {
                res.send(renderBillRecordData);
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }


}

module.exports = new BillRecord();