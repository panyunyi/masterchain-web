const BaseComponent = require('../prototype/baseComponent');
const UserActionHisModel = require("../models").UserActionHis;
const UserModel = require("../models").User;
const ContentModel = require("../models").Content;
const MessageModel = require("../models").Message;
const CommunityContentModel = require("../models").CommunityContent;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    if (!validator.isLength(fields.target, 1, 12)) {
        errMsg = res.__("validate_rangelength", {
            min: 1,
            max: 12,
            label: res.__("label_tag_name")
        });
    }
    if (!validator.isLength(fields.action, 2, 30)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 30,
            label: res.__("label_comments")
        });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class UserActionHis {
    constructor() {
        // super()
    }
    async getUserActionHiss(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {};
            let useClient = req.query.useClient;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = {
                    $regex: reKey
                }
            }

            const userActionHiss = await UserActionHisModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'users',
                select: 'name userName _id'
            }]).exec();
            const totalItems = await UserActionHisModel.count(queryObj);

            let userActionHisData = {
                docs: userActionHiss,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderUserActionHisData = siteFunc.renderApiData(req, res, 200, 'UserActionHis', userActionHisData);
            if (modules && modules.length > 0) {
                return renderUserActionHisData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getUserActionHiss', userActionHiss));
                } else {
                    res.send(renderUserActionHisData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }


    async getUserDynamic(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let userId = req.query.userId;

            let queryObj = {},
                targetUser = {};
            let useClient = req.query.useClient;

            if (userId) {
                targetUser = await UserModel.findOne({
                    _id: userId
                }, '_id walletAddress userName');
                if (!_.isEmpty(targetUser) && targetUser.walletAddress) {
                    queryObj.userid = targetUser.walletAddress;
                } else {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }
            } else {
                throw new siteFunc.UserException(res.__('validate_error_params'));
            }

            const userActionHiss = await UserActionHisModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize));
            const totalItems = await UserActionHisModel.count(queryObj);

            // let renderHisData = JSON.parse(JSON.stringify(userActionHiss));
            let dynamicArr = [];
            for (const hisItem of userActionHiss) {
                let targetAction = hisItem.action;
                let targetContent = hisItem.target_content;

                // "target_content" : "{\"id\":\"qqmiJozFM\",\"createdatetime\":\"2018-12-08 21:20:42\",\"status\":true,\"author\":\"master4\",\"userid\":\"\"}",
                if (targetAction && targetContent) {
                    let targetContentObj = JSON.parse(targetContent);
                    let thisContent = await ContentModel.findOne({
                        _id: targetContentObj.id,
                        state: '2'
                    }, 'title date state uAuthor').populate([{
                        path: 'uAuthor',
                        select: 'id userName'
                    }]).exec();
                    let thisMessage = await MessageModel.findOne({
                        _id: targetContentObj.id
                    }, 'author contentId state').populate([{
                        path: 'author',
                        select: 'id userName'
                    }, {
                        path: 'contentId',
                        select: 'title id'
                    }]).exec();
                    let thisCommunityContent = await CommunityContentModel.findOne({
                        _id: targetContentObj.id
                    }, 'comments date state user').populate([{
                        path: 'user',
                        select: 'id userName'
                    }]).exec();

                    dynamicArr.push({
                        type: targetAction,
                        targetUser: targetUser,
                        targetContent: thisContent || {},
                        targetMessage: thisMessage || {},
                        targetCommunityContent: thisCommunityContent || {}
                    })
                }
            }

            let userActionHisData = {
                docs: dynamicArr,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10
                }
            };
            let renderUserActionHisData = siteFunc.renderApiData(req, res, 200, 'UserActionHis', userActionHisData);
            if (modules && modules.length > 0) {
                return renderUserActionHisData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getUserActionHiss', dynamicArr));
                } else {
                    res.send(renderUserActionHisData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }


    async updateUserActionHis(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
                const userObj = {
                    target: fields.target,
                    action: fields.action,
                    user_id: fields.user_id,
                    flag: fields.flag,
                    updatetime: new Date()
                }
                const item_id = fields._id;

                await UserActionHisModel.findOneAndUpdate({
                    _id: item_id
                }, {
                    $set: userObj
                });
                res.send(siteFunc.renderApiData(req, res, 200, 'UserActionHis', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delUserActionHis(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await UserActionHisModel.remove({
                _id: req.query.ids
            });
            res.send(siteFunc.renderApiData(req, res, 200, 'UserActionHis', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new UserActionHis();