const BaseComponent = require('../prototype/baseComponent');
const ClassPayRecordModel = require("../models").ClassPayRecord;
const ClassInfoModel = require("../models").ClassInfo;
const BillRecordModel = require("../models").BillRecord;
const UserModel = require("../models").User;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')
const settings = require('../../../configs/settings');


function checkFormData(req, res, fields) {
    let errMsg = '';

    if (!shortid.isValid(fields.class)) {
        errMsg = res.__("validate_error_params");
    }

    if (fields.comments && !validator.isLength(fields.comments, 2, 50)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 50,
            label: res.__("label_comments")
        });
    }

    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class ClassPayRecord {
    constructor() {
        // super()
    }
    async getClassPayRecords(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {};
            let useClient = req.query.useClient;
            let userId = req.query.userId;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (userId) {
                queryObj.user = userId;
            }

            const classPayRecords = await ClassPayRecordModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'user',
                select: 'name userName _id'
            }, {
                path: 'class',
                populate: {
                    path: 'author',
                    select: 'userName _id'
                }
            }]).exec();
            const totalItems = await ClassPayRecordModel.count(queryObj);

            let classPayRecordData = {
                docs: classPayRecords,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || '',
                    totalPage: Math.ceil(totalItems / pageSize)
                }
            };
            let renderClassPayRecordData = siteFunc.renderApiData(req, res, 200, 'ClassPayRecord', classPayRecordData);
            if (modules && modules.length > 0) {
                return renderClassPayRecordData.data;
            } else {
                if (useClient == '1') {
                    res.send(renderClassPayRecordData);
                } else if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getClassPayRecords', classPayRecords));
                } else {
                    res.send(renderClassPayRecordData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }


    async getOneClassPayRecords(params = {}) {
        return await ClassPayRecordModel.findOne(params);
    }

    async addClassPayRecord(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                // console.log('----fields----', fields)
                let userInfo = req.session.user;
                checkFormData(req, res, fields);

                // 校验资金密码是否正确
                let targetUser = await UserModel.findOne({
                    _id: userInfo._id
                });

                if (_.isEmpty(targetUser)) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                } else {
                    // TODO 如果是游客，则不校验
                    if (!targetUser.deviceId) {

                        if (!fields.password) {
                            throw new siteFunc.UserException(res.__('lc_write_current_password'));
                        }

                        if (targetUser.fundPassword != service.encrypt(fields.password, settings.encrypt_key)) {
                            throw new siteFunc.UserException(res.__('lc_write_current_password'));
                        }

                    }
                }


                let targetClass = await ClassInfoModel.findOne({
                    _id: fields.class
                });

                // console.log('----targetClass----', targetClass)

                if (_.isEmpty(targetClass)) {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }

                let oldClassPayRecords = await ClassPayRecordModel.findOne({
                    user: userInfo._id,
                    class: fields.class,
                    state: true
                });

                if (!_.isEmpty(oldClassPayRecords)) {
                    throw new siteFunc.UserException(res.__("validate_error_repost"));
                }

                let currentClassPrice = 0;
                // 获取我的账户余额
                let totalBill_num = await siteFunc.getMECLeftCoins(targetUser.walletAddress);

                console.log('--onlineMECCoins----', totalBill_num);
                console.log('--targetClass.price----', targetClass.price);
                console.log('--targetUser----', targetUser._id);

                if (targetUser.vip) {
                    if (totalBill_num < Number(targetClass.vipPrice)) {
                        throw new siteFunc.UserException(res.__("bill_notice_no_coins"));
                    } else {
                        currentClassPrice = targetClass.vipPrice;
                    }
                } else {
                    if (totalBill_num < Number(targetClass.price)) {
                        throw new siteFunc.UserException(res.__("bill_notice_no_coins"));
                    } else {
                        currentClassPrice = targetClass.price;
                    }
                }

                // 调用接口购买
                let transferState = await siteFunc.buyClassByMec(targetUser.walletAddress, Number(currentClassPrice));
                if (transferState == '1') {
                    const orderObj = {
                        class: fields.class,
                        money: currentClassPrice,
                        user: userInfo._id,
                        state: true,
                        comments: fields.comments || ''
                    }

                    const newClassPayRecord = new ClassPayRecordModel(orderObj);

                    await newClassPayRecord.save();

                    // 添加账单和行为
                    await siteFunc.addUserActionHis(req, res, settings.user_bill_type_subscribe, {
                        targetId: fields.class,
                        orderPrice: currentClassPrice
                    });

                    // 推送消息
                    await siteFunc.jsPushFunc(userInfo, targetClass.author, 'subscribeClass', targetClass._id);

                    res.send(siteFunc.renderApiData(req, res, 200, 'ClassPayRecord', {}, 'save'))

                } else {
                    throw new siteFunc.UserException(res.__('bill_label_subscribe_state_failed'));
                }

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    // 加入学习，针对免费课程或者是会员加入会员免费课程
    async addClassToStudy(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                // console.log('----fields----', fields)
                let userInfo = req.session.user;

                if (!shortid.isValid(fields.class)) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                let targetClass = await ClassInfoModel.findOne({
                    _id: fields.class
                });

                if (_.isEmpty(targetClass)) {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }

                let oldClassPayRecords = await ClassPayRecordModel.findOne({
                    user: userInfo._id,
                    class: fields.class,
                    state: true
                });
                if (!_.isEmpty(oldClassPayRecords)) {
                    throw new siteFunc.UserException(res.__("validate_error_repost"));
                }

                if (targetClass.priceType != '0' && targetClass.priceType != '1') {
                    throw new siteFunc.UserException(res.__('bill_label_subscribe_state_failed'));
                }

                let targetUser = await UserModel.findOne({
                    _id: userInfo._id
                });

                if (!targetUser.vip) {

                    if (targetClass.priceType != '0') {
                        throw new siteFunc.UserException(res.__("bill_label_subscribe_state_failed"));
                    }

                }

                const orderObj = {
                    class: fields.class,
                    money: 0,
                    user: userInfo._id,
                    state: true
                }

                const newClassPayRecord = new ClassPayRecordModel(orderObj);

                await newClassPayRecord.save();

                res.send(siteFunc.renderApiData(req, res, 200, 'ClassPayRecord', {}, 'save'))


            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }



}

module.exports = new ClassPayRecord();