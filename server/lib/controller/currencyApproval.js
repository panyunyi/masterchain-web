const BaseComponent = require('../prototype/baseComponent');
const CurrencyApprovalModel = require("../models").CurrencyApproval;
const BillRecordModel = require("../models").BillRecord;
const User_reward_hisModel = require("../models").User_reward_his;
const WalletAddressModel = require("../models").WalletAddress;
const UserModel = require("../models").User;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')
const settings = require('../../../configs/settings');


function checkFormData(req, res, fields) {
    let errMsg = '';

    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }

    if (!fields.coins || !validator.isNumeric((fields.coins).toString())) {
        errMsg = res.__("validate_error_params");
    }

    if (!shortid.isValid(fields.walletAddress)) {
        errMsg = res.__("validate_inputCorrect", {
            label: res.__("user_bill_label_walletAddress")
        });
    }

    if (!fields.fundPassword) {
        errMsg = res.__("validate_inputCorrect", {
            label: res.__("label_user_password")
        });
    }

    if (fields.comments && !validator.isLength(fields.comments, 2, 30)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 30,
            label: res.__("label_comments")
        });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class CurrencyApproval {
    constructor() {
        // super()
    }
    async getCurrencyApprovals(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {};
            let useClient = req.query.useClient;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.uuid = {
                    $regex: reKey
                }
            }

            const currencyApprovals = await CurrencyApprovalModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'user',
                select: 'name userName _id'
            }, {
                path: 'walletAddress',
                select: 'wallet _id'
            }]).exec();
            const totalItems = await CurrencyApprovalModel.count(queryObj);

            let currencyApprovalData = {
                docs: currencyApprovals,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderCurrencyApprovalData = siteFunc.renderApiData(req, res, 200, 'CurrencyApproval', currencyApprovalData);
            if (modules && modules.length > 0) {
                return renderCurrencyApprovalData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getCurrencyApprovals', currencyApprovals));
                } else {
                    res.send(renderCurrencyApprovalData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async addCurrencyApproval(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                let userInfo = req.session.user,
                    errMsg = '';
                checkFormData(req, res, fields);

                let targetUser = await UserModel.findOne({
                    _id: userInfo._id,
                    fundPassword: service.encrypt(fields.fundPassword, settings.encrypt_key)
                });

                if (_.isEmpty(targetUser)) {
                    errMsg = res.__("validate_inputCorrect", {
                        label: res.__("label_user_password")
                    });
                }

                if (!targetUser.authState) {
                    errMsg = res.__("bill_label_approval_state_notIdentity");
                }

                if (errMsg) {
                    throw new siteFunc.UserException(errMsg);
                }

                // 获取我的账户余额
                let leftCoins = 0;

                // 余额校验
                if (fields.unit == 'MVPC') {

                    let msttTotalCoins = await siteFunc.getMVPCLeftCoins(req.session.user.walletAddress);
                    if (msttTotalCoins < fields.coins) {
                        throw new siteFunc.UserException(res.__("bill_notice_no_coins"));
                    } else {
                        leftCoins = msttTotalCoins - Number(fields.coins);
                    }

                } else {
                    let totalBillObj = await BillRecordModel.aggregate([{
                            $match: {
                                user: req.session.user._id,
                                unit: fields.unit
                            }
                        },
                        {
                            $group: {
                                _id: null,
                                total_num: {
                                    $sum: "$coins"
                                }
                            }
                        }
                    ]);
                    let totalBill_num = totalBillObj.length > 0 ? totalBillObj[0].total_num : 0;
                    if (totalBill_num < fields.coins) {
                        throw new siteFunc.UserException(res.__("bill_notice_no_coins"));
                    } else {
                        leftCoins = totalBill_num - Number(fields.coins);
                    }
                }


                // 校验钱包用户一致性
                let currentWallet = await WalletAddressModel.findOne({
                    _id: fields.walletAddress,
                    user: req.session.user._id
                });
                if (_.isEmpty(currentWallet)) {
                    throw new siteFunc.UserException(res.__("validate_inputCorrect", {
                        label: res.__("user_bill_label_walletAddress")
                    }));
                }

                // 添加账单和行为,币状态锁定并扣除
                let recordObj = await siteFunc.addUserActionHis(req, res, settings.user_bill_type_transfer_coins, {
                    coins: fields.coins,
                    comments: fields.comments,
                    unit: fields.unit
                });

                const tagObj = {
                    coins: fields.coins,
                    unit: fields.unit,
                    user: req.session.user._id,
                    walletAddress: fields.walletAddress,
                    state: '0',
                    comments: fields.comments,
                    billRecord: recordObj.billRecord
                }

                const newCurrencyApproval = new CurrencyApprovalModel(tagObj);

                await newCurrencyApproval.save();


                res.send(siteFunc.renderApiData(req, res, 200, res.__("restful_api_response_success", {
                    label: res.__("user_action_tips_currencyApproval")
                }), {
                    state: res.__('bill_label_approval_state_wait'),
                    leftCoins
                }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateCurrencyApproval(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                // checkFormData(req, res, fields);
                const userObj = {
                    state: fields.state ? '1' : '0',
                    approver: req.session.adminUserInfo._id
                }
                const item_id = fields._id;
                let recordId = fields.billRecord;

                // 校验账单是否存在
                if (!shortid.isValid(item_id) || !shortid.isValid(recordId)) {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }

                // 更新审核状态
                let targetApproval = await CurrencyApprovalModel.findOneAndUpdate({
                    _id: item_id
                }, {
                    $set: userObj
                }).populate(['user', 'walletAddress']);


                let msttTotalCoins = await siteFunc.getMVPCLeftCoins(targetApproval.user.walletAddress);
                if (msttTotalCoins < targetApproval.coins) {
                    throw new siteFunc.UserException(res.__("bill_notice_no_coins"));
                }

                // 开始转账
                console.log('---2222-', msttTotalCoins)
                let transferHash = await siteFunc.transfer(targetApproval.user.walletAddress, targetApproval.walletAddress.wallet, targetApproval.coins, targetApproval.user.walletAddressPassword);

                console.log('--transferHash--', transferHash);

                if (transferHash) {
                    let transferInfo = {
                        hash: transferHash,
                        from: targetApproval.user.walletAddress,
                        to: targetApproval.walletAddress.wallet
                    }
                    await BillRecordModel.findOneAndUpdate({
                        user: targetApproval.user._id,
                        _id: recordId
                    }, {
                        $set: {
                            updatetime: new Date(),
                            state: transferHash ? '1' : '2',
                            transferInfo: transferInfo
                        }
                    });

                    res.send(siteFunc.renderApiData(req, res, 200, 'CurrencyApproval', {}, 'update'))

                } else {
                    throw new siteFunc.UserException(res.__('bill_label_transfer_state_failed'));
                }

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delCurrencyApproval(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await CurrencyApprovalModel.remove({
                _id: req.query.ids
            });
            res.send(siteFunc.renderApiData(req, res, 200, 'CurrencyApproval', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }

    async askCashShou(req, res, next) {

        try {
            let rewardObj = {
                userid: req.query.id,
                EV: 100,
                MI: 100,
                rewardcontentmstt: 100000,
                rewardactionmstt: 100000,
                rewardreviewermstt: 100000,
                rewardid: req.session.user._id
            }
            let saveObj = new User_reward_hisModel(rewardObj);
            await saveObj.save();
            res.send(siteFunc.renderApiData(req, res, 200, 'get data success', {}, 'save'));

        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'save'));

        }
    }


}

module.exports = new CurrencyApproval();