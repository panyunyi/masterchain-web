const BaseComponent = require('../prototype/baseComponent');
const ClassContentModel = require("../models").ClassContent;
const ClassPayRecordModel = require("../models").ClassPayRecord;
const UserModel = require("../models").User;
const ClassInfoModel = require("../models").ClassInfo;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')
const xss = require("xss");
var RPCClient = require('@alicloud/pop-core').RPCClient;
const settings = require('../../../configs/settings');


function checkFormData(req, res, fields) {
    let errMsg = '';

    // console.log('---fields--', fields._id)
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    if (fields.basicInfo && !siteFunc.checkCurrentId(fields.basicInfo)) {
        errMsg = res.__("validate_selectNull", {
            label: res.__('classContent_label_class')
        });
    }
    if (fields.catalog && !validatorUtil.isRegularCharacter(fields.catalog)) {
        errMsg = res.__("validate_error_field", {
            label: res.__("classContent_label_catalog")
        });
    }
    if (fields.catalog && !validator.isLength(fields.catalog, 1, 100)) {
        errMsg = res.__("validate_rangelength", {
            min: 1,
            max: 100,
            label: res.__("classContent_label_catalog")
        });
    }
    if (fields.comments && !validator.isLength(fields.comments, 20, 50000)) {
        errMsg = res.__("validate_rangelength", {
            min: 20,
            max: 50000,
            label: res.__("label_comments")
        });
    }
    if (fields.duration && !validator.isNumeric(fields.duration)) {
        errMsg = res.__("validate_inputCorrect", {
            label: res.__("classContent_label_duration")
        });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

function initVodClient() {
    var regionId = 'cn-shanghai'; // 点播服务接入区域
    var client = new RPCClient({
        accessKeyId: settings.accessKeyId,
        secretAccessKey: settings.accessKeySecret,
        endpoint: 'http://vod.' + regionId + '.aliyuncs.com',
        apiVersion: '2017-03-21'
    });
    return client;
}
var vodClient = initVodClient();

function getVideoInfo(videoId) {
    return new Promise((resolve, reject) => {

        if (!videoId) {
            throw new siteFunc.UserException(settings.system_error_upload);
        }
        var params = {
            "VideoId": videoId
        }
        var requestOption = {
            method: 'POST'
        };

        vodClient.request('GetVideoInfo', params, requestOption).then((result) => {
            console.log(result);
            let videoInfo = {};
            if (!_.isEmpty(result)) {
                // console.log('----result1--', result.Video)
                // console.log('----infoData--', infoData)
                videoInfo = {
                    duration: result.Video.Duration,
                    imageArr: result.Video.CoverURL,
                    size: result.Video.Size
                };
            }
            // console.log('----videoInfo----', videoInfo)
            resolve(videoInfo);

        }, (ex) => {
            console.log(ex);
            reject(ex);
        })
    })
}

class ClassContent {
    constructor() {
        // super()
    }
    async getClassContents(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {};
            let useClient = req.query.useClient;
            let classId = req.query.classId;
            let state = req.query.state;
            let userInfo = req.session.user || {};
            let files = null,
                populateStr = [{
                    path: 'author',
                    select: 'name userName _id'
                }, {
                    path: 'basicInfo',
                    populate: {
                        path: 'categories'
                    }
                }];

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.catalog = {
                    $regex: reKey
                }
            }

            if (useClient != '0') {
                files = "catalog catalogIndex _id id date free imageArr videoArr audioArr duration size description"
            }

            if (classId) {
                queryObj.basicInfo = classId;
                populateStr = [];
            }

            if (state) {
                queryObj.state = state
            } else {
                queryObj.state = {
                    $ne: '0'
                }
            }

            if (!_.isEmpty(userInfo) && useClient == '1') {
                let classCount = await ClassContentModel.count({
                    author: userInfo._id,
                    basicInfo: classId
                })
                // 本人课程不限制状态
                if (classCount > 0) {
                    delete queryObj.state;
                }
            }

            let sortDate = 1;
            if (useClient == '0') {
                sortDate = -1;
            }

            const classContents = await ClassContentModel.find(queryObj, files).sort({
                date: sortDate
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate(populateStr).exec();
            const totalItems = await ClassContentModel.count(queryObj);

            let classContentData = {
                docs: classContents,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderClassContentData = siteFunc.renderApiData(req, res, 200, 'ClassContent', classContentData);
            if (modules && modules.length > 0) {
                return renderClassContentData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getClassContents', classContents));
                } else {
                    res.send(renderClassContentData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async getOneClassInfo(queryObj = {}) {

        // queryObj.state = "2"

        return await ClassContentModel.findOne(queryObj).populate([{
            path: 'author',
            select: 'name userName _id'
        }, {
            path: 'basicInfo',
            populate: {
                path: 'categories'
            }
        }]).exec();
    }

    async getOneClassByParams(req, res, next) {
        try {
            let targetId = req.query.id || req.query.classId;
            let useClient = req.query.useClient;
            let userInfo = req.session.user || {};
            let modules = req.query.modules;

            if (!shortid.isValid(targetId)) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }


            let targetClass = await ClassContentModel.findOne({
                _id: targetId
            }).populate([{
                path: 'author',
                select: 'name userName _id'
            }, {
                path: 'basicInfo',
                populate: {
                    path: 'categories'
                }
            }]).exec();

            if (!_.isEmpty(targetClass)) {

                if (!_.isEmpty(userInfo)) {

                    let targetUser = await UserModel.findOne({
                        _id: userInfo._id
                    }, 'vip');
                    // 自己的课程不用限制
                    if (targetClass.author._id != req.session.user._id) {

                        // 还未发布的课程他人不能访问
                        if (targetClass.state != '2') {
                            throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                        }

                        // 会员免费
                        if (targetClass.basicInfo.priceType == '1') {
                            if (!targetUser.vip && !targetClass.free) {
                                throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                            }
                        }

                        // 会员付费
                        if (targetClass.basicInfo.priceType == '2') {

                            let classRecord = await ClassPayRecordModel.findOne({
                                user: req.session.user._id,
                                class: targetClass.basicInfo._id
                            });

                            if (!targetClass.free && _.isEmpty(classRecord)) {
                                throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                            }

                        }

                    } else {
                        console.log('自己的课程');
                    }

                } else {
                    if (!targetClass.free && targetClass.basicInfo.priceType != '0') {
                        throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                    }
                }

                let renderClass = targetClass;

                if (!_.isEmpty(targetClass) && req.query.useClient == '2') {
                    renderClass = JSON.parse(JSON.stringify(targetClass));
                    delete renderClass.comments;
                    if (renderClass.basicInfo.type == '0' && renderClass.simpleComments) {
                        renderClass.simpleComments = JSON.parse(renderClass.simpleComments);
                    }
                }

                if (modules && modules.length > 0) {
                    return renderClass;
                } else {
                    res.send(siteFunc.renderApiData(req, res, 200, 'get data success', renderClass, 'save'));
                }

            } else {
                throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
            }


        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'getlist'))
        }
    }



    async addClassContent(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                checkFormData(req, res, fields);

                if (req.session.user.group != '1') {
                    throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                }

                if (req.query.useClient == '1') {
                    let targetClassInfo = await ClassInfoModel.findOne({
                        _id: fields.basicInfo,
                        author: req.session.user._id
                    });

                    if (_.isEmpty(targetClassInfo)) {
                        throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                    }

                    if (targetClassInfo.state != '2') {
                        throw new siteFunc.UserException(res.__("label_nopower_add_clasContent"));
                    }
                }

                const tagObj = {
                    catalog: fields.catalog,
                    duration: fields.duration,
                    basicInfo: fields.basicInfo,
                    free: fields.free == '1' ? true : false,
                    author: req.session.user._id,
                    state: '0' // 新增的课程都是草稿
                }

                if (fields.type == '0') {
                    tagObj.comments = xss(fields.comments);
                    tagObj.simpleComments = xss(fields.simpleComments);
                    // 设置显示模式
                    let checkInfo = siteFunc.checkContentType(tagObj.simpleComments, 'class');
                    tagObj.appShowType = checkInfo.type;
                    tagObj.imageArr = checkInfo.imgArr;
                    tagObj.videoArr = checkInfo.videoArr;
                    if (checkInfo.type == '3') {
                        tagObj.videoImg = checkInfo.defaultUrl;
                    }
                    tagObj.simpleComments = siteFunc.renderSimpleContent(tagObj.simpleComments, checkInfo.imgArr, checkInfo.videoArr);

                }

                // 自动新增目录
                let oldClasses = await ClassContentModel.find({
                    basicInfo: fields.basicInfo
                });
                if (_.isEmpty(oldClasses)) {
                    tagObj.catalogIndex = '01';
                } else {
                    oldClasses = _.sortBy(oldClasses, (classItem) => {
                        return Number(classItem.catalogIndex) * -1
                    });
                    // console.log('--oldClasses---', oldClasses);
                    let oldClassCatalogIndex = Number(oldClasses[0].catalogIndex) + 1;
                    tagObj.catalogIndex = oldClassCatalogIndex < 10 ? ('0' + oldClassCatalogIndex) : oldClassCatalogIndex;
                }

                const newClassContent = new ClassContentModel(tagObj);
                await newClassContent.save();

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: '章节创建'
                }), {
                    id: newClassContent._id
                }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateClassContent(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                checkFormData(req, res, fields);
                let useClient = req.query.useClient;
                if (req.query.useClient == '1') {

                    let targetClassInfo = await ClassInfoModel.findOne({
                        _id: fields.basicInfo,
                        author: req.session.user._id
                    });

                    if (_.isEmpty(targetClassInfo)) {
                        throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                    }

                    if (targetClassInfo.state != '2') {
                        throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                    }

                    let targetClass = await ClassContentModel.findOne({
                        author: req.session.user._id,
                        _id: fields._id
                    });
                    if (_.isEmpty(targetClass)) {
                        throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
                    } else {
                        if (targetClass.state == '2') {
                            throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
                        }
                    }
                }

                const userObj = {
                    free: fields.free == '1' ? true : false,
                    basicInfo: fields.basicInfo,
                    state: fields.draft == '1' ? '0' : '1',
                }

                // web端修改草稿
                if (req.query.useClient != '0') {
                    if (fields.type == '0') {
                        userObj.comments = xss(fields.comments);
                        userObj.simpleComments = xss(fields.simpleComments);
                    } else if (fields.type == '1') {
                        userObj.videoArr = fields.videoArr;
                        let videoInfo = await getVideoInfo(fields.videoArr);
                        // console.log('--videoInfo----', videoInfo)
                        if (!_.isEmpty(videoInfo)) {
                            userObj.duration = videoInfo.duration;
                            userObj.imageArr = videoInfo.imageArr || 'http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1546767808590.jpeg';
                            userObj.size = videoInfo.size;
                        }
                    } else if (fields.type == '2') {
                        userObj.audioArr = fields.audioArr;
                        let audioInfo = await getVideoInfo(fields.audioArr);
                        if (!_.isEmpty(audioInfo)) {
                            userObj.duration = audioInfo.duration;
                            userObj.imageArr = audioInfo.imageArr || 'http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1546767643327.jpeg';
                            userObj.size = audioInfo.size;
                        }
                    }
                    userObj.updateDate = new Date();
                } else {
                    // 后台管理审核
                    delete userObj.free;
                    userObj.state = fields.state;
                    userObj.auditDate = new Date();
                }

                const item_id = fields._id;

                // 针对图文设置格式
                if (fields.type == '0') {
                    // 设置显示模式
                    let checkInfo = siteFunc.checkContentType(userObj.simpleComments, 'class');
                    // console.log('---checkInfo--', checkInfo)
                    userObj.appShowType = checkInfo.type;
                    userObj.imageArr = checkInfo.imgArr;
                    userObj.videoArr = checkInfo.videoArr;
                    if (checkInfo.type == '3') {
                        userObj.videoImg = checkInfo.defaultUrl;
                    }
                    userObj.simpleComments = siteFunc.renderSimpleContent(userObj.simpleComments, checkInfo.imgArr, checkInfo.videoArr);

                }

                // console.log('---userObj---', userObj)
                let targetClassInfo = await ClassContentModel.findOneAndUpdate({
                    _id: item_id
                }, {
                    $set: userObj
                }).populate([{
                    path: 'author',
                    select: 'followers'
                }]);

                if (useClient == '0' && targetClassInfo.state == '2') {
                    // 管理员审核通过推送消息
                    if (!_.isEmpty(targetClassInfo.author) && !_.isEmpty(targetClassInfo.author.followers) && targetClassInfo.followers.length > 0) {
                        for (const pushUserId of targetClassInfo.followers) {
                            await siteFunc.jsPushFunc(targetClassInfo.author, pushUserId, 'followMasterClass', item_id);
                        }
                    }
                }

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('lc_create_masterClass')
                }), {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async updateClassVideo(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                if (!fields.videoArr) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                let targetClass = await ClassContentModel.findOne({
                    author: req.session.user._id,
                    _id: fields._id
                });

                if (_.isEmpty(targetClass)) {
                    throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
                } else {
                    if (targetClass.state == '2') {
                        throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
                    }
                }

                const userObj = {
                    videoArr: fields.videoArr
                }

                const item_id = fields._id;

                await ClassContentModel.findOneAndUpdate({
                    _id: item_id
                }, {
                    $set: userObj
                });

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('lc_create_masterClass')
                }), {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delClassContent(req, res, next) {
        try {
            let userInfo = req.session.user;
            let useClient = req.query.useClient;
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }


            if (useClient == '1') {
                let classCount = await ClassContentModel.count({
                    author: userInfo._id,
                    _id: req.query.ids
                });
                if (classCount == 0) {
                    throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
                }
            }

            await ClassContentModel.remove({
                _id: req.query.ids
            });

            res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                label: "大师课章节删除"
            }), {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new ClassContent();