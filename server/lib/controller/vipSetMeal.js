const BaseComponent = require('../prototype/baseComponent');
const VipSetMealModel = require("../models").VipSetMeal;
const formidable = require('formidable');
const { service, validatorUtil, siteFunc } = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    if (!validator.isNumeric((fields.time).toString())) {
        errMsg = res.__("validate_error_params");
    }
    if (!validator.isNumeric((fields.coins).toString()) && !validator.isFloat((fields.coins).toString())) {
        errMsg = res.__("validate_error_params");
    }
    if (fields.comments && !validator.isLength(fields.comments, 2, 30)) {
        errMsg = res.__("validate_rangelength", { min: 2, max: 30, label: res.__("label_comments") });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class VipSetMeal {
    constructor() {
        // super()
    }
    async getVipSetMeals(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let useClient = req.query.useClient, queryObj = { state: true };

            const vipSetMeals = await VipSetMealModel.find(queryObj).sort({ date: -1 }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).exec();
            const totalItems = await VipSetMealModel.count(queryObj);

            let vipSetMealData = {
                docs: vipSetMeals,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10
                }
            };
            let renderVipSetMealData = siteFunc.renderApiData(req, res, 200, 'VipSetMeal', vipSetMealData);
            if (modules && modules.length > 0) {
                return renderVipSetMealData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getVipSetMeals', vipSetMeals));
                } else {
                    res.send(renderVipSetMealData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async addVipSetMeal(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);

                const tagObj = {
                    state: fields.state,
                    time: fields.time,
                    coins: fields.coins,
                    default: fields.default,
                    unit: 'MEC',
                    comments: fields.comments
                }

                const newVipSetMeal = new VipSetMealModel(tagObj);

                await newVipSetMeal.save();

                res.send(siteFunc.renderApiData(req, res, 200, 'VipSetMeal', { id: newVipSetMeal._id }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateVipSetMeal(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
                const userObj = {
                    // time: fields.time,
                    // coins: fields.time,
                    state: fields.state,
                    default: fields.default,
                    // unit: 'MEC',
                    comments: fields.comments
                }
                const item_id = fields._id;

                await VipSetMealModel.findOneAndUpdate({ _id: item_id }, { $set: userObj });
                res.send(siteFunc.renderApiData(req, res, 200, 'VipSetMeal', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delVipSetMeal(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await VipSetMealModel.remove({ _id: req.query.ids });
            res.send(siteFunc.renderApiData(req, res, 200, 'VipSetMeal', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new VipSetMeal();