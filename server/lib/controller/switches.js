const BaseComponent = require('../prototype/baseComponent');

const UserModel = require("../models").User;
const SwitchesModel = require("../models").Switches;
const formidable = require('formidable');
const _ = require("lodash");
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')



class Switches {
    constructor() {
        // super()
    }
    async getSwitches(req, res, next) {
        try {

            let userInfo = req.session.user || {};

            let queryObj = {
                user: userInfo._id
            };

            let switchess = await SwitchesModel.findOne(queryObj).sort({
                date: -1
            });

            if (_.isEmpty(switchess)) {
                let switchParams = {
                    commentOrReply: false,
                    follow: false,
                    followCommunity: false,
                    followSpecial: false,
                    thumbsUpComments: false,
                    thumbsUpContent: false,
                    appreciateContent: false,
                    subscribeClass: false,
                    followMasterClass: false,
                    followMasterContent: false,
                    user: userInfo._id,
                    date: new Date()
                }
                let switchObj = new SwitchesModel(switchParams);
                switchess = await switchObj.save();
            }

            res.send(siteFunc.renderApiData(req, res, 200, 'Switches', switchess));

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }



    async updateSwitches(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                let userObj = {}

                // if (fields.open) {
                //     userObj.open = fields.open == '1' ? true : false;
                // }
                if (fields.commentOrReply) {
                    userObj.commentOrReply = fields.commentOrReply == '1' ? true : false;
                }
                if (fields.follow) {
                    userObj.follow = fields.follow == '1' ? true : false;
                }
                if (fields.followCommunity) {
                    userObj.followCommunity = fields.followCommunity == '1' ? true : false;
                }
                if (fields.followSpecial) {
                    userObj.followSpecial = fields.followSpecial == '1' ? true : false;
                }
                if (fields.thumbsUpComments) {
                    userObj.thumbsUpComments = fields.thumbsUpComments == '1' ? true : false;
                }
                if (fields.thumbsUpContent) {
                    userObj.thumbsUpContent = fields.thumbsUpContent == '1' ? true : false;
                }
                if (fields.appreciateContent) {
                    userObj.appreciateContent = fields.appreciateContent == '1' ? true : false;
                }
                if (fields.subscribeClass) {
                    userObj.subscribeClass = fields.subscribeClass == '1' ? true : false;
                }
                if (fields.followMasterClass) {
                    userObj.followMasterClass = fields.followMasterClass == '1' ? true : false;
                }
                if (fields.followMasterContent) {
                    userObj.followMasterContent = fields.followMasterContent == '1' ? true : false;
                }

                // console.log('----', req.session.user._id)
                await SwitchesModel.findOneAndUpdate({
                    user: req.session.user._id
                }, {
                    $set: userObj
                });

                res.send(siteFunc.renderApiData(req, res, 200, 'Switches', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

}

module.exports = new Switches();