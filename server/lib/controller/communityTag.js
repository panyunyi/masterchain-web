const BaseComponent = require('../prototype/baseComponent');
const CommunityTagModel = require("../models").CommunityTag;
const CommunityModel = require("../models").Community;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const xss = require("xss");

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    if (!validatorUtil.isRegularCharacter(fields.name)) {
        errMsg = res.__("validate_error_field", {
            label: res.__("label_tag_name")
        });
    }
    if (!validator.isLength(fields.name, 1, 12)) {
        errMsg = res.__("validate_rangelength", {
            min: 1,
            max: 12,
            label: res.__("label_tag_name")
        });
    }
    if (!validator.isLength(fields.comments, 2, 30)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 30,
            label: res.__("label_comments")
        });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

async function renderCommunityTags(communityTags = []) {
    return new Promise(async (resolve, reject) => {
        try {
            let renderTagList = JSON.parse(JSON.stringify(communityTags));
            for (const tagItem of renderTagList) {
                let community_num = await CommunityModel.count({
                    state: '1',
                    tags: tagItem._id
                });
                tagItem.community_num = community_num;
            }
            resolve(renderTagList);
        } catch (error) {
            resolve([])
        }
    })
}

class CommunityTag {
    constructor() {
        // super()
    }
    async getCommunityTags(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {};
            let useClient = req.query.useClient;
            // console.log('----model--', model)
            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = {
                    $regex: reKey
                }
            }

            let communityTags = await CommunityTagModel.find(queryObj).sort({
                sort: 1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize));
            const totalItems = await CommunityTagModel.count(queryObj);

            communityTags = await renderCommunityTags(communityTags);

            let tagsData = {
                docs: communityTags,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderTagsData = siteFunc.renderApiData(req, res, 200, 'CommunityTag', tagsData);
            if (modules && modules.length > 0) {
                return renderTagsData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'contentlist', communityTags));
                } else {
                    res.send(renderTagsData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async addCommunityTag(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
            } catch (err) {
                console.log(err.message, err);
                res.send(siteFunc.renderApiErr(req, res, 500, err, 'checkform'));
            }

            const tagObj = {
                name: fields.name,
                alias: fields.alias,
                sImg: fields.sImg,
                comments: xss(fields.comments)
            }

            const newCommunityTag = new CommunityTagModel(tagObj);
            try {
                await newCommunityTag.save();

                res.send(siteFunc.renderApiData(req, res, 200, 'CommunityTag', {
                    id: newCommunityTag._id
                }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateCommunityTag(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
            } catch (err) {
                console.log(err.message, err);
                res.send(siteFunc.renderApiErr(req, res, 500, err, 'checkform'));
            }

            const userObj = {
                name: fields.name,
                alias: fields.alias,
                sImg: fields.sImg,
                comments: xss(fields.comments)
            }
            const item_id = fields._id;
            try {
                await CommunityTagModel.findOneAndUpdate({
                    _id: item_id
                }, {
                    $set: userObj
                });
                res.send(siteFunc.renderApiData(req, res, 200, 'CommunityTag', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delCommunityTag(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await CommunityTagModel.remove({
                _id: req.query.ids
            });
            res.send(siteFunc.renderApiData(req, res, 200, 'CommunityTag', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }

}

module.exports = new CommunityTag();