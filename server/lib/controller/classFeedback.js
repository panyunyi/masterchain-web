const BaseComponent = require('../prototype/baseComponent');
const ContentModel = require("../models").Content;
const ClassFeedBackModel = require("../models").ClassFeedback;
const SystemConfigModel = require("../models").SystemConfig;
const UserModel = require("../models").User;
const AdminUserModel = require("../models").AdminUser;
const formidable = require('formidable');
const _ = require('lodash');
const shortid = require('shortid');
const validator = require('validator');
const xss = require("xss");
const { service, validatorUtil, siteFunc } = require('../../../utils');
const settings = require('../../../configs/settings');


class ClassFeedBack {
    constructor() {
        // super()
    }
    async getClassFeedBacks(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let searchkey = req.query.searchkey;
            let classId = req.query.classId;
            let author = req.query.user;
            let useClient = req.query.useClient;
            let queryObj = {};
            let populateArr = [{
                path: 'classContent',
                populate: { path: 'basicInfo' }
            }, {
                path: 'author',
                select: 'userName _id enable date logo'
            }, {
                path: 'replyAuthor',
                select: 'userName _id enable date logo'
            }];

            // api 必须传参数
            if (useClient == '2') {
                if (classId) {
                    queryObj.classContent = classId;
                    populateArr = [{
                        path: 'author',
                        select: 'userName _id enable date logo'
                    }, {
                        path: 'replyAuthor',
                        select: 'userName _id enable date logo'
                    }]
                } else {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.content = { $regex: reKey }
            }
            if (author) {
                queryObj.author = author;
            }
            const classFeedBacks = await ClassFeedBackModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate(populateArr).exec();
            const totalItems = await ClassFeedBackModel.count(queryObj);

            let classFeedBackData = {
                docs: classFeedBacks,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || '',
                    totalPage: Math.ceil(totalItems / pageSize)
                }
            };
            let renderData = siteFunc.renderApiData(req, res, 200, 'ClassFeedBack', classFeedBackData, 'getlist');
            if (modules && modules.length > 0) {
                return renderData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'ClassFeedBack', classFeedBacks, 'getlist'))
                } else {
                    res.send(renderData);
                }
            }

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async postClassFeedBacks(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                let errMsg = '';
                if (_.isEmpty(req.session.user)) {
                    errMsg = res.__("label_notice_asklogin")
                }
                if (!shortid.isValid(fields.classContent)) {
                    errMsg = res.__("classFeedback_label_classContent")
                }
                if (fields.content && (fields.content.length < 5 || fields.content.length > 200)) {
                    errMsg = res.__("validate_rangelength", { min: 5, max: 200, label: res.__("classFeedback_label_content") })
                }
                if (!fields.content) {
                    errMsg = res.__("validate_inputNull", { label: res.__("classFeedback_label_content") })
                }
                if (errMsg) {
                    throw new siteFunc.UserException(errMsg);
                }

                const classFeedBackObj = {
                    classContent: fields.classContent,
                    content: xss(fields.content),
                    replyAuthor: fields.replyAuthor,
                    relationMsgId: fields.relationMsgId,
                    author: req.session.user._id
                    // author: "d_K_O6UIV"
                }

                // console.log('----classFeedBackObj---', classFeedBackObj);
                const newClassFeedBack = new ClassFeedBackModel(classFeedBackObj);

                await newClassFeedBack.save();
                await ContentModel.findOneAndUpdate({ _id: fields.classContent }, { '$inc': { 'commentNum': 1 } })

                // 给被回复用户发送提醒邮件
                const systemConfigs = await SystemConfigModel.find({});
                const contentInfo = await ContentModel.findOne({ _id: fields.classContent });
                let replyAuthor;

                if (fields.replyAuthor) {
                    replyAuthor = await UserModel.findOne({ _id: fields.replyAuthor }, siteFunc.getAuthUserFields())
                }

                if (!_.isEmpty(systemConfigs) && !_.isEmpty(contentInfo) && !_.isEmpty(replyAuthor)) {
                    let mailParams = {
                        replyAuthor: replyAuthor,
                        content: contentInfo,
                        author: req.session.user
                    }

                    systemConfigs[0]['siteDomain'] = systemConfigs[0]['siteDomain'];
                    service.sendEmail(req, res, systemConfigs[0], settings.email_notice_user_contentMsg, mailParams);
                }

                res.send(siteFunc.renderApiData(req, res, 200, 'ClassFeedBack', { id: newClassFeedBack._id }, 'save'))
            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }


    async delClassFeedBack(req, res, next) {
        try {
            let errMsg = '', targetIds = req.query.ids;
            if (!siteFunc.checkCurrentId(targetIds)) {
                errMsg = res.__("validate_error_params");
            } else {
                targetIds = targetIds.split(',');
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }

            for (let i = 0; i < targetIds.length; i++) {
                let msgObj = await ClassFeedBackModel.findOne({ _id: targetIds[i] });
                if (msgObj) {
                    await ContentModel.findOneAndUpdate({ _id: msgObj.classContent }, { '$inc': { 'commentNum': -1 } })
                }
            }
            await ClassFeedBackModel.remove({ '_id': { $in: targetIds } });

            res.send(siteFunc.renderApiData(req, res, 200, 'ClassFeedBack', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new ClassFeedBack();