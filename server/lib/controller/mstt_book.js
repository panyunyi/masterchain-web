const BaseComponent = require('../prototype/baseComponent');
const Mstt_bookModel = require("../models").Mstt_book;
const formidable = require('formidable');
const { service, validatorUtil, siteFunc } = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    if (!validator.isLength(fields.name, 1, 12)) {
        errMsg = res.__("validate_rangelength", { min: 1, max: 12, label: res.__("label_tag_name") });
    }
    if (!validator.isLength(fields.comments, 2, 30)) {
        errMsg = res.__("validate_rangelength", { min: 2, max: 30, label: res.__("label_comments") });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class Mstt_book {
    constructor() {
        // super()
    }
    async getMstt_books(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey, queryObj = {};
            let useClient = req.query.useClient;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = { $regex: reKey }
            }

            const mstt_books = await Mstt_bookModel.find(queryObj).sort({ date: -1 }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'users',
                select: 'name userName _id'
            }]).exec();
            const totalItems = await Mstt_bookModel.count(queryObj);

            let mstt_bookData = {
                docs: mstt_books,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderMstt_bookData = siteFunc.renderApiData(req, res, 200, 'Mstt_book', mstt_bookData);
            if (modules && modules.length > 0) {
                return renderMstt_bookData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getMstt_books', mstt_books));
                } else {
                    res.send(renderMstt_bookData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async addMstt_book(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);

                const tagObj = {
                    name: fields.name,
                    comments: fields.comments
                }

                const newMstt_book = new Mstt_bookModel(tagObj);

                await newMstt_book.save();

                res.send(siteFunc.renderApiData(req, res, 200, 'Mstt_book', { id: newMstt_book._id }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateMstt_book(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
                const userObj = {
                    name: fields.name,
                    comments: fields.comments
                }
                const item_id = fields._id;

                await Mstt_bookModel.findOneAndUpdate({ _id: item_id }, { $set: userObj });
                res.send(siteFunc.renderApiData(req, res, 200, 'Mstt_book', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delMstt_book(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await Mstt_bookModel.remove({ _id: req.query.ids });
            res.send(siteFunc.renderApiData(req, res, 200, 'Mstt_book', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new Mstt_book();