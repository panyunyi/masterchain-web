const BaseComponent = require('../prototype/baseComponent');
const OpenVipRecordModel = require("../models").OpenVipRecord;
const VipSetMealModel = require("../models").VipSetMeal;
const BillRecordModel = require("../models").BillRecord;
const UserModel = require("../models").User;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')
const settings = require('../../../configs/settings');


function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }

    if (!shortid.isValid(fields.setMeal)) {
        errMsg = res.__("validate_error_params");
    }

    if (!fields.password) {
        throw new siteFunc.UserException(res.__('lc_password_null_tips'));
    }

    if (fields.comments && !validator.isLength(fields.comments, 2, 30)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 30,
            label: res.__("label_comments")
        });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class OpenVipRecord {
    constructor() {
        // super()
    }
    async getOpenVipRecords(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let searchkey = req.query.searchkey,
                queryObj = {};
            let useClient = req.query.useClient;

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = {
                    $regex: reKey
                }
            }

            const openVipRecords = await OpenVipRecordModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'user',
                select: 'name userName _id'
            }, {
                path: 'setMeal'
            }]).exec();
            const totalItems = await OpenVipRecordModel.count(queryObj);

            let openVipRecordData = {
                docs: openVipRecords,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderOpenVipRecordData = siteFunc.renderApiData(req, res, 200, 'OpenVipRecord', openVipRecordData);
            if (modules && modules.length > 0) {
                return renderOpenVipRecordData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getOpenVipRecords', openVipRecords));
                } else {
                    res.send(renderOpenVipRecordData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async addOpenVipRecord(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                let userInfo = req.session.user;
                checkFormData(req, res, fields);
                // console.log('---userInfo---',userInfo);
                // 校验资金密码是否正确
                let targetUser = await UserModel.findOne({
                    _id: userInfo._id,
                    fundPassword: service.encrypt(fields.password, settings.encrypt_key)
                });
                if (_.isEmpty(targetUser)) {
                    throw new siteFunc.UserException(res.__('lc_write_current_password'));
                }

                let currentMeal = await VipSetMealModel.findOne({
                    _id: fields.setMeal
                });
                // console.log('---currentMeal---', currentMeal);
                if (_.isEmpty(currentMeal)) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                // 校验是否已订购
                let targetRecord = await OpenVipRecordModel.count({
                    user: userInfo._id,
                    type: '0'
                });
                if (targetRecord > 0) {
                    throw new siteFunc.UserException(res.__('user_action_tips_repeat', {
                        label: res.__('user_bill_type_buyVip')
                    }));
                }

                // 校验余额
                let leftCoins = 0;
                // console.log('---req.session.user._id---', req.session.user._id);
                let totalBillObj = await BillRecordModel.aggregate([{
                        $match: {
                            user: req.session.user._id,
                            unit: 'MEC'
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            total_num: {
                                $sum: "$coins"
                            }
                        }
                    }
                ]);
                // console.log('----ss--', totalBillObj);
                let totalBill_num = totalBillObj.length > 0 ? totalBillObj[0].total_num : 0;
                if (totalBill_num < currentMeal.coins) {
                    throw new siteFunc.UserException(res.__("bill_notice_no_coins"));
                } else {
                    leftCoins = totalBill_num - Number(currentMeal.coins);
                }

                const tagObj = {
                    type: '0',
                    setMeal: fields.setMeal,
                    user: req.session.user._id,
                    comments: fields.comments || JSON.stringify(currentMeal),
                    time: currentMeal.time
                }

                const newOpenVipRecord = new OpenVipRecordModel(tagObj);

                await newOpenVipRecord.save();

                // 更新用户会员状态
                await UserModel.findOneAndUpdate({
                    _id: userInfo._id
                }, {
                    $set: {
                        vip: true
                    }
                });

                // 添加账单和行为
                await siteFunc.addUserActionHis(req, res, settings.user_bill_type_buyVip, {
                    coins: currentMeal.coins,
                    comments: fields.comments,
                });

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('user_bill_type_buyVip')
                }), {
                    leftCoins
                }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateOpenVipRecord(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
                const userObj = {
                    name: fields.name,
                    comments: fields.comments
                }
                const item_id = fields._id;

                await OpenVipRecordModel.findOneAndUpdate({
                    _id: item_id
                }, {
                    $set: userObj
                });
                res.send(siteFunc.renderApiData(req, res, 200, 'OpenVipRecord', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delOpenVipRecord(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await OpenVipRecordModel.remove({
                _id: req.query.ids
            });
            res.send(siteFunc.renderApiData(req, res, 200, 'OpenVipRecord', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }

    async extensionPeriod(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkAdminPostToken(req, res, fields.token);

                let errMsg = '';
                if (!siteFunc.checkCurrentId(fields.userId)) {
                    errMsg = res.__("validate_error_params");
                }

                if (!shortid.isValid(fields.setMeal)) {
                    errMsg = res.__("validate_error_params");
                }

                if (fields.comments && !validator.isLength(fields.comments, 2, 30)) {
                    errMsg = res.__("validate_rangelength", {
                        min: 2,
                        max: 30,
                        label: res.__("label_comments")
                    });
                }
                if (errMsg) {
                    throw new siteFunc.UserException(errMsg);
                }

                // 校验用户是否合法,必须是会员才可以续费
                let targetUser = await UserModel.findOne({
                    _id: fields.userId,
                    vip: true
                });

                if (_.isEmpty(targetUser)) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                let currentMeal = await VipSetMealModel.findOne({
                    _id: fields.setMeal
                });

                if (_.isEmpty(currentMeal)) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                const tagObj = {
                    type: '1',
                    setMeal: fields.setMeal,
                    user: fields.userId,
                    time: currentMeal.time,
                    comments: fields.comments || res.__('user_bill_type_vipRenewal')
                }

                const newOpenVipRecord = new OpenVipRecordModel(tagObj);

                await newOpenVipRecord.save();

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('user_bill_type_vipRenewal')
                }), {}, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

}

module.exports = new OpenVipRecord();