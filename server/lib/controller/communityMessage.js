const BaseComponent = require('../prototype/baseComponent');
const CommunityMessageModel = require("../models").CommunityMessage;
const CommunityContentModel = require("../models").CommunityContent;
const CommunityModel = require("../models").Community;
const SystemConfigModel = require("../models").SystemConfig;
const UserModel = require("../models").User;
const AdminUserModel = require("../models").AdminUser;
const BillRecordModel = require("../models").BillRecord;
const formidable = require('formidable');
const _ = require('lodash');
const shortid = require('shortid');
const validator = require('validator');
const xss = require("xss");
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const settings = require('../../../configs/settings');

function renderCommunityMessage(userInfo = {}, communityMessages = [], useClient = '') {

    return new Promise(async (resolve, reject) => {
        try {
            let newCommunityMessageArr = JSON.parse(JSON.stringify(communityMessages));
            // console.log('-----1111--', newCommunityMessageArr)
            for (const communityMessageItem of newCommunityMessageArr) {
                // console.log('-----11112222--', communityMessageItem)
                let had_comment = false;
                let had_despises = false;
                let had_praise = false;
                // console.log('-----3333--', userInfo)
                if (!_.isEmpty(userInfo)) {
                    userInfo = await UserModel.findOne({
                        _id: userInfo._id
                    });
                    // console.log('-----4444--', userInfo)
                    // 是否回复过
                    let myReplyRecord = await CommunityMessageModel.find({
                        author: userInfo._id,
                        relationMsgId: communityMessageItem._id
                    });
                    if (myReplyRecord.length > 0) {
                        had_comment = true;
                    }
                    // 是否踩过
                    if (!_.isEmpty(userInfo.despiseCommunityMessage) && userInfo.despiseCommunityMessage.indexOf(communityMessageItem._id) >= 0) {
                        had_despises = true;
                    }
                    // 是否赞过
                    // console.log('-------userInfo.praiseCommunityMessage-----' + userInfo.praiseCommunityMessage + '---communityMessageItem._id---' + communityMessageItem._id)
                    if (!_.isEmpty(userInfo.praiseCommunityMessage) && userInfo.praiseCommunityMessage.indexOf(communityMessageItem._id) >= 0) {
                        had_praise = true;
                    }
                }
                let praise_num = await UserModel.count({
                    praiseCommunityMessage: communityMessageItem._id
                });
                let despises_num = await UserModel.count({
                    despiseCommunityMessage: communityMessageItem._id
                });
                communityMessageItem.praise_num = praise_num;
                communityMessageItem.despises_num = despises_num;
                communityMessageItem.had_comment = had_comment;
                communityMessageItem.had_despises = had_despises;
                communityMessageItem.had_praise = had_praise;

                if (useClient == '2') {
                    let parentId = communityMessageItem._id;
                    let childCommunityMessages = await CommunityMessageModel.find({
                        relationMsgId: parentId
                    }).sort({
                        date: -1
                    }).skip(0).limit(5).populate([{
                        path: 'contentId',
                        select: 'title stitle _id'
                    }, {
                        path: 'author',
                        select: 'userName _id enable date logo'
                    }]).exec();
                    if (!_.isEmpty(childCommunityMessages)) {
                        communityMessageItem.childCommunityMessages = childCommunityMessages;
                    } else {
                        communityMessageItem.childCommunityMessages = [];
                    }
                    communityMessageItem.comment_num = await CommunityMessageModel.count({
                        relationMsgId: parentId
                    })
                }
            }
            resolve(newCommunityMessageArr);
        } catch (error) {
            resolve(communityMessages);
        }
    })
}


class communityMessage {
    constructor() {
        // super()
    }
    async getCommunityMessages(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let searchkey = req.query.searchkey;
            let contentId = req.query.contentId;
            let author = req.query.author;
            let useClient = req.query.useClient;
            let userInfo = req.session.user;

            let queryObj = {};
            if (contentId) {
                queryObj.contentId = contentId;
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.content = {
                    $regex: reKey
                }
            }

            if (author) {
                queryObj.author = author;
            }

            // console.log('---queryObj--', queryObj)

            const communityMessages = await CommunityMessageModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'contentId',
                select: 'comments _id'
            }, {
                path: 'author',
                select: 'userName _id enable date logo'
            }, {
                path: 'replyAuthor',
                select: 'userName _id enable date logo'
            }, {
                path: 'adminAuthor',
                select: 'userName _id enable date logo'
            }, {
                path: 'adminReplyAuthor',
                select: 'userName _id enable date logo'
            }]).exec();

            const totalItems = await CommunityMessageModel.count(queryObj);
            let renderCommunityMessageData = await renderCommunityMessage(userInfo, communityMessages);

            let communityMessageData = {
                docs: renderCommunityMessageData,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || '',
                    totalPage: Math.ceil(totalItems / pageSize)
                }
            };
            let renderData = siteFunc.renderApiData(req, res, 200, 'communityMessage', communityMessageData, 'getlist');
            if (modules && modules.length > 0) {
                return renderData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'communityMessage', renderCommunityMessageData, 'getlist'));
                } else {
                    res.send(renderData);
                }
            }

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }


    async getAppCommunityMessages(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let searchkey = req.query.searchkey;
            let contentId = req.query.contentId;
            let useClient = req.query.useClient;
            let userInfo = req.session.user;

            let queryObj = {
                $or: [{
                    relationMsgId: ''
                }, {
                    relationMsgId: {
                        $exists: false
                    }
                }]
            };

            if (contentId) {
                queryObj.contentId = contentId;
            }

            // console.log('---queryObj--', queryObj)

            // 查询一级留言
            const communityMessages = await CommunityMessageModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'contentId',
                select: 'comments _id'
            }, {
                path: 'author',
                select: 'userName _id enable date logo'
            }]).exec();

            const totalItems = await CommunityMessageModel.count(queryObj);
            let renderCommunityMessages = await renderCommunityMessage(userInfo, communityMessages, useClient);

            let communityMessageData = {
                docs: renderCommunityMessages,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || '',
                    totalPage: Math.ceil(totalItems / pageSize)
                }
            };
            let renderData = siteFunc.renderApiData(req, res, 200, 'communityMessage', communityMessageData, 'getlist');
            if (modules && modules.length > 0) {
                return renderData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'communityMessage', renderCommunityMessages, 'getlist'));
                } else {
                    res.send(renderData);
                }
            }

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async postCommunityMessages(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                let errMsg = '';
                if (_.isEmpty(req.session.user) && _.isEmpty(req.session.adminUserInfo)) {
                    errMsg = res.__("validate_error_params")
                }
                if (!shortid.isValid(fields.contentId)) {
                    errMsg = res.__("validate_message_add_err")
                }
                if (fields.content && (fields.content.length < 5 || fields.content.length > 200)) {
                    errMsg = res.__("validate_rangelength", {
                        min: 5,
                        max: 200,
                        label: res.__("label_messages_content")
                    })
                }
                if (!fields.content) {
                    errMsg = res.__("validate_inputNull", {
                        label: res.__("label_messages_content")
                    })
                }

                if (!_.isEmpty(req.session.user)) {
                    let userInfo = req.session.user;
                    let watchCommunityState = userInfo.watchCommunityState;
                    let targetCommunityContent = await CommunityContentModel.findOne({
                        _id: fields.contentId
                    }, 'community');
                    let checkCommunity = _.filter(watchCommunityState, (item) => {
                        return (item.community == targetCommunityContent.community) && (item.state == '1');
                    })
                    if (_.isEmpty(checkCommunity)) {
                        throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
                    }
                }
                if (errMsg) {
                    throw new siteFunc.UserException(errMsg);
                }

                const communityMessageObj = {
                    contentId: fields.contentId,
                    content: xss(fields.content),
                    replyAuthor: fields.replyAuthor,
                    adminReplyAuthor: fields.adminReplyAuthor,
                    relationMsgId: fields.relationMsgId,
                    utype: fields.utype || '0'
                }

                if (fields.utype === '1') { // 管理员
                    communityMessageObj.adminAuthor = req.session.adminUserInfo._id;
                } else {
                    communityMessageObj.author = req.session.user._id;
                }

                // console.log('----communityMessageObj---', communityMessageObj);
                const newCommunityMessage = new CommunityMessageModel(communityMessageObj);

                let targetCommunityMessage = await newCommunityMessage.save();


                // 给被回复用户发送提醒邮件
                const systemConfigs = await SystemConfigModel.find({});
                const contentInfo = await CommunityContentModel.findOne({
                    _id: fields.contentId
                });
                let replyAuthor;

                if (fields.replyAuthor) {
                    replyAuthor = await UserModel.findOne({
                        _id: fields.replyAuthor
                    }, siteFunc.getAuthUserFields())
                } else {
                    replyAuthor = await AdminUserModel.findOne({
                        _id: fields.adminReplyAuthor
                    });
                }

                // if (!_.isEmpty(systemConfigs) && !_.isEmpty(contentInfo) && !_.isEmpty(replyAuthor)) {
                //     let mailParams = {
                //         replyAuthor: replyAuthor,
                //         content: contentInfo
                //     }
                //     if (fields.utype === '1') {
                //         mailParams.adminAuthor = req.session.adminUserInfo
                //     } else {
                //         mailParams.author = req.session.user
                //     }
                //     systemConfigs[0]['siteDomain'] = systemConfigs[0]['siteDomain'];
                //     service.sendEmail(req, res, systemConfigs[0], settings.email_notice_user_contentMsg, mailParams);
                // }

                // 发送消息给客户端
                let passiveUser = fields.replyAuthor ? fields.replyAuthor : contentInfo.user;
                siteFunc.addSiteMessage('5', req.session.user, passiveUser, targetCommunityMessage._id, {
                    targetMediaType: '3'
                });

                let rangeTime = siteFunc.getDateStr(-1);
                let comment_count = await BillRecordModel.count({
                    user: req.session.user._id,
                    type: settings.user_action_type_community_comment,
                    date: {
                        "$gte": new Date(rangeTime.startTime),
                        "$lte": new Date(rangeTime.endTime)
                    }
                });
                let configIntegral = await siteFunc.getIntegralonfig();
                if (comment_count < configIntegral.community_comment_limit) {
                    // 添加账单和行为
                    await siteFunc.addUserActionHis(req, res, settings.user_action_type_community_comment, {
                        targetId: targetCommunityMessage._id,
                        contentType: '3'
                    });
                } else {
                    console.log('评论超过限制，不计算奖励')
                }

                let returnCommunityMessage = await CommunityMessageModel.findOne({
                    _id: newCommunityMessage._id
                }).populate([{
                    path: 'contentId',
                    select: 'title stitle _id'
                }, {
                    path: 'author',
                    select: 'userName _id enable date logo'
                }, {
                    path: 'replyAuthor',
                    select: 'userName _id enable date logo'
                }, {
                    path: 'adminAuthor',
                    select: 'userName _id enable date logo'
                }, {
                    path: 'adminReplyAuthor',
                    select: 'userName _id enable date logo'
                }]).exec();
                res.send(siteFunc.renderApiData(req, res, 200, 'communityMessage', returnCommunityMessage, settings.user_action_type_community_comment, 'postcommunityMessage'))
            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }


    async delCommunityMessage(req, res, next) {
        try {
            let errMsg = '',
                targetIds = req.query.ids;
            let userInfo = req.session.user || {}

            if (!siteFunc.checkCurrentId(targetIds)) {
                errMsg = res.__("validate_error_params");
            } else {
                targetIds = targetIds.split(',');
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }


            for (const messageId of targetIds) {
                let messageObj = await CommunityMessageModel.findOne({
                    _id: messageId
                }).populate('contentId').exec();

                if (!_.isEmpty(messageObj) && messageObj.contentId.community) {
                    let targetCommunity = await CommunityModel.findOne({
                        _id: messageObj.contentId.community,
                        creator: userInfo._id
                    })
                    if (!_.isEmpty(targetCommunity)) {
                        await CommunityMessageModel.remove({
                            _id: messageId
                        });
                    } else {
                        throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                    }
                } else {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }
            }

            // for (let i = 0; i < targetIds.length; i++) {
            //     let msgObj = await CommunityMessageModel.findOne({
            //         _id: targetIds[i]
            //     });
            //     if (msgObj) {
            //         await CommunityContentModel.findOneAndUpdate({
            //             _id: msgObj.contentId
            //         }, {
            //             '$inc': {
            //                 'commentNum': -1
            //             }
            //         })
            //     }
            // }
            // await CommunityMessageModel.remove({
            //     '_id': {
            //         $in: targetIds
            //     }
            // });

            res.send(siteFunc.renderApiData(req, res, 200, 'communityMessage', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new communityMessage();