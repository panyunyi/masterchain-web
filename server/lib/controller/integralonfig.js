const BaseComponent = require('../prototype/baseComponent');
const IntegralonfigModel = require("../models").Integralonfig;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    // console.log('---fields-', fields)
    if (!fields.login_first ||
        !validator.isNumeric((fields.login_first).toString()) ||
        !fields.browse ||
        !validator.isNumeric((fields.browse).toString()) ||
        !fields.browse_limit ||
        !validator.isNumeric((fields.browse_limit).toString()) ||
        !fields.give_thumbs_up ||
        !validator.isNumeric((fields.give_thumbs_up).toString()) ||
        !fields.give_thumbs_up_limit ||
        !validator.isNumeric((fields.give_thumbs_up_limit).toString()) ||
        !fields.comment ||
        !validator.isNumeric((fields.comment).toString()) ||
        !fields.comment_limit ||
        !validator.isNumeric((fields.comment_limit).toString()) ||
        !fields.community_comment ||
        !validator.isNumeric((fields.community_comment).toString()) ||
        !fields.community_comment_limit ||
        !validator.isNumeric((fields.community_comment_limit).toString()) ||
        !fields.appreciate ||
        !validator.isNumeric((fields.appreciate).toString()) ||
        !fields.appreciate_limit ||
        !validator.isNumeric((fields.appreciate_limit).toString()) ||
        !fields.collections ||
        !validator.isNumeric((fields.collections).toString()) ||
        !fields.follow ||
        !validator.isNumeric((fields.follow).toString()) ||
        !fields.collections_limit ||
        !validator.isNumeric((fields.collections_limit).toString()) ||
        !fields.follow_limit ||
        !validator.isNumeric((fields.follow_limit).toString()) ||
        !fields.forward ||
        !validator.isNumeric((fields.forward).toString()) ||
        !fields.forward_limit ||
        !validator.isNumeric((fields.forward_limit).toString()) ||
        !fields.community_building ||
        !validator.isNumeric((fields.community_building).toString()) ||
        !fields.community_building_limit ||
        !validator.isNumeric((fields.community_building_limit).toString()) ||
        !fields.invitation ||
        !validator.isNumeric((fields.invitation).toString()) ||
        !fields.report ||
        !validator.isNumeric((fields.report).toString()) ||
        !fields.review ||
        !validator.isNumeric((fields.review).toString()) ||
        !fields.creat_content ||
        !validator.isNumeric((fields.creat_content).toString()) ||
        !fields.creat_identity ||
        !validator.isNumeric((fields.creat_identity).toString()) ||
        !fields.subscribe ||
        !validator.isNumeric((fields.subscribe).toString()) ||
        !fields.buyVip ||
        !validator.isNumeric((fields.buyVip).toString()) ||
        !fields.signIn ||
        !validator.isNumeric((fields.signIn).toString()) ||
        !fields.signIn_continuity ||
        !validator.isNumeric((fields.signIn_continuity).toString()) ||
        !fields.brithday ||
        !validator.isNumeric((fields.brithday).toString())
    ) {
        errMsg = res.__("validate_inputCorrect", {
            label: res.__("lc_number")
        });
    }


    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class Integralonfig {
    constructor() {
        // super()
    }
    async getIntegralonfigs(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {};
            let restful = req.query.restful;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = {
                    $regex: reKey
                }
            }

            const integralonfigs = await IntegralonfigModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize));

            let configData = {
                docs: integralonfigs
            };

            let renderData = siteFunc.renderApiData(req, res, 200, 'getIntegralonfigs', configData, 'getlist');

            if (modules && modules.length > 0) {
                return renderData.data.docs;
            } else {
                res.send(renderData);
            }

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async updateIntegralonfig(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
            } catch (err) {
                console.log(err.message, err);
                res.send(siteFunc.renderApiErr(req, res, 500, err, 'checkform'));
            }

            const systemObj = {
                login_first: fields.login_first, // 首次登陆
                register: fields.register, // 用户注册
                browse: fields.browse, // 浏览
                give_thumbs_up: fields.give_thumbs_up, // 点赞
                comment: fields.comment, // 评论
                community_comment: fields.community_comment, // 社群评论
                appreciate: fields.appreciate, // 打赏
                appreciate_in: fields.appreciate_in, // 打赏收入
                appreciate_out: fields.appreciate_out, // 打赏支出
                collections: fields.collections, // 收藏
                follow: fields.follow, // 关注
                forward: fields.forward, // 转发
                community_building: fields.community_building, // 社群建设
                invitation: fields.invitation, // 邀请
                report: fields.report, // 举报
                review: fields.review, // 审核
                creat_content: fields.creat_content, // 内容创作
                creat_identity: fields.creat_identity, // 实名认证
                shop_send: fields.shop_send, // 商城会员发放
                transfer_coins: fields.transfer_coins, // 提币
                recharge: fields.recharge, // 充值
                subscribe: fields.subscribe, // 课程订购
                buyVip: fields.buyVip, // 购买会员
                signIn: fields.signIn, // 签到
                signIn_continuity: fields.signIn_continuity, // 签到
                brithday: fields.brithday, // 生日
                browse_limit: fields.browse_limit, // 浏览
                give_thumbs_up_limit: fields.give_thumbs_up_limit, // 点赞
                comment_limit: fields.comment_limit, // 评论
                community_comment_limit: fields.community_comment_limit, // 社群评论
                appreciate_limit: fields.appreciate_limit, // 打赏
                collections_limit: fields.collections_limit, // 收藏关注
                follow_limit: fields.follow_limit, // 收藏关注
                forward_limit: fields.forward_limit, // 转发
                community_building_limit: fields.community_building_limit, // 社群建设
            }
            const item_id = fields._id;
            try {
                if (item_id) {
                    await IntegralonfigModel.findOneAndUpdate({
                        _id: item_id
                    }, {
                        $set: systemObj
                    });
                } else {
                    const newAdminUser = new IntegralonfigModel(systemObj);
                    await newAdminUser.save();
                }
                res.send(siteFunc.renderApiData(req, res, 200, 'Integralonfig', {}, 'update'))
            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }




}

module.exports = new Integralonfig();