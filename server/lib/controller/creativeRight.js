const BaseComponent = require('../prototype/baseComponent');
const CreativeRightModel = require("../models").CreativeRight;
const UserModel = require("../models").User;
const SpecialModel = require("../models").Special;
const ContentCategoryModel = require("../models").ContentCategory;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }

    if (!fields.category) {
        errMsg = res.__("validate_error_params");
    }

    if (!fields.newCateName || !validatorUtil.isRegularCharacter(fields.newCateName)) {
        errMsg = res.__("validate_error_params");
    }

    if (!validator.isLength(fields.comments, 2, 100)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 100,
            label: res.__("label_comments")
        });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class CreativeRight {
    constructor() {
        // super()
    }
    async getCreativeRights(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {};
            let useClient = req.query.useClient;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = {
                    $regex: reKey
                }
            }

            const creativeRights = await CreativeRightModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'user',
                select: 'name userName _id'
            }, {
                path: 'category',
                select: 'name _id'
            }, {
                path: 'approver',
                select: 'name userName _id'
            }]).exec();
            const totalItems = await CreativeRightModel.count(queryObj);

            let creativeRightData = {
                docs: creativeRights,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderCreativeRightData = siteFunc.renderApiData(req, res, 200, 'CreativeRight', creativeRightData);
            if (modules && modules.length > 0) {
                return renderCreativeRightData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getCreativeRights', creativeRights));
                } else {
                    res.send(renderCreativeRightData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async addCreativeRight(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                checkFormData(req, res, fields);

                const tagObj = {
                    type: '0',
                    newCateName: fields.newCateName,
                    category: fields.category,
                    user: req.session.user._id,
                    comments: fields.comments,
                    state: '0'
                }

                const newCreativeRight = new CreativeRightModel(tagObj);

                await newCreativeRight.save();

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('user_action_type_creat_topic')
                }), {
                    state: res.__('bill_label_approval_state_wait'),
                    id: newCreativeRight._id
                }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateCreativeRight(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
                let childCate = fields.category._id,
                    cateArr = [];
                let parentCate = await ContentCategoryModel.findOne({
                    _id: fields.category._id
                }, 'parentId _id');
                cateArr.push(parentCate);
                cateArr.push(childCate);
                const userObj = {
                    state: fields.state ? '1' : '0',
                    comments: fields.comments,
                    name: fields.newCateName,
                    category: cateArr,
                    creator: fields.user._id,
                    approver: req.session.adminUserInfo._id,
                    sImg: fields.sImg || "http://masterchain.oss-cn-hongkong.aliyuncs.com/upload/images/img1544623778539.png",
                    updatetime: new Date()
                }
                const item_id = fields._id;

                let targetRight = await CreativeRightModel.findOneAndUpdate({
                    _id: item_id
                }, {
                    $set: userObj
                });

                // 更新用户状态
                await UserModel.findOneAndUpdate({
                    _id: targetRight.user
                }, {
                    $set: {
                        creativeRight: true
                    }
                });

                // 创建新专题
                let newSpecialObj = new SpecialModel(userObj);
                await newSpecialObj.save();

                res.send(siteFunc.renderApiData(req, res, 200, 'CreativeRight', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delCreativeRight(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await CreativeRightModel.remove({
                _id: req.query.ids
            });
            res.send(siteFunc.renderApiData(req, res, 200, 'CreativeRight', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new CreativeRight();