const BaseComponent = require('../prototype/baseComponent');
const CommunityModel = require("../models").Community;
const CommunityContentModel = require("../models").CommunityContent;
const CommunityMessageModel = require("../models").CommunityMessage;
const BillRecordModel = require("../models").BillRecord;
const UserModel = require("../models").User;
const SystemConfigModel = require("../models").SystemConfig;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash');
const xss = require("xss");
const settings = require('../../../configs/settings');


function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }

    if (!fields.tags) {
        errMsg = res.__("validate_error_params");
    }

    if (!validatorUtil.isRegularCharacter(fields.name)) {
        errMsg = res.__("validate_error_field", {
            label: res.__("label_tag_name")
        });
    }
    if (!validator.isLength(fields.name, 1, 12)) {
        errMsg = res.__("validate_rangelength", {
            min: 1,
            max: 12,
            label: res.__("label_tag_name")
        });
    }
    if (!validator.isLength(fields.comments, 2, 100)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 100,
            label: res.__("label_comments")
        });
    }
    if (!fields.sImg) {
        errMsg = res.__('validate_uploadNull', {
            label: res.__('community_label_icon')
        });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

function renderCommunityData(userId, communityList) {
    return new Promise(async (resolve, reject) => {
        try {
            let newCommunityList = JSON.parse(JSON.stringify(communityList));
            let userInfo = {};
            if (userId) {
                userInfo = await UserModel.findOne({
                    _id: userId
                }, siteFunc.getAuthUserFields('session'));
            }
            for (const communityItem of newCommunityList) {
                let watchState = false;
                communityItem.had_watched_success = false;
                let watchNum = await UserModel.count({
                    // watchCommunity: communityItem._id,
                    "watchCommunityState": {
                        $elemMatch: {
                            "community": communityItem._id,
                            "state": '1'
                        }
                    }
                });
                communityItem.watch_num = watchNum;
                if (!_.isEmpty(userInfo) && userInfo.watchCommunity && (userInfo.watchCommunity).indexOf(communityItem._id) >= 0) {
                    // watchState = true;

                    let targetCommunity = _.filter(userInfo.watchCommunityState, (item) => {
                        return item.community == communityItem._id;
                    })
                    // 是否已成功加入(被管理员接受)
                    if (!_.isEmpty(targetCommunity) && targetCommunity.length == 1) {
                        communityItem.had_watched_success = targetCommunity[0].state == '1' ? true : false;
                        watchState = targetCommunity[0].state != '2' ? true : false;
                    }
                }
                communityItem.had_watched = watchState;
                let communityContents = await CommunityContentModel.find({
                    community: communityItem._id
                }, '_id');
                communityItem.content_num = communityContents.length;

                let communityContentArr = [];
                for (const contentItem of communityContents) {
                    communityContentArr.push(contentItem._id);
                }
                // console.log('--communityContentArr---', communityContentArr)
                // 查询社群总收益
                let totalBillObj = await BillRecordModel.aggregate([{
                        $match: {
                            target_communityContent: {
                                $in: communityContentArr
                            },
                            type: settings.user_action_type_appreciate_in,
                            unit: 'MEC',
                        }
                    },
                    {
                        $group: {
                            _id: null,
                            total_num: {
                                $sum: "$coins"
                            }
                        }
                    }
                ]);

                let totalBill_num = totalBillObj.length > 0 ? totalBillObj[0].total_num : 0;
                communityItem.total_rewards = totalBill_num;

            }
            resolve(newCommunityList)
        } catch (error) {
            resolve([])
        }
    })

}

class Community {
    constructor() {
        // super()
    }
    async getCommunity(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {};
            let tag = req.query.tag;
            let userInfo = req.session.user || {};
            let useClient = req.query.useClient;
            let creator = req.query.creator;
            let appVersion = req.query.v;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = {
                    $regex: reKey
                }
            }

            if (tag) {
                queryObj.tags = tag;
            }

            if (useClient != '0') {
                queryObj.state = '1';
            }

            if (creator) {
                queryObj.creator = creator;
                // 本人可以显示自己创建的社群
                if (creator == req.session.user._id) {
                    delete queryObj.state;
                }
            }

            // 针对老版本过滤不显示不开放的社群
            if (!appVersion && useClient == '2') {
                queryObj.open = '1';
            }

            const communities = await CommunityModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'tags',
                select: 'name _id'
            }, {
                path: 'creator',
                select: 'userName _id'
            }]).exec();
            const totalItems = await CommunityModel.count(queryObj);

            let renderCommunityList = await renderCommunityData(userInfo._id, communities);
            let tagsData = {
                docs: renderCommunityList,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderTagsData = siteFunc.renderApiData(req, res, 200, 'Community', tagsData, 'options');
            if (modules && modules.length > 0) {
                return renderTagsData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'CommunityList', renderCommunityList, 'options'));
                } else {
                    res.send(renderTagsData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }


    async getHotCommunity(req, res, next) {
        try {

            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let queryObj = {
                state: '1'
            };
            let userInfo = req.session.user || {};
            let useClient = req.query.useClient;
            let appVersion = req.query.v;


            // 针对老版本过滤不显示不开放的社群
            if (!appVersion && useClient == '2') {
                queryObj.open = '1';
            }

            if (!_.isEmpty(userInfo)) {
                userInfo = await UserModel.findOne({
                    _id: userInfo._id
                });

                queryObj._id = {
                    $nin: userInfo.watchCommunity
                }

            }

            // 查询所有社群
            let communities = [];
            let allCommunities = await CommunityModel.find(queryObj).populate([{
                path: 'tags',
                select: 'name _id'
            }, {
                path: 'creator',
                select: 'userName _id'
            }]).exec();

            allCommunities = JSON.parse(JSON.stringify(allCommunities));

            for (const communityItem of allCommunities) {

                let contentObj = await CommunityContentModel.find({
                    community: communityItem._id
                }, '_id id');
                let communityMessageNum = 0,
                    likeNum = 0;
                for (const contentItem of contentObj) {
                    communityMessageNum += await CommunityMessageModel.count({
                        contentId: contentItem._id
                    });
                    likeNum += await UserModel.count({
                        praiseCommunityContent: communityItem._id
                    });
                }

                // 帖子总数
                communityItem.contentNum = contentObj.length;
                // 社群评论总数
                communityItem.messageNum = communityMessageNum;
                // 点赞人数
                communityItem.likeNum = likeNum;
            }

            allCommunities = _.sortBy(allCommunities, (item) => {
                return -item.contentNum || -item.messageNum || -item.likeNum;
            })

            let skipNum = Number(pageSize) * (Number(current) - 1);
            if (Number(pageSize) * (Number(current)) <= allCommunities.length) {
                communities = allCommunities.splice(skipNum, pageSize);
            } else {
                communities = allCommunities;
            }

            let renderCommunityList = await renderCommunityData(userInfo._id, communities);

            res.send(siteFunc.renderApiData(req, res, 200, 'CommunityList', renderCommunityList, 'options'));

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }


    async getMyFollowCommunities(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let searchkey = req.query.searchkey;
            let useClient = req.query.useClient;
            let userInfo = req.session.user;
            let userId = req.query.userId; // 用于关注社群

            let queryObj = {
                state: '1'
            };

            if (userId) {
                let targetUser = await UserModel.findOne({
                    _id: userId
                });
                let userWatchCommunityStates = targetUser.watchCommunityState || [];
                let userCommunityArr = [];
                for (const stateItem of userWatchCommunityStates) {
                    if (stateItem.state == '1') {
                        userCommunityArr.push(stateItem.community);
                    }
                }
                queryObj = {
                    _id: {
                        $in: userCommunityArr
                    }
                }
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = {
                    $regex: reKey
                }
            }

            const communities = await CommunityModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'tags',
                select: 'name _id'
            }, {
                path: 'creator',
                select: 'userName _id'
            }]).exec();
            const totalItems = await CommunityModel.count(queryObj);

            let renderCommunities = await renderCommunityData(userInfo._id, communities);

            let communityData = {
                docs: renderCommunities,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderCommunity = siteFunc.renderApiData(req, res, 200, 'Community', communityData);
            if (modules && modules.length > 0) {
                return renderCommunity.data;
            } else {
                if (req.query.useClient == '1') {
                    res.send(renderCommunity);
                } else if (req.query.useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getMyFollowCommunities', renderCommunities));
                } else {
                    res.send(renderCommunity);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }



    async getOneCommunity(req, res, next) {
        try {

            let targetId = req.query.id;
            let userInfo = req.session.user || {};
            if (!shortid.isValid(targetId)) {
                throw new siteFunc.UserException(res.__('validate_error_params'));
            }

            const communityInfo = await CommunityModel.find({
                _id: targetId,
                state: '1'
            }).populate([{
                path: 'tags',
                select: 'name _id'
            }, {
                path: 'creator',
                select: 'userName _id'
            }]).exec();

            let renderCommunityList = await renderCommunityData(userInfo._id, communityInfo);

            res.send(siteFunc.renderApiData(req, res, 200, 'get community success', renderCommunityList[0], 'getItem'));

        } catch (err) {
            res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
        }
    }

    async addCommunity(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                checkFormData(req, res, fields);

                // 私有社群需要填写问题和答案
                if (fields.open != '1' && _.isEmpty(fields.questions)) {
                    // TODO 兼容老版本
                    throw new siteFunc.UserException(res.__('label_system_needupadate'));
                }

                // 开关允许才可以创建社群
                if (req.query.useClient != '0') {

                    let configs = await SystemConfigModel.find({}, 'allowUserPostContent allowUserCreateCommunity');

                    if (req.session.user.group != '1') {
                        if (!_.isEmpty(configs) && configs.length == 1) {
                            if (!configs[0].allowUserCreateCommunity) {
                                throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
                            }
                        } else {
                            throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
                        }
                    }

                }

                let targetQuestions = fields.questions;
                const tagObj = {
                    name: fields.name,
                    sImg: fields.sImg,
                    needAuth: fields.needAuth || '0',
                    questions: !_.isEmpty(targetQuestions) ? JSON.parse(targetQuestions) : [],
                    open: fields.open || '1',
                    tags: (fields.tags).split(','),
                    comments: xss(fields.comments),
                    state: '0'
                }

                if (req.query.useClient != '0') {
                    tagObj.creator = req.session.user._id;
                }

                const newCommunity = new CommunityModel(tagObj);

                await newCommunity.save();

                // 将群主加入社群
                let userInfo = req.session.user
                await UserModel.update({
                    _id: userInfo._id
                }, {
                    $addToSet: {
                        watchCommunity: newCommunity._id,
                        watchCommunityState: {
                            community: newCommunity._id,
                            state: '1',
                            questions: !_.isEmpty(targetQuestions) ? JSON.parse(targetQuestions) : [],
                        },
                    }
                });

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('user_action_type_community_building')
                }), {
                    id: newCommunity._id
                }))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateCommunity(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
                let useClient = req.query.isApi ? '2' : '0';
                let communityId = fields.id;
                let userInfo = {};
                let targetQuestions = fields.questions;
                let targetTags = fields.tags;
                let queryObj = {
                    _id: communityId,
                };

                if (useClient == '2') {
                    await siteFunc.checkPostToken(req, res, fields.token);
                    userInfo = req.session.user;
                    queryObj.creator = userInfo._id;
                }


                if (useClient != '0' && _.isEmpty(userInfo)) {
                    throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
                } else {

                    if (!shortid.isValid(communityId)) {
                        throw new siteFunc.UserException(res.__('validate_error_params'));
                    }

                    let targetCommunity = await CommunityModel.findOne(queryObj);
                    // 非作者本人无法更新
                    if (_.isEmpty(targetCommunity)) {
                        throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
                    }

                    if (targetTags) {
                        targetTags = useClient == '2' ? (fields.tags).split(',') : fields.tags;
                    }

                }

                const userObj = {
                    name: fields.name,
                    sImg: fields.sImg,
                    tags: targetTags,
                    comments: xss(fields.comments)
                }

                if (fields.open) {
                    userObj.open = fields.open;
                }

                if (fields.needAuth) {
                    userObj.needAuth = fields.needAuth;
                }

                // 只有管理员才负责做状态变更
                if (useClient == '0') {
                    userObj.state = fields.state;
                }

                const item_id = fields.id;

                await CommunityModel.findOneAndUpdate({
                    _id: item_id
                }, {
                    $set: userObj
                });
                res.send(siteFunc.renderApiData(req, res, 200, 'Community', {}, 'options'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }
    async updateQuestions(req, res, next) {

        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);
                let userInfo = req.session.user;
                let communityId = fields.communityId;
                let targetQuestions = fields.questions;
                let needAuth = fields.needAuth || '0';

                if (needAuth != '0' && needAuth != '1') {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                if (!shortid.isValid(communityId)) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                if (_.isEmpty(targetQuestions)) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                let targetCommunity = await CommunityModel.findOne({
                    _id: communityId,
                    creator: userInfo._id
                });
                if (_.isEmpty(targetCommunity)) {
                    throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
                }

                const userObj = {
                    needAuth,
                    questions: !_.isEmpty(targetQuestions) ? JSON.parse(targetQuestions) : [],
                }

                await CommunityModel.findOneAndUpdate({
                    _id: communityId
                }, {
                    $set: userObj
                });

                res.send(siteFunc.renderApiData(req, res, 200, 'Community', {}, 'options'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delCommunity(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await CommunityModel.remove({
                _id: req.query.ids
            });
            res.send(siteFunc.renderApiData(req, res, 200, 'Community', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }

}

module.exports = new Community();