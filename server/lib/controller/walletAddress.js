const BaseComponent = require('../prototype/baseComponent');
const WalletAddressModel = require("../models").WalletAddress;
const formidable = require('formidable');
const { service, validatorUtil, siteFunc } = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const xss = require("xss");
const _ = require('lodash');

function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    if (!fields.wallet || fields.wallet.length != 42) {
        errMsg = res.__("validate_rangelength", { min: 1, max: 42, label: res.__("label_tag_name") });
    }
    if (!validator.isLength(fields.comments, 2, 30)) {
        errMsg = res.__("validate_rangelength", { min: 2, max: 30, label: res.__("label_comments") });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class WalletAddress {
    constructor() {
        // super()
    }
    async getWalletAddress(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey, queryObj = {};
            let useClient = req.query.useClient;
            let user = req.session.user;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.wallet = { $regex: reKey }
            }

            if (user) {
                queryObj.user = user._id;
            }

            const walletAddress = await WalletAddressModel.find(queryObj).sort({ date: -1 }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize));
            const totalItems = await WalletAddressModel.count(queryObj);

            let tagsData = {
                docs: walletAddress,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderTagsData = siteFunc.renderApiData(req, res, 200, 'WalletAddress', tagsData);
            if (modules && modules.length > 0) {
                return renderTagsData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'WalletAddress', walletAddress))
                } else {
                    res.send(renderTagsData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async addWalletAddress(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                checkFormData(req, res, fields);

                let myWalletCount = await WalletAddressModel.count({ user: req.session.user._id });

                // 限制钱包数量
                if (myWalletCount >= 6) {
                    throw new siteFunc.UserException(res.__("wallet_label_lessThen_num"));
                }

                const tagObj = {
                    user: req.session.user._id,
                    wallet: fields.wallet,
                    comments: fields.comments
                }

                const newWalletAddress = new WalletAddressModel(tagObj);

                await newWalletAddress.save();

                res.send(siteFunc.renderApiData(req, res, 200, 'WalletAddress', { id: newWalletAddress._id }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async updateWalletAddress(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                checkFormData(req, res, fields);

                const userObj = {
                    wallet: fields.wallet,
                    comments: fields.comments
                }
                const item_id = fields.id;

                await WalletAddressModel.findOneAndUpdate({ _id: item_id }, { $set: userObj });
                res.send(siteFunc.renderApiData(req, res, 200, 'WalletAddress', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delWalletAddress(req, res, next) {
        try {
            let errMsg = '';
            let userInfo = req.session.user;
            if (!siteFunc.checkCurrentId(req.query.id)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }

            let targetWallet = await WalletAddressModel.findOne({ _id: req.query.id, user: userInfo._id });
            if (_.isEmpty(targetWallet)) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }

            await WalletAddressModel.remove({ _id: req.query.id, user: userInfo._id });
            res.send(siteFunc.renderApiData(req, res, 200, 'delWalletAddress', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }

}

module.exports = new WalletAddress();