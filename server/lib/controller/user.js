const BaseComponent = require('../prototype/baseComponent');
const UserModel = require("../models").User;
const MessageModel = require("../models").Message;
const NotifyModel = require("../models").Notify;
const UserNotifyModel = require("../models").UserNotify;
const AdminUserModel = require("../models").AdminUser;
const SystemConfigModel = require("../models").SystemConfig;
const ContentTemplateModel = require("../models").ContentTemplate;
const ContentModel = require("../models").Content;
const SpecialModel = require("../models").Special;
const BillRecordModel = require("../models").BillRecord;
const CommunityModel = require("../models").Community;
const WalletManagerModel = require("../models").WalletManager;
const CommunityContentModel = require("../models").CommunityContent;
const CommunityMessageModel = require("../models").CommunityMessage;
const OpenVipRecordModel = require("../models").OpenVipRecord;
const ContentTagModel = require("../models").ContentTag;
const ResetAmountModel = require("../models").ResetAmount;
const IdentityAuthenticationModel = require("../models").IdentityAuthentication;
const schedule = require('node-schedule');

const formidable = require('formidable');
const crypto = require('crypto');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
// const authToken = require('../../../utils/middleware/token');
const jwt = require('jsonwebtoken');
const cache = require('../../../utils/middleware/cache');
const settings = require('../../../configs/settings');
const shortid = require('shortid');
const validator = require('validator');
const _ = require('lodash')
const fs = require('fs')
const captcha = require('trek-captcha')
const xss = require("xss");
const moment = require('moment');
const https = require('https');


function checkFormData(req, res, fields) {
    let errMsg = '';
    // console.log('----')
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    if (fields.profession && !validator.isNumeric(fields.profession)) {
        errMsg = res.__("validate_error_field", {
            label: res.__("label_profession")
        });
    }
    if (fields.industry && !validator.isNumeric(fields.industry)) {
        errMsg = res.__("validate_error_field", {
            label: res.__("label_introduction")
        });
    }
    if (fields.experience && !validator.isNumeric(fields.experience)) {
        errMsg = res.__("validate_error_field", {
            label: res.__("label_experience")
        });
    }
    if (fields.userName && !validatorUtil.isRegularCharacter(fields.userName)) {
        errMsg = res.__("validate_error_field", {
            label: res.__("label_user_userName")
        });
    }
    if (fields.userName && !validator.isLength(fields.userName, 2, 30)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 12,
            label: res.__("label_user_userName")
        });
    }
    if (fields.name && !validatorUtil.isRegularCharacter(fields.name)) {
        errMsg = res.__("validate_error_field", {
            label: res.__("label_name")
        });
    }
    if (fields.name && !validator.isLength(fields.name, 2, 20)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 20,
            label: res.__("label_name")
        });
    }
    // if (fields.phoneNum && !validator.isNumeric((fields.phoneNum).toString())) {
    //     errMsg = res.__("validate_inputCorrect", { label: res.__("label_user_phoneNum") });
    // }
    if (fields.gender && (fields.gender != '0' && fields.gender != '1')) {
        errMsg = res.__("validate_inputCorrect", {
            label: res.__("lc_gender")
        });
    }
    if (fields.email && !validatorUtil.checkEmail(fields.email)) {
        errMsg = res.__("validate_inputCorrect", {
            label: res.__("label_user_email")
        });
    }
    // if (fields.profession && !validatorUtil.isRegularCharacter(fields.profession)) {
    //     errMsg = res.__("validate_error_field", { label: res.__("label_profession") });
    // }
    // if (fields.profession && !validator.isLength(fields.profession, 2, 100)) {
    //     errMsg = res.__("validate_rangelength", { min: 2, max: 100, label: res.__("label_profession") });
    // }
    if (fields.introduction && !validatorUtil.isRegularCharacter(fields.introduction)) {
        errMsg = res.__("validate_error_field", {
            label: res.__("label_introduction")
        });
    }
    if (fields.introduction && !validator.isLength(fields.introduction, 2, 100)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 100,
            label: res.__("label_introduction")
        });
    }
    if (fields.comments && !validatorUtil.isRegularCharacter(fields.comments)) {
        errMsg = res.__("validate_error_field", {
            label: res.__("label_comments")
        });
    }
    if (fields.comments && !validator.isLength(fields.comments, 2, 100)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 100,
            label: res.__("label_comments")
        });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

function renderUserList(userInfo = {}, userList = [], useClient = '2', params = {}) {

    return new Promise(async (resolve, reject) => {
        try {
            let newUserList = JSON.parse(JSON.stringify(userList));
            for (let userItem of newUserList) {

                if (useClient != '0') {
                    let userContents = await ContentModel.count({
                        uAuthor: userItem._id,
                        state: '2'
                    });
                    userItem.content_num = userContents;
                    userItem.watch_num = _.uniq(userItem.watchers).length;
                    userItem.follow_num = _.uniq(userItem.followers).length;
                    userItem.had_followed = false;

                    // 参与的评论数
                    let comments_num = await MessageModel.count({
                        author: userItem._id
                    });
                    userItem.comments_num = comments_num;

                    // 收藏的文章数量
                    userItem.favorites_num = userItem.favorites ? userItem.favorites.length : 0;

                    // MEC 余额
                    if (process.env.NODE_ENV == 'production') {
                        userItem.total_reward_num = await siteFunc.getMECLeftCoins(userItem.walletAddress);
                    } else {
                        userItem.total_reward_num = 0;
                    }

                    // 只有查询单个用户才查询点赞总数和被关注人数
                    if (params.apiName == 'getUserInfoById') {
                        let total_likeNum = 0,
                            total_despiseNum = 0;

                        let total_likeNum_obj = await ContentModel.aggregate([{
                                $unwind: '$praiseUser'
                            },
                            {
                                $match: {
                                    uAuthor: userItem._id,
                                    state: '2'
                                }
                            },
                            {
                                $group: {
                                    _id: null,
                                    count: {
                                        $sum: 1
                                    }
                                }
                            }
                        ]);
                        if (!_.isEmpty(total_likeNum_obj) && total_likeNum_obj.length == 1) {
                            total_likeNum = total_likeNum_obj[0].count;
                        }

                        let total_despiseNum_obj = await ContentModel.aggregate([{
                                $unwind: '$despiseUser'
                            },
                            {
                                $match: {
                                    uAuthor: userItem._id,
                                    state: '2'
                                }
                            },
                            {
                                $group: {
                                    _id: null,
                                    count: {
                                        $sum: 1
                                    }
                                }
                            }
                        ]);
                        if (!_.isEmpty(total_despiseNum_obj) && total_despiseNum_obj.length == 1) {
                            total_despiseNum = total_despiseNum_obj[0].count;
                        }
                        userItem.total_likeNum = total_likeNum;
                        userItem.total_despiseNum = total_despiseNum;
                    }

                    // 社群统计成员的帖子点赞和社群帖子总数
                    if (params.apiName == 'getCommunityUsers') {
                        let userCommunityContents = await CommunityContentModel.find({
                            user: userItem._id,
                            community: params.communityId
                        });

                        if (!_.isEmpty(userCommunityContents)) {
                            let total_community_likeNum = 0;
                            for (const communityItem of userCommunityContents) {
                                total_community_likeNum += await UserModel.count({
                                    praiseCommunityContent: communityItem._id
                                });
                            }
                            userItem.total_community_likeNum = total_community_likeNum;
                        }
                        userItem.total_communityContent_Num = userCommunityContents.length;

                        // 查询认证信息
                        let targetCommunity = await CommunityModel.findOne({
                            _id: params.communityId
                        }, 'open needAuth');

                        if (!_.isEmpty(targetCommunity) && targetCommunity.open == '0' && targetCommunity.needAuth == '1') {
                            userItem.authInfo = await IdentityAuthenticationModel.findOne({
                                user: userItem._id
                            }, 'name');
                        }

                    }


                    // console.log('---userItem.followers---', userItem.followers)
                    // console.log('userId--', userId)
                    if (!_.isEmpty(userInfo)) {
                        if (userInfo.watchers.indexOf(userItem._id) >= 0) {
                            userItem.had_followed = true;
                        }
                    }
                    if (useClient == '3') {
                        userItem.walletAddress && delete userItem.walletAddress;
                        userItem.watchSpecials && delete userItem.watchSpecials;
                        userItem.watchCommunity && delete userItem.watchCommunity;
                        userItem.watchCommunityState && delete userItem.watchCommunityState;
                        userItem.praiseCommunityContent && delete userItem.praiseCommunityContent;
                        userItem.praiseMessages && delete userItem.praiseMessages;
                        userItem.praiseContents && delete userItem.praiseContents;
                        userItem.favoriteCommunityContent && delete userItem.favoriteCommunityContent;
                        userItem.favorites && delete userItem.favorites;
                        userItem.despiseCommunityContent && delete userItem.despiseCommunityContent;
                        userItem.despiseMessage && delete userItem.despiseMessage;
                        userItem.despises && delete userItem.despises;
                        userItem.watchers && delete userItem.watchers;
                        userItem.followers && delete userItem.followers;
                    } else {
                        siteFunc.clearUserSensitiveInformation(userItem);
                    }
                } else {

                    let endTime = await siteFunc.getVipTime(userItem);

                    if (endTime) userItem.endTime = endTime;
                    // MEC 余额
                    if (process.env.NODE_ENV == 'production') {
                        userItem.total_reward_num = await siteFunc.getMECLeftCoins(userItem.walletAddress);;
                    }
                }

            }

            // 按粉丝量排序
            if (params.sortby == '1') {
                // newUserList = _.sortBy(newUserList, (item) => {
                //     return -item.follow_num;
                // })
            }

            // 按社群加入时间排序
            if (params.apiName == 'getCommunityUsers') {
                // newUserList = _.sortBy(newUserList, (item) => {
                //     let targetJoinCommunityArr = _.filter(item.watchCommunityState, (communityItem) => {
                //         return communityItem.community == params.communityId
                //     })
                //     if (!_.isEmpty(targetJoinCommunityArr)) {
                //         let targetJoinCommunity = targetJoinCommunityArr[0];
                //         if (!_.isEmpty(targetJoinCommunity) && targetJoinCommunity.accessDate) {
                //             let time = new Date(targetJoinCommunity.accessDate).getTime()
                //             console.log('符合排序条件:', time);
                //             return time * -1;
                //         } else {
                //             let time = new Date(item.date).getTime()
                //             return time * -1;
                //         }
                //     } else {
                //         let time = new Date(item.date).getTime()
                //         return time * -1;
                //     }
                // })
            }

            resolve(newUserList);
        } catch (error) {
            resolve(userList);
        }
    })

}


function getCacheValueByKey(key) {
    return new Promise((resolve, reject) => {
        cache.get(key, (targetValue) => {
            if (targetValue) {
                resolve(targetValue)
            } else {
                resolve('');
            }
        })
    })
}


function askItunesApi(receiptData, apiUrl) {
    return new Promise((resolve, reject) => {
        var receiptEnvelope = {
            "receipt-data": receiptData
        };
        var receiptEnvelopeStr = JSON.stringify(receiptEnvelope);
        var options = {
            host: apiUrl,
            port: 443,
            path: '/verifyReceipt',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(receiptEnvelopeStr)
            }
        };

        var request = https.request(options, function (response) {
            var _data = '';
            response.setEncoding('utf8');
            response.on('data', function (chunk) {
                _data += chunk;
            });
            response.on('end', async () => {
                // console.log("body: " + _data);
                let resData = JSON.parse(_data);
                // console.log('-resData----', resData);
                resolve(resData)
            });
        });

        request.write(receiptEnvelopeStr);
        request.end();
    })
}


function chargeForIos(req, res, resData, userInfo) {
    return new Promise(async (resolve, reject) => {
        let appResData = resData["receipt"]["in_app"][0];
        let product_id = appResData.product_id;
        let orderId = appResData.transaction_id;


        let orderChargeBill = await BillRecordModel.findOne({
            user: userInfo._id,
            orderId
        });
        // 判断是否重复充值
        if (!_.isEmpty(orderChargeBill)) {
            reject(res.__("bill_label_recharge_state_failed"));
        }

        // 调用api充值
        let targetUser = await UserModel.findOne({
            _id: userInfo._id
        }, 'walletAddress');
        // console.log('---targetUser--', targetUser)
        let transerInfo = await siteFunc.transferMec(targetUser.walletAddress, settings.iapPriceMap[product_id]);
        // console.log('-transerInfo--', transerInfo);
        if (transerInfo == '1') {
            let billPrams = {
                unit: "MEC",
                coins: settings.iapPriceMap[product_id],
                iapComments: JSON.stringify(resData),
                orderId
            }
            console.log('----添加充值记录--', settings.iapPriceMap[product_id])
            await siteFunc.addUserActionHis(req, res, settings.user_bill_type_recharge, billPrams);
            console.log('----完成充值--')
            resolve('1');
        } else {
            reject(res.__("bill_label_recharge_state_failed"));
        }
    })
}

class User {
    constructor() {
        // super()
    }

    async getImgCode(req, res) {
        const {
            token,
            buffer
        } = await captcha();
        req.session.imageCode = token;
        res.writeHead(200, {
            'Content-Type': 'image/png'
        });
        res.write(buffer);
        res.end();
    }

    async getUsers(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let searchkey = req.query.searchkey,
                queryObj = {};
            let isTopBar = req.query.isTopBar;
            let follow = req.query.follow;
            let category = req.query.category;
            let userInfo = req.session.user || {};
            let group = req.query.group;
            let useClient = req.query.useClient;
            let sortby = req.query.sortby;
            let sortObj = {
                date: -1
            }
            let populateArr = [{
                path: 'category',
                select: 'name _id'
            }];

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i');
                if (isTopBar == '1') {
                    queryObj.$or = [{
                        userName: {
                            $regex: reKey
                        }
                    }, {
                        phoneNum: {
                            $regex: reKey
                        }
                    }, {
                        email: {
                            $regex: reKey
                        }
                    }];
                } else {
                    queryObj.userName = {
                        $regex: reKey
                    }
                }

                // 只搜大师
                (useClient != '0') && (queryObj.group = '1');
            }

            if (category) {
                queryObj.category = category;
            }

            if (group) {
                queryObj.group = group;
            }

            if (sortby == '1') {
                sortObj = {
                    followers: 1
                }
            }
            // console.log('--req.session.user---', req.session.user);
            if (follow) {
                if (follow == '1' && req.session.user) {
                    queryObj._id = {
                        $in: req.session.user.watchers
                    };
                    populateArr = [{
                        path: 'category',
                        select: 'name _id'
                    }];
                } else {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }
            }
            let files = (useClient != '0') ? siteFunc.getAuthUserFields('base') : null;
            // console.log('--queryObj---', queryObj)
            let users = await UserModel.find(queryObj, files).sort(sortObj).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate(populateArr).exec();
            let renderUsers = await renderUserList(userInfo, users, useClient, {
                sortby
            });
            // console.log('---renderUsers--', renderUsers)
            const totalItems = await UserModel.count(queryObj);
            let renderData = {
                docs: renderUsers,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || '',
                    isTopBar: isTopBar
                }
            }
            if (modules && modules.length > 0) {
                return renderUsers;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'user', renderUsers, 'getlist'));
                } else {
                    res.send(siteFunc.renderApiData(req, res, 200, 'user', renderData, 'getlist'));
                }
            }

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }


    // 获取随机大师
    async getRandomMasters(req, res, next) {
        try {
            let queryObj = {};
            let userInfo = req.session.user || {};
            _.assign(queryObj, {
                enable: true,
                group: '1'
            })
            const totalContents = await UserModel.count(queryObj);
            const randomArticles = await UserModel.find(queryObj, siteFunc.getAuthUserFields('base')).skip(Math.floor(totalContents * Math.random())).limit(10);
            let renderRandomList = await renderUserList(userInfo, randomArticles)
            res.send(siteFunc.renderApiData(req, res, 200, 'get getRandomMasters success', renderRandomList, 'save'));
        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'getRandomMasters'));
        }

    }

    async getOneUserByParams(params) {
        return await UserModel.findOne(params, siteFunc.getAuthUserFields('session')).populate([{
            path: 'category',
            select: 'name _id'
        }]);
    }

    async getUserInfoById(req, res, next) {
        try {
            let targetId = req.query.id;
            let useClient = req.query.useClient;
            let user = req.session.user || {};
            if (!shortid.isValid(targetId)) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }
            let targetUser = await UserModel.find({
                _id: targetId
            }, siteFunc.getAuthUserFields('base'));
            let renderUser = await renderUserList(user, targetUser, useClient, {
                apiName: 'getUserInfoById'
            });
            let userInfo = {};
            if (!_.isEmpty(renderUser) && (renderUser.length == 1)) {
                userInfo = renderUser[0];
                userInfo.birthBoxState = false; // 弹窗状态
                let userBirth = userInfo.birth;
                if (userBirth) {
                    let currentDay = moment().format("MMDD");
                    let birthDay = moment(userBirth).format("MMDD");

                    // 一年之内只发一次
                    let rangeTime = siteFunc.getDateStr(-365);
                    let birthRecords = await BillRecordModel.findOne({
                        user: userInfo._id,
                        type: settings.user_bill_type_brithday,
                        date: {
                            "$gte": new Date(rangeTime.startTime),
                            "$lte": new Date(rangeTime.endTime)
                        }
                    });

                    // console.log(currentDay, '------', birthDay)
                    // console.log(currentDay, '------', birthDay)

                    // 判断今天是否生日
                    if (currentDay == birthDay) {
                        // 今年还没有发
                        if (_.isEmpty(birthRecords)) {
                            userInfo.birthBoxState = true;
                            // 生成一条待领取记录
                            await siteFunc.addUserActionHis(req, res, settings.user_bill_type_brithday, {
                                targetUser: userInfo
                            });
                        } else {
                            // 还未领取
                            if (birthRecords.coins == 0) {
                                userInfo.birthBoxState = true;
                            }
                        }
                    } else {
                        // 有记录，但是未领取，判断时间是否已过期
                        if (!_.isEmpty(birthRecords) && birthRecords.coins == 0) {
                            let currentYear = new Date().getFullYear();
                            var gateDays = new Date().getTime() - new Date(currentYear + birthDay).getTime();
                            var gapTime = parseInt(gateDays / (1000 * 60 * 60 * 24));
                            if (gapTime > 0 && gapTime < 30) {
                                userInfo.birthBoxState = true;
                            }

                        }
                    }
                }
            }
            res.send(siteFunc.renderApiData(req, res, 200, 'getUserInfoById', userInfo, 'getlist'));

        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'getlist'))
        }

    }


    // 我关注的专题、大师等信息
    async getMyFollowInfos(req, res, next) {
        try {
            let userInfo = req.session.user;
            let targetUser = await UserModel.findOne({
                _id: userInfo._id
            }, 'watchers watchSpecials').populate([{
                path: 'watchers',
                select: 'name userName _id logo'
            }, {
                path: 'watchSpecials',
                select: 'name _id sImg comments'
            }]).exec();
            // console.log('-targetUser----', targetUser)
            let watchersList = targetUser.watchers;
            let watchSpecialList = targetUser.watchSpecials;

            let watchMasterContents = [];
            for (const master of watchersList) {
                let masterId = master._id;
                let masterContents = await ContentModel.find({
                    uAuthor: masterId,
                    state: '2'
                }, siteFunc.getContentListFields(true)).populate([{
                    path: 'uAuthor',
                    select: '_id userName logo name group'
                }]);
                if (!_.isEmpty(masterContents)) {
                    watchMasterContents = [].concat(masterContents);
                }
            }

            let renderData = {
                watchersList,
                watchSpecialList,
                watchMasterContents
            }

            res.send(siteFunc.renderApiData(req, res, 200, 'getMyFollowInfos', renderData, 'save'));

        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'getlist'))
        }
    }

    async updateUser(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                checkFormData(req, res, fields);

                const userObj = {};
                if (fields.enable != 'undefined' && fields.enable != undefined) {
                    userObj.enable = fields.enable;
                }
                if (fields.userName) {
                    userObj.userName = fields.userName;
                }
                if (fields.name) {
                    userObj.name = fields.name;
                }
                if (fields.gender) {
                    userObj.gender = fields.gender;
                }
                // TODO 邮箱暂不支持独立更改
                // if (fields.email) {
                //     userObj.email = fields.email;
                // }
                if (fields.logo) {
                    userObj.logo = fields.logo;
                }
                // if (fields.phoneNum) {
                //     userObj.phoneNum = fields.phoneNum;
                // }
                if (fields.confirm) {
                    userObj.confirm = fields.confirm;
                }
                if (fields.group) {
                    userObj.group = fields.group;
                }
                if (fields.category) {
                    userObj.category = fields.category;
                }
                if (fields.comments) {
                    userObj.comments = xss(fields.comments);
                }
                // if (fields.introduction) {
                userObj.introduction = xss(fields.introduction);
                // }
                if (fields.company) {
                    userObj.company = fields.company;
                }
                if (fields.province) {
                    userObj.province = fields.province;
                }
                if (fields.city) {
                    userObj.city = fields.city;
                }
                if (fields.birth) {
                    // 生日日期不能大于当前时间
                    if (new Date(fields.birth).getTime() > new Date().getTime()) {
                        throw new siteFunc.UserException(res.__('validate_error_params'));
                    }
                    userObj.birth = fields.birth;
                }
                if (fields.industry) {
                    userObj.industry = xss(fields.industry);
                }
                if (fields.profession) {
                    userObj.profession = xss(fields.profession);
                }
                if (fields.experience) {
                    userObj.experience = xss(fields.experience);
                }
                if (fields.password) {
                    userObj.password = service.encrypt(fields.password, settings.encrypt_key);
                }

                let targetUserId = '';
                // console.log('---cc-', req.query.useClient)
                if (req.query.useClient == '0') {
                    targetUserId = fields._id;
                } else {
                    targetUserId = req.session.user._id;
                }

                // console.log('--userObj--', userObj);
                // console.log('--targetUserId--', targetUserId);
                await UserModel.findOneAndUpdate({
                    _id: targetUserId
                }, {
                    $set: userObj
                });


                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('lc_basic_sub_btn')
                }), {}, 'updateUserInfo'));

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    // 更新邀请码（被邀请）
    async updateUserBeInvitationCode(req, res) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                let userInfo = req.session.user || {};
                let userId = userInfo._id;
                if (!userId || !shortid.isValid(userId)) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }
                // 填写邀请码
                if (fields.invitationCode && shortid.isValid(fields.invitationCode)) {
                    // let userObj = {};
                    // 邀请者
                    let invitUser = await UserModel.findOne({
                        _id: fields.invitationCode
                    });
                    // 被邀请者
                    let inviteRecord = await BillRecordModel.findOne({
                        type: settings.user_action_type_invitation,
                        target_user: userId
                    });
                    if (!_.isEmpty(invitUser) && _.isEmpty(inviteRecord)) {
                        // userObj.invitationCode = fields.invitationCode;

                        // 一天内邀请数量不超过5次不添加奖励
                        let rangeTime = siteFunc.getDateStr(-1);
                        let thumbs_up_count = await BillRecordModel.count({
                            user: fields.invitationCode,
                            type: settings.user_action_type_invitation,
                            date: {
                                "$gte": new Date(rangeTime.startTime),
                                "$lte": new Date(rangeTime.endTime)
                            }
                        });
                        let inviteState = '1';
                        if (thumbs_up_count < 5) {
                            inviteState = '1';
                        } else {
                            inviteState = '0';
                        }
                        // 添加行为积分
                        await siteFunc.addUserActionHis(req, res, settings.user_action_type_invitation, {
                            targetUser: {
                                _id: fields.invitationCode
                            },
                            beInvitedUser: userId,
                            inviteState
                        });

                    }

                    res.send(siteFunc.renderApiData(req, res, 200, 'updateUserBeInvitationCode', {}, 'updateUserInfo'));

                } else {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }
            } catch (error) {
                res.send(siteFunc.renderApiErr(req, res, 500, error, 'updateUserBeInvitationCode'));
            }
        })

    }

    async bindEmailOrPhoneNum(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                let userInfo = req.session.user;
                let bindType = fields.type;
                let errMsg = '';

                if (bindType != '1' && bindType != '2') {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                if (bindType == '1') {
                    if (!fields.phoneNum || !validatorUtil.checkPhoneNum((fields.phoneNum).toString())) {
                        errMsg = res.__("validate_inputCorrect", {
                            label: res.__("label_user_phoneNum")
                        })
                    }

                    if (!fields.countryCode) {
                        errMsg = res.__("validate_selectNull", {
                            label: res.__("label_user_countryCode")
                        });
                    }

                    if (userInfo.phoneNum) {
                        throw new siteFunc.UserException(res.__("user_action_tips_repeat", {
                            label: res.__('lc_bind')
                        }));
                    }

                    let queryUserObj = {
                        $or: [{
                            phoneNum: fields.phoneNum,
                        }, {
                            phoneNum: '0' + fields.phoneNum,
                        }],
                        countryCode: fields.countryCode
                    };

                    if (fields.phoneNum.indexOf('0') == '0') {
                        queryUserObj = {
                            $or: [{
                                phoneNum: fields.phoneNum
                            }, {
                                phoneNum: fields.phoneNum.substr(1),
                            }],
                            countryCode: fields.countryCode
                        };
                    }

                    let userRecords = await UserModel.findOne(queryUserObj);
                    if (!_.isEmpty(userRecords)) {
                        throw new siteFunc.UserException(res.__('validate_user_had_bind'));
                    }

                } else {
                    if (!validatorUtil.checkEmail(fields.email)) {
                        errMsg = res.__("validate_inputCorrect", {
                            label: res.__("label_user_email")
                        });
                    }
                    // console.log('--userInfo-', userInfo)
                    if (userInfo.email) {
                        throw new siteFunc.UserException(res.__("user_action_tips_repeat", {
                            label: res.__('lc_bind')
                        }));
                    }
                    let userRecords = await UserModel.findOne({
                        email: fields.email
                    });
                    if (!_.isEmpty(userRecords)) {
                        throw new siteFunc.UserException(res.__('validate_user_had_bind'));
                    }
                }

                if (errMsg) {
                    throw new siteFunc.UserException(errMsg);
                }

                let endStr = bindType == '2' ? fields.email : (fields.countryCode + fields.phoneNum);

                let currentCode = await getCacheValueByKey(settings.session_secret + '_sendMessage_tourist_bindAccount_' + endStr);

                if (!fields.messageCode || !validator.isNumeric((fields.messageCode).toString()) || (fields.messageCode).length != 6 || currentCode != fields.messageCode) {
                    errMsg = res.__("validate_inputCorrect", {
                        label: res.__("label_user_imageCode")
                    })
                }

                if (errMsg) {
                    throw new siteFunc.UserException(errMsg);
                }

                const userObj = {};

                if (bindType == '1') {
                    userObj.phoneNum = fields.phoneNum;
                    userObj.countryCode = fields.countryCode;
                } else {
                    userObj.email = fields.email;
                }

                await UserModel.findOneAndUpdate({
                    _id: userInfo._id
                }, {
                    $set: userObj
                });

                siteFunc.clearRedisByType(endStr, '_sendMessage_tourist_bindAccount_');

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('lc_basic_sub_btn')
                }), {}, 'bindEmailOrPhoneNum'));

            } catch (err) {
                res.send(siteFunc.renderApiErr(req, res, 500, err, 'bindEmailOrPhoneNum'));
            }
        })

    }


    async delUser(req, res, next) {
        try {
            let errMsg = '',
                targetIds = req.query.ids;
            if (!siteFunc.checkCurrentId(targetIds)) {
                errMsg = res.__("validate_error_params");
            } else {
                targetIds = targetIds.split(',');
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            for (let i = 0; i < targetIds.length; i++) {
                let regUserMsg = await MessageModel.find({
                    'author': targetIds[i]
                });
                if (!_.isEmpty(regUserMsg)) {

                    res.send(siteFunc.renderApiErr(req, res, 500, res.__("validate_del_adminUser_notice"), 'delete'));

                    return;
                }
            }
            await UserModel.remove({
                '_id': {
                    $in: targetIds
                }
            });
            res.send(siteFunc.renderApiData(req, res, 200, 'user', {}, 'delete'));

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }

    async loginAction(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                // let currentCode = '123456';
                let errMsg = '',
                    loginType = fields.loginType || '1'; // 1:手机验证码登录 2:手机号密码登录 3:邮箱密码登录

                // TODO 临时兼容没有改动的APP端
                if (fields.phoneNum && fields.password) {
                    loginType = 2;
                }

                if (fields.email && fields.password) {
                    loginType = 3;
                }

                // console.log('--loginType--', loginType)
                if (loginType != '1' && loginType != '2' && loginType != '3' && loginType != '4') {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                if (loginType == '1' || loginType == '2') {

                    if (!fields.phoneNum || !validatorUtil.checkPhoneNum((fields.phoneNum).toString())) {
                        errMsg = res.__("validate_inputCorrect", {
                            label: res.__("label_user_phoneNum")
                        })
                    }

                    if (!fields.countryCode) {
                        errMsg = res.__("validate_selectNull", {
                            label: res.__("label_user_countryCode")
                        });
                    }

                    if (loginType == '2') {
                        if (!validatorUtil.checkPwd(fields.password, 6, 12)) {
                            errMsg = res.__("validate_rangelength", {
                                min: 6,
                                max: 12,
                                label: res.__("label_user_password")
                            })
                        }
                    } else if (loginType == '1') {

                        let currentCode = await getCacheValueByKey(settings.session_secret + '_sendMessage_login_' + (fields.countryCode + fields.phoneNum));
                        if (!fields.messageCode || !validator.isNumeric((fields.messageCode).toString()) || (fields.messageCode).length != 6 || currentCode != fields.messageCode) {
                            errMsg = res.__("validate_inputCorrect", {
                                label: res.__("label_user_imageCode")
                            })
                        }
                    }

                } else if (loginType == '3') {
                    if (!validatorUtil.checkEmail(fields.email)) {
                        errMsg = res.__("validate_inputCorrect", {
                            label: res.__("label_user_email")
                        });
                    }
                    if (!validatorUtil.checkPwd(fields.password, 6, 12)) {
                        errMsg = res.__("validate_rangelength", {
                            min: 6,
                            max: 12,
                            label: res.__("label_user_password")
                        })
                    }
                } else if (loginType == '4') {
                    if (!validatorUtil.checkEmail(fields.email)) {
                        errMsg = res.__("validate_inputCorrect", {
                            label: res.__("label_user_email")
                        });
                    }
                    let currentCode = await getCacheValueByKey(settings.session_secret + '_sendMessage_login_' + fields.email);
                    if (!fields.messageCode || !validator.isNumeric((fields.messageCode).toString()) || (fields.messageCode).length != 6 || currentCode != fields.messageCode) {
                        errMsg = res.__("validate_inputCorrect", {
                            label: res.__("label_user_imageCode")
                        })
                    }
                }

                if (errMsg) {
                    throw new siteFunc.UserException(errMsg);
                }

                let queryUserObj = {
                    $or: [{
                        phoneNum: fields.phoneNum
                    }, {
                        phoneNum: '0' + fields.phoneNum
                    }],
                    countryCode: fields.countryCode
                };

                if (loginType != '3' && loginType != '4' && fields.phoneNum.indexOf('0') == '0') {
                    queryUserObj = {
                        $or: [{
                            phoneNum: fields.phoneNum
                        }, {
                            phoneNum: fields.phoneNum.substr(1)
                        }],
                        countryCode: fields.countryCode
                    };
                }

                let userObj = {};
                if (loginType == '1') {
                    _.assign(userObj, queryUserObj)
                } else if (loginType == '2') {
                    _.assign(userObj, queryUserObj, {
                        password: service.encrypt(fields.password, settings.encrypt_key)
                    })
                } else if (loginType == '3') {
                    _.assign(userObj, {
                        email: fields.email
                    }, {
                        password: service.encrypt(fields.password, settings.encrypt_key)
                    })
                } else if (loginType == '4') {
                    _.assign(userObj, {
                        email: fields.email
                    })
                    queryUserObj = {
                        email: fields.email
                    }
                }

                // 初级校验
                // console.log('---queryUserObj--', queryUserObj)
                let userCount = await UserModel.count(queryUserObj);
                // console.log('-userCount----', userCount)
                if (userCount > 0 || loginType == '2' || loginType == '3') {
                    // 校验登录用户合法性
                    let user = await UserModel.findOne(userObj, siteFunc.getAuthUserFields('login'));

                    if (_.isEmpty(user)) {
                        if (loginType == '2') {
                            throw new siteFunc.UserException(res.__('validate_login_notSuccess_1'));
                        } else {
                            throw new siteFunc.UserException(res.__('validate_login_notSuccess'));
                        }
                    }
                    if (!user.enable) {
                        throw new siteFunc.UserException(res.__("validate_user_forbiden"));
                    }

                    if (!user.loginActive) {
                        // 添加行为积分
                        await siteFunc.addUserActionHis(req, res, settings.user_action_type_login_first, {
                            targetUser: user
                        });
                        await UserModel.findOneAndUpdate({
                            _id: user._id
                        }, {
                            $set: {
                                loginActive: true
                            }
                        });
                    }

                    let renderUser = JSON.parse(JSON.stringify(user));
                    let auth_token = user._id + '$$$$' + settings.encrypt_key; // 以后可能会存储更多信息，用 $$$$ 来分隔

                    // 针对 App 端同时创建 Token
                    renderUser.token = jwt.sign({
                        userId: user._id
                    }, settings.encrypt_key, {
                        expiresIn: '30day'
                    })

                    // 将cookie存入缓存
                    res.cookie(settings.auth_cookie_name, auth_token, {
                        path: '/',
                        maxAge: 1000 * 60 * 60 * 24 * 30,
                        signed: true,
                        httpOnly: true
                    }); //cookie 有效期30天

                    // 重置验证码
                    let endStr = loginType == '3' ? fields.email : (fields.countryCode + fields.phoneNum);
                    siteFunc.clearRedisByType(endStr, '_sendMessage_login_');
                    // console.log('--111---',renderUser)
                    res.send(siteFunc.renderApiData(req, res, 200, res.__("validate_user_loginOk"), renderUser));
                } else {
                    console.log('No user,create new User');
                    // 没有该用户数据，新建该用户
                    let randomWallets = await WalletManagerModel.find({
                        userid: ''
                    }).limit(10);

                    if (randomWallets.length < 100) {
                        console.log('less wallets,begin create!')
                        siteFunc.getWallets(500);
                        console.log('create wallets success!')
                    }

                    if (_.isEmpty(randomWallets)) {
                        throw new siteFunc.UserException(res.__('user_bill_notice_randomWalletAddress'));
                    }

                    let createUserObj = {
                        group: '0',
                        creativeRight: false,
                        walletAddress: randomWallets[0].walletaddress,
                        walletAddressPassword: randomWallets[0].password,
                        loginActive: true,
                        birth: '1770-01-01',
                        enable: true
                    };

                    if (loginType == '1') {
                        createUserObj.phoneNum = fields.phoneNum;
                        createUserObj.countryCode = fields.countryCode;
                        createUserObj.userName = fields.phoneNum;
                    } else if (loginType == '4') {
                        createUserObj.email = fields.email;
                        createUserObj.userName = fields.email;
                    }

                    let createUser = new UserModel(createUserObj);
                    let currentUser = await createUser.save();

                    // 更新钱包使用状态
                    await WalletManagerModel.findOneAndUpdate({
                        _id: randomWallets[0]._id
                    }, {
                        $set: {
                            userid: currentUser._id,
                            updatetime: new Date()
                        }
                    });

                    let newUser = await UserModel.findOne({
                        _id: currentUser._id
                    }, siteFunc.getAuthUserFields('login'));
                    let renderUser = JSON.parse(JSON.stringify(newUser));

                    let auth_token = renderUser._id + '$$$$' + settings.encrypt_key; // 以后可能会存储更多信息，用 $$$$ 来分隔
                    renderUser.token = jwt.sign({
                        userId: renderUser._id
                    }, settings.encrypt_key, {
                        expiresIn: '30day'
                    })
                    res.cookie(settings.auth_cookie_name, auth_token, {
                        path: '/',
                        maxAge: 1000 * 60 * 60 * 24 * 30,
                        signed: true,
                        httpOnly: true
                    });

                    // 添加行为积分
                    await siteFunc.addUserActionHis(req, res, settings.user_action_type_login_first, {
                        targetUser: renderUser
                    });

                    // 重置验证码
                    let endStr = loginType == '3' ? fields.email : (fields.countryCode + fields.phoneNum);
                    siteFunc.clearRedisByType(endStr, '_sendMessage_login_');
                    // console.log('--222---',renderUser)
                    res.send(siteFunc.renderApiData(req, res, 200, res.__("validate_user_loginOk"), renderUser));

                }
            } catch (err) {
                res.send(siteFunc.renderApiErr(req, res, 500, err));
            }

        })
    }
    async touristLoginAction(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                let userCode = fields.userCode;

                if (!userCode) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                let renderCode = service.decryptApp(settings.encryptApp_key, settings.encryptApp_vi, userCode);

                console.log('---renderCode---', renderCode);

                if (!renderCode) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                let targetUser = await UserModel.findOne({
                    deviceId: renderCode
                });

                if (!_.isEmpty(targetUser)) {

                    console.log('get old tourist User');

                    if (!targetUser.enable) {
                        throw new siteFunc.UserException(res.__("validate_user_forbiden"));
                    }

                    let renderUser = JSON.parse(JSON.stringify(targetUser));

                    // 针对 App 端同时创建 Token
                    renderUser.token = jwt.sign({
                        userId: targetUser._id
                    }, settings.encrypt_key, {
                        expiresIn: '30day'
                    })

                    res.send(siteFunc.renderApiData(req, res, 200, res.__("validate_user_loginOk"), renderUser));

                } else {

                    console.log('create new tourist User');
                    // 没有该用户数据，新建该用户
                    let randomWallets = await WalletManagerModel.find({
                        userid: ''
                    }).limit(10);

                    if (randomWallets.length < 100) {
                        console.log('less wallets,begin create!')
                        siteFunc.getWallets(500);
                        console.log('create wallets success!')
                    }

                    if (_.isEmpty(randomWallets)) {
                        throw new siteFunc.UserException(res.__('user_bill_notice_randomWalletAddress'));
                    }

                    let createUserObj = {
                        userName: renderCode,
                        deviceId: renderCode,
                        group: '0',
                        creativeRight: false,
                        walletAddress: randomWallets[0].walletaddress,
                        walletAddressPassword: randomWallets[0].password,
                        loginActive: true,
                        birth: '1770-01-01',
                        enable: true
                    };

                    let createUser = new UserModel(createUserObj);
                    let currentUser = await createUser.save();

                    // 更新钱包使用状态
                    await WalletManagerModel.findOneAndUpdate({
                        _id: randomWallets[0]._id
                    }, {
                        $set: {
                            userid: currentUser._id,
                            updatetime: new Date()
                        }
                    });

                    let newUser = await UserModel.findOne({
                        _id: currentUser._id
                    }, siteFunc.getAuthUserFields('login'));
                    let renderUser = JSON.parse(JSON.stringify(newUser));

                    renderUser.token = jwt.sign({
                        userId: renderUser._id
                    }, settings.encrypt_key, {
                        expiresIn: '30day'
                    })

                    res.send(siteFunc.renderApiData(req, res, 200, res.__("validate_user_loginOk"), renderUser));

                }

            } catch (err) {
                res.send(siteFunc.renderApiErr(req, res, 500, err));
            }

        })
    }

    async regAction(req, res, next) {

        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                let errMsg = '';
                let uid = fields.uid;
                let regType = fields.regType || '1'; // 1:手机号注册  2:邮箱注册

                if (regType != '1' && regType != '2') {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                if (regType == '1') {
                    if (!fields.phoneNum || !validatorUtil.checkPhoneNum((fields.phoneNum).toString())) {
                        errMsg = res.__("validate_inputCorrect", {
                            label: res.__("label_user_phoneNum")
                        })
                    }

                    if (!fields.countryCode) {
                        errMsg = res.__("validate_selectNull", {
                            label: res.__("label_user_countryCode")
                        });
                    }

                } else if (regType == '2') {
                    if (!validatorUtil.checkEmail(fields.email)) {
                        errMsg = res.__("validate_inputCorrect", {
                            label: res.__("label_user_email")
                        });
                    }
                }

                let endStr = regType == '1' ? (fields.countryCode + fields.phoneNum) : fields.email;
                let currentCode = await getCacheValueByKey(settings.session_secret + '_sendMessage_reg_' + endStr);

                console.log('--currentCode---', currentCode)
                if (!validator.isNumeric((fields.messageCode).toString()) || (fields.messageCode).length != 6 || currentCode != fields.messageCode) {
                    errMsg = res.__("validate_inputCorrect", {
                        label: res.__("label_user_imageCode")
                    })
                }

                if (fields.userName && !validator.isLength(fields.userName, 2, 12)) {
                    errMsg = res.__("validate_rangelength", {
                        min: 2,
                        max: 12,
                        label: res.__("label_user_userName")
                    });
                }

                if (fields.userName && !validatorUtil.isRegularCharacter(fields.userName)) {
                    errMsg = res.__("validate_error_field", {
                        label: res.__("label_user_userName")
                    });
                }

                if (!validatorUtil.checkPwd(fields.password, 6, 12)) {
                    errMsg = res.__("validate_rangelength", {
                        min: 6,
                        max: 12,
                        label: res.__("label_user_password")
                    })
                }

                if (errMsg) {
                    throw new siteFunc.UserException(errMsg);
                }

                // 分配钱包
                let randomWallets = await WalletManagerModel.find({
                    userid: ''
                }).limit(10);

                if (randomWallets.length < 100) {
                    console.log('less wallets,begin create!')
                    siteFunc.getWallets(500);
                    console.log('create wallets success!')
                }

                if (_.isEmpty(randomWallets)) {
                    throw new siteFunc.UserException(res.__('user_bill_notice_randomWalletAddress'));
                }

                const userObj = {
                    userName: fields.userName || fields.phoneNum,
                    countryCode: fields.countryCode,
                    logo: fields.logo,
                    phoneNum: fields.phoneNum,
                    email: fields.email,
                    group: '0',
                    creativeRight: false,
                    password: service.encrypt(fields.password, settings.encrypt_key),
                    walletAddress: randomWallets[0].walletaddress,
                    walletAddressPassword: randomWallets[0].password,
                    loginActive: false,
                    enable: true
                }

                let queryUserObj = {};
                if (regType == '1') {

                    queryUserObj = {
                        $or: [{
                            phoneNum: fields.phoneNum
                        }, {
                            phoneNum: '0' + fields.phoneNum
                        }]
                    };

                    if (fields.phoneNum.indexOf('0') == '0') {
                        queryUserObj = {
                            $or: [{
                                phoneNum: fields.phoneNum
                            }, {
                                phoneNum: fields.phoneNum.substr(1)
                            }]
                        };
                    }

                } else if (regType == '2') {
                    queryUserObj = {
                        email: fields.email
                    }
                    userObj.userName = fields.email;
                }

                let user = await UserModel.find(queryUserObj);

                if (!_.isEmpty(user)) {
                    res.send(siteFunc.renderApiErr(req, res, 500, res.__("validate_hadUse_userNameOrEmail")))
                } else {
                    let newUser = new UserModel(userObj);
                    let endUser = await newUser.save();
                    req.session.user = await UserModel.findOne({
                        _id: endUser._id
                    }, siteFunc.getAuthUserFields('session'));
                    // 更新钱包使用状态
                    await WalletManagerModel.findOneAndUpdate({
                        _id: randomWallets[0]._id
                    }, {
                        $set: {
                            userid: endUser._id,
                            updatetime: new Date()
                        }
                    });

                    // 如果是被邀请进来的
                    if (uid) {
                        if (shortid.isValid(uid)) {
                            let inviteUser = await UserModel.findOne({
                                _id: uid
                            });
                            if (!_.isEmpty(inviteUser)) {
                                console.log('符合邀请条件，准备奖励')
                                // 一天内邀请数量不超过5次不添加奖励
                                let rangeTime = siteFunc.getDateStr(-1);
                                let thumbs_up_count = await BillRecordModel.count({
                                    user: uid,
                                    type: settings.user_action_type_invitation,
                                    date: {
                                        "$gte": new Date(rangeTime.startTime),
                                        "$lte": new Date(rangeTime.endTime)
                                    }
                                });
                                let inviteState = '1';
                                if (thumbs_up_count < 5) {
                                    inviteState = '1';
                                } else {
                                    inviteState = '0';
                                }
                                // 添加行为积分
                                await siteFunc.addUserActionHis(req, res, settings.user_action_type_invitation, {
                                    targetUser: {
                                        _id: uid
                                    },
                                    beInvitedUser: endUser._id,
                                    inviteState
                                });
                            }
                        }
                    }

                    let noticeConfig = siteFunc.getNoticeConfig('reg', fields.userName);
                    let notify = new NotifyModel(noticeConfig);
                    // 发系统消息管理员
                    let newNotify = await notify.save();
                    let users = await AdminUserModel.find({}, '_id');
                    if (users.length > 0) {
                        for (let i = 0; i < users.length; i++) {
                            let userNotify = new UserNotifyModel({
                                systemUser: users[i]._id,
                                notify: newNotify
                            });
                            await userNotify.save();
                        }
                    }

                    // 重置验证码
                    siteFunc.clearRedisByType(endStr, '_sendMessage_reg_');

                    let reSendData = siteFunc.renderApiData(req, res, 200, res.__("validate_user_regOk"));
                    res.send(reSendData);
                }
            } catch (err) {
                res.send(siteFunc.renderApiErr(req, res, 500, err))
            }

        })
    }

    async checkPhoneNumExist(req, res, next) {
        try {

            let phoneNum = req.query.phoneNum || '';
            let countryCode = req.query.countryCode || '';
            let errMsg = "";

            if (!phoneNum || !validatorUtil.checkPhoneNum((phoneNum).toString())) {
                errMsg = res.__("validate_inputCorrect", {
                    label: res.__("label_user_phoneNum")
                })
            }

            if (!validator.isNumeric(countryCode.toString())) {
                errMsg = res.__("validate_inputCorrect", {
                    label: res.__("label_user_countryCode")
                })
            }

            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }

            let queryUserObj = {
                $or: [{
                    phoneNum: phoneNum
                }, {
                    phoneNum: '0' + phoneNum
                }],
                countryCode: countryCode
            };

            if (phoneNum.indexOf('0') == '0') {
                queryUserObj = {
                    $or: [{
                        phoneNum: phoneNum
                    }, {
                        phoneNum: phoneNum.substr(1)
                    }],
                    countryCode: countryCode
                };
            }

            let targetUser = await UserModel.findOne(queryUserObj);
            let checkState = false;
            if (!_.isEmpty(targetUser)) {
                checkState = true;
            }

            res.send(siteFunc.renderApiData(req, res, 200, 'checkPhoneNumExist success', {
                checkState
            }, 'save'));

        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'save'));
        }
    }

    async checkHadSetLoginPassword(req, res, next) {

        try {

            let userInfo = req.session.user;
            let targetUser = await UserModel.findOne({
                _id: userInfo._id
            });
            let checkState = false;
            if (!_.isEmpty(targetUser) && targetUser.password) {
                checkState = true;
            }

            res.send(siteFunc.renderApiData(req, res, 200, 'checkHadSetLoginPassword success', {
                checkState
            }, 'save'));

        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'save'));
        }
    }


    async logOut(req, res, next) {
        req.session.destroy();
        res.clearCookie(settings.auth_cookie_name, {
            path: '/'
        });
        res.send(siteFunc.renderApiData(req, res, 200, res.__("validate_user_logoutOk")));
    }

    async sentConfirmEmail(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            let targetEmail = fields.email;
            // 获取当前发送邮件的时间
            let retrieveTime = new Date().getTime();
            if (!validator.isEmail(targetEmail)) {
                res.send(siteFunc.renderApiErr(req, res, 500, res.__("validate_error_params")))
            } else {
                try {
                    let user = await UserModel.findOne({
                        'email': targetEmail
                    });
                    if (!_.isEmpty(user) && user._id) {
                        await UserModel.findOneAndUpdate({
                            'email': targetEmail
                        }, {
                            $set: {
                                retrieve_time: retrieveTime
                            }
                        })
                        //发送通知邮件给用户
                        const systemConfigs = await SystemConfigModel.find({});
                        if (!_.isEmpty(systemConfigs)) {
                            service.sendEmail(req, res, systemConfigs[0], settings.email_findPsd, {
                                email: targetEmail,
                                userName: user.userName,
                                password: user.password
                            })
                            res.send(siteFunc.renderApiData(req, res, 200, res.__("label_resetpwd_sendEmail_success")));
                        }
                    } else {
                        res.send(siteFunc.renderApiErr(req, res, 500, res.__("label_resetpwd_noemail")))
                    }
                } catch (error) {
                    res.send(siteFunc.renderApiErr(req, res, 500, error))
                }

            }

        })
    }

    async reSetPass(req, res, next) {
        let params = req.query;
        let tokenId = params.key;
        let keyArr = service.getKeyArrByTokenId(tokenId);

        if (keyArr && validator.isEmail(keyArr[1])) {

            try {
                let defaultTemp = await ContentTemplateModel.findOne({
                    'using': true
                }).populate('items').exec();
                let noticeTempPath = settings.SYSTEMTEMPFORDER + defaultTemp.alias + '/users/userNotice.html';
                let reSetPwdTempPath = settings.SYSTEMTEMPFORDER + defaultTemp.alias + '/users/userResetPsd.html';

                let user = await UserModel.findOne({
                    'email': keyArr[1]
                });
                if (!_.isEmpty(user) && user._id) {

                    if (user.password == keyArr[0] && keyArr[2] == settings.session_secret) {
                        //  校验链接是否过期
                        let now = new Date().getTime();
                        let oneDay = 1000 * 60 * 60 * 24;
                        let localKeys = await siteFunc.getSiteLocalKeys(req.session.locale, res);
                        if (!user.retrieve_time || now - user.retrieve_time > oneDay) {
                            let renderData = {
                                infoType: "warning",
                                infoContent: res.__("label_resetpwd_link_timeout"),
                                staticforder: defaultTemp.alias,
                                lk: localKeys.renderKeys
                            }
                            res.render(noticeTempPath, renderData);
                        } else {
                            let renderData = {
                                tokenId,
                                staticforder: defaultTemp.alias,
                                lk
                            };
                            res.render(reSetPwdTempPath, renderData);
                        }
                    } else {
                        let localKeys = await siteFunc.getSiteLocalKeys(req.session.locale, res);
                        res.render(noticeTempPath, {
                            infoType: "warning",
                            infoContent: res.__("label_resetpwd_error_message"),
                            staticforder: defaultTemp.alias,
                            lk: localKeys.renderKeys
                        });
                    }
                } else {
                    res.send(siteFunc.renderApiErr(req, res, 500, res.__("label_resetpwd_noemail")))
                }
            } catch (error) {
                res.send(siteFunc.renderApiErr(req, res, 500, error))
            }
        } else {
            res.send(siteFunc.renderApiErr(req, res, 500, res.__("label_resetpwd_noemail")))
        }
    }

    // 根据手机验证码找回密码
    async resetMyPassword(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                let phoneNum = fields.phoneNum;
                let countryCode = fields.countryCode;
                let messageCode = fields.messageCode;
                let email = fields.email;
                let type = fields.type || '1';
                let errMsg = "";

                if (type != '1' && type != '2') {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }

                if (type == '1') {
                    if (!phoneNum || !validator.isNumeric(phoneNum.toString())) {
                        throw new siteFunc.UserException(res.__("validate_inputCorrect", {
                            label: res.__("label_user_phoneNum")
                        }));
                    }

                    if (!countryCode) {
                        errMsg = res.__("validate_selectNull", {
                            label: res.__("label_user_countryCode")
                        });
                    }

                } else if (type == '2') {
                    if (!validatorUtil.checkEmail(fields.email)) {
                        throw new siteFunc.UserException(res.__("validate_inputCorrect", {
                            label: res.__("label_user_email")
                        }));
                    }
                }

                let endStr = type == '1' ? (fields.countryCode + fields.phoneNum) : fields.email;
                // console.log('----', settings.session_secret + '_sendMessage_resetPassword_' + endStr)
                // let currentCode = await getCacheValueByKey(settings.session_secret + '_sendMessage_reg_' + endStr);
                let currentCode = await getCacheValueByKey(settings.session_secret + '_sendMessage_resetPassword_' + endStr);

                // console.log('-currentCode-----', currentCode)
                if (!validator.isNumeric((messageCode).toString()) || (messageCode).length != 6 || currentCode != fields.messageCode) {
                    errMsg = res.__("validate_inputCorrect", {
                        label: res.__("label_user_imageCode")
                    })
                }

                if (!fields.password) {
                    errMsg = res.__("validate_inputCorrect", {
                        label: res.__("label_user_password")
                    })
                }

                if (errMsg) {
                    throw new siteFunc.UserException(errMsg);
                }

                let queryUserObj = {
                    $or: [{
                        phoneNum: fields.phoneNum
                    }, {
                        phoneNum: '0' + fields.phoneNum
                    }],
                    countryCode: fields.countryCode
                };

                if (type == '1') {
                    if (fields.phoneNum.indexOf('0') == '0') {
                        queryUserObj = {
                            $or: [{
                                phoneNum: fields.phoneNum
                            }, {
                                phoneNum: fields.phoneNum.substr(1)
                            }],
                            countryCode: fields.countryCode
                        };
                    }
                } else if (type == '2') {
                    queryUserObj = {
                        email: fields.email
                    }
                }


                let targetUser = await UserModel.findOne(queryUserObj);

                if (!_.isEmpty(targetUser)) {

                    await UserModel.findOneAndUpdate({
                        _id: targetUser._id
                    }, {
                        $set: {
                            password: service.encrypt(fields.password, settings.encrypt_key)
                        }
                    });

                    // 重置验证码
                    siteFunc.clearRedisByType(endStr, '_sendMessage_resetPassword_');

                    res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                        label: res.__('lc_basic_set_password')
                    })));

                } else {
                    throw new siteFunc.UserException(res.__('label_resetpwd_error_message'));
                }


            } catch (error) {
                res.send(siteFunc.renderApiErr(req, res, 500, error))
            }
        })

    }


    // web 端找回密码
    async updateNewPsd(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            let errMsg = '';
            if (!fields.tokenId) {
                errMsg = 'token is null'
            }

            if (!fields.password) {
                errMsg = 'password is null'
            }

            if (fields.password != fields.confirmPassword) {
                errMsg = res.__("validate_error_pass_atypism")
            }

            if (errMsg) {
                res.send(siteFunc.renderApiErr(req, res, 500, errMsg))
            } else {
                var keyArr = service.getKeyArrByTokenId(fields.tokenId);
                if (keyArr && validator.isEmail(keyArr[1])) {
                    try {
                        let user = await UserModel.findOne({
                            'email': keyArr[1]
                        });
                        if (!_.isEmpty(user) && user._id) {
                            if (user.password == keyArr[0] && keyArr[2] == settings.session_secret) {
                                let currentPwd = service.encrypt(fields.password, settings.encrypt_key);
                                await UserModel.findOneAndUpdate({
                                    'email': keyArr[1]
                                }, {
                                    $set: {
                                        password: currentPwd,
                                        retrieve_time: ''
                                    }
                                });
                                res.send(siteFunc.renderApiData(req, res, 200, 'reset pwd success'));
                            } else {
                                res.send(siteFunc.renderApiErr(req, res, 500, res.__("validate_error_params")))
                            }
                        } else {
                            res.send(siteFunc.renderApiErr(req, res, 500, res.__("validate_error_params")))
                        }
                    } catch (error) {
                        res.send(siteFunc.renderApiErr(req, res, 500, error))
                    }
                } else {
                    res.send(siteFunc.renderApiErr(req, res, 500, res.__("validate_error_params")))
                }
            }
        })

    }

    // app 端修改密码
    async modifyMyPsd(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {

            try {
                await siteFunc.checkPostToken(req, res, fields.token);

                let errMsg = '';
                let userInfo = req.session.user || {};

                if (!fields.oldPassword) {
                    errMsg = 'oldPassword is null'
                }

                if (!fields.password) {
                    errMsg = 'password is null'
                }

                if (errMsg) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                let targetUser = await UserModel.findOne({
                    _id: userInfo._id,
                    password: service.encrypt(fields.oldPassword, settings.encrypt_key)
                });

                if (!_.isEmpty(targetUser)) {

                    await UserModel.findOneAndUpdate({
                        _id: userInfo._id
                    }, {
                        $set: {
                            password: service.encrypt(fields.password, settings.encrypt_key)
                        }
                    });

                    res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                        label: res.__('lc_basic_set_password')
                    })));

                } else {
                    throw new siteFunc.UserException(res.__('label_resetpwd_error_message'));
                }

            } catch (error) {
                res.send(siteFunc.renderApiErr(req, res, 500, error, 'modifyMyPsd'));
            }

        })

    }


    async postEmailToAdminUser(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                let errMsg = "";
                if (fields.name && !validator.isLength(fields.name, 2, 16)) {
                    errMsg = res.__("validate_rangelength", {
                        min: 2,
                        max: 16,
                        label: res.__("label_name")
                    });
                }
                if (fields.phoneNum && !validatorUtil.checkPhoneNum(fields.phoneNum)) {
                    errMsg = res.__("validate_inputCorrect", {
                        label: res.__("label_user_phoneNum")
                    });
                }
                if (!validatorUtil.checkEmail(fields.email)) {
                    errMsg = res.__("validate_inputCorrect", {
                        label: res.__("label_user_email")
                    });
                }
                if (fields.comments && !validator.isLength(fields.comments, 5, 1000)) {
                    errMsg = res.__("validate_rangelength", {
                        min: 5,
                        max: 1000,
                        label: res.__("label_comments")
                    });
                }
                if (errMsg) {
                    throw new siteFunc.UserException(errMsg);
                } else {
                    const systemConfigs = await SystemConfigModel.find({});
                    service.sendEmail(req, res, systemConfigs[0], settings.email_notice_admin_byContactUs, {
                        email: fields.email,
                        name: fields.name,
                        phoneNum: fields.phoneNum,
                        comments: xss(fields.comments)
                    })
                    // 给用户发邮件
                    service.sendEmail(req, res, systemConfigs[0], settings.email_notice_user_byContactUs, {
                        email: fields.email,
                        name: fields.name,
                        phoneNum: fields.phoneNum,
                        comments: xss(fields.comments)
                    })
                    res.send(siteFunc.renderApiData(req, res, 200, res.__("lc_sendEmail_user_success_notice")));
                }
            } catch (error) {
                res.send(siteFunc.renderApiErr(req, res, 500, error))
            }

        })
    }

    // 关注标签
    async addTags(req, res) {

        try {

            let userInfo = await UserModel.findOne({
                _id: req.session.user
            }, 'watchTags');
            let tagId = req.query.tagId;
            let followState = req.query.type;
            if (!shortid.isValid(tagId)) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }
            let targetTag = await ContentTagModel.findOne({
                _id: tagId
            });
            if (_.isEmpty(targetTag)) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }
            let oldWatchTag = userInfo.watchTags || [];
            let oldWatchTagArr = _.concat([], oldWatchTag);
            if (oldWatchTagArr.indexOf(tagId) >= 0 && followState == '1') {
                throw new siteFunc.UserException(res.__("validate_error_repost"));
            } else {
                if (followState == '1') {
                    oldWatchTagArr.push(tagId);
                } else if (followState == '0') {
                    oldWatchTagArr = _.filter(oldWatchTagArr, (item) => {
                        return item != tagId;
                    })
                } else {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }
                oldWatchTagArr = _.uniq(oldWatchTagArr);
                // console.log('--userInfo._id----', userInfo._id);
                await UserModel.findOneAndUpdate({
                    _id: userInfo._id
                }, {
                    $set: {
                        watchTags: oldWatchTagArr
                    }
                });

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('lc_add')
                }), {}, 'addTags'))
            }
        } catch (err) {
            res.send(siteFunc.renderApiErr(req, res, 500, err, 'addTags'));
        }

    }


    // 关注大师
    async followMaster(req, res) {

        try {
            // TODO 暂供测试用
            let userInfo = req.session.user;
            let userId = userInfo._id;
            let masterIds = req.query.masterId;
            let masterFollowState = req.query.followState;

            if (!masterIds) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }

            let masterIdArr = masterIds.split(',');
            let targetWatcher = await UserModel.findOne({
                _id: userId
            });
            for (const masterId of masterIdArr) {
                if (!shortid.isValid(masterId)) {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }

                if (masterId == userId) {
                    throw new siteFunc.UserException(res.__("user_action_tips_subscribe_self"));
                }
                // console.log('---masterId---', masterId);
                let targetMasterFollow = await UserModel.findOne({
                    _id: masterId
                });

                // console.log('---targetMasterFollow---', targetMasterFollow);
                if (_.isEmpty(targetMasterFollow)) {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }
                // 不是大师不可以被关注
                if (targetMasterFollow && targetMasterFollow.group != '1') {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }
                let userWatcherArr = _.concat([], targetWatcher.watchers);
                let masterFollowersArr = _.concat([], targetMasterFollow.followers);

                if (userWatcherArr.indexOf(userId) >= 0 && masterFollowState == 'in') {
                    throw new siteFunc.UserException(res.__("validate_error_repost"));
                } else {
                    if (masterFollowState == 'in') {
                        userWatcherArr.push(targetMasterFollow._id);
                        masterFollowersArr.push(userId);
                    } else if (masterFollowState == 'out') {
                        userWatcherArr = _.filter(userWatcherArr, (item) => {
                            return item != targetMasterFollow._id;
                        })
                        masterFollowersArr = _.filter(masterFollowersArr, (item) => {
                            return item != userId;
                        })
                    } else {
                        throw new siteFunc.UserException(res.__("validate_error_params"));
                    }
                    // 去重
                    userWatcherArr = _.uniq(userWatcherArr);
                    masterFollowersArr = _.uniq(masterFollowersArr);
                    // 记录本人主动关注
                    await UserModel.findOneAndUpdate({
                        _id: userId
                    }, {
                        $set: {
                            watchers: userWatcherArr
                        }
                    });
                    // 记录大师被关注
                    await UserModel.findOneAndUpdate({
                        _id: targetMasterFollow._id
                    }, {
                        $set: {
                            followers: masterFollowersArr
                        }
                    });

                    // 发送关注消息
                    if (masterFollowState == 'in') {
                        siteFunc.addSiteMessage('2', userInfo, targetMasterFollow._id);

                        await siteFunc.addActionBills(req, res, userId, settings.user_action_type_follow, masterId, '4');

                    }
                }

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__(masterFollowState === 'in' ? 'user_action_tips_add_master' : 'user_action_tips_unsubscribe_master')
                }), {}, 'followMaster'))
            }
        } catch (err) {
            res.send(siteFunc.renderApiErr(req, res, 500, err, 'followMaster'));
        }

    }

    // 关注专题
    async addSpecial(req, res) {

        try {
            // TODO 暂供测试用
            let userInfo = await UserModel.findOne({
                _id: req.session.user
            }, 'userName watchSpecials');
            let specialId = req.query.specialId;
            let specialState = req.query.specialState;
            if (!shortid.isValid(specialId)) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }

            let targetSpecial = await SpecialModel.findOne({
                _id: specialId
            });
            if (_.isEmpty(targetSpecial)) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }
            let oldSpecials = userInfo.watchSpecials || [];
            let oldSpecialsArr = _.concat([], oldSpecials);
            if (oldSpecialsArr.indexOf(specialId) >= 0 && specialState == 'in') {
                throw new siteFunc.UserException(res.__("validate_error_repost"));
            } else {
                if (specialState == 'in') {
                    oldSpecialsArr.push(specialId);
                } else if (specialState == 'out') {
                    oldSpecialsArr = _.filter(oldSpecialsArr, (item) => {
                        return item != specialId;
                    })
                } else {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }
                oldSpecialsArr = _.uniq(oldSpecialsArr);
                // console.log('--userInfo._id----', userInfo._id);
                await UserModel.findOneAndUpdate({
                    _id: userInfo._id
                }, {
                    $set: {
                        watchSpecials: oldSpecialsArr
                    }
                });

                if (specialState == 'in') {
                    await siteFunc.addActionBills(req, res, userInfo._id, settings.user_action_type_follow, specialId, '5');
                    // 推送消息
                    await siteFunc.jsPushFunc(userInfo, targetSpecial.creator, 'followSpecial', targetSpecial._id);

                }
                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('user_action_tips_add_special')
                }), {}, 'addSpecial'))
            }
        } catch (err) {
            res.send(siteFunc.renderApiErr(req, res, 500, err, 'addUsers'));
        }

    }

    // 关注社群
    async addCommunity(req, res) {

        try {
            // TODO 暂供测试用
            let userInfo = await UserModel.findOne({
                _id: req.session.user
            }, 'userName watchCommunity watchCommunityState');;
            let communityIds = req.query.communityId;
            let communityState = req.query.communityState;
            let questions = req.query.questions || [];

            if (!communityIds) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }

            let communityIdsArr = communityIds.split(',');

            for (const communityId of communityIdsArr) {
                if (!shortid.isValid(communityId)) {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }
                let targetCommunity = await CommunityModel.findOne({
                    _id: communityId,
                    state: '1'
                });
                if (_.isEmpty(targetCommunity)) {
                    throw new siteFunc.UserException(res.__("label_systemnotice_nopower"));
                }

                let oldCommunity = userInfo.watchCommunity || [];
                let oldCommunityArr = _.concat([], oldCommunity);
                let oldCommunityStateArr = _.concat([], userInfo.watchCommunityState);
                // console.log('--oldCommunityArr----', oldCommunityArr)
                // console.log('--userId----', userId)
                // 如果之前记录已经存在
                if (oldCommunityArr.indexOf(communityId) >= 0) {
                    // console.log('000000000')
                    let targetWatchState = _.filter(oldCommunityStateArr, (item) => {
                        return item.community == communityId;
                    })

                    if (targetWatchState.length > 1) {
                        throw new siteFunc.UserException(res.__("validate_error_repost"));
                    }
                    if (targetWatchState[0].state == '1' && communityState == 'in') {
                        throw new siteFunc.UserException(res.__("validate_error_repost"));
                    }
                    if (targetWatchState[0].state == '0' && communityState == 'out') {
                        throw new siteFunc.UserException(res.__("validate_error_repost"));
                    }
                    // 主要针对拒绝后重新申请
                    oldCommunityStateArr = _.filter(oldCommunityStateArr, (item) => {
                        return item.community != communityId;
                    })

                }
                if (communityState == 'in') {

                    if (targetCommunity.open != '1' && _.isEmpty(questions)) {
                        // TODO 兼容老版本
                        throw new siteFunc.UserException(res.__('label_system_needupadate'));
                    }

                    oldCommunityArr.push(communityId);
                    let pushParams = {
                        joinDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                        community: communityId,
                        state: targetCommunity.open == '1' ? '1' : '0',
                        questions: !_.isEmpty(questions) ? JSON.parse(questions) : [],
                    };
                    if (targetCommunity.open == '1') {
                        pushParams.accessDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
                    }
                    oldCommunityStateArr.push(pushParams);
                } else if (communityState == 'out') {
                    oldCommunityArr = _.filter(oldCommunity, (item) => {
                        return item != communityId;
                    })
                    oldCommunityStateArr = _.filter(oldCommunityStateArr, (item) => {
                        return item.community != communityId;
                    })
                } else {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }
                oldCommunityArr = _.uniq(oldCommunityArr);
                oldCommunityStateArr = _.uniq(oldCommunityStateArr);
                await UserModel.findOneAndUpdate({
                    _id: userInfo._id
                }, {
                    $set: {
                        watchCommunity: oldCommunityArr,
                        watchCommunityState: oldCommunityStateArr,
                    }
                });

                if (communityState == 'in') {
                    await siteFunc.addActionBills(req, res, userInfo._id, settings.user_action_type_follow, communityId, '6');
                    // 推送消息
                    await siteFunc.jsPushFunc(userInfo, targetCommunity.creator, 'followCommunity', targetCommunity._id);
                }

            }

            res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                label: res.__('user_action_tips_add_community')
            }), {}, 'addCommunity'))

        } catch (err) {
            res.send(siteFunc.renderApiErr(req, res, 500, err, 'addCommunity'));
        }

    }

    // 踢出社群、同意加入、拒绝加入
    async kickOutCommunity(req, res) {
        try {
            let userInfo = req.session.user;
            let communityId = req.query.communityId;
            let targetUserId = req.query.userId;
            let communityState = req.query.communityState;
            let questions = req.query.questions;

            if (!shortid.isValid(communityId)) {
                throw new siteFunc.UserException(res.__('validate_error_params'));
            }

            if (!shortid.isValid(targetUserId)) {
                throw new siteFunc.UserException(res.__('validate_error_params'));
            }

            // 查询社群归属
            let targetCommunity = await CommunityModel.findOne({
                creator: userInfo._id,
                _id: communityId,
                state: '1'
            });

            if (_.isEmpty(targetCommunity)) {
                throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
            }

            // 自己不能踢出自己
            if (userInfo._id == targetUserId) {
                throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
            }

            let targetUser = await UserModel.findOne({
                _id: targetUserId
            }, 'watchCommunity watchCommunityState');

            if (!_.isEmpty(targetUser)) {

                let oldCommunityArr = _.concat([], targetUser.watchCommunity);
                let oldCommunityStateArr = _.concat([], targetUser.watchCommunityState);

                if (communityState == 'out') {
                    oldCommunityArr = _.filter(oldCommunityArr, (item) => {
                        return item != communityId;
                    })
                    oldCommunityStateArr = _.filter(oldCommunityStateArr, (item) => {
                        return item.community != communityId;
                    })
                } else if (communityState == 'in') {

                    let updateCommunityStateArr = _.filter(oldCommunityStateArr, (item) => {
                        return item.community == communityId;
                    })

                    oldCommunityStateArr = _.filter(oldCommunityStateArr, (item) => {
                        return item.community != communityId;
                    })

                    if (!_.isEmpty(updateCommunityStateArr)) {

                        // 如果已经允许就不再操作了
                        if (updateCommunityStateArr[0].state == '1') {
                            throw new siteFunc.UserException(res.__("validate_error_repost"));
                        }

                        oldCommunityStateArr.push({
                            joinDate: updateCommunityStateArr[0].joinDate,
                            accessDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                            community: communityId,
                            state: '1',
                            questions: updateCommunityStateArr[0].questions,
                        });

                    } else {
                        throw new siteFunc.UserException(res.__('validate_error_params'));
                    }

                } else if (communityState == 'refuse') {
                    let updateCommunityStateArr = _.filter(oldCommunityStateArr, (item) => {
                        return item.community == communityId;
                    })

                    oldCommunityStateArr = _.filter(oldCommunityStateArr, (item) => {
                        return item.community != communityId;
                    })

                    if (!_.isEmpty(updateCommunityStateArr)) {

                        // 如果已经拒绝就不再操作了
                        if (updateCommunityStateArr[0].state == '2') {
                            throw new siteFunc.UserException(res.__("validate_error_repost"));
                        }

                        oldCommunityStateArr.push({
                            joinDate: updateCommunityStateArr[0].joinDate,
                            refuseDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                            community: communityId,
                            state: '2',
                            questions: updateCommunityStateArr[0].questions,
                        });

                    } else {
                        throw new siteFunc.UserException(res.__('validate_error_params'));
                    }
                }


                oldCommunityArr = _.uniq(oldCommunityArr);
                oldCommunityStateArr = _.uniq(oldCommunityStateArr);

                await UserModel.findOneAndUpdate({
                    _id: targetUserId
                }, {
                    $set: {
                        watchCommunity: oldCommunityArr,
                        watchCommunityState: oldCommunityStateArr,
                    }
                });

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('user_action_tips_add_community')
                }), {}, 'addCommunity'));

            } else {
                throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
            }

        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'kickOutCommunity'));
        }
    }

    // 获取社区成员列表
    async getCommunityUsers(req, res) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let type = req.query.type || '1';
            let communityId = req.query.communityId;
            let userInfo = req.session.user || {};
            let useClient = req.query.useClient;
            let sortby = req.query.sortby;
            let sortObj = {
                date: -1
            }

            let populateArr = [{
                path: 'category',
                select: 'name _id'
            }];

            if (!shortid.isValid(communityId)) {
                throw new siteFunc.UserException(res.__('validate_error_params'));
            }

            let queryObj = {
                "watchCommunityState": {
                    $elemMatch: {
                        "community": communityId,
                        "state": type
                    }
                }
            };

            let files = (useClient != '0') ? siteFunc.getAuthUserFields('community') : null;
            // console.log('--queryObj---', queryObj)
            let targetUsers = await UserModel.find(queryObj, files).populate(populateArr).exec();

            let newUserList = _.sortBy(targetUsers, (item) => {
                let targetJoinCommunityArr = _.filter(item.watchCommunityState, (communityItem) => {
                    return communityItem.community == communityId
                })
                if (!_.isEmpty(targetJoinCommunityArr)) {
                    let targetJoinCommunity = targetJoinCommunityArr[0];
                    if (!_.isEmpty(targetJoinCommunity) && targetJoinCommunity.accessDate) {
                        let time = new Date(targetJoinCommunity.accessDate).getTime()
                        console.log('符合排序条件:', time);
                        return time * -1;
                    } else {
                        let time = new Date(item.date).getTime()
                        return time * -1;
                    }
                } else {
                    let time = new Date(item.date).getTime()
                    return time * -1;
                }
            })

            let skipNum = Number(pageSize) * (Number(current) - 1);
            newUserList = newUserList.splice(skipNum, pageSize);


            let renderUsers = await renderUserList(userInfo, newUserList, useClient, {
                sortby,
                apiName: 'getCommunityUsers',
                communityId
            });
            // console.log('---renderUsers--', renderUsers)
            const totalItems = await UserModel.count(queryObj);
            let renderData = {
                docs: renderUsers,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                }
            }
            if (modules && modules.length > 0) {
                return renderUsers;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getCommunityUsers', renderUsers, 'getlist'));
                } else {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getCommunityUsers', renderData, 'getlist'));
                }
            }

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async sendVerificationCode(req, res) {

        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {

            try {
                let phoneNum = fields.phoneNum;
                let email = fields.email;
                let countryCode = fields.countryCode;
                let messageType = fields.messageType;
                let sendType = fields.sendType || '1'; // 1: 短信验证码  2:邮箱验证码

                // 针对管理员
                let userName = fields.userName;
                let password = fields.password;

                let cacheKey = '',
                    errMsg = "";

                // 管理员登录
                if (messageType == '5') {

                    if (!userName || !password) {
                        throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
                    }

                    let targetAdminUser = await AdminUserModel.findOne({
                        userName,
                        password
                    })

                    if (!_.isEmpty(targetAdminUser)) {
                        phoneNum = targetAdminUser.phoneNum;
                        countryCode = targetAdminUser.countryCode;
                    } else {
                        throw new siteFunc.UserException(res.__('label_systemnotice_nopower'));
                    }

                } else {

                    if (sendType == '1') {
                        if (!phoneNum || !validator.isNumeric(phoneNum.toString())) {
                            errMsg = res.__("validate_inputCorrect", {
                                label: res.__("label_user_phoneNum")
                            });
                        }

                        if (!fields.countryCode) {
                            errMsg = res.__("validate_selectNull", {
                                label: res.__("label_user_countryCode")
                            });
                        }
                    } else if (sendType == '2') {
                        if (!validatorUtil.checkEmail(fields.email)) {
                            errMsg = res.__("validate_inputCorrect", {
                                label: res.__("label_user_email")
                            });
                        }
                    }

                }

                if (!messageType) {
                    errMsg = res.__("validate_error_params");
                }

                if (errMsg) {
                    throw new siteFunc.UserException(errMsg);
                }

                // 生成短信验证码
                let currentStr = siteFunc.randomString(6, '123456789');

                // console.log('---messageCode----', currentStr);

                if (messageType == '0') { // 注册验证码
                    cacheKey = '_sendMessage_reg_';
                } else if (messageType == '1') { // 登录获取验证码
                    cacheKey = '_sendMessage_login_';
                } else if (messageType == '2') { // 忘记资金密码获取验证码
                    cacheKey = '_sendMessage_reSetFunPassword_';
                } else if (messageType == '3') { // 忘记登录密码找回
                    cacheKey = '_sendMessage_resetPassword_';
                } else if (messageType == '4') { // 身份认证
                    cacheKey = '_sendMessage_identity_verification_';
                } else if (messageType == '5') { // 管理员登录
                    cacheKey = '_sendMessage_adminUser_login_';
                } else if (messageType == '6') { // 游客绑定邮箱或手机号
                    cacheKey = '_sendMessage_tourist_bindAccount_';
                } else {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }

                let endStr = sendType == '1' ? (countryCode + phoneNum) : email;
                let currentKey = settings.session_secret + cacheKey + endStr;
                console.log(currentStr, '---currentKey---', currentKey)
                cache.set(currentKey, currentStr, 1000 * 60 * 10); // 验证码缓存10分钟

                // 验证码加密
                let renderCode = service.encryptApp(settings.encryptApp_key, settings.encryptApp_vi, currentStr);
                console.log('renderCode: ', renderCode);

                if (sendType == '1') {
                    // 发送短消息
                    (process.env.NODE_ENV == 'production') && siteFunc.sendTellMessagesByPhoneNum(countryCode, phoneNum, currentStr.toString());
                } else if (sendType == '2') {
                    //发送通知邮件给用户
                    const systemConfigs = await SystemConfigModel.find({});
                    if (!_.isEmpty(systemConfigs)) {
                        (process.env.NODE_ENV == 'production') && service.sendEmail(req, res, systemConfigs[0], settings.email_sendMessageCode, {
                            email: email,
                            renderCode: currentStr
                        })
                    } else {
                        throw new siteFunc.UserException(res.__('validate_error_params'));
                    }
                } else {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('user_action_tips_sendMessage')
                }), {
                    messageCode: renderCode
                }, 'save'));

            } catch (error) {
                res.send(siteFunc.renderApiErr(req, res, 500, error, 'sendVerificationCode'));

            }
        })

    }

    // 重置资金密码（忘记资金密码）
    async reSetFunPassword(req, res, next) {

        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                await siteFunc.checkPostToken(req, res, fields.token);

                // let phoneNum = fields.phoneNum;
                let messageCode = fields.messageCode;
                let user = req.session.user;
                let messageType = req.query.messageType;
                let type = fields.type || '1';
                let errMsg = "";

                if (type != '1' && type != '2') {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                if (!fields.password || fields.password != fields.confirmPassword) {
                    errMsg = res.__("validate_inputCorrect", {
                        label: res.__("label_user_password")
                    })
                }

                if (messageType != '2' && messageType != '3') {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }

                if (messageType == '2') {
                    if (!validator.isNumeric((fields.messageCode).toString()) || (fields.messageCode).length != 6) {
                        errMsg = res.__("validate_inputCorrect", {
                            label: res.__("label_user_imageCode")
                        })
                    }
                }

                if (messageType == '3' && !fields.oldPassword) {
                    errMsg = res.__("wallet_label_incorrect_oldFundPassword");
                }

                if (messageType == '2') {
                    if (type == '1') {
                        let currentMessageCode = await getCacheValueByKey(settings.session_secret + '_sendMessage_reSetFunPassword_' + (user.countryCode + user.phoneNum));
                        let currentMessageCode1 = await siteFunc.adapterZeroPhoneNumMessageCode(currentMessageCode, user, '_sendMessage_reSetFunPassword_');
                        if (messageCode != currentMessageCode && messageCode != currentMessageCode1) {
                            errMsg = res.__("validate_inputCorrect", {
                                label: res.__("label_user_imageCode")
                            })
                        }
                    } else if (type == '2') {
                        if (user.email) {
                            let currentMessageCode = await getCacheValueByKey(settings.session_secret + '_sendMessage_reSetFunPassword_' + user.email);
                            if (messageCode != currentMessageCode) {
                                errMsg = res.__("validate_inputCorrect", {
                                    label: res.__("label_user_imageCode")
                                })
                            }
                        } else {
                            throw new siteFunc.UserException(res.__('validate_error_params'));
                        }
                    }
                }

                if (errMsg) {
                    throw new siteFunc.UserException(errMsg);
                }

                // 校验是否存在该用户
                let messageType2Params = type == '1' ? {
                    phoneNum: user.phoneNum,
                    countryCode: user.countryCode
                } : {
                    email: user.email
                }
                let targetUser = await UserModel.findOne(messageType2Params);

                if (messageType == '3') {
                    targetUser = await UserModel.findOne({
                        phoneNum: user.phoneNum,
                        countryCode: user.countryCode,
                        fundPassword: service.encrypt(fields.oldPassword, settings.encrypt_key)
                    });
                }

                if (!_.isEmpty(targetUser)) {
                    // 加密保存密码
                    let currentPwd = service.encrypt(fields.password, settings.encrypt_key);

                    await UserModel.findOneAndUpdate(messageType2Params, {
                        $set: {
                            fundPassword: currentPwd
                        }
                    })

                    res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                        label: res.__('lc_basic_set_password')
                    }), {}, 'save'));
                } else {
                    throw new siteFunc.UserException(res.__("wallet_label_incorrect_oldFundPassword"));
                }

            } catch (error) {
                res.send(siteFunc.renderApiErr(req, res, 500, error, 'reSetFunPassword'));
            }

        })
    }

    // 点赞/取消赞
    async askContentThumbsUp(req, res, next) {
        try {
            let userInfo = req.session.user;
            let userId = userInfo._id;

            let contentId = req.query.contentId;
            let praiseState = req.query.praiseState;
            if (!shortid.isValid(contentId)) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }
            let targetContent = await ContentModel.findOne({
                _id: contentId,
                state: '2'
            });
            let targetMessage = await MessageModel.findOne({
                _id: contentId
            });
            let targetCommunityContent = await CommunityContentModel.findOne({
                _id: contentId
            });
            let targetCommunityMessage = await CommunityMessageModel.findOne({
                _id: contentId
            });

            let targetMediaType = '0';

            if (!_.isEmpty(targetContent)) {
                targetMediaType = '0'; // 帖子
                if (targetContent.uAuthor == userId) {
                    throw new siteFunc.UserException(res.__("user_action_tips_praise_self"));
                }
            } else if (!_.isEmpty(targetMessage)) {
                targetMediaType = '1'; // 评论
                if (targetMessage.author == userId) {
                    throw new siteFunc.UserException(res.__("user_action_tips_praise_self"));
                }
            } else if (!_.isEmpty(targetCommunityContent)) {
                targetMediaType = '2'; // 社群帖子
                if (targetCommunityContent.user == userId) {
                    throw new siteFunc.UserException(res.__("user_action_tips_praise_self"));
                }
            } else if (!_.isEmpty(targetCommunityMessage)) {
                targetMediaType = '3'; // 社群评论
                if (targetCommunityMessage.author == userId) {
                    throw new siteFunc.UserException(res.__("user_action_tips_praise_self"));
                }
            } else {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }

            userInfo = await UserModel.findOne({
                _id: userId
            });

            let oldPraise = userInfo.praiseContents || [];
            if (targetMediaType == '1') {
                oldPraise = userInfo.praiseMessages || [];
            } else if (targetMediaType == '2') {
                oldPraise = userInfo.praiseCommunityContent || [];
            } else if (targetMediaType == '3') {
                oldPraise = userInfo.praiseCommunityMessage || [];
            }

            let oldPraiseArr = _.concat([], oldPraise);
            if (oldPraiseArr.indexOf(contentId) >= 0 && praiseState == 'in') {
                throw new siteFunc.UserException(res.__("user_action_tips_repeat", {
                    label: res.__('user_action_type_give_thumbs_up')
                }));
            } else {
                if (praiseState == 'in') {
                    oldPraiseArr.push(contentId);
                } else if (praiseState == 'out') {
                    oldPraiseArr = _.filter(oldPraise, (item) => {
                        return item != contentId;
                    })
                } else {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }
                oldPraiseArr = _.uniq(oldPraiseArr);

                if (targetMediaType == '0') {
                    await UserModel.findOneAndUpdate({
                        _id: userInfo._id
                    }, {
                        $set: {
                            praiseContents: oldPraiseArr
                        }
                    });
                    // 给指定文档添加标记
                    let praiseObj = {};
                    if (praiseState == 'in') {
                        praiseObj = {
                            '$addToSet': {
                                praiseUser: userInfo._id
                            }
                        }
                    } else if (praiseState == 'out') {
                        praiseObj = {
                            '$pull': {
                                praiseUser: userInfo._id
                            }
                        }
                    }
                    if (!_.isEmpty(praiseObj)) {
                        await ContentModel.findOneAndUpdate({
                            _id: contentId
                        }, praiseObj)
                    }
                } else if (targetMediaType == '1') {
                    await UserModel.findOneAndUpdate({
                        _id: userInfo._id
                    }, {
                        $set: {
                            praiseMessages: oldPraiseArr
                        }
                    });
                } else if (targetMediaType == '2') {
                    await UserModel.findOneAndUpdate({
                        _id: userInfo._id
                    }, {
                        $set: {
                            praiseCommunityContent: oldPraiseArr
                        }
                    });
                } else if (targetMediaType == '3') {
                    await UserModel.findOneAndUpdate({
                        _id: userInfo._id
                    }, {
                        $set: {
                            praiseCommunityMessage: oldPraiseArr
                        }
                    });
                }

                if (praiseState == 'in') {
                    // 发送提醒消息
                    if (targetMediaType == '0') {
                        siteFunc.addSiteMessage('4', userInfo, targetContent.uAuthor, contentId, {
                            targetMediaType
                        });
                    } else if (targetMediaType == '1') {
                        siteFunc.addSiteMessage('4', userInfo, targetMessage.author, contentId, {
                            targetMediaType
                        });
                    } else if (targetMediaType == '2') {
                        siteFunc.addSiteMessage('4', userInfo, targetCommunityContent.user, contentId, {
                            targetMediaType
                        });
                    } else if (targetMediaType == '3') {
                        siteFunc.addSiteMessage('4', userInfo, targetCommunityMessage.user, contentId, {
                            targetMediaType
                        });
                    }

                    // 一天内点赞数量不超过5次不添加奖励
                    let rangeTime = siteFunc.getDateStr(-1);
                    // console.log('---rangeTime--', rangeTime)
                    let thumbs_up_count = await BillRecordModel.count({
                        user: userId,
                        type: settings.user_action_type_give_thumbs_up,
                        date: {
                            "$gte": new Date(rangeTime.startTime),
                            "$lte": new Date(rangeTime.endTime)
                        }
                    });
                    // console.log('---thumbs_up_count--', thumbs_up_count)
                    let configIntegral = await siteFunc.getIntegralonfig();
                    if (thumbs_up_count < configIntegral.give_thumbs_up_limit) {
                        // 添加账单和行为
                        await siteFunc.addUserActionHis(req, res, settings.user_action_type_give_thumbs_up, {
                            targetId: contentId,
                            contentType: targetMediaType
                        });
                    } else {
                        console.log('点赞超过限制，不计算奖励')
                    }

                }

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('user_action_type_give_thumbs_up')
                }), {}, 'addPraise'))
            }
        } catch (err) {
            res.send(siteFunc.renderApiErr(req, res, 500, err, 'addPraise'));
        }

    }

    // 收藏帖子
    async favoriteContent(req, res, next) {
        try {
            let userInfo = await UserModel.findOne({
                _id: req.session.user
            }, siteFunc.getAuthUserFields('session'));
            let userId = userInfo._id;
            let contentId = req.query.contentId;
            let favoriteState = req.query.favoriteState;
            if (!shortid.isValid(contentId)) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }

            let targetContent = await ContentModel.findOne({
                _id: contentId,
                state: '2'
            });
            let targetCommunityContent = await CommunityContentModel.findOne({
                _id: contentId
            });

            let targetContentType = '0';
            if (!_.isEmpty(targetContent)) {
                targetContentType = '0'; // 普通帖子
            } else if (!_.isEmpty(targetCommunityContent)) {
                targetContentType = '2'; // 社群帖子
            } else {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }

            let oldFavorite = userInfo.favorites || [];
            if (targetContentType == '2') {
                oldFavorite = userInfo.favoriteCommunityContent || [];
            }
            let oldFavoriteArr = _.concat([], oldFavorite);
            if (oldFavoriteArr.indexOf(contentId) >= 0 && favoriteState == 'in') {
                throw new siteFunc.UserException(res.__("user_action_tips_repeat", {
                    label: res.__('user_action_type_give_favorite')
                }));
            } else {
                if (favoriteState == 'in') {
                    oldFavoriteArr.push(contentId);
                } else if (favoriteState == 'out') {
                    oldFavoriteArr = _.filter(oldFavorite, (item) => {
                        return item != contentId;
                    })
                } else {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }
                oldFavoriteArr = _.uniq(oldFavoriteArr);
                if (targetContentType == '0') {
                    await UserModel.findOneAndUpdate({
                        _id: userInfo._id
                    }, {
                        $set: {
                            favorites: oldFavoriteArr
                        }
                    });
                } else if (targetContentType == '2') {
                    await UserModel.findOneAndUpdate({
                        _id: userInfo._id
                    }, {
                        $set: {
                            favoriteCommunityContent: oldFavoriteArr
                        }
                    });
                }

                if (favoriteState == 'in') {
                    // 一天内收藏数量不超过5次不添加奖励
                    let rangeTime = siteFunc.getDateStr(-1);
                    let thumbs_up_count = await BillRecordModel.count({
                        user: userId,
                        type: settings.user_action_type_collection,
                        date: {
                            "$gte": new Date(rangeTime.startTime),
                            "$lte": new Date(rangeTime.endTime)
                        }
                    });
                    let configIntegral = await siteFunc.getIntegralonfig();
                    if (thumbs_up_count < configIntegral.collections_limit) {
                        // 添加账单和行为
                        await siteFunc.addUserActionHis(req, res, settings.user_action_type_collection, {
                            targetId: contentId,
                            contentType: targetContentType
                        });
                    } else {
                        console.log('收藏超过限制，不计算奖励')
                    }
                }

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('user_action_type_give_favorite')
                }), {}, 'addFavorite'))
            }
        } catch (err) {
            res.send(siteFunc.renderApiErr(req, res, 500, err, 'addFavorite'));
        }
    }

    // 帖子踩
    async despiseContent(req, res, next) {
        try {
            let userInfo = req.session.user;
            let contentId = req.query.contentId;
            let despiseState = req.query.despiseState;
            if (!shortid.isValid(contentId)) {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }
            let targetContent = await ContentModel.findOne({
                _id: contentId,
                state: '2'
            });
            let targetMessage = await MessageModel.findOne({
                _id: contentId
            });
            let targetCommunityContent = await CommunityContentModel.findOne({
                _id: contentId
            });
            let targetCommunityMessage = await CommunityMessageModel.findOne({
                _id: contentId
            });

            let targetMediaType = '0';

            if (!_.isEmpty(targetContent)) {
                targetMediaType = '0'; // 帖子
            } else if (!_.isEmpty(targetMessage)) {
                targetMediaType = '1'; // 评论
            } else if (!_.isEmpty(targetCommunityContent)) {
                targetMediaType = '2'; // 社群帖
            } else if (!_.isEmpty(targetCommunityMessage)) {
                targetMediaType = '3'; // 社群评论
            } else {
                throw new siteFunc.UserException(res.__("validate_error_params"));
            }

            // console.log('--userInfo-1---',req.session.user);
            let oldDespise = userInfo.despises || [];
            if (targetMediaType == '1') {
                oldDespise = userInfo.despiseMessage || [];
            } else if (targetMediaType == '2') {
                oldDespise = userInfo.despiseCommunityContent || [];
            } else if (targetMediaType == '3') {
                oldDespise = userInfo.despiseCommunityMessage || [];
            }

            let oldDespiseArr = _.concat([], oldDespise);

            // console.log('--oldDespiseArr----', oldDespiseArr);

            if (oldDespiseArr.indexOf(contentId) >= 0 && despiseState == 'in') {
                throw new siteFunc.UserException(res.__("user_action_tips_repeat", {
                    label: res.__('user_action_type_give_despise')
                }));
            } else {
                if (despiseState == 'in') {
                    oldDespiseArr.push(contentId);
                } else if (despiseState == 'out') {
                    oldDespiseArr = _.filter(oldDespise, (item) => {
                        return item != contentId;
                    })
                } else {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }
                oldDespiseArr = _.uniq(oldDespiseArr);

                if (targetMediaType == '0') {
                    await UserModel.findOneAndUpdate({
                        _id: userInfo._id
                    }, {
                        $set: {
                            despises: oldDespiseArr
                        }
                    });
                    // 给指定文档添加标记
                    let disPraiseObj = {};
                    if (despiseState == 'in') {
                        disPraiseObj = {
                            '$addToSet': {
                                despiseUser: userInfo._id
                            }
                        }
                    } else if (despiseState == 'out') {
                        disPraiseObj = {
                            '$pull': {
                                despiseUser: userInfo._id
                            }
                        }
                    }
                    if (!_.isEmpty(disPraiseObj)) {
                        await ContentModel.findOneAndUpdate({
                            _id: contentId
                        }, disPraiseObj)
                    }
                } else if (targetMediaType == '1') {
                    await UserModel.findOneAndUpdate({
                        _id: userInfo._id
                    }, {
                        $set: {
                            despiseMessage: oldDespiseArr
                        }
                    });
                } else if (targetMediaType == '2') {
                    await UserModel.findOneAndUpdate({
                        _id: userInfo._id
                    }, {
                        $set: {
                            despiseCommunityContent: oldDespiseArr
                        }
                    });
                } else if (targetMediaType == '3') {
                    await UserModel.findOneAndUpdate({
                        _id: userInfo._id
                    }, {
                        $set: {
                            despiseCommunityMessage: oldDespiseArr
                        }
                    });
                }

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('user_action_type_give_despise')
                }), {}, 'addDespise'))
            }
        } catch (err) {
            res.send(siteFunc.renderApiErr(req, res, 500, err, 'addDespise'));
        }
    }


    // 帖子打赏
    async rewordContent(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                await siteFunc.checkPostToken(req, res, fields.token);

                let userInfo = req.session.user;
                // console.log('--userInfo--', userInfo)
                let userId = userInfo._id;
                let contentId = fields.contentId;
                let coins = fields.coins;
                let comments = fields.comments;

                if (!shortid.isValid(contentId)) {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }

                let targetContent = await ContentModel.findOne({
                    _id: contentId,
                    state: '2'
                }).populate([{
                    path: 'uAuthor',
                    select: 'id _id walletAddress'
                }]).exec();

                let targetCommunityContent = await CommunityContentModel.findOne({
                    _id: contentId
                }).populate([{
                    path: 'user',
                    select: 'id _id walletAddress'
                }]).exec();

                let targetMediaType = '0',
                    passiveUser = '';

                // console.log('--targetContent---', targetContent)
                // console.log('--targetCommunityContent---', targetCommunityContent)

                if (!_.isEmpty(targetContent)) {
                    targetMediaType = '0'; // 帖子
                    passiveUser = targetContent.uAuthor;
                } else if (!_.isEmpty(targetCommunityContent)) {
                    targetMediaType = '2'; // 社群帖子
                    passiveUser = targetCommunityContent.user;
                } else {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }

                if (passiveUser._id == userId) {
                    throw new siteFunc.UserException(res.__("user_action_tips_reword_self"));
                }

                if ((!validator.isNumeric(coins.toString()) && !validator.isFloat(coins.toString())) || Number(coins) <= 0) {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }

                if (fields.unit != 'MEC') {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }

                let billParams = {
                    user: userInfo._id,
                    type: settings.user_action_type_appreciate
                }

                if (targetMediaType == '0') {
                    billParams.target_content = contentId;
                } else if (targetMediaType == '2') {
                    billParams.target_communityContent = contentId;
                }

                let oldReword = await BillRecordModel.find(billParams);

                if (!_.isEmpty(oldReword)) {
                    throw new siteFunc.UserException(res.__("user_action_tips_repeat", {
                        label: res.__('user_action_type_appreciate')
                    }));
                }

                // if (!fields.password) {
                //     throw new siteFunc.UserException(res.__('lc_password_null_tips'));
                // }

                // console.log('--111--', service.encrypt(fields.password, settings.encrypt_key))
                let targetUser = await UserModel.findOne({
                    _id: userId
                });
                // console.log('--targetUser---', targetUser)
                if (_.isEmpty(targetUser)) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                } else {
                    // TODO 如果是游客，则不校验
                    if (!targetUser.deviceId) {

                        if (!fields.password) {
                            throw new siteFunc.UserException(res.__('lc_write_current_password'));
                        }

                        if (targetUser.fundPassword != service.encrypt(fields.password, settings.encrypt_key)) {
                            throw new siteFunc.UserException(res.__('lc_write_current_password'));
                        }

                    }
                }
                // console.log('--targetUser--', targetUser)
                // 获取我的账户余额
                let totalBill_num = await siteFunc.getMECLeftCoins(targetUser.walletAddress);

                // console.log('--onlineMECCoins----', totalBill_num);
                // console.log('--total_MEC_Bill_Records----', total_MEC_Bill_Records);
                // let forBid_MEC_num = total_MEC_Bill_Records.length > 0 ? total_MEC_Bill_Records[0].total_num : 0;
                // let totalBill_num = onlineMECCoins + forBid_MEC_num;

                if (totalBill_num < coins) {
                    throw new siteFunc.UserException(res.__("bill_notice_no_coins"));
                }

                // 执行转账操作
                let transferState = await siteFunc.transferMecByUser(targetUser.walletAddress, targetUser.walletAddressPassword, passiveUser.walletAddress, coins);

                if (transferState == '1') {

                    // 一天内打赏数量不超过5次不添加奖励
                    let recordObj = "";
                    let rangeTime = siteFunc.getDateStr(-1);
                    let reword_count = await BillRecordModel.count({
                        user: userId,
                        type: settings.user_action_type_appreciate,
                        date: {
                            "$gte": new Date(rangeTime.startTime),
                            "$lte": new Date(rangeTime.endTime)
                        }
                    });
                    let configIntegral = await siteFunc.getIntegralonfig();
                    if (reword_count < configIntegral.appreciate_limit) {
                        // 添加账单和行为
                        recordObj = await siteFunc.addUserActionHis(req, res, settings.user_action_type_appreciate, {
                            targetId: contentId,
                            rewordCoins: coins,
                            logs: fields.comments,
                            contentType: targetMediaType
                        });
                    }

                    siteFunc.addSiteMessage('1', userInfo, passiveUser.id, contentId, {
                        recordId: recordObj.billRecord,
                        targetMediaType
                    });

                    res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                        label: res.__('user_action_type_appreciate')
                    }), {}, 'rewordContent'))
                } else {
                    throw new siteFunc.UserException(res.__('bill_label_reword_state_failed'));
                }

            } catch (err) {
                res.send(siteFunc.renderApiErr(req, res, 500, err, 'addReword'));
            }

        })

    }

    // 充值
    async rechargeCoins(req, res, next) {

        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                await siteFunc.checkPostToken(req, res, fields.token);

                let userInfo = req.session.user;
                // console.log('----fields--', fields)
                if (fields.unit != 'MEC') {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }

                if (!validator.isNumeric((fields.coins).toString()) || Number(fields.coins) <= 0) {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }

                // 调用api充值
                let targetUser = await UserModel.findOne({
                    _id: userInfo._id
                }, 'walletAddress');
                // console.log('---targetUser--', targetUser)
                let transerInfo = await siteFunc.transferMec(targetUser.walletAddress, fields.coins);

                // console.log('-transerInfo--', transerInfo);
                if (transerInfo == '1') {
                    let billPrams = {
                        unit: fields.unit,
                        coins: fields.coins,
                        comments: fields.comments
                    }

                    await siteFunc.addUserActionHis(req, res, settings.user_bill_type_recharge, billPrams);

                    res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                        label: res.__('user_bill_type_recharge')
                    }), {}, 'options'))
                } else {
                    throw new siteFunc.UserException(res.__("bill_label_recharge_state_failed"));
                }

            } catch (error) {
                res.send(siteFunc.renderApiErr(req, res, 500, error, 'rechargeCoins'));
            }

        })
    }
    async authIapTransactionVoucher(req, res) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                await siteFunc.checkPostToken(req, res, fields.token);

                let receiptData = fields.receiptData;
                let userInfo = req.session.user || {};

                if (!receiptData) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                let askResponse = await askItunesApi(receiptData, 'buy.itunes.apple.com');
                // console.log('-askResponse----', askResponse);
                if (askResponse.status == 0) {
                    await chargeForIos(req, res, askResponse, userInfo);
                } else if (askResponse.status == 21007) {
                    askResponse = await askItunesApi(receiptData, 'sandbox.itunes.apple.com');
                    // console.log('-askResponse--1--', askResponse);
                    if (askResponse.status == 0) {
                        await chargeForIos(req, res, askResponse, userInfo);
                    } else {
                        throw new siteFunc.UserException(res.__("bill_label_recharge_state_failed"));
                    }
                }

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('user_bill_type_recharge')
                }), {}, 'options'))

            } catch (error) {
                res.send(siteFunc.renderApiErr(req, res, 500, error, 'rechargeCoins'));
            }

        })
    }
    async authAndroidTransactionVoucher(req, res) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                await siteFunc.checkPostToken(req, res, fields.token);

                let chargeId = fields.chargeId;
                let inappDataSignature = fields.inappDataSignature;
                let orderId = fields.orderId;
                let packageName = 'com.percent.master';
                let productId = fields.productId;
                let purchaseTime = fields.purchaseTime;
                let purchaseToken = fields.purchaseToken;

                let userInfo = req.session.user || {};

                if (!chargeId || !inappDataSignature ||
                    !orderId ||
                    !packageName ||
                    !productId ||
                    !purchaseTime ||
                    !purchaseToken) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                let authParams = {
                    "orderId": orderId,
                    "packageName": packageName,
                    "productId": productId,
                    "purchaseTime": Number(purchaseTime),
                    "purchaseState": 0,
                    "purchaseToken": purchaseToken,
                };

                console.log('--authParams--', authParams)
                let purchaseData = JSON.stringify(authParams);
                var verifySignature = function (publicKey, purchaseData, signature) {
                    var genPublicKey = function (key) {
                        var chunkSize, chunks, str;
                        str = key;
                        chunks = [];
                        chunkSize = 64;
                        while (str) {
                            if (str.length < chunkSize) {
                                chunks.push(str);
                                break;
                            } else {
                                chunks.push(str.substr(0, chunkSize));
                                str = str.substr(chunkSize);
                            }
                        }
                        str = chunks.join("\n");
                        str = '-----BEGIN PUBLIC KEY-----\n' + str + '\n-----END PUBLIC KEY-----';
                        return str;
                    }
                    return crypto.createVerify('RSA-SHA1').update(purchaseData).verify(genPublicKey(publicKey), signature, 'base64');
                }

                let isSuccess = verifySignature(settings.googlePublicKey, purchaseData, inappDataSignature);
                console.log("result:", isSuccess);

                // 验签成功开始充值
                if (!isSuccess) {
                    throw new siteFunc.UserException(res.__("bill_label_recharge_state_failed"));
                } else {
                    console.log('google play 订单验证成功！')
                }

                let orderChargeBill = await BillRecordModel.findOne({
                    user: userInfo._id,
                    orderId
                });

                // console.log('---11-', orderChargeBill)
                // 判断是否重复充值
                if (!_.isEmpty(orderChargeBill)) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }
                // console.log('--chargeId--', chargeId);
                let chargeInfo = await ResetAmountModel.findOne({
                    _id: chargeId
                });
                // console.log('---112-', chargeInfo)recharge_1mec
                if (_.isEmpty(chargeInfo)) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                } else {
                    let checkProductId = `recharge_${chargeInfo.money}mec`;
                    if (checkProductId != productId) {
                        throw new siteFunc.UserException(res.__('validate_error_params'));
                    }
                }
                // 调用api充值
                let targetUser = await UserModel.findOne({
                    _id: userInfo._id
                }, 'walletAddress');
                console.log('---targetUser--', targetUser)
                let transerInfo = await siteFunc.transferMec(targetUser.walletAddress, chargeInfo.money);
                console.log('-transerInfo--', transerInfo);
                if (transerInfo == '1') {
                    let billPrams = {
                        unit: "MEC",
                        coins: chargeInfo.money,
                        iapComments: JSON.stringify(authParams),
                        orderId
                    }
                    console.log('----添加充值记录--', chargeInfo.money);
                    await siteFunc.addUserActionHis(req, res, settings.user_bill_type_recharge, billPrams);
                    console.log('----完成充值--')
                } else {
                    throw new siteFunc.UserException(res.__("bill_label_recharge_state_failed"));
                }

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('user_bill_type_recharge')
                }), {}, 'options'))

            } catch (error) {
                res.send(siteFunc.renderApiErr(req, res, 500, error, 'rechargeCoins'));
            }

        })
    }

    async checkGoogleAuth(req, res, next) {
        try {
            let verify = crypto.createVerify('RSA-SHA1'); //请注意，这里要用RSA-SHA1
            let authParams = {
                "orderId": "GPA.3372-5306-4752-56040",
                "packageName": "com.percent.master",
                "productId": "recharge_1mec",
                "purchaseTime": 1556011377172,
                "purchaseState": 0,
                "purchaseToken": "dfoklanabiibeffbcliklbgd.AO-J1OxXU8tJHdNA5ghfuHvRXD3I3VoSPh3lKfMMfAIkkTfWObyupkXb-ucsy1eYgKZZjZQEYAdrppuZSEm1EYQQecXsytv1duiaPfiHEKfc6yTC2ROja29k52G0CsjXCP7Z-Za5KmCj"
            };

            // console.log('--authParams---', authParams);

            let signature = "MdO8aq0vaoSQAFHyeNZLmR02aiWYpSICsqZJ7xZv3YSXLSckfG0/4zGR32DQcNYQg/zQ9pwIa/Imxw8n/JGmzSGCKlUX5+jGCW6y4XnmuV86k9KXKRDO0HoD53OleUqFIAthN6YZBlr8ALAsfUMIttKUsOcGF14Q21dzXsTke+UcVonKASACNbfKQD6A5pMltwzmekZAzFznckYTzgVeiGUuie8Ot560Dm0WE8MG2rzue5ZPqj16aK3EqV87Ccpjw2LA2HeLKomgbDRz9zqnyuF9v3kcmWif+sMF3f4aG/zh+feXbvd2vv1cG9jLl29SyZSa/8D3bwqQ5HOIkKMDsw==";
            // console.log('--authParams--', authParams);
            let purchaseData = JSON.stringify(authParams);
            var verifySignature = function (publicKey, purchaseData, signature) {
                var genPublicKey = function (key) {
                    var chunkSize, chunks, str;
                    str = key;
                    chunks = [];
                    chunkSize = 64;
                    while (str) {
                        if (str.length < chunkSize) {
                            chunks.push(str);
                            break;
                        } else {
                            chunks.push(str.substr(0, chunkSize));
                            str = str.substr(chunkSize);
                        }
                    }
                    str = chunks.join("\n");
                    str = '-----BEGIN PUBLIC KEY-----\n' + str + '\n-----END PUBLIC KEY-----';
                    return str;
                }

                return crypto.createVerify('RSA-SHA1').update(purchaseData).verify(genPublicKey(publicKey), signature, 'base64');
            }

            let testResult = verifySignature(settings.googlePublicKey, purchaseData, signature);
            console.log("result:", testResult);
            res.send({
                status: 200,
                result: testResult
            })
        } catch (error) {
            res.send({
                status: 500
            })
        }
    }

    // VIP基础信息
    async getVipInfo(req, res, next) {
        try {

            let userInfo = req.session.user;
            let targetUser = await UserModel.findOne({
                _id: userInfo._id
            }, 'vip');
            if (!_.isEmpty(targetUser)) {

                let isVip = targetUser.vip;
                let endTime = await siteFunc.getVipTime(targetUser);

                let vipState = {
                    isVip,
                    endTime
                }

                res.send(siteFunc.renderApiData(req, res, 200, 'getVipInfo success', vipState, 'save'));

            } else {
                throw new siteFunc.UserException(res.__('validate_error_params'));
            }

        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'getVipInfo'));
        }
    }


    async appForwardAward(req, res) {
        try {

            let userInfo = req.session.user || {};
            let contentId = req.query.contentId;
            let userId = userInfo._id;
            // 一天内转发数量不超过5次不添加奖励
            let rangeTime = siteFunc.getDateStr(-1);
            let forward_count = await BillRecordModel.count({
                user: userInfo._id,
                type: settings.user_action_type_forward,
                date: {
                    "$gte": new Date(rangeTime.startTime),
                    "$lte": new Date(rangeTime.endTime)
                }
            });

            let configIntegral = await siteFunc.getIntegralonfig();
            if (forward_count < configIntegral.forward_limit) {

                if (!shortid.isValid(contentId)) {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }
                let targetContent = await ContentModel.findOne({
                    _id: contentId,
                    state: '2'
                });
                let targetCommunityContent = await CommunityContentModel.findOne({
                    _id: contentId
                });

                let targetMediaType = '0';

                if (!_.isEmpty(targetContent)) {
                    targetMediaType = '0'; // 帖子
                    if (targetContent.uAuthor == userId) {
                        throw new siteFunc.UserException(res.__("user_action_tips_praise_self"));
                    }
                } else if (!_.isEmpty(targetCommunityContent)) {
                    targetMediaType = '2'; // 社群帖子
                    if (targetCommunityContent.user == userId) {
                        throw new siteFunc.UserException(res.__("user_action_tips_praise_self"));
                    }
                } else {
                    throw new siteFunc.UserException(res.__("validate_error_params"));
                }


                await siteFunc.addUserActionHis(req, res, settings.user_action_type_forward, {
                    targetId: contentId,
                    contentType: targetMediaType
                });
            } else {
                console.log('进入转发奖励超出限制，不再发放')
            }

            res.send(siteFunc.renderApiData(req, res, 200, 'appForwardAward', {}));


        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'appForwardAward'));
        }
    }

    async appBirthAward(req, res) {
        try {

            let userInfo = req.session.user || {};

            if (!userInfo.birth) {
                res.__('validate_user_addInfo')
            }

            // 一年内只发放一次
            let rangeTime = siteFunc.getDateStr(-365);
            let birth_record = await BillRecordModel.findOne({
                user: userInfo._id,
                type: settings.user_bill_type_brithday,
                coins: 0,
                date: {
                    "$gte": new Date(rangeTime.startTime),
                    "$lte": new Date(rangeTime.endTime)
                }
            });

            if (!_.isEmpty(birth_record)) {

                var gateDays = new Date().getTime() - new Date(userInfo.birth).getTime();
                var gapTime = parseInt(gateDays / (1000 * 60 * 60 * 24));
                if (gapTime > 30) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                let configIntegral = await siteFunc.getIntegralonfig();
                await BillRecordModel.findOneAndUpdate({
                    _id: birth_record._id
                }, {
                    $set: {
                        coins: configIntegral.brithday
                    }
                })
            } else {
                throw new siteFunc.UserException(res.__('validate_error_params'));
            }

            res.send(siteFunc.renderApiData(req, res, 200, 'appBirthAward', {}));


        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'appForwardAward'));
        }
    }

    async makeWallets(req, res) {
        try {
            siteFunc.getWallets(req.query.num);
            res.send(siteFunc.renderApiData(req, res, 200, 'makeWallets', {}));

        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'makeWallets'));
        }
    }

    // 关注社群
    async addCommunityTest(req, res) {

        try {

            let targetCommunities = await CommunityModel.find({
                open: '1',
                needAuth: '0'
            });
            let communityArr = [];
            for (const communityItem of targetCommunities) {
                let communityId = communityItem._id;
                communityArr.push(communityId);
            }

            let targetUsers = await UserModel.find({
                "watchCommunity": {
                    $in: communityArr
                }
            });

            for (const userItem of targetUsers) {
                let communityStateArr = userItem.watchCommunityState;
                for (const stateItem of communityStateArr) {
                    if (communityArr.indexOf(stateItem.community) >= 0) {
                        console.log('------------符合条件-----');
                        stateItem.accessDate = stateItem.joinDate;
                        stateItem.state = '1'
                    }
                }
                await UserModel.findOneAndUpdate({
                    _id: userItem._id
                }, {
                    $set: {
                        watchCommunityState: communityStateArr
                    }
                })
            }

            res.send(siteFunc.renderApiData(req, res, 200, 'getVipInfo success', '', 'save'));


        } catch (err) {
            res.send(siteFunc.renderApiErr(req, res, 500, err, 'addCommunity'));
        }

    }


    async renderJoinTime(req, res) {
        try {
            let targetUsers = await UserModel.find({
                watchCommunity: {
                    $ne: []
                }
            });

            for (const userItem of targetUsers) {
                let stateArr = userItem.watchCommunityState;
                if (!_.isEmpty(stateArr)) {
                    for (const stateItem of stateArr) {
                        let joinDate = stateItem.joinDate;
                        let accessDate = stateItem.accessDate;
                        let refuseDate = stateItem.refuseDate;
                        // console.log('--joinDate--', joinDate);
                        // console.log('--joinDate-1-', moment(joinDate).utcOffset(8).format('YYYY-MM-DD HH:mm:ss'));
                        // console.log('---stateItem0-', stateItem)
                        if (joinDate) {
                            stateItem.joinDate = moment(joinDate).utcOffset(8).format('YYYY-MM-DD HH:mm:ss');
                        }
                        if (accessDate) {
                            stateItem.accessDate = moment(accessDate).utcOffset(8).format('YYYY-MM-DD HH:mm:ss');
                        }
                        if (refuseDate) {
                            stateItem.refuseDate = moment(refuseDate).utcOffset(8).format('YYYY-MM-DD HH:mm:ss');
                        }

                        // console.log('---stateItem-', stateItem)
                    }
                    // console.log('---userItem-', userItem)
                    await UserModel.findOneAndUpdate({
                        _id: userItem._id
                    }, {
                        $set: userItem
                    });
                }
            }

            res.send(siteFunc.renderApiData(req, res, 200, 'getVipInfo success', '', 'save'));
        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error))
        }
    }

    // 处理数据，针对获取被点赞/踩数目统计
    async renderContentLikeOrDisLikeNum() {
        try {

            console.log('ready task renderContentLikeOrDisLikeNum');
            if (!_.isEmpty(global.renderLikeTask)) {
                global.renderLikeTask.cancel();
            }
            let taskRule = '0 32 23 * * *';
            global.renderLikeTask = schedule.scheduleJob(taskRule, async function () {

                let allUsers = await UserModel.find({}, 'praiseContents despises _id').populate('praiseContents despises').exec();
                for (const userItem of allUsers) {
                    let {
                        praiseContents,
                        despises
                    } = userItem;

                    for (const contentItem of praiseContents) {
                        console.log('update praiseUser: ', userItem._id);
                        await ContentModel.findOneAndUpdate({
                            _id: contentItem._id
                        }, {
                            '$addToSet': {
                                praiseUser: userItem._id
                            }
                        })

                    }

                    for (const contentItem of despises) {
                        console.log('update despiseUser: ', userItem._id);

                        await ContentModel.findOneAndUpdate({
                            _id: contentItem._id
                        }, {
                            '$addToSet': {
                                despiseUser: userItem._id
                            }
                        })

                    }
                }
                console.log('update contents success: ', allUsers.length);

            });


        } catch (error) {
            console.log('--error--', error);
        }

    }
}

module.exports = new User();