const BaseComponent = require('../prototype/baseComponent');
const ReportRecordModel = require("../models").ReportRecord;
const UserActionHisModel = require("../models").UserActionHis;
const ContentModel = require("../models").Content;
const MessageModel = require("../models").Message;
const CommunityMessageModel = require("../models").CommunityMessage;
const CommunityContentModel = require("../models").CommunityContent;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')
const settings = require('../../../configs/settings');


function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    if (!siteFunc.checkCurrentId(fields.contentId)) {
        errMsg = res.__("validate_error_params");
    }
    if (fields.comments && !validator.isLength(fields.comments, 2, 30)) {
        errMsg = res.__("validate_rangelength", {
            min: 2,
            max: 30,
            label: res.__("label_comments")
        });
    }
    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class ReportRecord {
    constructor() {
        // super()
    }
    async getReportRecords(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {};
            let useClient = req.query.useClient;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = {
                    $regex: reKey
                }
            }

            const reportRecords = await ReportRecordModel.find(queryObj).sort({
                time: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'user',
                select: 'name userName _id'
            }, {
                path: 'approver',
                select: 'name userName _id'
            }, {
                path: 'target_content',
                select: 'title _id'
            }, {
                path: 'target_message',
                select: 'content _id'
            }, {
                path: 'target_communityContent',
                select: 'comments _id'
            }]).exec();
            const totalItems = await ReportRecordModel.count(queryObj);

            let reportRecordData = {
                docs: reportRecords,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderReportRecordData = siteFunc.renderApiData(req, res, 200, 'ReportRecord', reportRecordData);
            if (modules && modules.length > 0) {
                return renderReportRecordData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getReportRecords', reportRecords));
                } else {
                    res.send(renderReportRecordData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async addReportRecord(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                await siteFunc.checkPostToken(req, res, fields.token);

                checkFormData(req, res, fields);

                let tagObj = {
                    user: req.session.user._id,
                }

                if (fields.type == '0') {
                    tagObj.target_content = fields.contentId;
                } else if (fields.type == '1') {
                    tagObj.target_message = fields.contentId;
                } else if (fields.type == '2') {
                    tagObj.target_communityContent = fields.contentId;
                } else if (fields.type == '3') {
                    tagObj.target_communityMessage = fields.contentId;
                }

                let oldReport = await ReportRecordModel.findOne(tagObj);
                if (!_.isEmpty(oldReport)) {
                    throw new siteFunc.UserException(res.__("user_action_tips_repeat", {
                        label: res.__('user_action_type_report')
                    }));
                }

                tagObj.comments = fields.comments;
                tagObj.type = fields.type;
                const newReportRecord = new ReportRecordModel(tagObj);

                await newReportRecord.save();

                res.send(siteFunc.renderApiData(req, res, 200, res.__('restful_api_response_success', {
                    label: res.__('user_action_type_report')
                }), {
                    id: newReportRecord._id
                }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    // 审核举报
    async updateReportRecord(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                // checkFormData(req, res, fields);
                const userObj = {
                    updatetime: new Date(),
                    state: fields.state ? '1' : '0',
                    approver: req.session.adminUserInfo._id
                }

                const item_id = fields._id;

                let targetRecord = await ReportRecordModel.findOneAndUpdate({
                    _id: item_id
                }, {
                    $set: userObj
                });

                let targetContentType = '',
                    contentId = '';
                if (!_.isEmpty(targetRecord)) {

                    if (targetRecord.target_content) {
                        targetContentType = '0';
                        contentId = targetRecord.target_content;
                        await ContentModel.updateOne({
                            _id: contentId
                        }, {
                            $set: {
                                state: '3'
                            }
                        });
                    } else if (targetRecord.target_message) {
                        targetContentType = '1';
                        contentId = targetRecord.target_message;
                        await MessageModel.remove({
                            _id: contentId
                        });
                    } else if (targetRecord.target_communityContent) {
                        targetContentType = '2';
                        contentId = targetRecord.target_communityContent;
                        // await CommunityContent.remove({
                        //     _id: contentId
                        // });
                        await CommunityContent.updateOne({
                            _id: contentId
                        }, {
                            state: '0'
                        });
                    } else if (targetRecord.target_communityMessage) {
                        targetContentType = '3';
                        contentId = targetRecord.target_communityMessage;
                        await CommunityMessageModel.remove({
                            _id: contentId
                        });
                    } else {
                        throw new siteFunc.UserException(res.__('validate_error_params'));
                    }

                    await siteFunc.addUserActionHis(req, res, settings.user_action_type_report, {
                        targetId: contentId,
                        contentType: targetContentType,
                        targetUser: {
                            _id: targetRecord.user
                        }
                    });

                } else {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }


                res.send(siteFunc.renderApiData(req, res, 200, 'ReportRecord', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delReportRecord(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await ReportRecordModel.remove({
                _id: req.query.ids
            });
            res.send(siteFunc.renderApiData(req, res, 200, 'ReportRecord', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new ReportRecord();