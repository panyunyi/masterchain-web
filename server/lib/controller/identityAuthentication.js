const BaseComponent = require('../prototype/baseComponent');
const IdentityAuthenticationModel = require("../models").IdentityAuthentication;
const UserModel = require("../models").User;
const formidable = require('formidable');
const {
    service,
    validatorUtil,
    siteFunc
} = require('../../../utils');
const shortid = require('shortid');
const validator = require('validator')
const _ = require('lodash')
const settings = require('../../../configs/settings');
const xss = require("xss");


function checkFormData(req, res, fields) {
    let errMsg = '';
    if (fields._id && !siteFunc.checkCurrentId(fields._id)) {
        errMsg = res.__("validate_error_params");
    }
    if (!validator.isLength(fields.name, 1, 12)) {
        errMsg = res.__("validate_rangelength", {
            min: 1,
            max: 12,
            label: res.__("label_tag_name")
        });
    }
    if (!fields.cardType) {
        errMsg = res.__("validate_inputCorrect", {
            label: res.__("validate_identifyAuth_cardType")
        });
    }
    if (!fields.areaType) {
        // errMsg = res.__("validate_inputCorrect", {
        //     label: res.__("validate_identifyAuth_areaType")
        // });
    }
    if (!fields.idCardNo) {
        errMsg = res.__("validate_inputCorrect", {
            label: res.__("validate_identifyAuth_idCardNo")
        });
    }
    if (!fields.cardFront) {
        errMsg = res.__("validate_inputCorrect", {
            label: res.__("validate_identifyAuth_cardFront")
        });
    }
    if (!fields.cardBack) {
        errMsg = res.__("validate_inputCorrect", {
            label: res.__("validate_identifyAuth_cardBack")
        });
    }

    if (errMsg) {
        throw new siteFunc.UserException(errMsg);
    }
}

class IdentityAuthentication {
    constructor() {
        // super()
    }
    async getIdentityAuthentications(req, res, next) {
        try {
            let modules = req.query.modules;
            let current = req.query.current || 1;
            let pageSize = req.query.pageSize || 10;
            let model = req.query.model; // 查询模式 full/simple
            let searchkey = req.query.searchkey,
                queryObj = {};
            let validateType = req.query.type;
            let useClient = req.query.useClient;

            if (model === 'full') {
                pageSize = '1000'
            }

            if (searchkey) {
                let reKey = new RegExp(searchkey, 'i')
                queryObj.name = {
                    $regex: reKey
                }
            }

            if (validateType) {
                queryObj.type = validateType;
            }

            const identityAuthentications = await IdentityAuthenticationModel.find(queryObj).sort({
                date: -1
            }).skip(Number(pageSize) * (Number(current) - 1)).limit(Number(pageSize)).populate([{
                path: 'user',
                select: 'name userName _id'
            }]).exec();
            const totalItems = await IdentityAuthenticationModel.count(queryObj);

            let identityAuthenticationData = {
                docs: identityAuthentications,
                pageInfo: {
                    totalItems,
                    current: Number(current) || 1,
                    pageSize: Number(pageSize) || 10,
                    searchkey: searchkey || ''
                }
            };
            let renderIdentityAuthenticationData = siteFunc.renderApiData(req, res, 200, 'IdentityAuthentication', identityAuthenticationData);
            if (modules && modules.length > 0) {
                return renderIdentityAuthenticationData.data;
            } else {
                if (useClient == '2') {
                    res.send(siteFunc.renderApiData(req, res, 200, 'getIdentityAuthentications', identityAuthentications));
                } else {
                    res.send(renderIdentityAuthenticationData);
                }
            }
        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'getlist'))

        }
    }

    async addIdentityAuthentication(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                console.log('--fields--', fields)
                let useClient = req.query.useClient;
                await siteFunc.checkPostToken(req, res, fields.token);

                checkFormData(req, res, fields);

                let type = fields.type || '1';
                // 验证类型，身份认证或大师认证
                let validateType = fields.validateType || '1';

                if (type != '1' && type != '2') {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                let userInfo = req.session.user,
                    errMsg = "";

                // TODO 手机端认证才校验验证码
                if (useClient == '2') {

                    if (type == '1') {
                        let currentCode = await siteFunc.getCacheValueByKey(settings.session_secret + '_sendMessage_identity_verification_' + ((userInfo.countryCode).toString() + (userInfo.phoneNum).toString()));
                        let currentCode1 = await siteFunc.adapterZeroPhoneNumMessageCode(currentCode, userInfo, '_sendMessage_identity_verification_');
                        if (!validator.isNumeric((fields.messageCode).toString()) || (fields.messageCode).length != 6 || (currentCode != fields.messageCode && currentCode1 != fields.messageCode)) {
                            errMsg = res.__("validate_inputCorrect", {
                                label: res.__("label_user_imageCode")
                            })
                        }
                    } else {
                        let currentCode = await siteFunc.getCacheValueByKey(settings.session_secret + '_sendMessage_identity_verification_' + userInfo.email);
                        if (!validator.isNumeric((fields.messageCode).toString()) || (fields.messageCode).length != 6 || (currentCode != fields.messageCode)) {
                            errMsg = res.__("validate_inputCorrect", {
                                label: res.__("label_user_imageCode")
                            })
                        }
                    }

                }

                if (errMsg) {
                    throw new siteFunc.UserException(errMsg);
                }

                const tagObj = {
                    name: fields.name,
                    cardType: fields.cardType,
                    areaType: fields.areaType,
                    idCardNo: fields.idCardNo,
                    cardFront: fields.cardFront,
                    cardBack: fields.cardBack,
                    user: req.session.user._id,
                    state: "0",
                    type: fields.validateType || '1',
                    authStep: '1'
                }

                let oldIdentityInfo = await IdentityAuthenticationModel.findOne({
                    user: userInfo.id,
                    type: validateType
                });

                // 新增或修改
                if (_.isEmpty(oldIdentityInfo)) {
                    const newIdentityAuthentication = new IdentityAuthenticationModel(tagObj);
                    await newIdentityAuthentication.save();
                } else {
                    if (oldIdentityInfo.state == '0' || oldIdentityInfo.state == '2') {
                        await IdentityAuthenticationModel.findOneAndUpdate({
                            user: userInfo._id,
                            type: validateType
                        }, {
                            $set: tagObj
                        });
                    }
                }


                // 重置验证码
                if (useClient == '2') {
                    let endStr = type == '2' ? userInfo.email : (userInfo.countryCode + userInfo.phoneNum);
                    siteFunc.clearRedisByType(endStr, '_sendMessage_identity_verification_');
                }

                res.send(siteFunc.renderApiData(req, res, 200, res.__("restful_api_response_success", {
                    label: res.__("user_action_type_creat_identity")
                }), {
                    state: res.__('bill_label_approval_state_wait')
                }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    // web端大师认证第二步
    async identityMasterInfo(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {

                console.log('--fields--', fields)
                let useClient = req.query.useClient;
                await siteFunc.checkPostToken(req, res, fields.token);

                let userInfo = req.session.user,
                    errMsg = "";

                let oldRecrod = await IdentityAuthenticationModel.findOne({
                    user: userInfo._id,
                    type: '2'
                });

                if (_.isEmpty(oldRecrod)) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                } else {
                    if (oldRecrod.state == '1') {
                        throw new siteFunc.UserException(res.__("user_action_tips_repeat", {
                            label: res.__('label_class_submit_audit')
                        }));
                    }
                }

                if (!fields.masterCategory) {
                    throw new siteFunc.UserException(res.__('lc_please_select_documentCate'));
                }
                // 证书必须选择一种
                if (!fields.diploma_label && !fields.professionalCertificate_label && !fields.otherCertificate_label) {
                    throw new siteFunc.UserException(res.__('validate_identyfy_require'));
                }

                // 如果填了证书备注，必须上传证书
                if (fields.diploma_label && fields.diploma.length == 0 ||
                    fields.professionalCertificate_label && fields.professionalCertificate.length == 0 ||
                    fields.otherCertificate_label && fields.otherCertificate.length == 0) {
                    throw new siteFunc.UserException(res.__('validate_identyfy_nocertificate'));
                }

                // 其余四项均为选填
                if (!_.isEmpty(fields.pastExperience) && typeof fields.pastExperience != 'object') {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                if (!_.isEmpty(fields.pastArticles) && typeof fields.pastArticles != 'object') {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                if (!_.isEmpty(fields.publishedBooks) && typeof fields.publishedBooks != 'object') {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                if (!_.isEmpty(fields.uploadAttachments) && typeof fields.uploadAttachments != 'object') {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                if (errMsg) {
                    throw new siteFunc.UserException(errMsg);
                }

                const tagObj = {
                    masterCategory: fields.masterCategory,
                    diploma_label: xss(fields.diploma_label),
                    diploma: fields.diploma,
                    professionalCertificate_label: xss(fields.professionalCertificate_label),
                    professionalCertificate: fields.professionalCertificate,
                    otherCertificate_label: xss(fields.otherCertificate_label),
                    otherCertificate: fields.otherCertificate,
                    pastExperience: fields.pastExperience,
                    pastArticles: fields.pastArticles,
                    publishedBooks: fields.publishedBooks,
                    uploadAttachments: fields.uploadAttachments,
                    authStep: '2'
                }

                await IdentityAuthenticationModel.findOneAndUpdate({
                    _id: oldRecrod._id
                }, {
                    $set: tagObj
                });

                res.send(siteFunc.renderApiData(req, res, 200, res.__("restful_api_response_success", {
                    label: res.__("user_action_type_creat_identity")
                }), {
                    state: res.__('bill_label_approval_state_wait')
                }, 'save'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'save'));
            }
        })
    }

    async getIdentityAuthenticationInfo(req, res, next) {
        try {

            let userInfo = req.session.user;
            let type = req.query.type || '1';
            let modules = req.query.modules;
            let targetUser = {};

            let wantUserInfo = await IdentityAuthenticationModel.findOne({
                user: userInfo._id,
                type
            });

            if (!_.isEmpty(wantUserInfo)) {
                targetUser = wantUserInfo;
            } else {
                targetUser = {
                    state: '-1' // 未提交审核
                }
            }

            if (modules && modules.length > 0) {
                return targetUser.state;
            } else {
                res.send(siteFunc.renderApiData(req, res, 200, 'get data success', targetUser, 'getIdentityAuthenticationInfo'));
            }

        } catch (error) {
            res.send(siteFunc.renderApiErr(req, res, 500, error, 'getIdentityAuthenticationInfo'));
        }
    }

    async updateIdentityAuthentication(req, res, next) {
        const form = new formidable.IncomingForm();
        form.parse(req, async (err, fields, files) => {
            try {
                checkFormData(req, res, fields);
                let authType = fields.authType || '1';
                let authState = fields.state;
                let dismissReason = fields.dismissReason;
                if (authType != '1' && authType != '2') {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                if (authState != '0' && authState != '1' && authState != '2') {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }

                const userObj = {
                    approver: req.session.adminUserInfo._id,
                    state: authState,
                    dismissReason: dismissReason,
                    comments: fields.comments,
                    updatetime: new Date()
                }
                const item_id = fields._id;

                let targetAuthObj = await IdentityAuthenticationModel.findOneAndUpdate({
                    _id: item_id,
                    type: authType
                }, {
                    $set: userObj
                });
                // 更新用户认证状态

                if (authState == '1') {

                    if (authType == '1') {
                        await UserModel.findOneAndUpdate({
                            _id: targetAuthObj.user
                        }, {
                            $set: {
                                authState: true
                            }
                        });

                        // 添加行为积分
                        await siteFunc.addUserActionHis(req, res, settings.user_action_type_creat_identity, {
                            targetUser: {
                                _id: targetAuthObj.user
                            }
                        });

                    } else {
                        // 将用户类型改变为大师
                        await UserModel.findOneAndUpdate({
                            _id: targetAuthObj.user
                        }, {
                            $set: {
                                group: '1'
                            }
                        });
                    }

                }



                res.send(siteFunc.renderApiData(req, res, 200, 'IdentityAuthentication', {}, 'update'))

            } catch (err) {

                res.send(siteFunc.renderApiErr(req, res, 500, err, 'update'));
            }
        })

    }

    async delIdentityAuthentication(req, res, next) {
        try {
            let errMsg = '';
            if (!siteFunc.checkCurrentId(req.query.ids)) {
                errMsg = res.__("validate_error_params");
            }
            if (errMsg) {
                throw new siteFunc.UserException(errMsg);
            }
            await IdentityAuthenticationModel.remove({
                _id: req.query.ids
            });
            res.send(siteFunc.renderApiData(req, res, 200, 'IdentityAuthentication', {}, 'delete'))

        } catch (err) {

            res.send(siteFunc.renderApiErr(req, res, 500, err, 'delete'));
        }
    }


}

module.exports = new IdentityAuthentication();