const shell = require('shelljs');
const fs = require('fs');


function modifyFileByPath(targetPath, startStr, endStr) {
    var readText = fs.readFileSync(targetPath, 'utf-8');
    var star = readText.indexOf(startStr);
    var end_ = readText.indexOf(endStr) + endStr.length;
    let startContent = readText.substr(0, star);
    let endContent = readText.substr(end_);
    newRenderContent = startContent + endContent;
    fs.writeFileSync(targetPath, newRenderContent);
}

function modifyFileByReplace(targetPath, replaceStr, targetStr) {
    var readText = fs.readFileSync(targetPath, 'utf-8');
    var reg = new RegExp(replaceStr, "g")
    var newRenderContent = readText.replace(reg, targetStr);
    fs.writeFileSync(targetPath, newRenderContent);
}

let rootPath = process.cwd();
let webPath = rootPath.replace('masterChainApp', '') + "/masterWeb/master_web";
let apiPath = rootPath.replace('masterChainApp', '') + "/masterApi/master_api";
let apiShopPath = rootPath.replace('masterChainApp', '') + "/masterShop/master_shop";
let mongolink = `mongodb://masterChain2048:IuM~AIcBi~BsCiJ~oYu@dds-3ns56ae8e3c19f641506-pub.mongodb.rds.aliyuncs.com:3717,dds-3ns56ae8e3c19f642909-pub.mongodb.rds.aliyuncs.com:3717/masterchainweb?replicaSet=mgset-12664683`


function setDbInfo(targetPath) {
    // 修改MongoDB连接数据
    // modifyFileByPath(targetPath + '/server/lib/models/index.js', "// 云数据库连接开始", '// 云数据库连接结束');
    // modifyFileByReplace(targetPath + '/server/lib/models/index.js', '// 云数据库连接脚本', mongolink);
    // 修改配置信息
    modifyFileByReplace(targetPath + '/configs/settings.js', "redis_host", '// redis_host');
    modifyFileByReplace(targetPath + '/configs/settings.js', "// redis Host配置", 'redis_host: "r-3nse96c566f676a4.redis.rds.aliyuncs.com",');

    modifyFileByReplace(targetPath + '/configs/settings.js', "mongo_connection_uri", '// mongo_connection_uri');
    modifyFileByReplace(targetPath + '/configs/settings.js', "// 数据库配置", 'mongo_connection_uri: "' + mongolink + '",');


    modifyFileByReplace(targetPath + '/configs/settings.js', 'HuiPing520@Xiaoshen@520', 'Aa@12345');
    modifyFileByReplace(targetPath + '/configs/settings.js', 'HuiPing520@Xiaoshen@520', 'Aa@12345');
    modifyFileByReplace(targetPath + '/configs/settings.js', '172.31.88.223', '172.31.185.240');

}


let distPath = rootPath + '/dist/';
let buildPath = rootPath + '/build/';
let configsPath = rootPath + '/configs/';
let localesPath = rootPath + '/locales/';
let publicPath = rootPath + '/public/';
let serverPath = rootPath + '/server/';
let srcPath = rootPath + '/src/';
let utilsPath = rootPath + '/utils/';
let viewsPath = rootPath + '/views/';
// shell.cp('-R', distPath, webPath + '/dist/');
let packageFile = rootPath + '/package.json';
let serverFile = rootPath + '/server.js';
let faviconFile = rootPath + '/favicon.ico';

// -------------------------web工程开始
shell.rm('-rf', webPath + '/*');
// 拷贝工程
shell.cp('-R', [distPath, buildPath, configsPath, localesPath, publicPath, serverPath, srcPath, utilsPath, viewsPath], webPath);
shell.cp(packageFile, webPath);
shell.cp(serverFile, webPath);
shell.cp(faviconFile, webPath);

// 修改指定文件
// modifyFileByPath(webPath + '/server.js', '// api路由开始', '// api路由结束');
// modifyFileByPath(webPath + '/server.js', "// api入口开始", '// api入口结束');
modifyFileByPath(webPath + '/server.js', '// shopApi入口开始', '// shopApi入口结束');
modifyFileByReplace(webPath + '/configs/settings.js', '8884', '8881');

setDbInfo(webPath);

shell.rm('-rf', webPath + '/public/apidoc/');
shell.rm('-rf', webPath + '/public/shopApidoc/');
// modifyFileByReplace(webPath + '/public/themes/dorawhite/js/dora.front.js', 'http://47.52.231.97:8884', 'https://api.masterchain.media');


// -------------------------api工程开始
shell.rm('-rf', apiPath + '/*');
// 拷贝工程
shell.cp('-R', [distPath, buildPath, configsPath, localesPath, publicPath, serverPath, srcPath, utilsPath, viewsPath], apiPath);
shell.cp(packageFile, apiPath);
shell.cp(serverFile, apiPath);
shell.cp(faviconFile, apiPath);

// 修改指定文件
modifyFileByPath(apiPath + '/server.js', '// shopApi入口开始', '// shopApi入口结束');
modifyFileByPath(apiPath + '/server.js', '// 入口路由开始', '// 入口路由结束');
modifyFileByPath(apiPath + '/server.js', '// 前端路由开始', '// 前端路由结束');
modifyFileByPath(apiPath + '/server.js', '// 后台渲染开始', '// 后台渲染结束');
modifyFileByPath(apiPath + '/server.js', "// 前端路由入口开始", '// 前端路由入口结束');
modifyFileByPath(apiPath + '/server.js', "// 机器人抓取开始", '// 机器人抓取结束');
modifyFileByPath(apiPath + '/server.js', "// 设置 express 根目录开始", '// 设置 express 根目录结束');
modifyFileByPath(apiPath + '/server.js', "// 鉴权用户开始", '// 鉴权用户结束');
modifyFileByPath(apiPath + '/server.js', "// 引用 nunjucks 模板引擎开始", '// 引用 nunjucks 模板引擎结束');
modifyFileByPath(apiPath + '/server.js', "// 国际化开始", '// 国际化结束');
modifyFileByReplace(apiPath + '/configs/settings.js', '8884', '8882');

setDbInfo(apiPath);

let apiPublicPath = apiPath + '/public/';
let apiViewPath = apiPath + '/views/';
let apiSrcPath = apiPath + '/src/';
let apiDistPath = apiPath + '/dist/';
shell.rm('-rf', apiPublicPath);
shell.rm('-rf', apiViewPath);
shell.rm('-rf', apiSrcPath);
shell.rm('-rf', apiDistPath);


// -------------------------shopApi工程开始
shell.rm('-rf', apiShopPath + '/*');
// 拷贝工程
shell.cp('-R', [distPath, buildPath, configsPath, localesPath, publicPath, serverPath, srcPath, utilsPath, viewsPath], apiShopPath);
shell.cp(packageFile, apiShopPath);
shell.cp(serverFile, apiShopPath);
shell.cp(faviconFile, apiShopPath);

// 修改指定文件
modifyFileByPath(apiShopPath + '/server.js', '// api入口开始', '// api入口结束');
modifyFileByPath(apiShopPath + '/server.js', '// 入口路由开始', '// 入口路由结束');
modifyFileByPath(apiShopPath + '/server.js', '// 前端路由开始', '// 前端路由结束');
modifyFileByPath(apiShopPath + '/server.js', '// 后台渲染开始', '// 后台渲染结束');
modifyFileByPath(apiShopPath + '/server.js', "// 前端路由入口开始", '// 前端路由入口结束');
modifyFileByPath(apiShopPath + '/server.js', "// 机器人抓取开始", '// 机器人抓取结束');
modifyFileByPath(apiShopPath + '/server.js', "// 设置 express 根目录开始", '// 设置 express 根目录结束');
modifyFileByPath(apiShopPath + '/server.js', "// 鉴权用户开始", '// 鉴权用户结束');
modifyFileByPath(apiShopPath + '/server.js', "// 引用 nunjucks 模板引擎开始", '// 引用 nunjucks 模板引擎结束');
modifyFileByPath(apiShopPath + '/server.js', "// 国际化开始", '// 国际化结束');
modifyFileByReplace(apiShopPath + '/configs/settings.js', '8884', '8883');

setDbInfo(apiShopPath);

let apiShopPublicPath = apiShopPath + '/public/';
let apiShopViewPath = apiShopPath + '/views/';
let apiShopSrcPath = apiShopPath + '/src/';
let apiShopDistPath = apiShopPath + '/dist/';
shell.rm('-rf', apiShopPublicPath);
shell.rm('-rf', apiShopViewPath);
shell.rm('-rf', apiShopSrcPath);
shell.rm('-rf', apiShopDistPath);