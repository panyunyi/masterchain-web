const jwt = require('jsonwebtoken');
let settings = require('../../configs/settings');
let cache = require('./cache');

var token = {

    checkToken: function (token) {
        return new Promise((resolve, reject) => {
            jwt.verify(token, settings.encrypt_key, function (err, decoded) {
                if (err) {
                    console.log('check token failed', err);
                    // 如果不匹配或者过期,使用redis辅助延续登录态
                    if (err.message == "jwt expired") {
                        console.log('token失效1');
                        cache.get(token, (userId) => {
                            if (userId) {
                                console.log('token过期，继续延期', userId);
                                // 1000 * 60 * 60 * 24 * 30
                                cache.set(token, userId, 1000 * 60 * 60 * 24 * 30);
                                resolve({
                                    userId
                                })
                            } else {
                                console.log('token失效2');
                                resolve(false);
                            }
                        })
                    } else {
                        console.log('token错误');
                        resolve(false);
                    }
                } else {
                    console.log('token正常，存入redis', decoded);
                    cache.set(token, decoded.userId, 1000 * 60 * 60 * 24 * 30);
                    resolve(decoded);
                }
            });
        })
    }

}
module.exports = token;