var uuid = require('node-uuid');
var crypto = require('crypto');
var moment = require('moment');
var http = require('http');
var alidayuUrl = 'http://mts.cn-hongkong.aliyuncs.com';
let settings = require('../../configs/settings');

var config = {
    AppKey: settings.accessKeyId,
    AppSecret: settings.accessKeySecret
};

var mts = {
    videoShot: async (ossPath = '', fileName = '', callback = () => { }) => {
        var sendurl = mts.url(ossPath, fileName);
        var req = http.request(sendurl, function (res) {
            var status = res.statusCode;
            if (status != 200) {
                callback(new Error('网络异常'));
            }
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                var value = JSON.parse(chunk);
                if (value.SnapshotJob && value.SnapshotJob.State == 'Success') {
                    console.log('截图作业提交成功！')
                    callback('success');
                } else {
                    console.log('截图作业提交异常')
                    callback('failed');
                }
            }).on('error', function (e) {
                callback(new Error('截图作业提交异常'));
            });
        });
        req.write('执行完毕');
        req.end();
    },
    sign: function (params, accessSecret) {
        var param = {}, qstring = [];
        var oa = Object.keys(params);
        for (var i = 0; i < oa.length; i++) {
            param[oa[i]] = params[oa[i]];
        }
        for (var key in param) {
            qstring.push(encodeURIComponent(key) + '=' + encodeURIComponent(param[key]));
        }
        qstring = qstring.join('&');
        var StringToSign = 'GET' + '&' + encodeURIComponent('/') + '&' + encodeURIComponent(qstring);
        accessSecret = accessSecret + '&';
        var signature = crypto.createHmac('sha1', accessSecret).update(StringToSign).digest().toString('base64');
        signature = signature.replace(/\*/, '%2A').replace(/%7E/, '~');
        return signature;
    },
    url: function (ossPath, fileName) {

        let inputParams = {
            "Bucket": settings.bucket,
            "Location": settings.region,
            "Object": ossPath + fileName
        }
        let snapshotConfig = {
            "OutputFile": {
                "Bucket": settings.bucket,
                "Location": settings.region,
                "Object": ossPath + (fileName.split('.')[0] + '.jpg')
            },
            "Time": "5"
        }

        var obj = {
            AccessKeyId: config.AppKey,
            Action: 'SubmitSnapshotJob',
            Format: 'JSON',
            Input: JSON.stringify(inputParams),
            SignatureMethod: 'HMAC-SHA1',
            SignatureNonce: uuid.v1(),
            SignatureVersion: '1.0',
            SnapshotConfig: JSON.stringify(snapshotConfig),
            Timestamp: '',
            Version: '2014-06-18'
        }
        var timestamp = moment(new Date().getTime() - 3600 * 1000 * 8).format("YYYY-MM-DDTHH:mm:ss") + 'Z';
        obj.SignatureNonce = uuid.v1();
        obj.Timestamp = timestamp;

        var sign = this.sign(obj, config.AppSecret);
        var arr = [];
        for (var p in obj) {
            arr.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
        }
        arr.push(encodeURIComponent('Signature') + '=' + encodeURIComponent(sign))
        var msg = arr.join('&')
        var sendurl = alidayuUrl + '?' + msg;
        return sendurl;
    }
}
module.exports = mts;
