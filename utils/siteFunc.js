/**
 * Created by Administrator on 2015/5/30.
 */

const settings = require("../configs/settings");
const shortid = require('shortid');
const _ = require('lodash');
const moment = require('moment');
const cache = require('./middleware/cache')
const logUtil = require('./middleware/logUtil')
const authToken = require('./middleware/token');
const JPush = require('jpush-async').JPushAsync;
var clientPush = JPush.buildClient(settings.jg_appkey, settings.jg_masterSecret);
const {
    UserActionHis,
    BillRecord,
    User,
    Content,
    SiteMessage,
    Message,
    CommunityContent,
    CommunityMessage,
    OpenVipRecord,
    IdentityAuthentication,
    Integralonfig,
    AdminUser,
    SystemConfig,
    Switches
} = require('../server/lib/models');
const uuidv1 = require('uuid/v1');
const axios = require('axios');
const SMSClient = require('@alicloud/international-sms-sdk')

// 生产钱包任务计数
let makeWalletsTaskNum = 0;
let hadCacheLaguage = false;

var siteFunc = {

    randomString(len, charSet) {
        charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var randomString = '';
        for (var i = 0; i < len; i++) {
            var randomPoz = Math.floor(Math.random() * charSet.length);
            randomString += charSet.substring(randomPoz, randomPoz + 1);
        }
        return randomString;
    },

    setConfirmPassWordEmailTemp: function (res, sysConfigs, name, token) {

        let siteTitle = sysConfigs.siteName;
        var html = '<p>' + res.__("label_sendActiveEmail_text1") + '：' + name + '</p><br/>' +
            '<p>' + res.__("label_sendActiveEmail_text2") + '</p><br/>' +
            '<p><strong>' + siteTitle + '</strong> ' + res.__("label_sendActiveEmail_text2_1") + '</p><br/><br/>' +
            '<p>' + res.__("label_sendActiveEmail_text3") + '</p><br/>' +
            '<a href="' + sysConfigs.siteDomain + '/users/reset_pass?key=' + token + '">' + res.__("label_sendActiveEmail_text4") + '</a><br/>' +
            '<a href="' + sysConfigs.siteDomain + '/users/reset_pass?key=' + token + '">' + sysConfigs.siteDomain + '/users/reset_pass?key=' + token + '</a><br/>' +
            '<p> <strong>' + siteTitle + ' </strong> </p>';
        return html;
    },

    setNoticeToAdminEmailTemp: function (res, sysConfigs, obj) {
        let siteTitle = sysConfigs.siteName;
        var msgDate = moment(obj.date).format('YYYY-MM-DD HH:mm:ss');
        var html = '';
        html += '主人您好，<strong>' + obj.author.userName + '</strong> 于 ' + msgDate + ' 在 <strong>' + siteTitle + '</strong> 的文章 <a href="' + sysConfigs.siteDomain + '/details/' + obj.content._id + '.html">' + obj.content.title + '</a> 中留言了';
        return html;
    },

    setNoticeToAdminEmailByContactUsTemp: function (res, sysConfigs, obj) {
        let siteTitle = sysConfigs.siteName;
        var msgDate = moment(obj.date).format('YYYY-MM-DD HH:mm:ss');
        var html = '';
        html += res.__("lc_sendEmail_user_notice_title") + '<br/><br/>' +
            res.__("lc_sendEmail_user_success_notice") + '<br/><br/>' +
            res.__("lc_sendEmail_user_notice_Info") + '<br/><br/>' +
            '<strong>' + res.__("label_user_email") + ': </strong>' + obj.email + '<br/><br/>' +
            '<strong>' + res.__("label_user_phoneNum") + ': </strong>' + obj.phoneNum + '<br/><br/>' +
            '<strong>' + res.__("lc_sendEmail_user_content") + ': </strong><br/><br/>' + obj.comments + '<br/><br/>'
        return html;
    },

    setNoticeToUserByContactUsTemp: function (res, sysConfigs, obj) {
        var html = '';
        html += res.__("label_sendActiveEmail_text1") + '，<strong>' + obj.name + '</strong>' + res.__("lc_sendEmail_user_success_notice") + '<br/><br/>';
        return html;
    },

    setNoticeToUserEmailTemp: function (res, sysConfigs, obj) {
        let siteTitle = sysConfigs.siteName;
        var msgDate = moment(obj.date).format('YYYY-MM-DD HH:mm:ss');
        var html = '';
        var targetEmail;
        if (obj.author) {
            targetEmail = obj.author.userName;
        } else if (obj.adminAuthor) {
            targetEmail = obj.adminAuthor.userName;
        }
        html += '主人您好，<strong>' + targetEmail + '</strong> 于 ' + msgDate + ' 在 <strong>' + siteTitle + '</strong> 的文章 <a href="' + sysConfigs.siteDomain + '/details/' + obj.content._id + '.html">' + obj.content.title + '</a> 中回复了您';
        return html;
    },

    setBugToAdminEmailTemp: function (res, sysConfigs, obj) {
        let siteTitle = sysConfigs.siteName;
        var msgDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
        var html = '';
        html += '主人您好，测试管理员（' + obj.email + ')于 ' + msgDate + ' 在 <strong>' + siteTitle + '</strong> 的后台模块 <strong>' + obj.contentFrom + '</strong> 中说：<br>' + obj.content;
        return html;
    },

    setNoticeToUserRegSuccess: function (res, sysConfigs, obj) {
        let siteTitle = sysConfigs.siteName;
        var html = '';
        html += obj.userName + ' （' + obj.email + ') ' + res.__("label_sendRegEmail_text1") + '<br><br>' +
            '<p>' + siteTitle + ' ' + res.__("label_sendRegEmail_text3") + '</p><br>' +
            '<p>' + res.__("label_sendRegEmail_text2") + ' ' + res.__("label_sendRegEmail_text4") + ' <a href="' + sysConfigs.siteDomain + '/users/login" target="_blank">' + res.__("label_sendRegEmail_text5") + '</a></p><br><br>';
        return html;
    },

    setNoticeToUserGetMessageCode: function (res, sysConfigs, obj) {
        let siteTitle = sysConfigs.siteName;
        var html = '';
        html += obj.email + ' ' + res.__("label_sendRegEmail_text1") + '<br><br>' +
            '<p>' + res.__("label_sendRegEmail_text6") + '</p><br>' +
            '<p style="font-size:22px;font-weight:bold;">' + obj.renderCode + '</p><br><br>';
        return html;
    },

    getNoticeConfig: function (type, value) {
        var noticeObj;
        if (type == 'reg') {
            noticeObj = {
                type: '2',
                systemSender: 'doraCMS',
                title: '用户注册提醒',
                content: '新增注册用户 ' + value,
                action: type
            };
        } else if (type == 'msg') {
            noticeObj = {
                type: '2',
                sender: value.author,
                title: '用户留言提醒',
                content: '用户 ' + value.author.userName + ' 给您留言啦！',
                action: type
            };
        }
        return noticeObj;
    },
    // 校验合法ID
    checkCurrentId(ids) {
        if (!ids) return false;
        let idState = true;
        let idsArr = ids.split(',');
        if (typeof idsArr === "object" && idsArr.length > 0) {
            for (let i = 0; i < idsArr.length; i++) {
                if (!shortid.isValid(idsArr[i])) {
                    idState = false;
                    break;
                }
            }
        } else {
            idState = false;
        }
        return idState;
    },

    renderNoPowerMenus(manageCates, adminPower) {
        let newResources = [],
            newRootCates = [];
        let rootCates = _.filter(manageCates, (doc) => {
            return doc.parentId == '0';
        });
        let menuCates = _.filter(manageCates, (doc) => {
            return doc.type == '0' && doc.parentId != '0';
        });
        let optionCates = _.filter(manageCates, (doc) => {
            return doc.type != '0';
        });
        if (!_.isEmpty(adminPower)) {
            // 是否显示子菜单
            for (let i = 0; i < menuCates.length; i++) {
                let resourceObj = JSON.parse(JSON.stringify(menuCates[i]));
                let cateFlag = this.checkNoAllPower(resourceObj._id, optionCates, adminPower);
                if (!cateFlag) {
                    newResources.push(resourceObj);
                }
            }
            // 是否显示大类菜单
            for (const cate of rootCates) {
                let fiterSubCates = _.filter(newResources, (doc) => {
                    return doc.parentId == cate._id;
                });
                if (fiterSubCates.length != 0) {
                    newRootCates.push(cate);
                }
            }
        }
        return newResources.concat(newRootCates);
    },

    // 子菜单都无权限校验
    checkNoAllPower(resourceId, childCates, power) {
        let cateFlag = true;
        let rootCates = _.filter(childCates, (doc) => {
            return doc.parentId == resourceId
        });
        for (const cate of rootCates) {
            if ((power).indexOf(cate._id) > -1) {
                cateFlag = false;
                break;
            }
        }
        return cateFlag;
    },

    // 异常捕获对象
    UserException: function (message) {
        this.message = message;
        this.name = "UserException";
    },

    // 关注调用
    addActionBills(req, res, userId, actionType, targetId, contentType) {
        return new Promise(async (resolve, reject) => {
            // 一天内关注数量不超过5次不添加奖励
            let rangeTime = siteFunc.getDateStr(-1);
            let collection_count = await BillRecord.count({
                user: userId,
                type: actionType,
                date: {
                    "$gte": new Date(rangeTime.startTime),
                    "$lte": new Date(rangeTime.endTime)
                }
            });

            let configIntegral = await siteFunc.getIntegralonfig();
            if (collection_count < configIntegral.follow_limit) {
                // 添加账单和行为
                await siteFunc.addUserActionHis(req, res, actionType, {
                    targetId: targetId,
                    contentType: contentType
                });
            } else {
                console.log('超过限制，不计算奖励')
            }
            resolve();
        })
    },

    addUserActionHis(req, res, type, params = {}) {
        return new Promise(async (resolve, reject) => {
            // console.log('---params---', params)
            try {
                // 针对用户行为的记录
                if ((type.indexOf('user_action_type') >= 0 || type.indexOf('user_bill_type') >= 0) && (!_.isEmpty(req.session.user) || params.targetUser)) {

                    let configIntegral = await this.getIntegralonfig();
                    let targetUser = params.targetUser || req.session.user;
                    let target_content = '',
                        target_message = '',
                        target_communityContent = '',
                        target_communityMessage = '',
                        target_user = '',
                        target_special = '',
                        target_community = '',
                        target_class = '';
                    let wantCoins = 0,
                        actionType = ''; // wantCoins 标识 MBT分值
                    if (type == settings.user_action_type_login_first) {
                        wantCoins = configIntegral.login_first;
                    } else if (type == settings.user_action_type_creat_identity) {
                        wantCoins = configIntegral.creat_identity;
                    } else if (type == settings.user_action_type_browse) {
                        target_content = params.targetId;
                        wantCoins = configIntegral.browse;
                        actionType = 'view';
                    } else if (type == settings.user_action_type_give_thumbs_up) {
                        wantCoins = configIntegral.give_thumbs_up;
                        if (params.contentType == '0') {
                            target_content = params.targetId;
                        } else if (params.contentType == '1') {
                            target_message = params.targetId;
                        } else if (params.contentType == '2') {
                            target_communityContent = params.targetId;
                        } else if (params.contentType == '3') {
                            target_communityMessage = params.targetId;
                        }
                        actionType = 'good';
                    } else if (type == settings.user_action_type_comment) {
                        target_message = params.targetId;
                        wantCoins = configIntegral.comment;
                        actionType = 'comment';
                    } else if (type == settings.user_action_type_community_comment) {
                        target_communityMessage = params.targetId;
                        wantCoins = configIntegral.community_comment;
                        actionType = 'comment';
                    } else if (type == settings.user_action_type_appreciate) {
                        if (params.contentType == '0') {
                            target_content = params.targetId;
                        } else if (params.contentType == '2') {
                            target_communityContent = params.targetId;
                        } else if (params.contentType == '3') {
                            target_communityMessage = params.targetId;
                        }
                        wantCoins = configIntegral.appreciate;
                        actionType = 'populace';
                    } else if (type == settings.user_action_type_collection) {
                        wantCoins = configIntegral.collections;
                        if (params.contentType == '0') { // 收藏文章
                            target_content = params.targetId;
                        } else if (params.contentType == '2') { // 社群帖子
                            target_communityContent = params.targetId;
                        } else if (params.contentType == '3') {
                            target_communityMessage = params.targetId; // 社群留言
                        }
                        actionType = 'follow';
                    } else if (type == settings.user_action_type_follow) {
                        wantCoins = configIntegral.follow;
                        if (params.contentType == '4') { // 关注大师
                            target_user = params.targetId;
                        } else if (params.contentType == '5') { // 关注专题
                            target_special = params.targetId;
                        } else if (params.contentType == '6') { // 关注社群
                            target_community = params.targetId;
                        }
                        actionType = 'follow';
                    } else if (type == settings.user_action_type_forward) {
                        wantCoins = configIntegral.forward;
                        actionType = 'transmit';
                        if (params.contentType == '0') {
                            target_content = params.targetId;
                        } else if (params.contentType == '2') {
                            target_communityContent = params.targetId;
                        }
                    } else if (type == settings.user_action_type_community_building) {
                        wantCoins = configIntegral.community_building;
                        actionType = 'group';
                        target_communityContent = params.targetId;
                    } else if (type == settings.user_action_type_creat_content) {
                        wantCoins = configIntegral.creat_content;
                        target_content = params.targetId;
                        // actionType = 'create';
                    } else if (type == settings.user_action_type_invitation) {
                        wantCoins = configIntegral.invitation;
                        actionType = 'invite';
                        target_user = params.beInvitedUser;
                    } else if (type == settings.user_action_type_report) {
                        wantCoins = configIntegral.report;
                        // console.log('---params---', params);
                        if (params.contentType == '0') {
                            target_content = params.targetId;
                        } else if (params.contentType == '1') {
                            target_message = params.targetId;
                        } else if (params.contentType == '2') {
                            target_communityContent = params.targetId;
                        } else if (params.contentType == '3') {
                            target_communityMessage = params.targetId;
                        }
                        actionType = 'report';
                    } else if (type == settings.user_action_type_review) {
                        wantCoins = configIntegral.review;
                        actionType = 'review';
                    } else if (type == settings.user_bill_type_transfer_coins) {
                        // wantCoins = params.coins;
                    } else if (type == settings.user_bill_type_recharge) {} else if (type == settings.user_bill_type_subscribe) {
                        target_class = params.targetId;
                        wantCoins = configIntegral.subscribe;
                    } else if (type == settings.user_bill_type_signIn) {
                        wantCoins = params.wantCoins ? params.wantCoins : configIntegral.signIn;
                    } else if (type == settings.user_bill_type_buyVip) {
                        wantCoins = configIntegral.buyVip;
                    } else if (type == settings.user_bill_type_brithday) {
                        wantCoins = '0';
                    }

                    let userAction = '',
                        newBillRecordId = '';

                    if (actionType) {
                        let actionObj = {};
                        if (target_content) {
                            let thisContent = await Content.findOne({
                                _id: target_content
                            }, 'title date state uAuthor').populate([{
                                path: 'uAuthor'
                            }]).exec();
                            actionObj = {
                                id: thisContent._id,
                                createdatetime: new Date(thisContent.date),
                                status: 1,
                                author: thisContent.uAuthor.userName,
                                userid: thisContent.uAuthor.walletAddress ? thisContent.uAuthor.walletAddress : ''
                            }

                        } else if (target_message) {
                            let thisContent = await Message.findOne({
                                _id: target_message
                            }, 'author contentId state').populate([{
                                path: 'author'
                            }]).exec();
                            actionObj = {
                                id: thisContent._id,
                                createdatetime: new Date(thisContent.date),
                                status: 1,
                                author: thisContent.author.userName,
                                userid: thisContent.author.walletAddress ? thisContent.author.walletAddress : ''
                            }
                        } else if (target_communityContent) {
                            let thisContent = await CommunityContent.findOne({
                                _id: target_communityContent
                            }).populate([{
                                path: 'user'
                            }]).exec();

                            actionObj = {
                                id: thisContent._id,
                                createdatetime: new Date(thisContent.date),
                                status: 1,
                                author: thisContent.user.userName,
                                userid: thisContent.user.walletAddress ? thisContent.user.walletAddress : ''
                            }
                        } else if (target_communityMessage) {
                            let thisContent = await CommunityMessage.findOne({
                                _id: target_communityMessage
                            }, 'author contentId state').populate([{
                                path: 'author'
                            }]).exec();

                            actionObj = {
                                id: thisContent._id,
                                createdatetime: new Date(thisContent.date),
                                status: 1,
                                author: thisContent.author.userName,
                                userid: thisContent.author.walletAddress ? thisContent.author.walletAddress : ''
                            }
                        }

                        let targetUserWallets = await User.findOne({
                            _id: targetUser._id
                        }, 'walletAddress');

                        const targetUserActionObj = {
                            target_content: actionObj,
                            action: actionType,
                            userid: !_.isEmpty(targetUserWallets) ? targetUserWallets.walletAddress : '',
                            status: 1
                        }
                        // console.log('-targetUserActionObj----', targetUserActionObj)
                        const newUserActionHis = new UserActionHis(targetUserActionObj);
                        userAction = await newUserActionHis.save();
                        // console.log('==userAction==', userAction)
                    }

                    if (type) {
                        // 以下为因为行为新增的MEC账单记录
                        if (type == settings.user_action_type_appreciate) { // 打赏
                            // 打赏者扣款记录
                            let rewardUserRecord = new BillRecord({
                                target_content,
                                target_message,
                                target_communityContent,
                                coins: Number(params.rewordCoins) * -1,
                                type: settings.user_action_type_appreciate_out,
                                logs: params.logs || res.__(settings.user_action_type_appreciate_out),
                                user: targetUser._id,
                                action: userAction._id,
                                unit: 'MEC',
                                uuid: uuidv1().split('-').join('')
                            })
                            newBillRecordId = await rewardUserRecord.save();

                            // 被打赏者收款记录
                            let thisContent = {}; // 被打赏
                            if (target_content) {
                                thisContent = await Content.findOne({
                                    _id: target_content
                                }, 'uAuthor');
                            } else if (target_communityContent) {
                                thisContent = await CommunityContent.findOne({
                                    _id: target_communityContent
                                }, 'user');
                            }
                            if (!_.isEmpty(thisContent)) {
                                let beRewardRecord = new BillRecord({
                                    target_content,
                                    target_message,
                                    target_communityContent,
                                    coins: Number(params.rewordCoins),
                                    type: settings.user_action_type_appreciate_in,
                                    logs: params.logs || res.__(settings.user_action_type_appreciate_in),
                                    user: thisContent.uAuthor,
                                    action: userAction._id,
                                    unit: 'MEC',
                                    uuid: uuidv1().split('-').join('')
                                })
                                await beRewardRecord.save();
                            }

                        } else if (type == settings.user_bill_type_subscribe) { // 订购
                            let beRewardRecord = new BillRecord({
                                target_class,
                                coins: Number(params.orderPrice) * -1,
                                type: settings.user_bill_type_subscribe,
                                logs: params.logs || res.__(settings.user_bill_type_subscribe),
                                user: targetUser._id,
                                action: userAction._id,
                                unit: 'MEC',
                                uuid: uuidv1().split('-').join('')
                            })
                            await beRewardRecord.save();
                        } else if (type == settings.user_bill_type_recharge) {
                            let beRewardRecord = new BillRecord({
                                coins: Number(params.coins),
                                type: settings.user_bill_type_recharge,
                                logs: params.logs || res.__(settings.user_bill_type_recharge),
                                iapComments: params.iapComments || '',
                                user: targetUser._id,
                                action: userAction._id,
                                unit: 'MEC',
                                uuid: uuidv1().split('-').join('')
                            })
                            newBillRecordId = await beRewardRecord.save();
                        } else if (type == settings.user_bill_type_transfer_coins) { // 提币
                            let beRewardRecord = new BillRecord({
                                coins: Number(params.coins) * -1,
                                type: settings.user_bill_type_transfer_coins,
                                logs: params.comments || res.__(settings.user_bill_type_transfer_coins),
                                user: targetUser._id,
                                action: userAction._id,
                                unit: 'MVPC',
                                uuid: uuidv1().split('-').join('')
                            })
                            newBillRecordId = await beRewardRecord.save();
                        } else if (type == settings.user_bill_type_buyVip) { // 购买VIP
                            let beRewardRecord = new BillRecord({
                                coins: Number(params.coins) * -1,
                                type: settings.user_bill_type_buyVip,
                                logs: params.comments || res.__(settings.user_bill_type_buyVip),
                                user: targetUser._id,
                                action: userAction._id,
                                unit: 'MEC',
                                uuid: uuidv1().split('-').join('')
                            })
                            newBillRecordId = await beRewardRecord.save();
                        }

                        // 以下为因为行为产生的MBT（积分）
                        if (type == settings.user_action_type_browse // 浏览
                            ||
                            type == settings.user_action_type_login_first ||
                            type == settings.user_action_type_creat_content ||
                            type == settings.user_action_type_creat_identity ||
                            type == settings.user_action_type_give_thumbs_up ||
                            type == settings.user_action_type_comment ||
                            type == settings.user_action_type_community_comment ||
                            type == settings.user_action_type_appreciate ||
                            type == settings.user_action_type_collection ||
                            type == settings.user_action_type_follow ||
                            type == settings.user_action_type_forward ||
                            type == settings.user_action_type_community_building ||
                            type == settings.user_action_type_invitation ||
                            type == settings.user_action_type_report ||
                            type == settings.user_action_type_review ||
                            type == settings.user_bill_type_buyVip ||
                            type == settings.user_bill_type_brithday ||
                            type == settings.user_bill_type_subscribe ||
                            type == settings.user_bill_type_signIn
                        ) {

                            // 给邀请是否有效做标记
                            let inviteCoins = Number(wantCoins);
                            if (type == settings.user_action_type_invitation && params.inviteState == '0') {
                                inviteCoins = 0;
                            }

                            let beRewardRecord = new BillRecord({
                                coins: inviteCoins,
                                type: type,
                                logs: params.comments || res.__(type),
                                user: targetUser._id,
                                target_user,
                                target_special,
                                target_content,
                                target_community,
                                target_communityContent,
                                target_message,
                                target_class,
                                action: userAction._id || '',
                                unit: 'MBT',
                                uuid: uuidv1().split('-').join(''),
                                state: '0'
                            })
                            await beRewardRecord.save();
                        }
                        resolve({
                            billRecord: newBillRecordId._id,
                            userAction: userAction._id
                        });
                    }
                }
            } catch (error) {
                logUtil.error(error, req);
                reject(error);
            }

        })
    },

    // 封装api返回的数据
    renderApiData(req = {}, res, responseCode, responseMessage, data = {}, type = "") {

        if (type == 'getlist') {
            responseMessage = res.__("validate_error_getSuccess", {
                success: responseMessage
            })
        } else if (type == 'options') {
            responseMessage = res.__("sys_layer_option_success")
        }

        let sendData = {
            status: responseCode,
            message: responseMessage,
            server_time: (new Date()).getTime(),
            data
        }

        if ((type == 'addCommunity' || // 关注社群
                type == "addDespise" || // 踩帖
                type == "addFavorite" || // 添加收藏
                type == "followMaster" || // 关注大师
                type == "addSpecial" || // 关注专题
                type == "addPraise" || // 点赞
                type == "rewordContent" || // 打赏帖子
                type == 'updateUserInfo' || // 更新用户信息
                type == 'addTags' || // 添加标签
                type == "postMessage") && req.session.user) {
            // 添加需要更新缓存的标记
            req.session.refreshTag = true
            return sendData;
        } else {
            return sendData;
        }
    },

    renderApiErr(req, res, responseCode, responseMessage, type = '') {
        if (typeof responseMessage == 'object') {
            responseMessage = responseMessage.message;
        }

        // 处理登录失效的回调
        if (responseMessage == res.__("label_notice_asklogin")) {
            responseCode = 401;
        }

        let errorData = {
            status: responseCode,
            message: responseMessage,
            server_time: (new Date()).getTime(),
            data: {}
        }
        // 记录错误日志
        if (responseCode != 401) {
            logUtil.error({
                name: type,
                message: responseMessage
            }, req);
        }
        return errorData;
    },

    getSiteLocalKeys(local = 'zh-TW', res) {
        return new Promise((resolve, reject) => {
            if (!hadCacheLaguage) {
                console.log('begin clear redis session');
                cache.del(settings.session_secret + '_localkeys_zh-TW');
                cache.del(settings.session_secret + '_localkeys_zh-CN');
                hadCacheLaguage = true;
            }
            cache.get(settings.session_secret + '_localkeys_' + local, async (localRenderData) => {
                if (!_.isEmpty(localRenderData)) {
                    console.log('use old language cache:', settings.session_secret + '_localkeys_' + local)
                    resolve(localRenderData)
                } else {
                    let targetLocalJson = require(`../locales/${local}.json`);
                    let renderKeys = {};
                    // 记录针对组件的国际化信息
                    let sysKeys = {};
                    for (let lockey in targetLocalJson) {
                        renderKeys[lockey] = targetLocalJson[lockey];
                        if (lockey.indexOf('_layer_') > 0 || lockey.indexOf('label_system_') >= 0 || lockey.indexOf('label_uploader_') >= 0) {
                            sysKeys[lockey] = targetLocalJson[lockey];
                        }
                    }
                    let timeSet = process.env.NODE_ENV === 'production' ? 1000 * 60 * 60 * 24 : 1000;
                    console.log('language had cache:', settings.session_secret + '_localkeys_' + local)
                    cache.set(settings.session_secret + '_localkeys_' + local, {
                        renderKeys,
                        sysKeys
                    }, timeSet);
                    resolve({
                        renderKeys,
                        sysKeys
                    })
                }
            })
        })
    },
    renderLocalStr() {
        let str = [' ', '  '];
        if (settings.lang == 'en') {
            str = ['  ', '    ']
        }
        return str;
    },

    getStrLength(str) {
        let charCode = -1;
        const len = str.length;
        let realLength = 0;
        let zhChar = 0,
            enChar = 0;
        for (let i = 0; i < len; i++) {
            charCode = str.charCodeAt(i)
            if (charCode >= 0 && charCode <= 128) {
                realLength += 1;
                enChar++
            } else {
                realLength += 2;
                zhChar++
            }
        }
        return {
            length: realLength,
            enChar,
            zhChar
        }
    },

    setTempParentId(arr, key) {
        for (var i = 0; i < arr.length; i++) {
            var pathObj = arr[i];
            pathObj.parentId = key;
        }
        return arr;
    },

    getTempBaseFile: function (path) {
        var thisType = (path).split('.')[1];
        var basePath;
        if (thisType == 'html') {
            basePath = settings.SYSTEMTEMPFORDER;
        } else if (thisType == 'json') {
            basePath = process.cwd();
        } else {
            basePath = settings.TEMPSTATICFOLDER;
        }
        return basePath;
    },

    // 扫描某路径下文件夹是否存在
    checkExistFile(tempFilelist, forderArr) {

        let filterForderArr = [],
            distPath = false;
        for (let i = 0; i < forderArr.length; i++) {
            const forder = forderArr[i];
            let currentForder = _.filter(tempFilelist, (fileObj) => {
                return fileObj.name == forder;
            })
            filterForderArr = filterForderArr.concat(currentForder);
        }
        if (filterForderArr.length > 0 && (tempFilelist.length >= forderArr.length) && (filterForderArr.length == tempFilelist.length)) {
            distPath = true;
        }

        return distPath;
    },

    checkPostToken(req, res, token = '') {
        return new Promise(async (resolve, reject) => {
            if (req.query.authUser) {
                if (!token) { // PC 前端
                    // 管理员不需要鉴权
                    if (req.session.user && req.query.useClient == '1') {

                        req.session.user = await User.findOne({
                            _id: req.session.user._id
                        }, this.getAuthUserFields('session'));

                        console.log('request from web front');
                        resolve();
                    } else if (req.session.adminlogined && req.query.useClient == '0') {
                        console.log('request from web back');
                        resolve();
                    } else {
                        reject(res.__("label_notice_asklogin"))
                    }
                } else {
                    req.query.useClient = '2';
                    let checkToken = await authToken.checkToken(token);
                    if (checkToken) {
                        let targetUser = await User.findOne({
                            _id: checkToken.userId
                        });
                        if (!_.isEmpty(targetUser)) {
                            console.log('user had login, request from mobile');
                            req.session.user = targetUser;
                            resolve();
                        } else {
                            reject(res.__("label_notice_asklogin"))
                        }
                    } else {
                        reject(res.__("label_notice_asklogin"))
                    }
                }
            }
            resolve();
        })

    },

    getAuthUserFields(type = '') {
        let fieldStr = "id userName category group logo date enable";
        if (type == 'login') {
            fieldStr = "id userName category group logo date enable phoneNum countryCode email walletAddress comments position loginActive birth password";
        } else if (type == 'base') {
            fieldStr = "id userName category group logo date enable phoneNum countryCode email watchers followers comments favorites favoriteCommunityContent despises walletAddress comments profession experience industry introduction birth creativeRight gender";
        } else if (type == 'session') {
            fieldStr = "id userName category group logo date enable phoneNum countryCode email watchers followers praiseContents praiseMessages praiseCommunityContent praiseCommunityMessages watchSpecials watchCommunity watchCommunityState watchTags favorites favoriteCommunityContent despises despiseMessage despiseCommunityContent walletAddress comments position gender vip";
        } else if (type == 'community') {
            fieldStr = "id userName category group logo date enable phoneNum countryCode email watchers followers comments favorites favoriteCommunityContent despises walletAddress comments profession experience industry introduction birth creativeRight gender watchCommunity watchCommunityState lastCommunitySpeach";
        }
        return fieldStr;
    },

    getContentListFields(useClient = '', type = '') {
        let files = null;
        if (useClient == '2') {
            files = '_id title sImg uAuthor date updateDate discription type appShowType imageArr videoArr duration simpleComments videoImg state dismissReason clickNum isTop roofPlacement videoUrl audioUrl'
        } else {
            if (type == 'normal') {
                files = '_id title sImg uAuthor date updateDate discription type videoImg state dismissReason clickNum videoUrl audioUrl'
            } else if (type == 'simple') {
                files = '_id title sImg stitle date updateDate type videoImg state dismissReason clickNum videoUrl audioUrl';
            }
        }
        // console.log('--files----', files)
        return files;
    },

    getDateStr(addDayCount) {
        var dd = new Date();
        dd.setDate(dd.getDate() + addDayCount); //获取AddDayCount天后的日期
        var y = dd.getFullYear();
        var m = (dd.getMonth() + 1) < 10 ? "0" + (dd.getMonth() + 1) : (dd.getMonth() + 1); //获取当前月份的日期，不足10补0
        var d = dd.getDate() < 10 ? "0" + dd.getDate() : dd.getDate(); //获取当前几号，不足10补0
        let endDate = moment().format("YYYY-MM-DD");
        return {
            startTime: y + "-" + m + "-" + d + ' 23:59:59',
            endTime: endDate + ' 23:59:59'
        }
    },

    async addSiteMessage(type = '', activeUser = '', passiveUser = '', content = '', params = {
        targetMediaType: '0',
        recordId: ''
    }) {

        try {
            const messageObj = {
                type,
                activeUser: activeUser._id,
                passiveUser,
                recordId: params.recordId,
                isRead: false
            }

            if (type == '6') {
                delete messageObj.activeUser;
                messageObj.activeAdminUser = activeUser._id;
            }

            if (params.targetMediaType == '0') {
                messageObj.content = content;
            } else if (params.targetMediaType == '1') {
                messageObj.message = content;
            } else if (params.targetMediaType == '2') {
                messageObj.communityContent = content;
            } else if (params.targetMediaType == '3') {
                messageObj.communityMessage = content;
            }

            const newSiteMessage = new SiteMessage(messageObj);
            await newSiteMessage.save();
            // 客户端消息推送
            let pushType = '',
                pushId = '';
            if (type == '1') { // 赞赏
                pushType = 'appreciateContent';
                pushId = content;
            } else if (type == '2') { // 关注
                pushType = 'follow';
                pushId = passiveUser;
            } else if (type == '3') { // 文章评论与回复
                pushType = 'commentOrReply';
                pushId = content;
            } else if (type == '4') { // 点赞
                if (params.targetMediaType == '0') { // 文章
                    pushType = 'thumbsUpContent'
                } else { // 评论
                    pushType = 'thumbsUpComments'
                }
                pushId = content;
            } else if (type == '5') { // 社群评论与回复
                pushType = 'commentOrReply';
                pushId = content;
            }
            if (pushType) {
                await siteFunc.jsPushFunc(activeUser, passiveUser, pushType, pushId);
            }
        } catch (error) {
            logUtil.error(error, {});
        }
    },

    async getUserInfoByToken(req, next) {
        let currentToken = await authToken.checkToken(req.query.token);
        if (currentToken) {
            let targetUser = await User.findOne({
                _id: currentToken.userId
            }, siteFunc.getAuthUserFields('session'));
            if (!_.isEmpty(targetUser)) {
                console.log('user had login, request from ', req.query.useClient);
                req.session.user = targetUser;
            } else {
                console.log('user had no login, request from ', req.query.useClient);
            }
        }
        next();
    },

    async checkUserToken(req, res, next) {
        try {
            let userToken = req.query.token;
            if (!_.isEmpty(req.session.user) && !userToken) { // 针对web端
                // TODO 查询用户信息成本比较高，只在需要更新用户信息的地方重新查询数据
                if (req.session.refreshTag) {
                    req.session.user = await User.findOne({
                        _id: req.session.user._id
                    }, siteFunc.getAuthUserFields('session'));
                    req.session.refreshTag = false;
                    console.log('refresh session, request from ', req.query.useClient);
                } else {
                    console.log('user old session, request from ', req.query.useClient);
                }
                next();
            } else { // 针对非web端
                if (userToken) {
                    req.session.user = "";
                    let checkToken = await authToken.checkToken(req.query.token);
                    if (checkToken) {
                        let targetUser = await User.findOne({
                            _id: checkToken.userId
                        }, siteFunc.getAuthUserFields('session'));
                        if (!_.isEmpty(targetUser)) {
                            console.log('user had login,request from ', req.query.useClient);
                            req.session.user = targetUser;
                            next();
                        } else {
                            throw new siteFunc.UserException(res.__("label_notice_asklogin"));
                        }
                    } else {
                        throw new siteFunc.UserException(res.__("label_notice_asklogin"));
                    }
                } else {
                    throw new siteFunc.UserException(res.__("label_notice_asklogin"));
                }
            }
        } catch (error) {
            logUtil.error(error, req);
            res.send({
                status: 401,
                message: res.__("label_notice_asklogin")
            });
        }
    },

    // 针对前台api需要登录态校验
    checkUserSessionForApi(req, res, next) {
        if (req.method == 'GET') {
            siteFunc.checkUserToken(req, res, next);
        } else if (req.method == 'POST') {
            // 添加鉴权识别参数
            req.query.authUser = true;
            next();
        }
    },

    // 针对登录态非必须的状态校验(GET)
    async checkUserLoginState(req, res, next) {
        if (!_.isEmpty(req.session.user) && !(req.query.token)) {
            try {
                if (req.session.refreshTag) {
                    req.session.user = await User.findOne({
                        _id: req.session.user._id
                    }, siteFunc.getAuthUserFields('session'));
                    req.session.refreshTag = false;
                    console.log('refresh session, request from ', req.query.useClient);
                } else {
                    console.log('user old session, request from ', req.query.useClient);
                }
            } catch (error) {
                logUtil.error(error, req);
            }
            next();
        } else {
            if (req.query.token) {
                req.session.user = "";
                siteFunc.getUserInfoByToken(req, next);
            } else {
                next();
            }
        }
    },

    // 初始化设置来源，针对 get 有效
    setUserClient(req, res, next) {
        var deviceAgent = req.headers["user-agent"].toLowerCase();
        // console.log('-deviceAgent---', deviceAgent);
        if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
            req.session.os = 'ios';
        } else if (deviceAgent.match(/(android)/)) {
            req.session.os = 'android';
        }
        // console.log('---req.session.os---', req.session.os);
        // 这里设置白名单跨域
        if (req.query.token) {
            req.query.useClient = '2'; // 移动端
        } else {
            req.query.useClient = '1'; // PC端
        }
        next();
    },

    //筛选内容中的url
    getAHref(htmlStr, type = 'image') {
        var reg = /<img.+?src=('|")?([^'"]+)('|")?(?:\s+|>)/gim;
        if (type == 'video') {
            reg = /<video.+?src=('|")?([^'"]+)('|")?(?:\s+|>)/gim;
        } else if (type == 'audio') {
            reg = /<audio.+?src=('|")?([^'"]+)('|")?(?:\s+|>)/gim;
        }
        var arr = [];
        while (tem = reg.exec(htmlStr)) {
            arr.push(tem[2]);
        }
        return arr;
    },

    renderSimpleContent(htmlStr, imgLinkArr, videoLinkArr) {
        // console.log('----imgLinkArr-', imgLinkArr);
        let renderStr = [];
        // 去除a标签
        htmlStr = htmlStr.replace(/(<\/?a.*?>)|(<\/?span.*?>)/g, '');
        htmlStr = htmlStr.replace(/(<\/?br.*?>)/g, '\n\n');
        if (imgLinkArr.length > 0 || videoLinkArr.length > 0) {
            // console.log('----1111---')
            let delImgStr, delEndStr;
            var imgReg = /<img[^>]*>/gim;
            var videoReg = /<video[^>]*>/gim;
            if (imgLinkArr.length > 0) {
                delImgStr = htmlStr.replace(imgReg, "|I|");
            } else {
                delImgStr = htmlStr;
            }
            if (videoLinkArr.length > 0) {
                delEndStr = delImgStr.replace(videoReg, "|V|");
            } else {
                delEndStr = delImgStr;
            }
            // console.log('--delEndStr--', delEndStr);
            let imgArr = delEndStr.split("|I|");
            let imgTag = 0,
                videoTag = 0;
            for (let i = 0; i < imgArr.length; i++) {
                const imgItem = imgArr[i];
                // console.log('---imgItem---', imgItem);
                if (imgItem.indexOf("|V|") < 0) {
                    // console.log('----i----', imgItem);
                    imgItem && renderStr.push({
                        type: 'contents',
                        content: imgItem
                    })
                    if (imgLinkArr[imgTag]) {
                        renderStr.push({
                            type: 'image',
                            content: imgLinkArr[imgTag]
                        });
                        imgTag++;
                    }
                } else { // 包含视频片段
                    let smVideoArr = imgItem.split('|V|');
                    for (let j = 0; j < smVideoArr.length; j++) {
                        const smVideoItem = smVideoArr[j];
                        smVideoItem && renderStr.push({
                            type: 'contents',
                            content: smVideoItem
                        })
                        if (videoLinkArr[videoTag]) {
                            let videoImg = siteFunc.getVideoImgByLink(videoLinkArr[videoTag])
                            renderStr.push({
                                type: 'video',
                                content: videoLinkArr[videoTag],
                                videoImg
                            });
                            videoTag++;
                        }
                    }
                    if (imgLinkArr[imgTag]) {
                        renderStr.push({
                            type: 'image',
                            content: imgLinkArr[imgTag]
                        });
                        imgTag++;
                    }
                }
            }
        } else {

            renderStr.push({
                type: 'contents',
                content: htmlStr
            })
            // console.log('----2222---', renderStr)
        }

        return JSON.stringify(renderStr);
    },

    checkContentType(htmlStr, type = 'content') {
        let imgArr = this.getAHref(htmlStr, 'image');
        let videoArr = this.getAHref(htmlStr, 'video');
        let audioArr = this.getAHref(htmlStr, 'audio');

        let defaultType = '0',
            targetFileName = '';
        if (videoArr && videoArr.length > 0) {
            defaultType = '3';
            targetFileName = videoArr[0];
        } else if (audioArr && audioArr.length > 0) {
            defaultType = '4';
            targetFileName = audioArr[0];
        } else if (imgArr && imgArr.length > 0) {
            // 针对帖子有两种 大图 小图
            if (type == 'content') {
                defaultType = (Math.floor(Math.random() * 2) + 1).toString();
            } else if (type == 'class') {
                defaultType = '1';
            }
            targetFileName = imgArr[0];
        } else {
            defaultType = '1';
        }
        let renderLink = targetFileName;
        if (type == '3') {
            // 视频缩略图
            renderLink = siteFunc.getVideoImgByLink(targetFileName);
        }
        return {
            type: defaultType,
            defaultUrl: renderLink,
            imgArr,
            videoArr
        };
    },
    getVideoImgByLink(link) {
        let oldFileType = link.replace(/^.+\./, '')
        return link.replace('.' + oldFileType, '.jpg');
    },
    getCacheValueByKey(key) {
        return new Promise((resolve, reject) => {
            cache.get(key, (targetValue) => {
                if (targetValue) {
                    resolve(targetValue)
                } else {
                    resolve('');
                }
            })
        })
    },
    // 获取mstt剩余总数
    getMVPCLeftCoins(walletAddress) {
        return new Promise(async (resolve, reject) => {
            try {
                // let walletAddress = '0x262e95aba69088dc79c917bf2da7baabd3911423'
                let askLink = `${settings.getLeftMsttApi}/${walletAddress}`;
                let msttRes = await axios.get(askLink)
                if (msttRes.status == 200) {
                    let msttListData = msttRes.data || 0;
                    console.log('left mstt coins: ', msttListData)
                    resolve(msttListData);
                } else {
                    console.log('get mstt coins error!')
                    resolve(0);
                }
            } catch (error) {
                console.log('get mstt coins error!')
                logUtil.error(error, {});
                resolve(0);
            }
        })
    },

    // 获取MEC剩余总数
    getMECLeftCoins(walletAddress) {
        return new Promise(async (resolve, reject) => {
            try {
                // let walletAddress = '0x262e95aba69088dc79c917bf2da7baabd3911423'
                // console.log('--walletAddress----', walletAddress);
                let askLink = `${settings.getLeftMecApi}/${walletAddress}`;
                // console.log('--askLink----', askLink)
                let mecRes = await axios.get(askLink)
                if (mecRes.status == 200) {
                    let mecListData = mecRes.data || 0;
                    console.log('left mec coins', mecListData)
                    resolve(mecListData);
                } else {
                    console.log('get mec coins error!')
                    resolve(0);
                }
            } catch (error) {
                logUtil.error(error, {});
                console.log('get mec coins error!')
                resolve(0);
            }
        })
    },

    // 申请钱包
    getWallets: (walletNum = 0) => {

        if (makeWalletsTaskNum == 0) {
            makeWalletsTaskNum = 1;
            let askParams = {
                "count": walletNum
            }
            logUtil.info('getWallets begin', JSON.stringify(askParams));
            axios.post(settings.getWalletsApi, askParams)
                .then((response) => {
                    if (response.status == 200) {
                        let transferState = response.data;
                        if (transferState) {
                            console.log('getWallets success');
                            logUtil.info('getWallets success', transferState.length);
                        } else {
                            logUtil.info('getWallets failed', JSON.stringify(askParams));
                        }
                    }
                    makeWalletsTaskNum = 0;
                }).catch((err) => {
                    makeWalletsTaskNum = 0;
                    console.log('getWallets failed', err);
                    logUtil.error(err, {});
                })
        }

    },

    // 转账接口
    transfer: (from, to, coins, password) => {
        return new Promise(async (resolve, reject) => {
            let askParams = {
                "from": from,
                "password": password,
                "to": to,
                "value": coins
            }
            // console.log('---askParams--', askParams)
            logUtil.info('transfer begin', JSON.stringify(askParams));
            axios.post(settings.transferServer, askParams)
                .then((transferRes) => {
                    // console.log('---transferRes--11-', transferRes);
                    if (transferRes.status == 200) {
                        // console.log('--transferRes----', transferRes.data);
                        let transferState = transferRes.data;
                        if (transferState && transferState.result) {
                            let logData = _.assign({}, askParams, {
                                transactionHash: transferState.transactionHash
                            });
                            logUtil.info('transfer success', JSON.stringify(logData));
                            resolve(transferState.transactionHash);
                        } else {
                            logUtil.info('transfer failed', JSON.stringify(askParams));
                            resolve('');
                        }

                    } else {
                        resolve('');
                    }
                }).catch((err) => {
                    console.log('-transfer failed---', err);
                    logUtil.error(err, {});
                    resolve('');
                })
        })
    },

    // 文档上链接口
    upChain: (contentId) => {
        return new Promise(async (resolve, reject) => {
            let askParams = {
                "contentsid": contentId
            }
            logUtil.info('up chain begin', JSON.stringify(askParams));
            axios.post(settings.upChainServer, askParams)
                .then((transferRes) => {
                    console.log('---transferRes--11-', transferRes);
                    if (transferRes.status == 200) {
                        logUtil.info('up chain success', JSON.stringify(transferRes.data));
                        resolve('1');
                    } else {
                        logUtil.info('up chain failed', JSON.stringify(transferRes.data));
                        resolve('0');
                    }
                }).catch((err) => {
                    console.log('-up chain---', err.message);
                    logUtil.error(err, {});
                    resolve('0');
                })
        })
    },

    // mec充值
    transferMec: (walletAddress, coins) => {
        return new Promise(async (resolve, reject) => {
            let askParams = {
                "address": walletAddress,
                "value": coins
            }
            // console.log('---askParams--', askParams)
            logUtil.info('transfer begin', JSON.stringify(askParams));
            axios.post(settings.transferMecServer, askParams)
                .then((transferRes) => {
                    // console.log('---transferRes--11-', transferRes);
                    if (transferRes.status == 200) {
                        // console.log('--transferRes----', transferRes.data);
                        if (transferRes.data) {
                            logUtil.info('transfer success', JSON.stringify(askParams));
                            resolve('1');
                        } else {
                            logUtil.info('transfer failed', JSON.stringify(askParams));
                            resolve('0');
                        }
                    } else {
                        resolve('0');
                    }
                }).catch((err) => {
                    // console.log('-transfer failed---', err);
                    logUtil.error(err, {});
                    resolve('0');
                })
        })
    },

    // mec打赏
    transferMecByUser: (walletAddress, password, to, coins) => {
        return new Promise(async (resolve, reject) => {
            let askParams = {
                "from": walletAddress,
                "passwd": password,
                "to": to,
                "value": coins
            }
            // console.log('---askParams--', askParams)
            logUtil.info('transfer begin', JSON.stringify(askParams));
            // console.log('----server url---', settings.transferMecByUser)
            axios.post(settings.transferMecByUser, askParams)
                .then((transferRes) => {
                    // console.log('---transferRes--11-', transferRes);
                    if (transferRes.status == 200) {
                        // console.log('--transferRes----', transferRes.data);
                        if (transferRes.data) {
                            logUtil.info('transfer success', JSON.stringify(askParams));
                            resolve('1');
                        } else {
                            logUtil.info('transfer failed', JSON.stringify(askParams));
                            resolve('0');
                        }
                    } else {
                        resolve('0');
                    }
                }).catch((err) => {
                    console.log('-打赏MEC失败---', err);
                    logUtil.error(err, {});
                    resolve('0');
                })
        })
    },

    // mec购买大师课
    buyClassByMec: (walletAddress, coins) => {
        return new Promise(async (resolve, reject) => {
            let askParams = {
                "address": walletAddress,
                "value": coins
            }
            console.log('---askParams--', askParams)
            logUtil.info('transfer begin', JSON.stringify(askParams));
            console.log('----server url---', settings.transferMecByUser)
            axios.post(settings.buyClassMecApi, askParams)
                .then((transferRes) => {
                    // console.log('---transferRes--11-', transferRes);
                    if (transferRes.status == 200) {
                        // console.log('--transferRes----', transferRes.data);
                        if (transferRes.data) {
                            logUtil.info('buy class success', JSON.stringify(askParams));
                            resolve('1');
                        } else {
                            logUtil.info('buy class failed', JSON.stringify(askParams));
                            resolve('0');
                        }
                    } else {
                        resolve('0');
                    }
                }).catch((err) => {
                    console.log('-buy class error---', err);
                    logUtil.error(err, {});
                    resolve('0');
                })
        })
    },


    // 验证码接口
    sendTellMessagesByPhoneNum(countryCode = '86', phoneNum, targetCode) {
        let contentCode = countryCode.toString() == '86' ? 'SMS_10255048' : 'SMS_10241177';
        const accessKeyId = settings.accessKeyId;
        const secretAccessKey = settings.accessKeySecret;
        //初始化sms_client
        let smsClient = new SMSClient({
            accessKeyId,
            secretAccessKey
        })

        // send sms
        smsClient.sendSMS({
            PhoneNumbers: countryCode.toString() + phoneNum.toString(),
            ExternalId: 'E001203311',
            ContentCode: contentCode,
            ContentParam: '{"code":' + targetCode + '}',
            SignName: '大师链'
        }).then(function (res) {
            let {
                ResultCode
            } = res;
            if (ResultCode === 'OK') {
                // parse res
                console.log('send sms:', res);
                logUtil.info('send sms:', res)
            }
        }, function (err) {
            console.log('err:', err)
            logUtil.error(err)
        })
    },
    clearUserSensitiveInformation(targetObj) {
        if (!_.isEmpty(targetObj)) {
            targetObj.password && delete targetObj.password;
            targetObj.fundPassword && delete targetObj.fundPassword;
            targetObj.walletAddressPassword && delete targetObj.walletAddressPassword;
            targetObj.countryCode && delete targetObj.countryCode;
            targetObj.phoneNum && delete targetObj.phoneNum;
            targetObj.email && delete targetObj.email;
            targetObj.walletAddress && delete targetObj.walletAddress;
            targetObj.watchSpecials && delete targetObj.watchSpecials;
            targetObj.watchCommunity && delete targetObj.watchCommunity;
            targetObj.praiseCommunityContent && delete targetObj.praiseCommunityContent;
            targetObj.praiseMessages && delete targetObj.praiseMessages;
            targetObj.praiseContents && delete targetObj.praiseContents;
            targetObj.favoriteCommunityContent && delete targetObj.favoriteCommunityContent;
            targetObj.favorites && delete targetObj.favorites;
            targetObj.despiseCommunityContent && delete targetObj.despiseCommunityContent;
            targetObj.despiseMessage && delete targetObj.despiseMessage;
            targetObj.despises && delete targetObj.despises;
            targetObj.watchers && delete targetObj.watchers;
            targetObj.followers && delete targetObj.followers;
        }
    },
    clearRedisByType(str, cacheKey) {
        console.log('cacheStr', str);
        let currentKey = settings.session_secret + cacheKey + str;
        cache.set(currentKey, '', 2000);
    },

    // 获取vip有效期
    getVipTime(userInfo = {}) {
        return new Promise(async (resolve, reject) => {
            let isVip = userInfo.vip,
                endTime = '';
            try {
                // console.log('-userInfo---', userInfo)
                if (isVip) {
                    let totalOpenRecord = await OpenVipRecord.aggregate([{
                            $match: {
                                user: userInfo._id
                            }
                        },
                        {
                            $group: {
                                _id: null,
                                total_month: {
                                    $sum: "$time"
                                }
                            }
                        }
                    ]);

                    // console.log('-totalOpenRecord---', totalOpenRecord)

                    let throwMonth = totalOpenRecord.length > 0 ? totalOpenRecord[0].total_month : 0;

                    let orderInfo = await OpenVipRecord.findOne({
                        user: userInfo._id,
                        type: '0'
                    });
                    // console.log('-orderInfo---', orderInfo)
                    if (!_.isEmpty(orderInfo)) {
                        let beginDate = orderInfo.date;
                        let beginYear = beginDate.split('-')[0];
                        let beginMonth = beginDate.split('-')[1];

                        // let throwMonth = orderInfo.setMeal.time;
                        if ((Number(beginMonth) + Number(throwMonth)) > 12) {
                            beginYear = Number(beginYear) + 1;
                            beginMonth = Number(beginMonth) + Number(throwMonth) - 12;
                        } else {
                            beginMonth = Number(beginMonth) + Number(throwMonth);
                        }
                        endTime = beginYear + '-' + beginMonth + beginDate.substr(7);
                    }
                }
                resolve(endTime);
            } catch (error) {
                logUtil.error(error, {})
                resolve('');
            }
        })

    },

    // 兼容手机号为0的验证码
    async adapterZeroPhoneNumMessageCode(currentMessageCode = '', user = {}, messageType) {
        let currentMessageCode1 = currentMessageCode;
        try {
            if (user.phoneNum.indexOf('0') === 0) {
                let cutZPhoneNum = (user.phoneNum).substr(1);
                currentMessageCode1 = await this.getCacheValueByKey(settings.session_secret + messageType + (user.countryCode + cutZPhoneNum));
            }
            return currentMessageCode1;
        } catch (error) {
            return currentMessageCode1;
        }
    },

    // 跳转到提示页面
    async directToErrorPage(req, res, next) {
        try {
            let noticeTempPath = settings.SYSTEMTEMPFORDER + 'dorawhite/users/userNotice.html';
            let localKeys = await this.getSiteLocalKeys(req.session.locale, res);
            // console.log(noticeTempPath, '--localKeys--', localKeys)
            let renderData = {
                pageType: "notice",
                infoType: "warning",
                infoContent: req.query.message || '',
                // staticforder: 'dorawhite',
                lk: localKeys.renderKeys
            }
            res.render(noticeTempPath, renderData);
        } catch (error) {
            logUtil.error(error, req);
            next();
        }
    },

    async checkAdminUserSessionForApi(req, res, next) {
        if (req.method == 'GET') {
            try {
                let userToken = req.query.token;
                if (userToken) {
                    req.session.adminUserInfo = "";
                    // console.log('--req.query.token--')
                    let checkToken = await authToken.checkToken(req.query.token);
                    // console.log('--checkToken--', checkToken)
                    if (checkToken) {
                        let targetUser = await AdminUser.findOne({
                            userName: checkToken.userName,
                            password: checkToken.password
                        }, siteFunc.getAuthUserFields('session'));
                        if (!_.isEmpty(targetUser)) {
                            console.log('adminuser had login,request from ', req.query.useClient);
                            req.session.adminUserInfo = targetUser;
                            next();
                        } else {
                            throw new siteFunc.UserException(res.__("label_notice_asklogin"));
                        }
                    } else {
                        throw new siteFunc.UserException(res.__("label_notice_asklogin"));
                    }
                } else {
                    throw new siteFunc.UserException(res.__("label_notice_asklogin"));
                }
            } catch (error) {
                logUtil.error(error, req);
                res.send({
                    status: 401,
                    message: res.__("label_notice_asklogin")
                });
            }
        } else if (req.method == 'POST') {
            // 添加鉴权识别参数
            req.query.authAdminUser = true;
            next();
        }
    },

    // 校验商城操作token
    checkAdminPostToken(req, res, token = '') {
        return new Promise(async (resolve, reject) => {
            if (req.query.authAdminUser) {
                if (!token) { // PC 前端
                    reject(res.__("label_notice_asklogin"))
                } else {
                    req.query.useClient = '3';
                    let checkToken = await authToken.checkToken(token);
                    if (checkToken) {
                        let targetUser = await AdminUser.findOne({
                            userName: checkToken.userName,
                            password: checkToken.password
                        });
                        if (!_.isEmpty(targetUser)) {
                            console.log('adminuser had login, request from shop');
                            req.session.adminUserInfo = targetUser;
                            resolve();
                        } else {
                            reject(res.__("label_notice_asklogin"))
                        }
                    } else {
                        reject(res.__("label_notice_asklogin"))
                    }
                }
            }
            resolve();
        })

    },

    // 校验大师认证步骤
    async checkIdentifyStep(req, res, next) {

        try {
            let userInfo = req.session.user;
            let step = req.query.step;
            let targetUser = await User.findOne({
                _id: userInfo._id
            }, siteFunc.getAuthUserFields('session'));
            let targetIdentify = await IdentityAuthentication.findOne({
                user: userInfo._id,
                type: '2'
            });

            if (step == '1') {
                // 大师不用再次认证
                if (targetUser.group == '1') {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                } else {
                    next();
                }
            } else if (step == '2') {
                if (_.isEmpty(targetIdentify)) {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }
                // 第一步还没走，不能走第二步
                if (targetIdentify.authStep != '1') {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }
                next();
            } else if (step == '3') {
                // 第一步还没走，不能走第二步
                if (targetIdentify.authStep != '2') {
                    throw new siteFunc.UserException(res.__('validate_error_params'));
                }
                next();
            } else {
                throw new siteFunc.UserException(res.__('validate_error_params'));
            }

        } catch (error) {
            logUtil.error(error, req);
            res.redirect('/users/userCenter')
        }

    },

    // 获取积分配置
    async getIntegralonfig() {
        try {
            // 原始默认值
            let oldConfigs = {
                "login_first": 200,
                "register": 200,
                "browse": 3,
                "browse_limit": 30,
                "give_thumbs_up": 5,
                "give_thumbs_up_limit": 20,
                "comment": 10,
                "comment_limit": 10,
                "community_comment": 10,
                "community_comment_limit": 10,
                "appreciate": 30,
                "appreciate_limit": 10,
                "appreciate_in": 0,
                "appreciate_out": 0,
                "collections": 10,
                "follow": 50,
                "follow_limit": 10,
                "collections_limit": 10,
                "forward": 10,
                "forward_limit": 20,
                "community_building": 10,
                "community_building_limit": 5,
                "invitation": 100,
                "report": 25,
                "review": 10,
                "creat_content": 200,
                "creat_identity": 300,
                "shop_send": 0,
                "transfer_coins": 16,
                "recharge": 17,
                "subscribe": 18,
                "buyVip": 19,
                "signIn": 10,
                "signIn_continuity": 50,
                "brithday": 100
            };
            let targetConfig = await Integralonfig.find({});
            if (!_.isEmpty(targetConfig) && targetConfig.length == 1) {
                // console.log('--targetConfig[0]--', targetConfig[0]);
                return targetConfig[0];
            } else {
                // console.log('----xx--', oldConfigs)
                return oldConfigs;
            }
        } catch (error) {
            logUtil.error(error, {})
            return oldConfigs;
        }
    },


    async jsPushFunc(activeUser, passiveUser, pushType = '', pushId = "") {
        try {

            // console.log('--activeUser--', activeUser);
            // console.log('--passiveUser--', passiveUser);
            // console.log('--pushType--', pushType);

            if (_.isEmpty(activeUser) || !passiveUser) {
                throw new siteFunc.UserException(res.__('validate_error_params'));
            }

            if (activeUser._id == passiveUser) {
                throw new siteFunc.UserException(res.__('validate_error_params'));
            }

            if (!pushType) {
                throw new siteFunc.UserException(res.__('validate_error_params'));
            }

            // 默认值
            let switchParams = {
                open: false,
                commentOrReply: false,
                follow: false,
                followCommunity: false,
                followSpecial: false,
                thumbsUpComments: false,
                thumbsUpContent: false,
                appreciateContent: false,
                subscribeClass: false,
                followMasterClass: false,
                followMasterContent: false
            };


            let systemSwitchConfig = await Switches.findOne({
                user: passiveUser
            });

            if (!_.isEmpty(systemSwitchConfig)) {
                switchParams = systemSwitchConfig;
            }

            // console.log('--switchParams--', switchParams);
            // console.log('--pushType--', pushType);

            if (pushType == 'commentOrReply' && switchParams.commentOrReply ||
                pushType == 'follow' && switchParams.follow ||
                pushType == 'followCommunity' && switchParams.followCommunity ||
                pushType == 'followSpecial' && switchParams.followSpecial ||
                pushType == 'thumbsUpComments' && switchParams.thumbsUpComments ||
                pushType == 'thumbsUpContent' && switchParams.thumbsUpContent ||
                pushType == 'appreciateContent' && switchParams.appreciateContent ||
                pushType == 'subscribeClass' && switchParams.subscribeClass ||
                pushType == 'followMasterClass' && switchParams.followMasterClass ||
                pushType == 'followMasterContent' && switchParams.followMasterContent
            ) {

                let pushContent = '';

                if (pushType == 'commentOrReply') {
                    pushContent = '评论了你';
                } else if (pushType == 'follow') {
                    pushContent = '关注了你';
                } else if (pushType == 'followCommunity') {
                    pushContent = '关注了你的社群';
                } else if (pushType == 'followSpecial') {
                    pushContent = '关注了你的专题';
                } else if (pushType == 'thumbsUpComments') {
                    pushContent = '赞了你的评论';
                } else if (pushType == 'thumbsUpContent') {
                    pushContent = '赞了你的文章';
                } else if (pushType == 'appreciateContent') {
                    pushContent = '打赏你的文章';
                } else if (pushType == 'subscribeClass') {
                    pushContent = '订阅了你的课程';
                } else if (pushType == 'followMasterClass') {
                    pushContent = '发布了新的课程';
                } else if (pushType == 'followMasterContent') {
                    pushContent = '发布了新的文章';
                }


                pushContent = activeUser.userName + ' ' + pushContent;
                console.log('--pushContent---', pushContent);
                let targetAlis = passiveUser.replace(/-/g, "_");
                console.log('--targetAlis---', targetAlis);

                // 获取未读消息总数
                let noReadTotalMessagesNo = await SiteMessage.count({
                    isRead: false,
                    $or: [{
                            type: '1'
                        },
                        {
                            type: '2'
                        },
                        {
                            type: '3'
                        },
                    ],
                    passiveUser: passiveUser
                });

                console.log('--noReadTotalMessagesNo--', noReadTotalMessagesNo);

                clientPush.push().setPlatform(JPush.ALL)
                    .setAudience(JPush.alias(targetAlis))
                    .setNotification('MasterChain', JPush.ios(pushContent, 'happy', noReadTotalMessagesNo, true, {
                        'contentId': pushId,
                        'contentType': pushType
                    }), JPush.android(pushContent, null, 1, {
                        'contentId': pushId,
                        'contentType': pushType
                    }))
                    .setOptions(null, 60)
                    .send()
                    .then(function (result) {
                        console.log('push success', result);
                    }).catch(function (err) {
                        console.log('push error', err);
                    })

            }

        } catch (error) {
            logUtil.error(error, {});
        }
    },

    pushTest(req, res) {
        try {
            clientPush.push().setPlatform(JPush.ALL)
                .setAudience(JPush.alias('slB0HS6rq'))
                .setNotification('Hi, JPush', JPush.ios('ios alert 3', 'happy', 5, true, {
                    'contentId': '1111',
                    'contentType': '222'
                }), JPush.android('sssss', null, 1, {
                    'contentId': '1111',
                    'contentType': '222'
                }))
                .setOptions(null, 60)
                .send()
                .then(function (result) {
                    console.log(result);
                    res.send({
                        status: 200
                    })
                }).catch(function (err) {
                    console.log(err)
                    res.send({
                        status: 500
                    })
                })
        } catch (error) {
            console.log('--', error);
            res.send({
                status: 500
            })
        }
    },

    getRenderQueryObjForAudio(appVersion, useClient, queryObj = {}) {
        if (useClient == '2' && (!appVersion || appVersion && Number(appVersion)) < 230) {
            queryObj.type = {
                $nin: ['3', '4']
            }
        }
        return queryObj;
    }
};
module.exports = siteFunc;