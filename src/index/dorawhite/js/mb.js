$(function () {
    // $('.areaCode').text('中国0086');
    $('.areaCode').click(function () {
        $('.area_code_list').show();
    })
    $('.area_code_list dd').click(function () {

        var thisVal = $(this).find('span').text();
        $('.areaCode').text(thisVal);
        $('.area_code_list').hide();

    });
})

function getAjaxData(url, success = () => {}, type = 'get', params = {}) {
    layer.open({
        type: 2,
        shadeClose: false
    });
    let baseParams = {
        url: url,
        type: type.toLocaleUpperCase(),
        success: function (result) {
            layer.closeAll();
            if (result.status === 500) {
                layer.open({
                    content: result.message,
                    skin: 'msg',
                    time: 2,
                    anim: false
                });

            } else if (result.status === 401) {
                layer.open({
                    content: result.message,
                    btn: '确定',
                    shadeClose: false,
                    yes: function (index) {
                        layer.close(index);
                    }
                });
            } else {
                success && success(result);
            }
        },
        error: function (d) {
            console.log('error:', d)
            layer.open({
                content: d.message,
                skin: 'msg',
                time: 2,
                anim: false
            });
        }
    };
    if (type == 'post') {
        baseParams = Object.assign({}, baseParams, {
            contentType: 'application/json; charset=utf-8',
            traditional: true,
            data: JSON.stringify(params),
        })
    } else {
        if (url.indexOf('?') >= 0) {
            baseParams.url = url;
        } else {
            baseParams.url = url;
        }
    }
    $.ajax(baseParams);
}

// 获取url参数
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg); //search,查询？后面的参数，并匹配正则
    if (r != null) return unescape(r[2]);
    return null;
}



/**
 * 
 * reg avalon controller
 * 
 */

var regVm = avalon.define({
    $id: 'regAction',
    phoneNum: '',
    password: '',
    countryCode: '',
    message: '',
    showErr: false,
    basetime: 120,
    targetPanel: '1',
    reset: function () {
        regVm.phoneNum = " ";
        regVm.messageCode = " ";
    },
    getCountryCode: () => {
        // var areaInfo = $('.selected-flag').attr('title');
        // var areano = areaInfo.split('+')[1];
        // var thisVal = $('.area_code_list dd').find('span').text();
        let areano = $('#areaCode').text().split('00')[1];
        return areano;
    },
    choices: [],
    messageCode: '',
    lateResendTxt: ' s',
    reSendBtnTxt: '重新发送',
    showSuccess: () => {
        let wanthtml = `<div class="top-bg"><div class="success-tips"></div></div><h1 class="title">您已注册成功</h1><p class="content">MBT奖励已经送至您的账户，您可以进入WEB网页版立即使用，也可以进一步由网页上方的下载APP按钮下载大师链APP进行完整体验。</p>`;
        layer.open({
            className: 'reg-success',
            content: wanthtml,
            btn: '进入WEB网页版',
            shadeClose: false,
            yes: function () {
                window.location.href = '/';
                layer.closeAll();
            }
        });
    },
    openUserAgreement: (type) => {
        getAjaxData('/api/v0/system/getHelp?helpType=' + type, (result) => {
            if (result.status == 200) {
                let agreementObj = result.data.docs[0];
                if (agreementObj) {
                    layer.open({
                        className: 'agreement',
                        content: agreementObj.comments
                    });
                }
            }
        })
    },
    sendMsgCode: function () {
        if (regVm.basetime != 120) {
            return false;
        }
        if (!/^[0-9]*$/.test(regVm.phoneNum)) {
            layer.open({
                content: '请输入正确的手机号',
                skin: 'msg',
                time: 2,
                anim: false
            });
            return false
        }
        if (!regVm.password) {
            layer.open({
                content: '请输入正确的密码',
                skin: 'msg',
                time: 2,
                anim: false
            });
            return false
        }

        $('#get-v-code').prop('disabled', 'disabled')
        // 开始发短信
        var smsParams = {
            phoneNum: regVm.phoneNum,
            countryCode: regVm.getCountryCode() || '86',
            messageType: '0',
        }
        getAjaxData('/api/v0/user/sendVerificationCode', function (data) {
            layer.open({
                content: '已发送验证码',
                skin: 'msg',
                time: 2,
                anim: false
            });
            var mytask = setInterval(function () {
                $('#sendYzm').html(--regVm.basetime + regVm.lateResendTxt);
            }, 1000)
            setTimeout(function () {
                clearInterval(mytask)
                $('#sendYzm').removeAttr('disabled');
                $('#sendYzm').html(regVm.reSendBtnTxt);
                regVm.basetime = 120;
                clearMessageCode();
            }, 1000 * regVm.basetime);
        }, 'post', smsParams)

    },
    validate: {
        onError: function (reasons) {
            reasons.forEach(function (reason) {
                console.log(reason.getMessage())
            })
        },
        onValidateAll: function (reasons) {
            if (reasons.length) {
                console.log('有表单没有通过', reasons)
                layer.open({
                    content: reasons[0].message,
                    skin: 'msg',
                    time: 2,
                    anim: false
                });

            } else {
                if (regVm.choices != '1') {
                    layer.open({
                        content: "请先同意大师链协议",
                        skin: 'msg',
                        time: 2,
                        anim: false
                    });
                    return false;
                }
                let uid = getQueryString('uid');
                if (!uid) {
                    layer.open({
                        content: "请通过正规途径获取邀请链接！",
                        skin: 'msg',
                        time: 2,
                        anim: false
                    });
                    return false;
                }
                console.log('全部通过');
                var params = {
                    password: regVm.password,
                    phoneNum: regVm.phoneNum,
                    messageCode: regVm.messageCode,
                    countryCode: regVm.getCountryCode() || '86',
                    uid
                }
                getAjaxData('/api/v0/user/doReg', (data) => {
                    if (data.status == 200) {
                        regVm.password = "";
                        regVm.phoneNum = "";
                        regVm.messageCode = "";
                        regVm.countryCode = "";
                        let wanthtml = `<div class="top-bg"><div class="success-tips"></div></div><h1 class="title">您已注册成功</h1><p class="content">MBT奖励已经送至您的账户，您可以进入WEB网页版立即使用，也可以进一步由网页上方的下载APP按钮下载大师链APP进行完整体验。</p>`;
                        layer.open({
                            className: 'reg-success',
                            content: wanthtml,
                            btn: '进入WEB网页版',
                            shadeClose: false,
                            yes: function () {
                                window.location.href = '/'
                            }
                        });
                    }
                }, 'post', params);

            }
        }
    }
})