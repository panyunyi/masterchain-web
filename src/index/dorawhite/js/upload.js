var uploaderVm = avalon.define({
    $id: 'userUploader',
})

var getFileBlob = function (url, cb) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.responseType = "blob";
    xhr.addEventListener('load', function () {
        cb(xhr.response);
    });
    xhr.send();
};

var blobToFile = function (blob, name) {
    blob.lastModifiedDate = new Date();
    blob.name = name;
    return blob;
};

var getFileObject = function (filePathOrUrl, cb) {
    getFileBlob(filePathOrUrl, function (blob) {
        cb(blobToFile(blob, 'test.jpg'));
    });
};

window.uploaderArr = {};

function addOldUploadImages(uploaders, picList) {
    //需要编辑的图片列表
    // var picList = ['图片url', '图片url', '图片url', '图片url']
    if (typeof picList == 'object' && picList.length > 0) {
        $.each(picList, function (index, item) {
            getFileObject(item, function (fileObject) {
                var wuFile = new WebUploader.Lib.File(WebUploader.guid('rt_'), fileObject);
                var file = new WebUploader.File(wuFile);
                uploaders.addFiles(file)
            })
        });
    }
}

function initUploader(outDomId) {

    var $wrap = $('#' + outDomId),

        // 图片容器
        $queue = $('<ul class="filelist"></ul>')
        .appendTo($wrap.find('.queueList')),

        // 状态栏，包括进度和控制按钮
        $statusBar = $wrap.find('.statusBar'),

        // 文件总体选择信息。
        $info = $statusBar.find('.info'),

        // 上传按钮
        $upload = $wrap.find('.beginUpload'),

        // 没选择文件之前的内容。
        $placeHolder = $wrap.find('.placeholder'),

        $progress = $statusBar.find('.progress').hide(),

        // 添加的文件数量
        fileCount = 0,

        // 添加的文件总大小
        fileSize = 0,

        // 优化retina, 在retina下这个值是2
        ratio = window.devicePixelRatio || 1,

        // 缩略图大小
        thumbnailWidth = 110 * ratio,
        thumbnailHeight = 110 * ratio,

        // 可能有pedding, ready, uploading, confirm, done.
        state = 'pedding',

        // 所有文件的进度信息，key为file id
        percentages = {},
        // 判断浏览器是否支持图片的base64
        isSupportBase64 = (function () {
            var data = new Image();
            var support = true;
            data.onload = data.onerror = function () {
                if (this.width != 1 || this.height != 1) {
                    support = false;
                }
            }
            data.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
            return support;
        })(),

        // 检测是否已经安装flash，检测flash的版本
        flashVersion = (function () {
            var version;

            try {
                version = navigator.plugins['Shockwave Flash'];
                version = version.description;
            } catch (ex) {
                try {
                    version = new ActiveXObject('ShockwaveFlash.ShockwaveFlash')
                        .GetVariable('$version');
                } catch (ex2) {
                    version = '0.0';
                }
            }
            version = version.match(/\d+/g);
            return parseFloat(version[0] + '.' + version[1], 10);
        })(),

        supportTransition = (function () {
            var s = document.createElement('p').style,
                r = 'transition' in s ||
                'WebkitTransition' in s ||
                'MozTransition' in s ||
                'msTransition' in s ||
                'OTransition' in s;
            s = null;
            return r;
        })(),

        // WebUploader实例
        uploader;

    if (!WebUploader.Uploader.support('flash') && WebUploader.browser.ie) {

        // flash 安装了但是版本过低。
        if (flashVersion) {
            (function (container) {
                window['expressinstallcallback'] = function (state) {
                    switch (state) {
                        case 'Download.Cancelled':
                            layer.msg(getSysValueByKey('label_uploader_cancelled'), {
                                icon: 7
                            });
                            break;

                        case 'Download.Failed':
                            layer.msg(getSysValueByKey('label_uploader_failed'), {
                                icon: 7
                            });
                            break;

                        default:
                            layer.msg(getSysValueByKey('label_uploader_downSuccess'), {
                                icon: 7
                            });
                            break;
                    }
                    delete window['expressinstallcallback'];
                };

                var swf = './expressInstall.swf';
                // insert flash object
                var html = '<object type="application/' +
                    'x-shockwave-flash" data="' + swf + '" ';

                if (WebUploader.browser.ie) {
                    html += 'classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" ';
                }

                html += 'width="100%" height="100%" style="outline:0">' +
                    '<param name="movie" value="' + swf + '" />' +
                    '<param name="wmode" value="transparent" />' +
                    '<param name="allowscriptaccess" value="always" />' +
                    '</object>';

                container.html(html);

            })($wrap);

            // 压根就没有安转。
        } else {
            $wrap.html('<a href="http://www.adobe.com/go/getflashplayer" target="_blank" border="0"><img alt="get flash player" src="http://www.adobe.com/macromedia/style_guide/images/160x41_Get_Flash_Player.jpg" /></a>');
        }

        return;
    } else if (!WebUploader.Uploader.support()) {
        layer.msg(getSysValueByKey('label_uploader_notSupport'), {
            icon: 7
        });
        return;
    }

    // 实例化
    uploader = WebUploader.create({
        pick: {
            id: '#filePicker_' + outDomId,
            label: '选择文件'
        },
        formData: {
            keywords: uploaderVm.keywords
        },
        dnd: '#dndArea_' + outDomId,
        paste: '#uploader_' + outDomId,
        swf: '/plugins/webuploader/Uploader.swf',
        chunked: false,
        chunkSize: 512 * 1024,
        server: '/api/v0/upload/files?type=images',
        // runtimeOrder: 'flash',

        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/*'
        },

        // 禁掉全局的拖拽功能。这样不会出现图片拖进页面的时候，把图片打开。
        disableGlobalDnd: true,
        fileNumLimit: 2,
        fileSizeLimit: 15 * 1024 * 1024, // 15 M
        fileSingleSizeLimit: 5 * 1024 * 1024 // 5 M
    });

    // 拖拽时不接受 js, txt 文件。
    uploader.on('dndAccept', function (items) {
        var denied = false,
            len = items.length,
            i = 0,
            // 修改js类型
            unAllowed = 'text/plain;application/javascript ';

        for (; i < len; i++) {
            // 如果在列表里面
            if (~unAllowed.indexOf(items[i].type)) {
                denied = true;
                break;
            }
        }

        return !denied;
    });

    uploader.on('dialogOpen', function () {
        console.log('here');
    });

    // 添加“添加文件”的按钮，
    uploader.addButton({
        id: '#filePicker2_' + outDomId,
        label: '增加'
    });

    uploader.on('ready', function () {
        // window.uploader = uploader;
        initIdentyInfo(outDomId, function (oldImgArr) {

            let imgArr = []
            if (outDomId == 'uploadXueli') {
                imgArr = identityAuthStepTwoVm.diploma;
            } else if (outDomId == 'attachments') {
                imgArr = identityAuthStepTwoVm.uploadAttachments;
            } else if (outDomId == 'uploadZhiye') {
                imgArr = identityAuthStepTwoVm.professionalCertificate;
            } else if (outDomId == 'uploadOthers') {
                imgArr = identityAuthStepTwoVm.otherCertificate;
            } else if (outDomId == 'uploadIdCard') {
                imgArr = oldImgArr;
            }

            if (imgArr && imgArr.length > 0) {
                addOldUploadImages(uploader, imgArr);
            }

        })


    });

    // 当有文件添加进来时执行，负责view的创建
    function addFile(file) {
        var $li = $('<li id="' + file.id + '">' +
                '<p class="title">' + file.name + '</p>' +
                '<p class="imgWrap"></p>' +
                '<p class="progress"><span></span></p>' +
                '</li>'),

            $btns = $('<div class="file-panel">' +
                // '<span class="cancel">删除</span>' +
                '<span class="rotateRight">向右旋转</span>' +
                '<span class="rotateLeft">向左旋转</span></div>').appendTo($li),
            $prgress = $li.find('p.progress span'),
            $wrap = $li.find('p.imgWrap'),
            $info = $('<p class="error"></p>'),

            showError = function (code) {
                switch (code) {
                    case 'exceed_size':
                        text = getSysValueByKey('label_uploader_largeFile');
                        break;

                    case 'interrupt':
                        text = getSysValueByKey('label_uploader_suspendUpload');
                        break;

                    default:
                        text = getSysValueByKey('label_uploader_uploadFailed');
                        break;
                }

                $info.text(text).appendTo($li);
            };

        if (file.getStatus() === 'invalid') {
            showError(file.statusText);
        } else {
            // @todo lazyload
            $wrap.text(getSysValueByKey('label_uploader_previewImg'));
            uploader.makeThumb(file, function (error, src) {
                var img;

                if (error) {
                    // $wrap.text('Not Preview');
                    $wrap.text('');
                    return;
                }

                if (isSupportBase64) {
                    img = $('<img src="' + src + '">');
                    $wrap.empty().append(img);
                } else {
                    $.ajax('/api/v0/upload/files?type=images', {
                        method: 'POST',
                        data: src,
                        dataType: 'json'
                    }).done(function (response) {
                        if (response.result) {
                            img = $('<img src="' + response.result + '">');
                            $wrap.empty().append(img);
                        } else {
                            $wrap.text(getSysValueByKey('label_uploader_previewError'));
                        }
                    });
                }
            }, thumbnailWidth, thumbnailHeight);

            percentages[file.id] = [file.size, 0];
            file.rotation = 0;
        }

        file.on('statuschange', function (cur, prev) {
            if (prev === 'progress') {
                $prgress.hide().width(0);
            } else if (prev === 'queued') {
                $li.off('mouseenter mouseleave');
                $btns.remove();
            }

            // 成功
            if (cur === 'error' || cur === 'invalid') {
                console.log(file.statusText);
                showError(file.statusText);
                percentages[file.id][1] = 1;
            } else if (cur === 'interrupt') {
                showError('interrupt');
            } else if (cur === 'queued') {
                $info.remove();
                $prgress.css('display', 'block');
                percentages[file.id][1] = 0;
            } else if (cur === 'progress') {
                $info.remove();
                $prgress.css('display', 'block');
            } else if (cur === 'complete') {
                $prgress.hide().width(0);
                $li.append('<span class="success"></span>');
            }

            $li.removeClass('state-' + prev).addClass('state-' + cur);
        });

        $li.on('mouseenter', function () {
            $btns.stop().animate({
                height: 30
            });
        });

        $li.on('mouseleave', function () {
            $btns.stop().animate({
                height: 0
            });
        });

        $btns.on('click', 'span', function () {
            var index = $(this).index(),
                deg;

            switch (index) {
                case 0:
                    uploader.removeFile(file);
                    return;

                case 1:
                    file.rotation += 90;
                    break;

                case 2:
                    file.rotation -= 90;
                    break;
            }

            if (supportTransition) {
                deg = 'rotate(' + file.rotation + 'deg)';
                $wrap.css({
                    '-webkit-transform': deg,
                    '-mos-transform': deg,
                    '-o-transform': deg,
                    'transform': deg
                });
            } else {
                $wrap.css('filter', 'progid:DXImageTransform.Microsoft.BasicImage(rotation=' + (~~((file.rotation / 90) % 4 + 4) % 4) + ')');
            }


        });

        $li.appendTo($queue);
    }

    // 负责view的销毁
    function removeFile(file) {
        var $li = $('#' + outDomId + ' #' + file.id);

        delete percentages[file.id];
        updateTotalProgress();
        $li.off().find('.file-panel').off().end().remove();
    }

    function updateTotalProgress() {
        var loaded = 0,
            total = 0,
            spans = $progress.children(),
            percent;

        $.each(percentages, function (k, v) {
            total += v[0];
            loaded += v[0] * v[1];
        });

        percent = total ? loaded / total : 0;


        spans.eq(0).text(Math.round(percent * 100) + '%');
        spans.eq(1).css('width', Math.round(percent * 100) + '%');
        updateStatus();
    }

    function updateStatus() {
        var text = '',
            stats;

        if (state === 'ready') {
            text = getSysValueByKey('label_uploader_selectIng') + fileCount + getSysValueByKey('label_uploader_imgPic') +
                WebUploader.formatSize(fileSize) + '。';
        } else if (state === 'confirm') {
            stats = uploader.getStats();
            if (stats.uploadFailNum) {
                text = getSysValueByKey('label_uploader_hadSuccessUpload') + stats.successNum + getSysValueByKey('label_uploader_picsToAlbum') +
                    stats.uploadFailNum + getSysValueByKey('label_uploader_picsUploadFail') + '，<a class="retry" href="#">' + getSysValueByKey('label_uploader_reUpload') + '</a>' + getSysValueByKey('label_uploader_failPicAnd') + '<a class="ignore" href="#">' + getSysValueByKey('label_uploader_ignore') + '</a>'
            }

        } else {
            stats = uploader.getStats();
            text = getSysValueByKey('label_uploader_total') + fileCount + '' + getSysValueByKey('label_uploader_zhang') + '（' +
                WebUploader.formatSize(fileSize) +
                '），' + getSysValueByKey('label_uploader_hadUpload') + '' + stats.successNum + getSysValueByKey('label_uploader_zhang');

            if (stats.uploadFailNum) {
                text += '，' + getSysValueByKey('label_uploader_failedUpload') + '' + stats.uploadFailNum + getSysValueByKey('label_uploader_zhang');
            }
        }

        $info.html(text);
    }

    function setState(val) {
        var file, stats;

        if (val === state) {
            return;
        }

        $upload.removeClass('state-' + state);
        $upload.addClass('state-' + val);
        state = val;

        switch (state) {
            case 'pedding':
                $placeHolder.removeClass('element-invisible');
                $queue.hide();
                $statusBar.addClass('element-invisible');
                uploader.refresh();
                break;

            case 'ready':
                $placeHolder.addClass('element-invisible');
                $('#' + outDomId + ' #filePicker2_' + outDomId).removeClass('element-invisible');
                $queue.show();
                $statusBar.removeClass('element-invisible');
                uploader.refresh();
                break;

            case 'uploading':
                $('#' + outDomId + ' #filePicker2_' + outDomId).addClass('element-invisible');
                $progress.show();
                $upload.text(getSysValueByKey('label_uploader_suspendUpload'));
                break;

            case 'paused':
                $progress.show();
                $upload.text(getSysValueByKey('label_uploader_continueUpload'));
                break;

            case 'confirm':
                $progress.hide();
                $('#' + outDomId + ' #filePicker2_' + outDomId).removeClass('element-invisible');
                $upload.text(getSysValueByKey('label_uploader_beginUpload'));

                stats = uploader.getStats();
                if (stats.successNum && !stats.uploadFailNum) {
                    setState('finish');
                    return;
                }
                break;
            case 'finish':
                stats = uploader.getStats();
                if (stats.successNum) {
                    layer.msg(getSysValueByKey('label_uploader_hadSuccessUpload'), {
                        icon: 1
                    });
                } else {
                    // 没有成功的图片，重设
                    state = 'done';
                    location.reload();
                }
                break;
        }

        updateStatus();
    }

    uploader.onUploadProgress = function (file, percentage) {
        var $li = $('#' + outDomId + ' #' + file.id),
            $percent = $li.find('.progress span');

        $percent.css('width', percentage * 100 + '%');
        percentages[file.id][1] = percentage;
        updateTotalProgress();
    };

    uploader.onFileQueued = function (file) {
        fileCount++;
        fileSize += file.size;

        if (fileCount === 1) {
            $placeHolder.addClass('element-invisible');
            $statusBar.show();
        }
        addFile(file);
        setState('ready');
        updateTotalProgress();
    };

    uploader.onFileDequeued = function (file) {
        fileCount--;
        fileSize -= file.size;

        if (!fileCount) {
            setState('pedding');
        }

        removeFile(file);
        updateTotalProgress();

    };

    uploader.on('all', function (type) {
        var stats;
        switch (type) {
            case 'uploadFinished':
                setState('confirm');
                break;

            case 'startUpload':
                setState('uploading');
                break;

            case 'stopUpload':
                setState('paused');
                break;

        }
    });

    // 文件上传成功，给item添加成功class, 用样式标记上传成功。
    uploader.on('uploadSuccess', function (file, response) {
        // console.log('--file--', file)
        if (response._raw) {
            var currentRes = JSON.parse(response._raw).data.path;
            if (outDomId == 'uploadIdCard') {
                let indentityImgArr = identityAuthStepOneVm.identityImgArr;
                indentityImgArr.push(currentRes);
            } else if (outDomId == 'uploadXueli') {
                identityAuthStepTwoVm.diploma.push(currentRes);
            } else if (outDomId == 'uploadZhiye') {
                identityAuthStepTwoVm.professionalCertificate.push(currentRes);
            } else if (outDomId == 'uploadOthers') {
                identityAuthStepTwoVm.otherCertificate.push(currentRes);
            } else if (outDomId == 'attachments') {
                identityAuthStepTwoVm.uploadAttachments.push(currentRes);
            }
            console.log('--identityAuthStepTwoVm--', identityAuthStepTwoVm)
        }
    });

    uploader.onError = function (code) {
        // alert('Eroor: ' + code);
        var errMessage = '上传失败，请稍后重试！';
        if (code == 'Q_EXCEED_NUM_LIMIT') {
            errMessage = "文件数量超过上限！";
        } else if (code == 'Q_EXCEED_SIZE_LIMIT') {
            errMessage = "文件大小超过上限！";
        } else if (code == 'Q_TYPE_DENIED') {
            errMessage = "文件类型不正确！";
        }
        layer.msg(errMessage, {
            icon: 7
        });
    };

    $upload.on('click', function () {

        if ($(this).hasClass('disabled')) {
            return false;
        }
        uploader.options.formData.keywords = uploaderVm.keywords;
        if (state === 'ready') {
            uploader.upload();
        } else if (state === 'paused') {
            uploader.upload();
        } else if (state === 'uploading') {
            uploader.stop();
        }

    });

    $info.on('click', '.retry', function () {
        uploader.retry();
    });

    $info.on('click', '.ignore', function () {
        alert('todo');
    });

    $upload.addClass('state-' + state);
    updateTotalProgress();

}

// 当domReady的时候开始初始化
$(function () {

});