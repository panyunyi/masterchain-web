export default {
  route: {
    dashboard: 'トップページ',
    introduction: '简述',
    documentation: '文档',
    guide: '引导页',
    permission: '权限测试页',
    pagePermission: '页面权限',
    directivePermission: '指令权限',
    icons: '图标',
    components: '组件',
    componentIndex: '介绍',
    tinymce: '富文本编辑器',
    markdown: 'Markdown',
    jsonEditor: 'JSON编辑器',
    dndList: '列表拖拽',
    splitPane: 'Splitpane',
    avatarUpload: '头像上传',
    dropzone: 'Dropzone',
    sticky: 'Sticky',
    countTo: 'CountTo',
    componentMixin: '小组件',
    backToTop: '返回顶部',
    dragDialog: '拖拽 Dialog',
    dragKanban: '可拖拽看板',
    charts: '图表',
    keyboardChart: '键盘图表',
    lineChart: '折线图',
    mixChart: '混合图表',
    example: '综合实例',
    nested: '路由嵌套',
    menu1: '菜单1',
    'menu1-1': '菜单1-1',
    'menu1-2': '菜单1-2',
    'menu1-2-1': '菜单1-2-1',
    'menu1-2-2': '菜单1-2-2',
    'menu1-3': '菜单1-3',
    menu2: '菜单2',
    Table: 'Table',
    dynamicTable: '动态Table',
    dragTable: '拖拽Table',
    inlineEditTable: 'Table内编辑',
    complexTable: '综合Table',
    treeTable: '树形表格',
    customTreeTable: '自定义树表',
    tab: 'Tab',
    form: '表单',
    createArticle: '创建文章',
    editArticle: '编辑文章',
    articleList: '文章列表',
    errorPages: '错误页面',
    page401: '401',
    page404: '404',
    errorLog: '错误日志',
    excel: 'Excel',
    exportExcel: 'Export Excel',
    selectExcel: 'Export Selected',
    uploadExcel: 'Upload Excel',
    zip: 'Zip',
    exportZip: 'Export Zip',
    theme: '换肤',
    clipboardDemo: 'Clipboard',
    i18n: '国际化',
    externalLink: '外链',

    systemManage: "システム管理",
    adminUser: "ユーザ管理",
    getAdminUserList: "ユーザーを照会する",
    addNewAdminUser: "新しいユーザーを追加する",
    updateAdminUser: "ユーザ更新",
    delAdminUser: "ユーザーを削除する",
    roleManager: "キャラクター管理",
    getRoleList: "キャスティング",
    addRoles: "新たな役割",
    updateRoles: "キャラクターを更新する",
    delRoles: "キャラクターを削除する",
    resourceManage: "資源管理",
    getResourceList: "資源を取得する",
    addResources: "资源を追加する",
    updateResources: "編集リソース",
    delResources: "資源を削除する",
    sysTermConfig: "システム構成",
    getSystemConfig: "取得システム",
    updateSystemConfig: "システムを更新する",
    integralonfig: "积分配置",
    getIntegralonfig: "获取积分信息",
    updateIntegralonfig: "更新积分配置",
    dataBakManage: "データバックアップ",
    doDataBak: "データバックアップ",
    getDataList: "バックアップデータの取得",
    delDataBak: "バックアップデータを削除します",
    sysTermLogsManage: "ログマネジメント",
    getSystermLogList: "取得日记",
    delSystemLogs: "ログを削除する",
    delSystermLogs: "清明日誌",
    sysTermNoticeManage: "メッセージマネジメント",
    getSysTermNoticeList: "情報を取得する",
    delSysTermNotices: "システムを削除する",
    updateSysTermNotices: "メッセージはすでに読んでいる",
    announcementManage: "公告管理",
    getAnnouncementList: "取得広告",
    delAnnouncement: "削除する",
    updateAnnouncements: "公示編集",
    addAnnouncements: "新システム公告",
    adsManage: "広告管理",
    getAdsList: "広告を取得する",
    delAds: "広告を削除する",
    editAdsFunction: "広告編集",
    getAdsItem: "単一の広告を取得する",
    updateAds: "広告の更新",
    addAds: "新しい広告",
    addAdsFunction: "広告の新",
    addAds: "新しい広告",
    sysTemMain: "システム・ホームページ",
    getSysTermBaseInfo: "サイト情報取得",
    templateManage: "テンプレート編集",
    getTemplateList: "テンプレートを手に",
    getTemplateFileText: "テンプレートを読む",
    updateTemplateFileText: "テンプレートを更新",
    getTempsFromShop: "テンプレート市場を手に",
    installTemp: "テンプレートを取り付ける",
    enableTemp: "テンプレート作成",
    uninstallTemp: "取外しテンプレート",
    templateConfig: "テンプレート配置し",
    getMyTemplateList: "テンプレートを手に",
    addTemplateItem: "新たなテンプレートユニット",
    delTemplateItem: "削除テンプレートユニット",
    getTemplateItemList: "を当面のテンプレートファイル",
    contentManagement: "内容管理",
    contentManage: "ドキュメント管理",
    getContentList: "ドキュメントを手に",
    delContents: "ファイルを削除する",
    directContentsToUser: "指定文章给用户",
    directContentsToSpecial: "指定文章给专题",
    contentCategoryManage: "ドキュメント管理別",
    getContentCategoryList: "を別に",
    delContentCategory: "削除機能を分類し",
    addContentCategory: "添加分類し",
    updateContentCategory: "更新分類し",
    addContentFunction: "新たなドキュメントは",
    addContentItem: "添加新ドキュメント",
    substituteContentItem: "文章录入",
    editContentFunction: "ドキュメント编集ページ",
    updateContentItem: "更新ドキュメント",
    updateItemTop: "文档推荐",
    roofItemTop: "文档置顶",
    getContentItem: "シングルドキュメントを手に",
    tagManage: "ラベル管理",
    getTagList: "ラベルを手に",
    addTags: "新たにラベルが",
    updateTags: "編集ラベル",
    delTags: "ラベルを削除",
    contentMessageManage: "メッセージ管理",
    getContentMessageList: "を書き込み",
    replyContentMessage: "复書き込み",
    delContentMessage: "伝言を削除する",
    userManage: "会員管理",
    regUserFunction: "会員管理",
    getUserInfo: "查看会員",
    invitationFunction: "邀请管理",
    invitationInfo: "查看邀请",
    updateUserInfo: "更新会员",
    delUserInfo: "会員削除",
    communityManage: "社群管理",
    communityManage_getList: "获取社群列表",
    communityManage_update: "更新社群信息",
    communityManage_add: "添加社群信息",
    communityManage_del: "删除社群信息",
    communityTagManage: "社群标签管理",
    communityTagManage_getList: "获取社群标签列表",
    communityTagManage_update: "更新社群标签信息",
    communityTagManage_add: "添加社群标签信息",
    communityTagManage_del: "删除社群标签信息",
    communityContentMessageManage: "社群评论管理",
    getCommunityContentMessageList: "获取社群评论",
    replyCommunityContentMessage: "回复社群评论",
    delCommunityContentMessage: "删除社群评论",
    specialManage: "专题管理",
    specialManage_getList: "获取专题列表",
    specialManage_update: "更新专题信息",
    specialManage_add: "添加专题信息",
    specialManage_del: "删除专题信息",
    masterCategoryManage: "大师类别管理",
    masterCategoryManage_getList: "获取大师类别列表",
    masterCategoryManage_update: "更新大师类别信息",
    masterCategoryManage_add: "添加大师类别信息",
    masterCategoryManage_del: "删除大师类别信息",
    masterClassManage: "大师课管理",
    classTypeManage: "大师课类别管理",
    classTypeManage_getList: "获取大师课类别列表",
    classTypeManage_update: "更新大师课类别信息",
    classTypeManage_add: "添加大师课类别信息",
    classTypeManage_del: "删除大师课类别信息",
    classInfoManage: "课程信息管理",
    classInfoManage_getList: "获取课程信息列表",
    classInfoManage_update: "更新课程信息信息",
    classInfoManage_add: "添加课程信息信息",
    classInfoManage_del: "删除课程信息信息",
    classContentManage: "课程内容管理",
    classContentManage_getList: "获取课程内容列表",
    classContentManage_update: "更新课程内容内容",
    classContentManage_add: "添加课程内容内容",
    classContentManage_del: "删除课程内容内容",
    classContentManage_preview: "视频预览",
    classFeedbackManage: "课程反馈管理",
    classFeedbackManage_getList: "获取课程反馈列表",
    classFeedbackManage_del: "删除课程反馈反馈",
    communityContentManage: "社群帖子管理",
    communityContentManage_getList: "获取社群帖子列表",
    communityContentManage_del: "删除社群帖子",
    communityContentManage_update: "更新社群帖子",
    classScoreManage: "课程评分管理",
    classScoreManage_getList: "获取课程评分列表",
    classScoreManage_del: "删除课程评分",
    classPayRecordManage: "课程订阅管理",
    classPayRecordManage_getList: "获取课程订阅列表",
    rewardRecordManage: "打赏管理",
    rewardRecordManage_getList: "获取打赏列表",
    reSetAmountManage: "充值金额管理",
    reSetAmountManage_getList: "获取充值金额列表",
    reSetAmountManage_update: "更新充值金额内容",
    reSetAmountManage_add: "添加充值金额内容",
    reSetAmountManage_del: "删除充值金额内容",
    backstageManage: "后台设置",
    agreementManage: "协议管理",
    agreementManage_getList: "获取协议列表",
    agreementManage_update: "更新协议内容",
    agreementManage_add: "添加协议内容",
    agreementManage_del: "删除协议内容",
    helpCenterManage: "帮助中心管理",
    helpCenterManage_getList: "获取帮助中心列表",
    helpCenterManage_update: "更新帮助中心内容",
    helpCenterManage_add: "添加帮助中心内容",
    helpCenterManage_del: "删除帮助中心内容",
    feedBackManage: "意见反馈管理",
    feedBackManage_getList: "获取意见反馈列表",
    feedBackManage_update: "更新意见反馈内容",
    feedBackManage_add: "添加意见反馈内容",
    feedBackManage_del: "删除意见反馈内容",
    financialManage: "财务管理",
    billRecordManage: "业务流水管理",
    billRecordManage_getList: "获取业务流水列表",
    openVipRecordManage: "VIP用户管理",
    openVipRecordManage_getList: "获取VIP用户列表",
    mbtManager: "MBT管理",
    mbt_getList: "获取MBT列表",
    mbt_add: "新增MBT",
    vipSetMealManage: "VIP套餐管理",
    vipSetMealManage_getList: "获取VIP套餐列表",
    vipSetMealManage_update: "更新VIP套餐内容",
    vipSetMealManage_add: "添加VIP套餐内容",
    vipSetMealManage_del: "删除VIP套餐内容",
    approvalManage: "审核管理",
    currentApprovalManage: "提币审核",
    currentApprovalManage_getList: "获取提币审核列表",
    currentApprovalManage_update: "更新提币审核内容",
    currentApprovalManage_add: "添加提币审核内容",
    currentApprovalManage_del: "删除提币审核内容",
    creativeRightManage: "专题创作权限审查",
    creativeRightManage_getList: "获取专题创作权限审查列表",
    creativeRightManage_update: "更新专题创作权限审查内容",
    creativeRightManage_add: "添加专题创作权限审查内容",
    creativeRightManage_del: "删除专题创作权限审查内容",
    reportRecordManage: "举报审核",
    reportRecordManage_getList: "获取举报审核列表",
    reportRecordManage_update: "更新举报审核内容",
    reportRecordManage_add: "添加举报审核内容",
    reportRecordManage_del: "删除举报审核内容",
    identityAuthManage: "身份认证审核",
    identityAuthMasterManage: "大师认证审核",
    identityAuthMasterManage_getList: "获取大师认证审核列表",
    identityAuthManage_getList: "获取身份认证审核列表",
    identityAuthManage_update: "更新身份认证审核内容",
    identityAuthManage_add: "添加身份认证审核内容",
    identityAuthManage_del: "删除身份认证审核内容",
    signInManage: "签到管理",
    signInManage_getList: "获取签到列表",
    versionManage: "客户端版本配置",
    getVersionConfigs: "获取客户端版本",
    updateVersionConfigs: "更新客户端版本",
    // ROUTERLANGEND
  },
  navbar: {
    logOut: '退出登录',
    dashboard: '首页',
    github: '项目地址',
    screenfull: '全屏',
    theme: '换肤',
    size: '布局大小'
  },
  login: {
    title: '系统登录',
    logIn: '登录',
    username: '账号',
    password: '密码',
    any: '随便填',
    thirdparty: '第三方登录',
    thirdpartyTips: '本地不能模拟，请结合自己业务进行模拟！！！'
  },
  documentation: {
    documentation: '文档',
    github: 'Github 地址'
  },
  permission: {
    roles: '你的权限',
    switchRoles: '切换权限'
  },
  guide: {
    description: '引导页对于一些第一次进入项目的人很有用，你可以简单介绍下项目的功能。本 Demo 是基于',
    button: '打开引导'
  },
  components: {
    documentation: '文档',
    tinymceTips: '富文本是管理后台一个核心的功能，但同时又是一个有很多坑的地方。在选择富文本的过程中我也走了不少的弯路，市面上常见的富文本都基本用过了，最终权衡了一下选择了Tinymce。更详细的富文本比较和介绍见',
    dropzoneTips: '由于我司业务有特殊需求，而且要传七牛 所以没用第三方，选择了自己封装。代码非常的简单，具体代码你可以在这里看到 @/components/Dropzone',
    stickyTips: '当页面滚动到预设的位置会吸附在顶部',
    backToTopTips1: '页面滚动到指定位置会在右下角出现返回顶部按钮',
    backToTopTips2: '可自定义按钮的样式、show/hide、出现的高度、返回的位置 如需文字提示，可在外部使用Element的el-tooltip元素',
    imageUploadTips: '由于我在使用时它只有vue@1版本，而且和mockjs不兼容，所以自己改造了一下，如果大家要使用的话，优先还是使用官方版本。'
  },
  table: {
    dynamicTips1: '固定表头, 按照表头顺序排序',
    dynamicTips2: '不固定表头, 按照点击顺序排序',
    dragTips1: '默认顺序',
    dragTips2: '拖拽后顺序',
    title: '标题',
    importance: '重要性',
    type: '类型',
    remark: '点评',
    search: '搜索',
    add: '添加',
    export: '导出',
    reviewer: '审核人',
    id: '序号',
    date: '时间',
    author: '作者',
    readings: '阅读数',
    status: '状态',
    actions: '操作',
    edit: '编辑',
    publish: '发布',
    draft: '草稿',
    delete: '删除',
    cancel: '取 消',
    confirm: '确 定'
  },
  errorLog: {
    tips: '请点击右上角bug小图标',
    description: '现在的管理后台基本都是spa的形式了，它增强了用户体验，但同时也会增加页面出问题的可能性，可能一个小小的疏忽就导致整个页面的死锁。好在 Vue 官网提供了一个方法来捕获处理异常，你可以在其中进行错误处理或者异常上报。',
    documentation: '文档介绍'
  },
  excel: {
    export: '导出',
    selectedExport: '导出已选择项',
    placeholder: '请输入文件名(默认excel-list)'
  },
  zip: {
    export: '导出',
    placeholder: '请输入文件名(默认file)'
  },
  theme: {
    change: '换肤',
    documentation: '换肤文档',
    tips: 'Tips: 它区别于 navbar 上的 theme-pick, 是两种不同的换肤方法，各自有不同的应用场景，具体请参考文档。'
  },
  tagsView: {
    refresh: '刷新',
    close: '关闭',
    closeOthers: '关闭其它',
    closeAll: '关闭所有'
  },
  main: {
    myMessage: "メッセージ",
    settings: "設置",
    logOut: "ログアウト",
    lastLoginTime: "前回登録時間",
    lastLoginIp: "前回登録IP",
    myPower: "私の権限",
    seeDetails: "チエック",
    adminUserTotalNum: "管理員人数",
    regUserTotalNum: "登録ユーザー",
    contentsTotalNum: "文章総数",
    messagesTotalNum: "コメント総数",
    shortcutOption: "ショットカート",
    addAdminUser: "管理員追加",
    addContents: "文章追加",
    sourceManage: "リソース管理",
    systemConfigs: "システム配置",
    databak: "データバックアップ",
    nearMessages: "最新のコメント",
    messageIn: "いる",
    messageSaid: "言う",
    messageReply: "返事",
    noMessages: "データがありません",
    nearNewUsers: "新ユーザーアカウント",
    confirmBtnText: "確定",
    cancelBtnText: "キャンセル",
    reSetBtnText: "リセット",
    scr_modal_title: "提示",
    scr_modal_del_succes_info: "削除できました",
    scr_modal_del_error_info: "削除の申請を取消ました",
    form_btnText_update: "更新",
    form_btnText_save: "保存",
    radioOn: "はい",
    radioOff: "いいえ",
    updateSuccess: "更新できました",
    addSuccess: "追加できました",
    dataTableOptions: "操作",
    del_notice: "これは完全に削除され、元に戻せません。よろしいですか？",
    comments_label: "備考",
    sort_label: "並べ替え",
    ask_select_label: "選んでください",
    target_Item: "目標を指定",
    confirm_logout: "戻りますか？",
    login_timeout: "登録時間を超過しました",
    server_error_notice: "サーバエラー、少々お待ちください。",
    re_login: "再度ログインしてください。",
    addNew: "追加",
    modify: "編集",
    back: "戻る",
    post: "投稿",
    nopage: "このサイトにアクセスすることができません或いはアカウント権限がない可能性があります",
    close_modal: "閉じる",
    askForReInputContent: "保存されていないドキュメントがありますか？",
    cancelReInputContent: "データの削除をキャンセルしました。"
  },
  validate: {
    inputNull: "を入力してください {label}",
    inputCorrect: "を正しく入力してください {label}",
    selectNull: "を選んでください  {label}",
    rangelength: "入力文字数 {min} から {max} まで",
    ranglengthandnormal: "{min} から {max} 桁 まで,ローマ字、数字、アンダーバーのみを使えます",
    maxlength: "文字制限は {max} 文字までです",
    passwordnotmatching: "パスワードが一致しません",
    limitUploadImgType: "アップロードする画像の拡張子：JPG,PNG,GIF ",
    limitUploadImgSize: "アップロードする画像のサイズは {size} MBです",
    error_params: "不正なパラメータ、操作を直してください"
  },
  adminUser: {
    lb_form_title: "ユーザー情報を入力してください",
    lb_userName: "ユーザー名",
    lb_userGroup: "ユーザータイプ",
    lb_name: "氏名",
    lb_phoneNum: "携帯番号",
    lb_countryCode: "国家码",
    lb_password: "パスワード",
    lb_confirmPassword: "パスワード確認",
    lb_email: "メール",
    lb_enable: "有効にしますか",
    lb_comments: "備考",
    lb_options: "操作",
    scr_del_ask: "これは完全に削除され、元に戻せません。よろしいですか？",
  },
  adminGroup: {
    lb_roleForm_title: "役割情報を入力してください",
    lb_group_name: "役割名",
    lb_group_dis: "役割説明",
    lb_updatePower_success: "更新できました、もう一度ログインしたら、権限を有効になります",
    lb_give_power: "リソース配分"
  },
  adminResource: {
    lb_resourceForm_title: "リソース情報を入力してください",
    lb_parentType: "親オブジェクト",
    lb_name: "リソース名称",
    lb_type: "リソースタイプ",
    lb_router: "ルーターKey",
    lb_temp: "テンプレートパス",
    lb_showon_meun: "メニュー項目に表示します",
    lb_resource_file_path: "リソースアドレス",
    lb_resource_dis: "リソース説明",
    lb_base_menu: "基本メニュー",
    lb_options: "操作と機能"
  },
  contents: {
    categories: "文章カテゴリ",
    tags: "文章タグ",
    type: "文章タイプ",
    type_normal: "普通",
    type_special: "专题",
    type_video: "视频",
    type_audio: "音频",
    type_video_file: "视频档",
    type_audio_file: "音频档",
    type_masterClass: "大师课",
    twiterAuthor: "ツイッタラー",
    translate: "翻訳",
    title: "タイトル",
    stitle: "ショットタイトル",
    from: "来源",
    from_1: "オリジナル",
    from_2: "転載",
    from_3: "ユーザーが投稿",
    post: "投稿",
    tagOrKey: "タグ/キーワード",
    sImg: "サムネイル",
    postValue: "推奨度",
    discription: "内容摘要",
    comments: "文章詳細",
    flashComments: "速報摘要",
    rec: "推奨",
    roofPlacement: "置顶",
    date: "作成時間",
    updateDate: "更新时间",
    commentNum: "コメント数",
    clickNum: "クリック",
    enable: "表示",
    author: "作者"


  },
  sysTemConfigs: {
    email_server: "システムメールサーバー",
    site_domain: "システムドメイン名",
    site_email: "システムメール",
    site_email_password: "システムメールパスワード",
    site_name: "サイト名称",
    site_dis: "サイト説明",
    site_verify_code: "確認コードを表示します",
    site_keyWords: "サイトキーワード",
    site_tags: "タグ",
    site_icp: "サイト登録番号",
    mongoPath: "mongodbのbin項目を入力してください",
    databakPath: "データバックアップパス",
    scoreSet: "ポイント配分",
    score_post: "文章投稿",
    score_share_post: "文章シェア",
    score_post_message: "コメント投稿",
    perDate: "{label} 日ごとに",
    rechargeAmount: "充值额度",
    defaultRecharge: "默认额度",
  },
  ads: {
    name: "広告名称",
    type: "広告タイプ",
    typeText: "文字",
    typePic: "画像",
    enable: "表示",
    getCode: "コードを取得する",
    dis: "広告説明",
    textLink: "テキストリンク",
    link: "リンク",
    appLink: "app链接",
    appLinkType: "app链接类型",
    upload: "アップロード",
    textContent: "文字内容",
    enable: "有効",
    comments: "備考",
    slider: "スライダー",
    showHeight: "高さ",
    imglist: "画像リスト",
    addImgItem: "画像追加",
    imgAlt: "名称",
    imgLink: "リンク",
    textList: "リンクリスト",
    addTextLink: "リンク追加"
  },
  announce: {
    title: "タイトル",
    content: "アナウンスの詳細",
    author: "投稿者",
    happenTime: "発生時間"
  },
  backUpData: {
    fileName: "ファイル名",
    option: "行為",
    bakTime: "バックアップ時間",
    askBak: "データバックアップを実行します、よろしいですか?",
    bakSuccess: "データバックアップ成功しました",
    bakEr: "データバックアップ失敗しました"
  },
  contentCategory: {
    form_title: "カテゴリ情報を入力してください",
    cate_name: "カテゴリ名",
    enabel: "表示",
    type: "タイプ",
    seoUrl: "seoUrl",
    typeNormal: "普通",
    typeFlash: "速報",
    typeTwter: " Twitter ",
    sort: "並べ替え",
    keywords: "キーワード",
    comments: "説明"
  },
  contentMessage: {
    stitle: "文章タイトル",
    content: "コメント内容",
    author: "コメント者",
    replyAuthor: "関連ユーザー(返事された)",
    date: "時間",
    nimin: "匿名",
    userSaid: "言いました",
    replyUser: "コメントを返事します",
    reply: "返事"
  },
  contentTag: {
    form_title: "タグ情報を入力してください",
    name: "名称",
    comments: "備考"
  },
  regUser: {
    form_title: "ユーザー情報を完成にします",
    userName: "ユーザー名",
    name: "氏名",
    enable: "有効",
    phoneNum: "電話",
    email: "メール",
    comments: "備考",
    date: "アカウント作成時間",
    email: "メール",
    integral: "ポイント",
    group: "角色",
    category: "行业分类"
  },
  systemNotify: {
    title: "タイトル",
    content: "内容",
    date: "発生時間"
  },
  systemOptionLog: {
    actions: "行為",
    type: "カテゴリ",
    sysLogin: "システムログイン",
    syserror: "システム異常",
    date: "発生時間",
    logDetail: "ログの詳細"
  },
  topBar: {
    keywords: "キーワードを入力してください",
    tagName: "タグを入力してください",
    uuid: "请输入流水号",
    messageContent: "コメントを入力してください",
    regUser: "ユーザー名を入力してください",
    systemModelTypes_all: "全部",
    systemModelTypes_h5: "h5異常",
    systemModelTypes_node: "nodejs異常",
    systemModelTypes_login: "システムログイン",
    del_message: "コメント",
    del_user: "ユーザー",
    del_sysopt_log: "システムログ操作",
    del_sys_notice: "システムメッセージ",
  },
  templateConfig: {
    form_title: "テンプレートの追加",
    name: "テンプレートの名前",
    temp: "テンプレート",
    ask_ifinstall: "このテンプレートをインストールしますか？",
    ask_ifenable: "このテンプレートを有効にすることを確認しますか？",
    ask_ifuninstall: "そのテンプレートを落としますか？",
    install_success: "テンプレートのインストールに成功",
    install_failed: "テンプレートのインストールに失敗",
    right_tree_i18n: "国際化",
    right_tree_common_temp: "共用テンプレート",
    right_tree_users_temp: "ユーザーテンプレート",
    right_tree_styles_temp: "スタイル",
    right_tree_script_temp: "スクリプト",
    right_tree_script_tempUnit: "テンプレートユニット"
  },
  community: {
    name: "名称",
    comments: "备注",
    tags: "标签",
    state: "状态",
  },
  communityTag: {
    name: "名称",
    comments: "备注",
  },
  special: {
    name: "名称",
    category: "类别",
    sImg: "图标",
    creator: "创建人",
    approver: "审核人",
    date: "创建时间",
    comments: "简介",
    state: "可见",

  },
  masterCategory: {
    name: "名称",
    sImg: "缩略图",
    comments: "简介",
  },

  siteMessage: {
    name: "名称",
    comments: "简介",
  },

  classType: {
    name: "名称",
    comments: "简介",
  },

  classInfo: {
    name: "课程名称",
    comments: "课程简介",
    categories: "类别",
    buyTips: "购买须知",
    price: "价格",
    vipPrice: "vip价格",
    unit: "价格单位",
    recommend: "推荐",
    vip: "vip专享",
    discount: "优惠",
    boutique: "精品",
    sImg: "封面"

  },

  classContent: {
    catalogIndex: "目录索引",
    catalog: "目录",
    state: "审核状态",
    duration: "时长",
    basicInfo: "基础信息",
    author: "author作者",
    comments: "详情",
  },

  classFeedback: {
    classTitle: "课程标题",
    content: "反馈内容",
    author: "反馈者",
    replyAuthor: "关联用户(被回复)",
    date: "时间"
  },

  communityContent: {
    user: "作者",
    community: "所属社群",
    comments: "内容",
  },

  userActionHis: {
    name: "名称",
    comments: "简介",
  },

  classScore: {
    score: "分数",
    comments: "评论",
    user: "评论者",
    class: "评论对象"
  },

  billRecord: {
    type: "操作类型",
    user: "用户",
    action: "行为",
    coins: "币数",
    unit: "单位",
    date: "产生时间",
    logs: "备注",
    uuid: "流水号"
  },

  mstt_book: {
    name: "名称",
    comments: "简介",
  },

  reward_his: {
    name: "名称",
    comments: "简介",
  },

  review_his: {
    name: "名称",
    comments: "简介",
  },

  user_mi_mp_his: {
    name: "名称",
    comments: "简介",
  },

  user_ev_his: {
    name: "名称",
    comments: "简介",
  },

  user_reward_his: {
    name: "名称",
    comments: "简介",
  },

  threshold_master: {
    name: "名称",
    comments: "简介",
  },

  action_limit_master: {
    name: "名称",
    comments: "简介",
  },

  weight_master: {
    name: "名称",
    comments: "简介",
  },

  classPayRecord: {
    class: "课程",
    money: "金额",
    user: "付款者",
    state: "状态",
    learningProgress: "已学课程",
    comments: "备注",
  },

  rewardRecord: {
    class: "帖子",
    money: "金额",
    user: "付款者",
    state: "状态",
    comments: "备注",
  },

  resetAmount: {
    money: "金额",
    default: "默认",
    unit: "单位"
  },

  agreement: {
    name: "名称",
    time: "创建时间",
    updateTime: "更新时间",
    state: "状态",
    user: "创建者",
    comments: "内容",
  },

  helpCenter: {
    name: "文案标题",
    time: "创建时间",
    updateTime: "更新时间",
    state: "状态",
    user: "创建者",
    lang: "语言",
    comments: "文案内容",
  },

  feedBack: {
    time: "反馈时间",
    updateTime: "更新时间",
    state: "状态",
    user: "反馈者",
    comments: "反馈内容",
  },

  currencyApproval: {
    coins: "提币数额",
    user: "申请人",
    unit: "单位",
    date: "申请时间",
    walletAddress: "提币钱包",
    state: "提币状态",
    comments: "备注",
    approver: "审核人",
    uuid: "流水号"
  },

  creativeRight: {
    type: "审核类型",
    comments: "申请原因",
    user: "申请人",
    date: "申请时间",
    approver: "审核人",
    state: "申请状态",
    updatetime: "审核时间"
  },

  reportRecord: {
    name: "名称",
    comments: "简介",
  },

  identityAuthentication: {
    name: "姓名",
    cardType: "证件类型",
    areaType: "证件区域",
    idCardNo: "证件号码",
    cardFront: "证件照正面",
    cardBack: "证件照反面",
    date: "更新时间",
    user: "申请人",
    approver: "审核人",
    state: "申请状态",
    comments: "备注",
    updatetime: "审核时间"
  },

  signIn: {
    user: "签到人",
    date: "签到时间",
  },

  openVipRecord: {
    setMeal: "套餐",
    user: "购买者",
    unit: "单位",
    date: "创建时间",
    comments: "备注",
  },

  vipSetMeal: {
    time: "套餐时间",
    coins: "价格",
    unit: "单位",
    default: "默认",
    state: "有效",
    comments: "简介",
  },

  communityMessage: {
    stitle: "文章タイトル",
    content: "コメント内容",
    author: "コメント者",
    replyAuthor: "関連ユーザー(返事された)",
    date: "時間",
    nimin: "匿名",
    userSaid: "言いました",
    replyUser: "コメントを返事します",
    reply: "返事"
  },

  hotSearch: {
    word: "关键词",
    date: "产生时间",
    updatetime: "更新时间",
    frequency: "出现次数",
  },

  versionManage: {
    title: '标题',
    description: '描述',
    version: '版本号',
    versionName: '版本名称',
    forcibly: '是否强制更新',
    url: '更新地址'
  },
  mbtManage: {
    user: '用户',
    action: '事件',
    date: '时间',
    coins: '数量'
  },

  integralonfig: {
    name: "名称",
    comments: "简介",
  },
  //LangEnd


















}