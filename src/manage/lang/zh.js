export default {
  route: {
    dashboard: '首页',
    introduction: '简述',
    documentation: '文档',
    guide: '引导页',
    permission: '权限测试页',
    pagePermission: '页面权限',
    directivePermission: '指令权限',
    icons: '图标',
    components: '组件',
    componentIndex: '介绍',
    tinymce: '富文本编辑器',
    markdown: 'Markdown',
    jsonEditor: 'JSON编辑器',
    dndList: '列表拖拽',
    splitPane: 'Splitpane',
    avatarUpload: '头像上传',
    dropzone: 'Dropzone',
    sticky: 'Sticky',
    countTo: 'CountTo',
    componentMixin: '小组件',
    backToTop: '返回顶部',
    dragDialog: '拖拽 Dialog',
    dragKanban: '可拖拽看板',
    charts: '图表',
    keyboardChart: '键盘图表',
    lineChart: '折线图',
    mixChart: '混合图表',
    example: '综合实例',
    nested: '路由嵌套',
    menu1: '菜单1',
    'menu1-1': '菜单1-1',
    'menu1-2': '菜单1-2',
    'menu1-2-1': '菜单1-2-1',
    'menu1-2-2': '菜单1-2-2',
    'menu1-3': '菜单1-3',
    menu2: '菜单2',
    Table: 'Table',
    dynamicTable: '动态Table',
    dragTable: '拖拽Table',
    inlineEditTable: 'Table内编辑',
    complexTable: '综合Table',
    treeTable: '树形表格',
    customTreeTable: '自定义树表',
    tab: 'Tab',
    form: '表单',
    createArticle: '创建文章',
    editArticle: '编辑文章',
    articleList: '文章列表',
    errorPages: '错误页面',
    page401: '401',
    page404: '404',
    errorLog: '错误日志',
    excel: 'Excel',
    exportExcel: 'Export Excel',
    selectExcel: 'Export Selected',
    uploadExcel: 'Upload Excel',
    zip: 'Zip',
    exportZip: 'Export Zip',
    theme: '换肤',
    clipboardDemo: 'Clipboard',
    i18n: '国际化',
    externalLink: '外链',
    systemManage: "系统管理",
    adminUser: "用户管理",
    getAdminUserList: "查询用户",
    addNewAdminUser: "添加新用户",
    updateAdminUser: "用户更新",
    delAdminUser: "删除用户",
    roleManager: "角色管理",
    getRoleList: "获取角色",
    addRoles: "新增角色",
    updateRoles: "更新角色",
    delRoles: "删除角色",
    resourceManage: "资源管理",
    getResourceList: "获取资源",
    addResources: "添加资源",
    updateResources: "编辑资源",
    delResources: "删除资源",
    sysTermConfig: "系统配置",
    getSystemConfig: "获取系统",
    updateSystemConfig: "更新系统",
    integralonfig: "积分配置",
    getIntegralonfig: "获取积分信息",
    updateIntegralonfig: "更新积分配置",
    dataBakManage: "数据备份",
    doDataBak: "执行数据备份",
    getDataList: "获取备份数据",
    delDataBak: "删除备份数据",
    sysTermLogsManage: "日志管理",
    getSystermLogList: "获取日志",
    delSystemLogs: "删除日志",
    delSystermLogs: "清空所有日志",
    sysTermNoticeManage: "消息管理",
    getSysTermNoticeList: "获取消息",
    delSysTermNotices: "删除系统消息",
    updateSysTermNotices: "设置消息为已读",
    announcementManage: "公告管理",
    getAnnouncementList: "获取公告",
    delAnnouncement: "删除公告",
    updateAnnouncements: "公告编辑",
    addAnnouncements: "新增系统公告",
    adsManage: "广告管理",
    getAdsList: "获取广告",
    delAds: "删除广告",
    editAdsFunction: "广告编辑",
    getAdsItem: "获取单个广告",
    updateAds: "更新广告",
    addAds: "新增广告",
    addAdsFunction: "广告新增",
    addAds: "新增广告",
    sysTemMain: "系统主页",
    getSysTermBaseInfo: "获取站点信息",
    templateManage: "模板编辑",
    getTemplateList: "获取模板",
    getTemplateFileText: "读取模板",
    updateTemplateFileText: "更新模板",
    getTempsFromShop: "获取模板市场",
    installTemp: "安装模板",
    enableTemp: "启用模板",
    uninstallTemp: "卸载模板",
    templateConfig: "模板配置",
    getMyTemplateList: "获取模板",
    addTemplateItem: "新增模板单元",
    delTemplateItem: "删除模板单元",
    getTemplateItemList: "获取当前模板文件夹",
    contentManagement: "内容管理",
    contentManage: "文档管理",
    getContentList: "获取文档",
    delContents: "删除文档",
    directContentsToUser: "指定文章给用户",
    directContentsToSpecial: "指定文章给专题",
    contentCategoryManage: "专题类别管理",
    getContentCategoryList: "获取类别",
    delContentCategory: "删除分类功能",
    addContentCategory: "添加分类",
    updateContentCategory: "更新分类",
    addContentFunction: "新增文档",
    addContentItem: "添加新文档",
    substituteContentItem: "文章录入",
    editContentFunction: "文档编辑页面",
    updateContentItem: "更新文档",
    updateItemTop: "文档推荐",
    roofItemTop: "文档置顶",
    getContentItem: "获取单个文档",
    tagManage: "标签管理",
    getTagList: "获取标签",
    addTags: "新增标签",
    updateTags: "编辑标签",
    delTags: "删除标签",
    contentMessageManage: "留言管理",
    getContentMessageList: "获取留言",
    replyContentMessage: "回复留言",
    delContentMessage: "删除留言",
    userManage: "会员管理",
    regUserFunction: "会员管理",
    getUserInfo: "查看会员",
    invitationFunction: "邀请管理",
    invitationInfo: "查看邀请",
    updateUserInfo: "更新会员",
    delUserInfo: "删除会员",
    communityManage: "社群管理",
    communityManage_getList: "获取社群列表",
    communityManage_update: "更新社群信息",
    communityManage_add: "添加社群信息",
    communityManage_del: "删除社群信息",
    communityTagManage: "社群标签管理",
    communityTagManage_getList: "获取社群标签列表",
    communityTagManage_update: "更新社群标签信息",
    communityTagManage_add: "添加社群标签信息",
    communityTagManage_del: "删除社群标签信息",
    communityContentMessageManage: "社群评论管理",
    getCommunityContentMessageList: "获取社群评论",
    replyCommunityContentMessage: "回复社群评论",
    delCommunityContentMessage: "删除社群评论",
    specialManage: "专题管理",
    specialManage_getList: "获取专题列表",
    specialManage_update: "更新专题信息",
    specialManage_add: "添加专题信息",
    specialManage_del: "删除专题信息",
    masterCategoryManage: "大师类别管理",
    masterCategoryManage_getList: "获取大师类别列表",
    masterCategoryManage_update: "更新大师类别信息",
    masterCategoryManage_add: "添加大师类别信息",
    masterCategoryManage_del: "删除大师类别信息",
    masterClassManage: "大师课管理",
    classTypeManage: "大师课类别管理",
    classTypeManage_getList: "获取大师课类别列表",
    classTypeManage_update: "更新大师课类别信息",
    classTypeManage_add: "添加大师课类别信息",
    classTypeManage_del: "删除大师课类别信息",
    classInfoManage: "课程信息管理",
    classInfoManage_getList: "获取课程信息列表",
    classInfoManage_update: "更新课程信息信息",
    classInfoManage_add: "添加课程信息信息",
    classInfoManage_del: "删除课程信息信息",
    classContentManage: "课程内容管理",
    classContentManage_getList: "获取课程内容列表",
    classContentManage_update: "更新课程内容内容",
    classContentManage_add: "添加课程内容内容",
    classContentManage_del: "删除课程内容内容",
    classContentManage_preview: "视频预览",
    classFeedbackManage: "课程反馈管理",
    classFeedbackManage_getList: "获取课程反馈列表",
    classFeedbackManage_del: "删除课程反馈反馈",
    communityContentManage: "社群帖子管理",
    communityContentManage_getList: "获取社群帖子列表",
    communityContentManage_del: "删除社群帖子",
    communityContentManage_update: "更新社群帖子",
    classScoreManage: "课程评分管理",
    classScoreManage_getList: "获取课程评分列表",
    classScoreManage_del: "删除课程评分",
    classPayRecordManage: "课程订阅管理",
    classPayRecordManage_getList: "获取课程订阅列表",
    rewardRecordManage: "打赏管理",
    rewardRecordManage_getList: "获取打赏列表",
    reSetAmountManage: "充值金额管理",
    reSetAmountManage_getList: "获取充值金额列表",
    reSetAmountManage_update: "更新充值金额内容",
    reSetAmountManage_add: "添加充值金额内容",
    reSetAmountManage_del: "删除充值金额内容",
    backstageManage: "后台设置",
    agreementManage: "协议管理",
    agreementManage_getList: "获取协议列表",
    agreementManage_update: "更新协议内容",
    agreementManage_add: "添加协议内容",
    agreementManage_del: "删除协议内容",
    helpCenterManage: "帮助中心管理",
    helpCenterManage_getList: "获取帮助中心列表",
    helpCenterManage_update: "更新帮助中心内容",
    helpCenterManage_add: "添加帮助中心内容",
    helpCenterManage_del: "删除帮助中心内容",
    feedBackManage: "意见反馈管理",
    feedBackManage_getList: "获取意见反馈列表",
    feedBackManage_update: "更新意见反馈内容",
    feedBackManage_add: "添加意见反馈内容",
    feedBackManage_del: "删除意见反馈内容",
    financialManage: "财务管理",
    billRecordManage: "业务流水管理",
    billRecordManage_getList: "获取业务流水列表",
    openVipRecordManage: "VIP用户管理",
    openVipRecordManage_getList: "获取VIP用户列表",
    mbtManager: "MBT管理",
    mbt_getList: "获取MBT列表",
    mbt_add: "新增MBT",
    vipSetMealManage: "VIP套餐管理",
    vipSetMealManage_getList: "获取VIP套餐列表",
    vipSetMealManage_update: "更新VIP套餐内容",
    vipSetMealManage_add: "添加VIP套餐内容",
    vipSetMealManage_del: "删除VIP套餐内容",
    approvalManage: "审核管理",
    currentApprovalManage: "提币审核",
    currentApprovalManage_getList: "获取提币审核列表",
    currentApprovalManage_update: "更新提币审核内容",
    currentApprovalManage_add: "添加提币审核内容",
    currentApprovalManage_del: "删除提币审核内容",
    creativeRightManage: "专题创作权限审查",
    creativeRightManage_getList: "获取专题创作权限审查列表",
    creativeRightManage_update: "更新专题创作权限审查内容",
    creativeRightManage_add: "添加专题创作权限审查内容",
    creativeRightManage_del: "删除专题创作权限审查内容",
    reportRecordManage: "举报审核",
    reportRecordManage_getList: "获取举报审核列表",
    reportRecordManage_update: "更新举报审核内容",
    reportRecordManage_add: "添加举报审核内容",
    reportRecordManage_del: "删除举报审核内容",
    identityAuthManage: "身份认证审核",
    identityAuthMasterManage: "大师认证审核",
    identityAuthMasterManage_getList: "获取大师认证审核列表",
    identityAuthManage_getList: "获取身份认证审核列表",
    identityAuthManage_update: "更新身份认证审核内容",
    identityAuthManage_add: "添加身份认证审核内容",
    identityAuthManage_del: "删除身份认证审核内容",
    signInManage: "签到管理",
    signInManage_getList: "获取签到列表",
    versionManage: "客户端版本配置",
    getVersionConfigs: "获取客户端版本",
    updateVersionConfigs: "更新客户端版本",
    // ROUTERLANGEND
  },
  navbar: {
    logOut: '退出登录',
    dashboard: '首页',
    github: '项目地址',
    screenfull: '全屏',
    theme: '换肤',
    size: '布局大小'
  },
  login: {
    title: '系统登录',
    logIn: '登录',
    username: '账号',
    password: '密码',
    any: '随便填',
    thirdparty: '第三方登录',
    thirdpartyTips: '本地不能模拟，请结合自己业务进行模拟！！！'
  },
  documentation: {
    documentation: '文档',
    github: 'Github 地址'
  },
  permission: {
    roles: '你的权限',
    switchRoles: '切换权限'
  },
  guide: {
    description: '引导页对于一些第一次进入项目的人很有用，你可以简单介绍下项目的功能。本 Demo 是基于',
    button: '打开引导'
  },
  components: {
    documentation: '文档',
    tinymceTips: '富文本是管理后台一个核心的功能，但同时又是一个有很多坑的地方。在选择富文本的过程中我也走了不少的弯路，市面上常见的富文本都基本用过了，最终权衡了一下选择了Tinymce。更详细的富文本比较和介绍见',
    dropzoneTips: '由于我司业务有特殊需求，而且要传七牛 所以没用第三方，选择了自己封装。代码非常的简单，具体代码你可以在这里看到 @/components/Dropzone',
    stickyTips: '当页面滚动到预设的位置会吸附在顶部',
    backToTopTips1: '页面滚动到指定位置会在右下角出现返回顶部按钮',
    backToTopTips2: '可自定义按钮的样式、show/hide、出现的高度、返回的位置 如需文字提示，可在外部使用Element的el-tooltip元素',
    imageUploadTips: '由于我在使用时它只有vue@1版本，而且和mockjs不兼容，所以自己改造了一下，如果大家要使用的话，优先还是使用官方版本。'
  },
  table: {
    dynamicTips1: '固定表头, 按照表头顺序排序',
    dynamicTips2: '不固定表头, 按照点击顺序排序',
    dragTips1: '默认顺序',
    dragTips2: '拖拽后顺序',
    title: '标题',
    importance: '重要性',
    type: '类型',
    remark: '点评',
    search: '搜索',
    add: '添加',
    export: '导出',
    reviewer: '审核人',
    id: '序号',
    date: '时间',
    author: '作者',
    readings: '阅读数',
    status: '状态',
    actions: '操作',
    edit: '编辑',
    publish: '发布',
    draft: '草稿',
    delete: '删除',
    cancel: '取 消',
    confirm: '确 定'
  },
  errorLog: {
    tips: '请点击右上角bug小图标',
    description: '现在的管理后台基本都是spa的形式了，它增强了用户体验，但同时也会增加页面出问题的可能性，可能一个小小的疏忽就导致整个页面的死锁。好在 Vue 官网提供了一个方法来捕获处理异常，你可以在其中进行错误处理或者异常上报。',
    documentation: '文档介绍'
  },
  excel: {
    export: '导出',
    selectedExport: '导出已选择项',
    placeholder: '请输入文件名(默认excel-list)'
  },
  zip: {
    export: '导出',
    placeholder: '请输入文件名(默认file)'
  },
  theme: {
    change: '换肤',
    documentation: '换肤文档',
    tips: 'Tips: 它区别于 navbar 上的 theme-pick, 是两种不同的换肤方法，各自有不同的应用场景，具体请参考文档。'
  },
  tagsView: {
    refresh: '刷新',
    close: '关闭',
    closeOthers: '关闭其它',
    closeAll: '关闭所有'
  },
  main: {
    myMessage: "我的消息",
    settings: "设置",
    logOut: "退出登录",
    lastLoginTime: "上次登录时间",
    lastLoginIp: "上次登录IP",
    myPower: "我的权限",
    seeDetails: "查看",
    adminUserTotalNum: "管理员总数",
    regUserTotalNum: "注册用户",
    contentsTotalNum: "文档总数",
    messagesTotalNum: "留言总数",
    shortcutOption: "快捷操作",
    addAdminUser: "添加管理员",
    addContents: "添加文档",
    sourceManage: "资源管理",
    systemConfigs: "系统配置",
    databak: "数据备份",
    nearMessages: "近期评论",
    messageIn: "在",
    messageSaid: "说",
    messageReply: "回复",
    noMessages: "暂无数据",
    nearNewUsers: "新注册用户",
    confirmBtnText: "确定",
    cancelBtnText: "取消",
    reSetBtnText: "重置",
    scr_modal_title: "提示",
    scr_modal_del_succes_info: "删除成功",
    scr_modal_del_error_info: "已取消删除",
    form_btnText_update: "更新",
    form_btnText_save: "保存",
    radioOn: "是",
    radioOff: "否",
    updateSuccess: "更新成功",
    addSuccess: "添加成功",
    dataTableOptions: "操作",
    del_notice: "此操作将永久删除该条记录, 是否继续?",
    comments_label: "备注",
    sort_label: "排序",
    ask_select_label: "请选择",
    target_Item: "指定目标",
    confirm_logout: "确认退出吗？",
    login_timeout: "您的登录已超时！",
    server_error_notice: "服务异常,请稍后再试",
    re_login: "重新登录",
    addNew: "添加",
    modify: "编辑",
    back: "返回",
    post: "发布",
    nopage: "您访问的页面不存在或者您没有权限访问该模块",
    close_modal: "关闭",
    askForReInputContent: "发现您有未保存的文档，是否载入？",
    cancelReInputContent: "已取消载入并清除数据"
  },
  validate: {
    inputNull: "请输入 {label}",
    inputCorrect: "请输入正确的 {label}",
    selectNull: "请选择 {label}",
    rangelength: "输入长度在 {min} 到 {max} 之间",
    ranglengthandnormal: "{min} 到 {max} 位,只能包含字母、数字和下划线!",
    maxlength: "最多可以输入 {max} 个字符",
    passwordnotmatching: "两次输入密码不一致",
    limitUploadImgType: "上传图片只能是 JPG,PNG,GIF 格式",
    limitUploadImgSize: "上传图片大小不能超过 {size} MB",
    error_params: "参数非法,请重新操作"
  },
  adminUser: {
    lb_form_title: "填写用户信息",
    lb_userName: "用户名",
    lb_userGroup: "用户类型",
    lb_name: "姓名",
    lb_phoneNum: "手机号",
    lb_countryCode: "国家码",
    lb_password: "密码",
    lb_confirmPassword: "确认密码",
    lb_email: "邮箱",
    lb_enable: "是否有效",
    lb_comments: "备注",
    lb_options: "操作",
    scr_del_ask: "此操作将永久删除该用户, 是否继续?",
  },
  adminGroup: {
    lb_roleForm_title: "填写角色信息",
    lb_group_name: "角色名",
    lb_group_dis: "角色描述",
    lb_updatePower_success: "更新成功,重新登录后权限生效",
    lb_give_power: "分配资源"
  },
  adminResource: {
    lb_resourceForm_title: "填写资源信息",
    lb_parentType: "父对象",
    lb_name: "资源名称",
    lb_type: "资源类型",
    lb_router: "路由Key",
    lb_temp: "模板路径",
    lb_showon_meun: "显示在菜单项",
    lb_resource_file_path: "资源地址",
    lb_resource_dis: "资源描述",
    lb_base_menu: "基础菜单",
    lb_options: "操作和功能",
  },
  contents: {
    categories: "专题",
    tags: "标签",
    type: "文档类型",
    type_normal: "普通",
    type_special: "专题",
    type_video: "视频",
    type_audio: "音频",
    type_video_file: "视频档",
    type_audio_file: "音频档",
    type_masterClass: "大师课",
    twiterAuthor: "推特作者",
    translate: "翻译",
    title: "标题",
    stitle: "简短标题",
    from: "来源",
    from_1: "原创",
    from_2: "转载",
    from_3: "用户发布",
    post: "发布",
    tagOrKey: "标签/关键字",
    sImg: "缩略图",
    postValue: "推荐指数",
    discription: "内容摘要",
    comments: "文档详情",
    flashComments: "快讯摘要",
    rec: "推荐",
    roofPlacement: "置顶",
    date: "创建时间",
    updateDate: "更新时间",
    commentNum: "评论数",
    clickNum: "点击",
    enable: "显示",
    author: "作者"


  },
  sysTemConfigs: {
    email_server: "系统邮箱服务器",
    site_domain: "系统域名",
    site_email: "系统邮箱",
    site_email_password: "系统邮箱密码",
    site_name: "站点名称",
    site_dis: "站点描述",
    site_verify_code: "显示验证码",
    site_keyWords: "站点关键字",
    site_tags: "标签关键字",
    site_icp: "站点备案号",
    mongoPath: "请输入mongodb的bin目录",
    databakPath: "数据备份路径",
    scoreSet: "积分配置",
    score_post: "文章发布",
    score_share_post: "文章转发",
    score_post_message: "评论发布",
    perDate: "每 {label} 天",
    rechargeAmount: "充值额度",
    defaultRecharge: "默认额度",
  },
  ads: {
    name: "广告名",
    type: "广告类型",
    typeText: "文字",
    typePic: "图片",
    enable: "显示",
    getCode: "获取代码",
    dis: "广告描述",
    textLink: "文本链接",
    link: "链接",
    appLink: "app链接",
    appLinkType: "app链接类型",
    upload: "上传",
    textContent: "文字内容",
    enable: "有效",
    comments: "备注",
    slider: "轮播",
    showHeight: "高度",
    imglist: "图片列表",
    addImgItem: "添加图片",
    imgAlt: "名称",
    imgLink: "链接",
    textList: "链接列表",
    addTextLink: "添加链接"
  },
  announce: {
    title: "标题",
    content: "公告详情",
    author: "发布者",
    happenTime: "发生时间"
  },
  backUpData: {
    fileName: "文件名",
    option: "行为",
    bakTime: "备份时间",
    askBak: "您即将执行数据备份操作, 是否继续?",
    bakSuccess: "数据备份成功",
    bakEr: "数据备份失败"
  },
  contentCategory: {
    form_title: "填写分类信息",
    cate_name: "类别名称",
    enabel: "显示",
    type: "类型",
    seoUrl: "seoUrl",
    typeNormal: "普通",
    typeFlash: "快讯",
    typeTwter: "推特",
    sort: "排序",
    keywords: "关键字",
    comments: "描述"
  },
  contentMessage: {
    stitle: "文章标题",
    content: "留言内容",
    author: "留言者",
    replyAuthor: "关联用户(被回复)",
    date: "时间",
    nimin: "匿名",
    userSaid: "用户说",
    replyUser: "留言回复",
    reply: "回复"
  },
  contentTag: {
    form_title: "填写标签信息",
    name: "名称",
    comments: "备注"
  },
  regUser: {
    form_title: "完善用户信息",
    userName: "用户名",
    name: "姓名",
    enable: "有效",
    phoneNum: "电话",
    email: "邮箱",
    comments: "备注",
    date: "注册时间",
    email: "邮箱",
    integral: "积分",
    group: "角色",
    category: "行业分类"
  },
  systemNotify: {
    title: "标题",
    content: "内容",
    date: "发生时间"
  },
  systemOptionLog: {
    actions: "行为",
    type: "类别",
    sysLogin: "系统登录",
    syserror: "系统异常",
    date: "发生时间",
    logDetail: "日志详情"
  },
  topBar: {
    keywords: "请输入关键字",
    tagName: "请输入标签名称",
    uuid: "请输入流水号",
    messageContent: "请输入留言内容",
    regUser: "请输入用户名",
    systemModelTypes_all: "全部",
    systemModelTypes_h5: "h5异常",
    systemModelTypes_node: "nodejs异常",
    systemModelTypes_login: "系统登录",
    del_message: "留言",
    del_user: "用户",
    del_sysopt_log: "系统操作日志",
    del_sys_notice: "系统消息",
  },
  templateConfig: {
    form_title: "添加模板单元",
    name: "模板名称",
    temp: "模板",
    ask_ifinstall: "您确认要安装该模板吗？",
    ask_ifenable: "您确认要启用该模板吗？",
    ask_ifuninstall: "您确认要卸载该模板吗？",
    install_success: "模板安装成功",
    install_failed: "模板安装失败",
    right_tree_i18n: "国际化",
    right_tree_common_temp: "公用模板",
    right_tree_users_temp: "用户模板",
    right_tree_styles_temp: "样式",
    right_tree_script_temp: "脚本",
    right_tree_script_tempUnit: "模板单元"
  },
  community: {
    name: "名称",
    comments: "简介",
    tags: "标签",
    state: "状态",
  },
  communityTag: {
    name: "名称",
    comments: "简介",
  },
  special: {
    name: "名称",
    category: "类别",
    sImg: "图标",
    creator: "创建人",
    approver: "审核人",
    date: "创建时间",
    comments: "简介",
    state: "可见",
  },
  masterCategory: {
    name: "名称",
    sImg: "缩略图",
    comments: "简介",
  },

  siteMessage: {
    name: "名称",
    comments: "简介",
  },

  classType: {
    name: "名称",
    comments: "简介",
  },

  classInfo: {
    name: "课程名称",
    comments: "课程简介",
    categories: "类别",
    buyTips: "购买须知",
    price: "价格",
    vipPrice: "vip价格",
    unit: "价格单位",
    recommend: "推荐",
    vip: "vip专享",
    discount: "优惠",
    boutique: "精品",
    sImg: "封面"
  },

  classContent: {
    catalogIndex: "目录索引",
    catalog: "目录",
    state: "审核状态",
    duration: "时长",
    basicInfo: "基础信息",
    author: "作者",
    comments: "详情",
  },

  classFeedback: {
    classTitle: "课程标题",
    content: "反馈内容",
    author: "反馈者",
    replyAuthor: "关联用户(被回复)",
    date: "时间"
  },

  communityContent: {
    user: "作者",
    community: "所属社群",
    comments: "内容",
  },

  userActionHis: {
    name: "名称",
    comments: "简介",
  },

  classScore: {
    score: "分数",
    comments: "评论",
    user: "评论者",
    class: "评论对象"
  },

  billRecord: {
    type: "操作类型",
    user: "用户",
    action: "行为",
    coins: "币数",
    unit: "单位",
    date: "产生时间",
    logs: "备注",
    uuid: "流水号"
  },

  mstt_book: {
    name: "名称",
    comments: "简介",
  },

  reward_his: {
    name: "名称",
    comments: "简介",
  },

  review_his: {
    name: "名称",
    comments: "简介",
  },

  user_mi_mp_his: {
    name: "名称",
    comments: "简介",
  },

  user_ev_his: {
    name: "名称",
    comments: "简介",
  },

  user_reward_his: {
    name: "名称",
    comments: "简介",
  },

  threshold_master: {
    name: "名称",
    comments: "简介",
  },

  action_limit_master: {
    name: "名称",
    comments: "简介",
  },

  weight_master: {
    name: "名称",
    comments: "简介",
  },

  classPayRecord: {
    class: "课程",
    money: "金额",
    user: "付款者",
    state: "状态",
    learningProgress: "已学课程",
    comments: "备注",
  },

  rewardRecord: {
    class: "帖子",
    money: "金额",
    user: "付款者",
    state: "状态",
    comments: "备注",
  },

  resetAmount: {
    money: "金额",
    default: "默认",
    unit: "单位"
  },

  agreement: {
    name: "名称",
    time: "创建时间",
    updateTime: "更新时间",
    state: "状态",
    user: "创建者",
    comments: "内容",
  },

  helpCenter: {
    name: "文案标题",
    time: "创建时间",
    updateTime: "更新时间",
    state: "状态",
    user: "创建者",
    lang: "语言",
    comments: "文案内容",
  },

  feedBack: {
    time: "反馈时间",
    updateTime: "更新时间",
    state: "状态",
    user: "反馈者",
    comments: "反馈内容",
  },

  currencyApproval: {
    coins: "提币数额",
    user: "申请人",
    unit: "单位",
    date: "申请时间",
    walletAddress: "提币钱包",
    state: "提币状态",
    comments: "备注",
    approver: "审核人",
    uuid: "流水号"
  },

  creativeRight: {
    type: "审核类型",
    comments: "申请原因",
    user: "申请人",
    date: "申请时间",
    approver: "审核人",
    state: "申请状态",
    updatetime: "审核时间"
  },

  reportRecord: {
    user: "投诉人",
    comments: "投诉内容",
    date: "投诉时间",
    updatetime: "更新时间",
    approver: "审核人",
    state: "申请状态",
    target_content: "关联帖子",
    target_message: "关联评论",
    target_communityContent: "关联社群",
  },

  identityAuthentication: {
    name: "姓名",
    cardType: "证件类型",
    areaType: "证件区域",
    idCardNo: "证件号码",
    cardFront: "证件照正面",
    cardBack: "证件照反面",
    date: "申请时间",
    user: "申请人",
    approver: "审核人",
    state: "申请状态",
    comments: "备注",
    updatetime: "审核时间"

  },

  signIn: {
    user: "签到人",
    date: "签到时间",
  },

  openVipRecord: {
    setMeal: "套餐",
    user: "购买者",
    unit: "单位",
    date: "创建时间",
    comments: "备注",
  },

  vipSetMeal: {
    time: "套餐时间",
    coins: "价格",
    unit: "单位",
    state: "有效",
    default: "默认",
    comments: "简介",
  },

  communityMessage: {
    stitle: "文章标题",
    content: "留言内容",
    author: "留言者",
    replyAuthor: "关联用户(被回复)",
    date: "时间",
    nimin: "匿名",
    userSaid: "用户说",
    replyUser: "留言回复",
    reply: "回复"
  },

  hotSearch: {
    word: "关键词",
    date: "产生时间",
    updatetime: "更新时间",
    frequency: "出现次数",
  },

  versionManage: {
    title: '标题',
    description: '描述',
    version: '版本号',
    versionName: '版本名称',
    forcibly: '是否强制更新',
    url: '更新地址'
  },
  mbtManage: {
    user: '用户',
    action: '事件',
    date: '时间',
    coins: '数量'
  },

  integralonfig: {
    name: "名称",
    comments: "简介",
  },
  //LangEnd






















}