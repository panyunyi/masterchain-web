import Axios from "axios";

export function reqJsonData(url, params = {}, method = 'post') {
    if (method === 'get') {
        return Axios.get('/' + url, {
            params
        })
    } else if (method === 'post') {
        return Axios.post('/' + url, params)
    }
}
export default {

    logOut() {
        return reqJsonData('manage/logout', {}, 'get')
    },

    getUserSession() {
        return reqJsonData('manage/getUserSession', {}, 'get')
    },

    getSiteBasicInfo(params) {
        return reqJsonData('manage/getSitBasicInfo', params, 'get')
    },

    adminUserList(params) {
        return reqJsonData('manage/adminUser/getList', params, 'get')
    },

    addAdminUser(params) {
        return reqJsonData('manage/adminUser/addOne', params)
    },

    updateAdminUser(params) {
        return reqJsonData('manage/adminUser/updateOne', params)
    },

    deleteAdminUser(params) {
        return reqJsonData('manage/adminUser/deleteUser', params, 'get')
    },

    adminGroupList(params) {
        return reqJsonData('manage/adminGroup/getList', params, 'get')
    },

    addAdminGroup(params) {
        return reqJsonData('manage/adminGroup/addOne', params)
    },

    updateAdminGroup(params) {
        return reqJsonData('manage/adminGroup/updateOne', params)
    },

    deleteAdminGroup(params) {
        return reqJsonData('manage/adminGroup/deleteGroup', params, 'get')
    },

    adminResourceList(params) {
        return reqJsonData('manage/adminResource/getList', params, 'get')
    },

    adminTemplateList(params) {
        return reqJsonData('manage/template/getTemplateForderList', params, 'get')
    },

    getTemplateFileInfo(params) {
        return reqJsonData('manage/template/getTemplateFileText', params, 'get')
    },

    updateTemplateFileText(params) {
        return reqJsonData('manage/template/updateTemplateFileText', params)
    },

    getMyTemplateList(params) {
        return reqJsonData('manage/template/getMyTemplateList', params, 'get')
    },

    addTemplateItem(params) {
        return reqJsonData('manage/template/addTemplateItem', params)
    },

    delTemplateItem(params) {
        return reqJsonData('manage/template/delTemplateItem', params, 'get')
    },

    getTemplateItemlist(params) {
        return reqJsonData('manage/template/getTemplateItemlist', params, 'get')
    },

    getTemplatelistfromShop(params) {
        return reqJsonData('manage/template/getTempsFromShop', params, 'get')
    },

    installTemp(params) {
        return reqJsonData('manage/template/installTemp', params, 'get')
    },

    enableTemp(params) {
        return reqJsonData('manage/template/enableTemp', params, 'get')
    },

    uninstallTemp(params) {
        return reqJsonData('manage/template/uninstallTemp', params, 'get')
    },

    addAdminResource(params) {
        return reqJsonData('manage/adminResource/addOne', params)
    },

    updateAdminResource(params) {
        return reqJsonData('manage/adminResource/updateOne', params)
    },

    deleteAdminResource(params) {
        return reqJsonData('manage/adminResource/deleteResource', params, 'get')
    },

    getSystemConfigs(params) {
        return reqJsonData('manage/systemConfig/getConfig', params, 'get')
    },

    updateSystemConfigs(params) {
        return reqJsonData('manage/systemConfig/updateConfig', params)
    },

    contentCategoryList(params) {
        return reqJsonData('manage/contentCategory/getList', params, 'get')
    },

    addContentCategory(params) {
        return reqJsonData('manage/contentCategory/addOne', params)
    },

    updateContentCategory(params) {
        return reqJsonData('manage/contentCategory/updateOne', params)
    },

    deleteContentCategory(params) {
        return reqJsonData('manage/contentCategory/deleteCategory', params, 'get')
    },

    redictContentToUsers(params) {
        return reqJsonData('manage/content/redictContentToUsers', params)
    },

    redictContentToSpecials(params) {
        return reqJsonData('manage/content/redictContentToSpecials', params)
    },

    contentList(params) {
        return reqJsonData('manage/content/getList', params, 'get')
    },

    getOneContent(params) {
        return reqJsonData('manage/content/getContent', params, 'get')
    },

    addContent(params) {
        return reqJsonData('manage/content/addOne', params)
    },

    updateContent(params) {
        return reqJsonData('manage/content/updateOne', params)
    },

    updateContentToTop(params) {
        return reqJsonData('manage/content/topContent', params)
    },

    roofContent(params) {
        return reqJsonData('manage/content/roofContent', params)
    },

    deleteContent(params) {
        return reqJsonData('manage/content/deleteContent', params, 'get')
    },

    getAliOSSConfig(params) {
        return reqJsonData('manage/content/getAliOSSConfig', params, 'get')
    },

    askScreenshot(params) {
        return reqJsonData('api/v0/upload/video/askScreenshot', params, 'get')
    },

    contentTagList(params) {
        return reqJsonData('manage/contentTag/getList', params, 'get')
    },

    addContentTag(params) {
        return reqJsonData('manage/contentTag/addOne', params)
    },

    updateContentTag(params) {
        return reqJsonData('manage/contentTag/updateOne', params)
    },

    deleteContentTag(params) {
        return reqJsonData('manage/contentTag/deleteTag', params, 'get')
    },

    contentMessageList(params) {
        return reqJsonData('manage/contentMessage/getList', params, 'get')
    },

    addContentMessage(params) {
        return reqJsonData('manage/contentMessage/addOne', params)
    },

    deleteContentMessage(params) {
        return reqJsonData('manage/contentMessage/deleteMessage', params, 'get')
    },

    regUserList(params) {
        return reqJsonData('manage/regUser/getList', params, 'get')
    },

    updateRegUser(params) {
        return reqJsonData('manage/regUser/updateOne', params)
    },

    deleteRegUser(params) {
        return reqJsonData('manage/regUser/deleteUser', params, 'get')
    },

    getBakDataList(params) {
        return reqJsonData('manage/backupDataManage/getBakList', params, 'get')
    },

    bakUpData() {
        return reqJsonData('manage/backupDataManage/backUp', {})
    },

    deletetBakDataItem(params) {
        return reqJsonData('manage/backupDataManage/deleteDataItem', params, 'get')
    },

    getSystemOptionLogsList(params) {
        return reqJsonData('manage/systemOptionLog/getList', params, 'get')
    },

    deleteSystemOptionLogs(params) {
        return reqJsonData('manage/systemOptionLog/deleteLogItem', params, 'get')
    },

    clearSystemOptionLogs(params) {
        return reqJsonData('manage/systemOptionLog/deleteAllLogItem', params, 'get')
    },

    getSystemNotifyList(params) {
        return reqJsonData('manage/systemNotify/getList', params, 'get')
    },

    deleteSystemNotify(params) {
        return reqJsonData('manage/systemNotify/deleteNotifyItem', params, 'get')
    },

    setNoticeRead(params) {
        return reqJsonData('manage/systemNotify/setHasRead', params, 'get')
    },

    getSystemAnnounceList(params) {
        return reqJsonData('manage/systemAnnounce/getList', params, 'get')
    },

    deleteSystemAnnounce(params) {
        return reqJsonData('manage/systemAnnounce/deleteItem', params, 'get')
    },

    addSystemAnnounce(params) {
        return reqJsonData('manage/systemAnnounce/addOne', params)
    },

    getAdsList(params) {
        return reqJsonData('manage/ads/getList', params, 'get')
    },

    getOneAd(params) {
        return reqJsonData('manage/ads/getOne', params, 'get')
    },

    addOneAd(params) {
        return reqJsonData('manage/ads/addOne', params)
    },

    updateAds(params) {
        return reqJsonData('manage/ads/updateOne', params)
    },

    delAds(params) {
        return reqJsonData('manage/ads/delete', params, 'get')
    },

    communityList(params) {
        return reqJsonData('manage/community/getList', params, 'get')
    },

    addCommunity(params) {
        return reqJsonData('manage/community/addOne', params)
    },

    updateCommunity(params) {
        return reqJsonData('manage/community/updateOne', params)
    },

    deleteCommunity(params) {
        return reqJsonData('manage/community/delete', params, 'get')
    },

    communityTagList(params) {
        return reqJsonData('manage/communityTag/getList', params, 'get')
    },

    addCommunityTag(params) {
        return reqJsonData('manage/communityTag/addOne', params)
    },

    updateCommunityTag(params) {
        return reqJsonData('manage/communityTag/updateOne', params)
    },

    deleteCommunityTag(params) {
        return reqJsonData('manage/communityTag/delete', params, 'get')
    },

    specialList(params) {
        return reqJsonData('manage/special/getList', params, 'get')
    },

    addSpecial(params) {
        return reqJsonData('manage/special/addOne', params)
    },

    updateSpecial(params) {
        return reqJsonData('manage/special/updateOne', params)
    },

    deleteSpecial(params) {
        return reqJsonData('manage/special/delete', params, 'get')
    },

    masterCategoryList(params) {
        return reqJsonData('manage/masterCategory/getList', params, 'get')
    },

    addMasterCategory(params) {
        return reqJsonData('manage/masterCategory/addOne', params)
    },

    updateMasterCategory(params) {
        return reqJsonData('manage/masterCategory/updateOne', params)
    },

    deleteMasterCategory(params) {
        return reqJsonData('manage/masterCategory/delete', params, 'get')
    },

    siteMessageList(params) {
        return reqJsonData('manage/siteMessage/getList', params, 'get')
    },

    addSiteMessage(params) {
        return reqJsonData('manage/siteMessage/addOne', params)
    },

    updateSiteMessage(params) {
        return reqJsonData('manage/siteMessage/updateOne', params)
    },

    deleteSiteMessage(params) {
        return reqJsonData('manage/siteMessage/delete', params, 'get')
    },

    classTypeList(params) {
        return reqJsonData('manage/classType/getList', params, 'get')
    },

    addClassType(params) {
        return reqJsonData('manage/classType/addOne', params)
    },

    updateClassType(params) {
        return reqJsonData('manage/classType/updateOne', params)
    },

    deleteClassType(params) {
        return reqJsonData('manage/classType/delete', params, 'get')
    },

    classInfoList(params) {
        return reqJsonData('manage/classInfo/getList', params, 'get')
    },

    addClassInfo(params) {
        return reqJsonData('manage/classInfo/addOne', params)
    },

    updateClassInfo(params) {
        return reqJsonData('manage/classInfo/updateOne', params)
    },

    deleteClassInfo(params) {
        return reqJsonData('manage/classInfo/delete', params, 'get')
    },

    classContentList(params) {
        return reqJsonData('manage/classContent/getList', params, 'get')
    },

    addClassContent(params) {
        return reqJsonData('manage/classContent/addOne', params)
    },

    updateClassContent(params) {
        return reqJsonData('manage/classContent/updateOne', params)
    },

    deleteClassContent(params) {
        return reqJsonData('manage/classContent/delete', params, 'get')
    },

    classFeedbackList(params) {
        return reqJsonData('manage/classFeedback/getList', params, 'get')
    },

    addClassFeedback(params) {
        return reqJsonData('manage/classFeedback/addOne', params)
    },

    deleteClassFeedback(params) {
        return reqJsonData('manage/classFeedback/delete', params, 'get')
    },

    communityContentList(params) {
        return reqJsonData('manage/communityContent/getList', params, 'get')
    },

    addCommunityContent(params) {
        return reqJsonData('manage/communityContent/addOne', params)
    },

    updateCommunityContent(params) {
        return reqJsonData('manage/communityContent/updateOne', params)
    },

    deleteCommunityContent(params) {
        return reqJsonData('manage/communityContent/delete', params, 'get')
    },

    userActionHisList(params) {
        return reqJsonData('manage/userActionHis/getList', params, 'get')
    },

    addUserActionHis(params) {
        return reqJsonData('manage/userActionHis/addOne', params)
    },

    updateUserActionHis(params) {
        return reqJsonData('manage/userActionHis/updateOne', params)
    },

    deleteUserActionHis(params) {
        return reqJsonData('manage/userActionHis/delete', params, 'get')
    },

    classScoreList(params) {
        return reqJsonData('manage/classScore/getList', params, 'get')
    },

    addClassScore(params) {
        return reqJsonData('manage/classScore/addOne', params)
    },

    updateClassScore(params) {
        return reqJsonData('manage/classScore/updateOne', params)
    },

    deleteClassScore(params) {
        return reqJsonData('manage/classScore/delete', params, 'get')
    },

    billRecordList(params) {
        return reqJsonData('manage/billRecord/getList', params, 'get')
    },

    invitionRecordList(params) {
        return reqJsonData('manage/invitionRecord/getList', params, 'get')
    },

    addBillRecord(params) {
        return reqJsonData('manage/billRecord/addOne', params)
    },

    updateBillRecord(params) {
        return reqJsonData('manage/billRecord/updateOne', params)
    },

    deleteBillRecord(params) {
        return reqJsonData('manage/billRecord/delete', params, 'get')
    },

    mstt_bookList(params) {
        return reqJsonData('manage/mstt_book/getList', params, 'get')
    },

    addMstt_book(params) {
        return reqJsonData('manage/mstt_book/addOne', params)
    },

    updateMstt_book(params) {
        return reqJsonData('manage/mstt_book/updateOne', params)
    },

    deleteMstt_book(params) {
        return reqJsonData('manage/mstt_book/delete', params, 'get')
    },

    reward_hisList(params) {
        return reqJsonData('manage/reward_his/getList', params, 'get')
    },

    addReward_his(params) {
        return reqJsonData('manage/reward_his/addOne', params)
    },

    updateReward_his(params) {
        return reqJsonData('manage/reward_his/updateOne', params)
    },

    deleteReward_his(params) {
        return reqJsonData('manage/reward_his/delete', params, 'get')
    },

    review_hisList(params) {
        return reqJsonData('manage/review_his/getList', params, 'get')
    },

    addReview_his(params) {
        return reqJsonData('manage/review_his/addOne', params)
    },

    updateReview_his(params) {
        return reqJsonData('manage/review_his/updateOne', params)
    },

    deleteReview_his(params) {
        return reqJsonData('manage/review_his/delete', params, 'get')
    },

    user_mi_mp_hisList(params) {
        return reqJsonData('manage/user_mi_mp_his/getList', params, 'get')
    },

    addUser_mi_mp_his(params) {
        return reqJsonData('manage/user_mi_mp_his/addOne', params)
    },

    updateUser_mi_mp_his(params) {
        return reqJsonData('manage/user_mi_mp_his/updateOne', params)
    },

    deleteUser_mi_mp_his(params) {
        return reqJsonData('manage/user_mi_mp_his/delete', params, 'get')
    },

    user_ev_hisList(params) {
        return reqJsonData('manage/user_ev_his/getList', params, 'get')
    },

    addUser_ev_his(params) {
        return reqJsonData('manage/user_ev_his/addOne', params)
    },

    updateUser_ev_his(params) {
        return reqJsonData('manage/user_ev_his/updateOne', params)
    },

    deleteUser_ev_his(params) {
        return reqJsonData('manage/user_ev_his/delete', params, 'get')
    },

    user_reward_hisList(params) {
        return reqJsonData('manage/user_reward_his/getList', params, 'get')
    },

    addUser_reward_his(params) {
        return reqJsonData('manage/user_reward_his/addOne', params)
    },

    updateUser_reward_his(params) {
        return reqJsonData('manage/user_reward_his/updateOne', params)
    },

    deleteUser_reward_his(params) {
        return reqJsonData('manage/user_reward_his/delete', params, 'get')
    },

    threshold_masterList(params) {
        return reqJsonData('manage/threshold_master/getList', params, 'get')
    },

    addThreshold_master(params) {
        return reqJsonData('manage/threshold_master/addOne', params)
    },

    updateThreshold_master(params) {
        return reqJsonData('manage/threshold_master/updateOne', params)
    },

    deleteThreshold_master(params) {
        return reqJsonData('manage/threshold_master/delete', params, 'get')
    },

    action_limit_masterList(params) {
        return reqJsonData('manage/action_limit_master/getList', params, 'get')
    },

    addAction_limit_master(params) {
        return reqJsonData('manage/action_limit_master/addOne', params)
    },

    updateAction_limit_master(params) {
        return reqJsonData('manage/action_limit_master/updateOne', params)
    },

    deleteAction_limit_master(params) {
        return reqJsonData('manage/action_limit_master/delete', params, 'get')
    },

    weight_masterList(params) {
        return reqJsonData('manage/weight_master/getList', params, 'get')
    },

    addWeight_master(params) {
        return reqJsonData('manage/weight_master/addOne', params)
    },

    updateWeight_master(params) {
        return reqJsonData('manage/weight_master/updateOne', params)
    },

    deleteWeight_master(params) {
        return reqJsonData('manage/weight_master/delete', params, 'get')
    },

    classPayRecordList(params) {
        return reqJsonData('manage/classPayRecord/getList', params, 'get')
    },

    addClassPayRecord(params) {
        return reqJsonData('manage/classPayRecord/addOne', params)
    },

    updateClassPayRecord(params) {
        return reqJsonData('manage/classPayRecord/updateOne', params)
    },

    deleteClassPayRecord(params) {
        return reqJsonData('manage/classPayRecord/delete', params, 'get')
    },

    resetAmountList(params) {
        return reqJsonData('manage/resetAmount/getList', params, 'get')
    },

    addResetAmount(params) {
        return reqJsonData('manage/resetAmount/addOne', params)
    },

    updateResetAmount(params) {
        return reqJsonData('manage/resetAmount/updateOne', params)
    },

    deleteResetAmount(params) {
        return reqJsonData('manage/resetAmount/delete', params, 'get')
    },

    agreementList(params) {
        return reqJsonData('manage/agreement/getList', params, 'get')
    },

    addAgreement(params) {
        return reqJsonData('manage/agreement/addOne', params)
    },

    updateAgreement(params) {
        return reqJsonData('manage/agreement/updateOne', params)
    },

    deleteAgreement(params) {
        return reqJsonData('manage/agreement/delete', params, 'get')
    },

    helpCenterList(params) {
        return reqJsonData('manage/helpCenter/getList', params, 'get')
    },

    addHelpCenter(params) {
        return reqJsonData('manage/helpCenter/addOne', params)
    },

    updateHelpCenter(params) {
        return reqJsonData('manage/helpCenter/updateOne', params)
    },

    deleteHelpCenter(params) {
        return reqJsonData('manage/helpCenter/delete', params, 'get')
    },

    feedBackList(params) {
        return reqJsonData('manage/feedBack/getList', params, 'get')
    },

    addFeedBack(params) {
        return reqJsonData('manage/feedBack/addOne', params)
    },

    updateFeedBack(params) {
        return reqJsonData('manage/feedBack/updateOne', params)
    },

    deleteFeedBack(params) {
        return reqJsonData('manage/feedBack/delete', params, 'get')
    },

    currencyApprovalList(params) {
        return reqJsonData('manage/currencyApproval/getList', params, 'get')
    },

    addCurrencyApproval(params) {
        return reqJsonData('manage/currencyApproval/addOne', params)
    },

    updateCurrencyApproval(params) {
        return reqJsonData('manage/currencyApproval/updateOne', params)
    },

    deleteCurrencyApproval(params) {
        return reqJsonData('manage/currencyApproval/delete', params, 'get')
    },

    creativeRightList(params) {
        return reqJsonData('manage/creativeRight/getList', params, 'get')
    },

    addCreativeRight(params) {
        return reqJsonData('manage/creativeRight/addOne', params)
    },

    updateCreativeRight(params) {
        return reqJsonData('manage/creativeRight/updateOne', params)
    },

    deleteCreativeRight(params) {
        return reqJsonData('manage/creativeRight/delete', params, 'get')
    },

    reportRecordList(params) {
        return reqJsonData('manage/reportRecord/getList', params, 'get')
    },

    addReportRecord(params) {
        return reqJsonData('manage/reportRecord/addOne', params)
    },

    updateReportRecord(params) {
        return reqJsonData('manage/reportRecord/updateOne', params)
    },

    deleteReportRecord(params) {
        return reqJsonData('manage/reportRecord/delete', params, 'get')
    },

    identityAuthenticationList(params) {
        return reqJsonData('manage/identityAuthentication/getList', params, 'get')
    },

    addIdentityAuthentication(params) {
        return reqJsonData('manage/identityAuthentication/addOne', params)
    },

    updateIdentityAuthentication(params) {
        return reqJsonData('manage/identityAuthentication/updateOne', params)
    },

    deleteIdentityAuthentication(params) {
        return reqJsonData('manage/identityAuthentication/delete', params, 'get')
    },

    signInList(params) {
        return reqJsonData('manage/signIn/getList', params, 'get')
    },

    addSignIn(params) {
        return reqJsonData('manage/signIn/addOne', params)
    },

    updateSignIn(params) {
        return reqJsonData('manage/signIn/updateOne', params)
    },

    deleteSignIn(params) {
        return reqJsonData('manage/signIn/delete', params, 'get')
    },

    openVipRecordList(params) {
        return reqJsonData('manage/openVipRecord/getList', params, 'get')
    },

    addOpenVipRecord(params) {
        return reqJsonData('manage/openVipRecord/addOne', params)
    },

    updateOpenVipRecord(params) {
        return reqJsonData('manage/openVipRecord/updateOne', params)
    },

    deleteOpenVipRecord(params) {
        return reqJsonData('manage/openVipRecord/delete', params, 'get')
    },

    vipSetMealList(params) {
        return reqJsonData('manage/vipSetMeal/getList', params, 'get')
    },

    addVipSetMeal(params) {
        return reqJsonData('manage/vipSetMeal/addOne', params)
    },

    updateVipSetMeal(params) {
        return reqJsonData('manage/vipSetMeal/updateOne', params)
    },

    deleteVipSetMeal(params) {
        return reqJsonData('manage/vipSetMeal/delete', params, 'get')
    },

    communityMessageList(params) {
        return reqJsonData('manage/communityMessage/getList', params, 'get')
    },

    addCommunityMessage(params) {
        return reqJsonData('manage/communityMessage/addOne', params)
    },

    updateCommunityMessage(params) {
        return reqJsonData('manage/communityMessage/updateOne', params)
    },

    deleteCommunityMessage(params) {
        return reqJsonData('manage/communityMessage/delete', params, 'get')
    },

    hotSearchList(params) {
        return reqJsonData('manage/hotSearch/getList', params, 'get')
    },

    addHotSearch(params) {
        return reqJsonData('manage/hotSearch/addOne', params)
    },

    updateHotSearch(params) {
        return reqJsonData('manage/hotSearch/updateOne', params)
    },

    deleteHotSearch(params) {
        return reqJsonData('manage/hotSearch/delete', params, 'get')
    },

    versionManageList(params) {
        return reqJsonData('manage/versionManage/getList', params, 'get')
    },

    addVersionManage(params) {
        return reqJsonData('manage/versionManage/addOne', params)
    },

    updateVersionManage(params) {
        return reqJsonData('manage/versionManage/updateOne', params)
    },

    deleteVersionManage(params) {
        return reqJsonData('manage/versionManage/delete', params, 'get')
    },

    mbtManageList(params) {
        return reqJsonData('manage/mbtManage/getList', params, 'get')
    },

    addMbtManage(params) {
        return reqJsonData('manage/mbtManage/addOne', params)
    },

    integralonfigList(params) {
        return reqJsonData('manage/integralonfig/getList', params, 'get')
    },

    updateIntegralonfig(params) {
        return reqJsonData('manage/integralonfig/updateOne', params)
    },

    //StoreAppServiceEnd




































}