import Cookies from 'js-cookie'
import * as types from '../types.js';
import services from '../services.js';
import _ from 'lodash';

export function renderTreeData(result) {
  let newResult = result;
  let treeData = newResult.docs;
  let childArr = _.filter(treeData, (doc) => {
    return doc.parentId != '0'
  });

  for (let i = 0; i < childArr.length; i++) {
    let child = childArr[i];
    for (let j = 0; j < treeData.length; j++) {
      let treeItem = treeData[j];
      if (treeItem._id == child.parentId || treeItem.id == child.parentId) {
        if (!treeItem.children) treeItem.children = [];
        treeItem.children.push(child);
        break;
      }
    }
  }

  newResult.docs = _.filter(treeData, (doc) => {
    return doc.parentId == '0'
  });
  return newResult;
}

const app = {
  state: {
    sidebar: {
      opened: !+Cookies.get('sidebarStatus'),
      withoutAnimation: false
    },
    device: 'desktop',
    language: Cookies.get('language') || 'en',
    size: Cookies.get('size') || 'medium',
    count: 20,
    loginState: {
      state: false,
      userInfo: {
        userName: '',
        email: '',
        logo: '',
        group: []
      },
      noticeCounts: 0
    },
    adminUser: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          userName: '',
          password: '',
          confirmPassword: '',
          group: '',
          email: '',
          comments: '',
          phoneNum: '',
          countryCode: '',
        }
      },
      userList: {
        pageInfo: {},
        docs: []
      },
      addUser: {
        state: '',
        err: {}
      }
    },
    adminGroup: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          comments: '',
          enable: false,
          power: []
        }
      },
      roleFormState: {
        show: false,
        edit: true,
        formData: {
          name: '',
          comments: '',
          enable: false,
          power: []
        }
      },
      roleList: {
        pageInfo: {},
        docs: []
      },
      addGroup: {
        state: '',
        err: {}
      }
    },
    adminResource: {
      formState: {
        type: 'root',
        show: false,
        edit: false,
        formData: {
          label: '',
          type: '',
          api: '',
          icon: '',
          routePath: '',
          componentPath: '',
          enable: true,
          parentId: '',
          sortId: 0,
          comments: '',
          parent: {
            id: '',
            label: ''
          }
        }
      },
      resourceList: {
        pageInfo: {},
        docs: []
      },
      addResource: {
        state: '',
        err: {}
      }
    },
    adminTemplate: {
      formState: {

      },
      templateList: {
        pageInfo: {},
        docs: []
      }
    },
    myTemplates: {
      formState: {
        show: false,
        edit: false,
        formData: {

        }
      },
      templateList: {
        pageInfo: {},
        docs: []
      },
      templateItemForderList: {}
    },
    tempShoplist: {
      pageInfo: {},
      docs: []
    },
    systemConfig: {
      configs: {
        siteName: '',
        ogTitle: '',
        siteDomain: '',
        siteDiscription: '',
        siteKeywords: '',
        siteEmailServer: '',
        siteEmail: '',
        siteEmailPwd: '',
        mongoDBPath: '',
        databackForderPath: '',
        allowUserPostContent: true,
        allowUserCreateCommunity: true,
        shareArticlScore: '',
        bakDataRate: '1',
        bakDatabyTime: false
      }
    },
    contentCategory: {
      formState: {
        type: 'root',
        show: false,
        edit: false,
        formData: {
          label: '',
          enable: false,
          defaultUrl: '',
          parentId: '',
          contentTemp: '',
          parentObj: '',
          sortId: 0,
          comments: '',
          type: '1'
        }
      },
      categoryList: {
        pageInfo: {},
        docs: []
      },
      addContentCategory: {
        state: '',
        err: {}
      }
    },
    content: {
      formState: {
        edit: false,
        formData: {
          targetUser: '',
          title: '',
          stitle: '',
          type: '1',
          categories: [],
          keywords: '',
          sortPath: '',
          tags: [],
          keywords: '',
          sImg: '',
          discription: '',
          author: {},
          uAuthor: '',
          markDownComments: '',
          state: '1',
          isTop: 0,
          roofPlacement: '0',
          clickNum: 0,
          comments: '',
          simpleComments: '',
          commentNum: 0,
          likeNum: 0,
          dismissReason: '',
          videoUrl: '',
          audioUrl: '',

        }
      },
      contentList: {
        pageInfo: {},
        docs: []
      },
      addContent: {
        state: '',
        err: {}
      }
    },
    substituteContent: {
      formState: {
        edit: false,
        formData: {
          targetUser: '',
          title: '',
          stitle: '',
          type: '1',
          categories: [],
          keywords: '',
          sortPath: '',
          tags: [],
          keywords: '',
          sImg: '',
          discription: '',
          author: {},
          uAuthor: '',
          markDownComments: '',
          state: '1',
          isTop: 0,
          roofPlacement: '0',
          clickNum: 0,
          comments: '',
          simpleComments: '',
          commentNum: 0,
          likeNum: 0,
          dismissReason: '',
          videoUrl: '',
          audioUrl: '',
        }
      }
    },
    contentTag: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          alias: '',
          comments: ''
        }
      },
      tagList: {
        pageInfo: {},
        docs: []
      },
      addTag: {
        state: '',
        err: {}
      }
    },
    directUser: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          alias: '',
          targetUser: ''
        }
      }
    },
    directSpecial: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          alias: '',
          targetSpecials: ''
        }
      }
    },
    special: {
      formState: {
        show: false,
        edit: false,
        formData: {
          category: [],
          creator: {},
          approver: {},
          name: '',
          sImg: '',
          state: '1',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },
    masterCategory: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          sImg: '',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },
    communityTag: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          sImg: '',
          tags: [],
          comments: ''
        }
      },
      tagList: {
        pageInfo: {},
        docs: []
      }
    },
    community: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          alias: '',
          open: '',
          questions: [],
          comments: '',
          state: '0'
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },
    contentMessage: {
      formState: {
        show: false,
        edit: false,
        formData: {
          contentId: '',
          content: '',
          author: '',
          replyId: '',
          relationMsgId: ''
        },
        parentformData: {
          contentId: '',
          content: '',
          author: '',
          replyId: '',
          relationMsgId: ''
        }
      },
      messageList: {
        pageInfo: {},
        docs: []
      },
      addMessage: {
        state: '',
        err: {}
      }
    },
    regUser: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          userName: '',
          group: '',
          watchers: [],
          followers: [],
          category: [],
          email: '',
          comments: '',
          phoneNum: '',
          enable: true,
          integral: 0
        }
      },
      userList: {
        pageInfo: {},
        docs: []
      }
    },
    bakDataList: {
      pageInfo: {},
      docs: []
    },
    systemOptionLogs: {
      pageInfo: {},
      docs: []
    },
    systemNotify: {
      pageInfo: {},
      docs: []
    },
    systemAnnounce: {
      pageInfo: {},
      docs: []
    },
    announceFormState: {
      title: '',
      content: ''
    },
    ads: {
      list: {
        pageInfo: {},
        docs: []
      },
      infoFormState: {
        edit: false,
        formData: {
          name: '',
          type: '1',
          height: '',
          comments: '',
          items: [],
          state: true,
          carousel: true
        }
      },
      itemFormState: {
        show: false,
        edit: false,
        formData: {
          title: '',
          link: '',
          appLink: '',
          appLinkType: '',
          width: '',
          height: '',
          alt: '',
          sImg: ''
        }
      }
    },
    basicInfo: {
      adminUserCount: 0,
      regUserCount: 0,
      contentCount: 0,
      messageCount: 0
    },

    siteMessage: {
      formState: {
        show: false,
        edit: false,
        formData: {
          content: '',
          isRead: false,
          user: {},
          type: '1'
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    classType: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    classInfo: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          categories: [],
          comments: '',
          buyTips: '',
          sImg: '',
          price: 0,
          vipPrice: 0,
          unit: '',
          recommend: false,
          vip: false,
          discount: false,
          boutique: false
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    classContent: {
      formState: {
        show: false,
        edit: false,
        formData: {
          catalog: '',
          duration: '',
          basicInfo: '',
          author: '',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    classFeedback: {
      formState: {
        show: false,
        edit: false,
        formData: {
          classContent: '',
          content: '',
          author: '',
          replyId: '',
          relationMsgId: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    communityContent: {
      formState: {
        show: false,
        edit: false,
        formData: {
          user: {},
          community: {},
          comments: '',
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    userActionHis: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    classScore: {
      formState: {
        show: false,
        edit: false,
        formData: {
          score: '',
          comments: '',
          user: {},
          class: {}
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    billRecord: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          userName: '',
          comments: '',
          user: {}
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },
    invitionRecord: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          userName: '',
          comments: '',
          user: {},
          target_user: {},
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    mstt_book: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    reward_his: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    review_his: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    user_mi_mp_his: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    user_ev_his: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    user_reward_his: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    threshold_master: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    action_limit_master: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    weight_master: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    classPayRecord: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    resetAmount: {
      formState: {
        show: false,
        edit: false,
        formData: {
          money: '',
          default: false,
          unit: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    agreement: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          user: {},
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    helpCenter: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          type: '',
          lang: '1',
          user: {},
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    feedBack: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          phoneNum: '',
          user: {},
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    currencyApproval: {
      formState: {
        show: false,
        edit: false,
        formData: {
          coins: "",
          user: "",
          unit: "",
          date: "",
          walletAddress: "",
          state: "",
          comments: "",
          approver: '',
          uuid: "",
          dismissReason: '',
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    creativeRight: {
      formState: {
        show: false,
        edit: false,
        formData: {
          date: '',
          user: {},
          state: '',
          approver: '',
          comments: '',
          category: {},
          newCateName: '',
          dismissReason: '',
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    reportRecord: {
      formState: {
        show: false,
        edit: false,
        formData: {
          user: "",
          comments: "",
          date: "",
          updatetime: "",
          approver: "",
          state: "",
          target_content: "",
          target_message: "",
          target_communityContent: "",
          dismissReason: '',
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    identityAuthentication: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: "",
          cardType: "",
          idCardNo: "",
          cardFront: "",
          cardBack: "",
          date: "",
          user: {},
          approver: "",
          comments: '',
          state: '0',
          type: '1',
          masterCategory: '',
          diploma_label: '',
          diploma: [], // 学历证书
          professionalCertificate_label: '', // 专业证书
          professionalCertificate: [], // 专业证书
          otherCertificate_label: '', // 其它证书
          otherCertificate: [], // 其它证书
          pastExperience: [], // 过往经历
          pastArticles: [], // 过往文章
          publishedBooks: [], // 出刊书籍
          uploadAttachments: [], // 附件上传
          dismissReason: '',
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    signIn: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    openVipRecord: {
      formState: {
        show: false,
        edit: false,
        formData: {
          setMeal: '',
          user: {},
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    vipSetMeal: {
      formState: {
        show: false,
        edit: false,
        formData: {
          time: '',
          coins: 0,
          unit: '',
          default: false,
          state: false,
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    communityMessage: {
      formState: {
        show: false,
        edit: false,
        formData: {
          contentId: '',
          content: '',
          author: '',
          replyId: '',
          relationMsgId: ''
        },
        parentformData: {
          contentId: '',
          content: '',
          author: '',
          replyId: '',
          relationMsgId: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      },
      addMessage: {
        state: '',
        err: {}
      }
    },

    hotSearch: {
      formState: {
        show: false,
        edit: false,
        formData: {
          name: '',
          comments: ''
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    versionManage: {
      configs: {
        title: '',
        description: '',
        version: '',
        versionName: '',
        forcibly: false,
        url: ''
      }
    },
    versionManageIos: {
      configs: {
        title: '',
        description: '',
        version: '',
        versionName: '',
        forcibly: false,
        url: ''
      }
    },

    mbtManage: {
      formState: {
        show: false,
        edit: false,
        formData: {
          user: '',
          logs: '',
          date: '',
          coins: '',
        }
      },
      list: {
        pageInfo: {},
        docs: []
      }
    },

    integralonfig: {
      configs: {
        login_first: 0, // 首次登陆
        register: 0, // 用户注册
        browse: 0, // 浏览
        browse_limit: 0, // 浏览
        give_thumbs_up: 0, // 点赞
        give_thumbs_up_limit: 0, // 点赞
        comment: 0, // 评论
        comment_limit: 0, // 评论
        community_comment: 0, // 社群评论
        community_comment_limit: 0, // 社群评论
        appreciate: 0, // 打赏
        appreciate_limit: 0, // 打赏
        appreciate_in: 0, // 打赏收入
        appreciate_out: 0, // 打赏支出
        collections: 0, // 关注
        collections_limit: 0, // 关注
        follow: 0, // 关注
        follow_limit: 0, // 关注
        forward: 0, // 转发
        forward_limit: 0, // 转发
        community_building: 0, // 社群建设
        community_building_limit: 0, // 社群建设
        invitation: 0, // 邀请
        report: 0, // 举报
        review: 0, // 审核
        creat_content: 0, // 内容创作
        creat_identity: 0, // 实名认证
        shop_send: 0, // 商城会员发放
        transfer_coins: 0, // 提币
        recharge: 0, // 充值
        subscribe: 0, // 课程订购
        buyVip: 0, // 购买会员
        signIn: 0, // 签到
        signIn_continuity: 0, // 签到
        brithday: 0, // 生日
      }
    },
    //StoreAppInitState












  },
  mutations: {
    TOGGLE_SIDEBAR: state => {
      if (state.sidebar.opened) {
        Cookies.set('sidebarStatus', 1)
      } else {
        Cookies.set('sidebarStatus', 0)
      }
      state.sidebar.opened = !state.sidebar.opened
      state.sidebar.withoutAnimation = false
    },
    CLOSE_SIDEBAR: (state, withoutAnimation) => {
      Cookies.set('sidebarStatus', 1)
      state.sidebar.opened = false
      state.sidebar.withoutAnimation = withoutAnimation
    },
    TOGGLE_DEVICE: (state, device) => {
      state.device = device
    },
    SET_LANGUAGE: (state, language) => {
      state.language = language
      Cookies.set('language', language)
    },
    SET_SIZE: (state, size) => {
      state.size = size
      Cookies.set('size', size)
    },
    [types.INCREMENT](state) {
      state.count++
    },
    [types.DECREMENT](state) {
      state.count--
    },
    [types.ADMING_LOGINSTATE](state, params) {
      state.loginState = Object.assign({
        userInfo: {
          userName: '',
          email: '',
          logo: '',
          group: []
        },
        state: false
      }, {
        userInfo: params.userInfo,
        state: params.loginState || false,
        noticeCounts: params.noticeCounts
      });
    },
    [types.ADMINUSERFORMSTATE](state, formState) {
      state.adminUser.formState.show = formState.show;
      state.adminUser.formState.edit = formState.edit;
      if (!_.isEmpty(formState.formData)) {
        state.adminUser.formState.formData = formState.formData
      } else {
        state.adminUser.formState.formData = {
          name: '',
          userName: '',
          password: '',
          confirmPassword: '',
          group: '',
          email: '',
          comments: '',
          phoneNum: '',
          countryCode: '',
        }
      }

    },
    [types.ADMINUSERLIST](state, userlist) {
      state.adminUser.userList = userlist
    },
    [types.ADMINGROUP_FORMSTATE](state, formState) {
      state.adminGroup.formState.show = formState.show;
      state.adminGroup.formState.edit = formState.edit;
      if (!_.isEmpty(formState.formData)) {
        state.adminGroup.formState.formData = formState.formData
      } else {
        state.adminGroup.formState.formData = {
          name: '',
          comments: '',
          enable: false
        }
      }

    },
    [types.ADMINGROUP_ROLEFORMSTATE](state, formState) {
      state.adminGroup.roleFormState.show = formState.show;
      state.adminGroup.roleFormState.edit = formState.edit;
      state.adminGroup.roleFormState.formData = Object.assign({
        name: '',
        comments: '',
        enable: false,
        power: []
      }, formState.formData);
    },
    [types.ADMINGROUP_LIST](state, rolelist) {
      state.adminGroup.roleList = rolelist
    },
    [types.ADMINRESOURCE_FORMSTATE](state, formState) {
      state.adminResource.formState.show = formState.show;
      state.adminResource.formState.edit = formState.edit;
      state.adminResource.formState.type = formState.type;
      state.adminResource.formState.formData = Object.assign({
        label: '',
        type: '',
        api: '',
        icon: '',
        routePath: '',
        componentPath: '',
        enable: true,
        parentId: '',
        sortId: 0,
        comments: '',
        parent: {
          id: '',
          label: ''
        }
      }, formState.formData);

    },
    [types.ADMINRESOURCE_LIST](state, resourceList) {
      state.adminResource.resourceList = resourceList
    },
    [types.ADMINTEMPLATE_LIST](state, templateList) {
      state.adminTemplate.templateList = templateList
    },
    [types.MYTEMPLATE_LIST](state, templateList) {
      state.myTemplates.templateList = templateList
    },
    [types.TEMPLATECONFIG_FORMSTATE](state, formState) {
      state.myTemplates.formState.show = formState.show;
      state.myTemplates.formState.edit = formState.edit;
      state.myTemplates.formState.formData = Object.assign({
        name: '',
        alias: '',
        comments: ''
      }, formState.formData);
    },
    [types.TEMPLATEITEMFORDER_LIST](state, forderList) {
      state.myTemplates.templateItemForderList = forderList
    },
    [types.DORACMSTEMPLATE_LIST](state, templist) {
      state.tempShoplist = templist
    },
    [types.SYSTEMCONFIG_CONFIGLIST](state, config) {
      state.systemConfig.configs = Object.assign({
        siteName: '',
        ogTitle: '',
        siteDomain: '',
        siteDiscription: '',
        siteKeywords: '',
        siteEmailServer: '',
        siteEmail: '',
        siteEmailPwd: '',
        mongoDBPath: '',
        databackForderPath: '',
        allowUserPostContent: true,
        allowUserCreateCommunity: true,
        shareArticlScore: '',
        bakDataRate: '1',
        bakDatabyTime: false
      }, config)
    },
    [types.CONTENTCATEGORYS_FORMSTATE](state, formState) {
      state.contentCategory.formState.show = formState.show;
      state.contentCategory.formState.edit = formState.edit;
      state.contentCategory.formState.type = formState.type;
      state.contentCategory.formState.formData = Object.assign({
        name: '',
        enable: false,
        defaultUrl: '',
        parentId: '',
        parentObj: {},
        contentTemp: '',
        sortId: 0,
        comments: '',
        type: '1'
      }, formState.formData);

    },
    [types.CONTENTCATEGORYS_LIST](state, categoryList) {
      state.contentCategory.categoryList = categoryList
    },
    [types.CONTENT_FORMSTATE](state, formState) {
      state.content.formState.edit = formState.edit;
      state.content.formState.formData = Object.assign({
        targetUser: '',
        title: '',
        stitle: '',
        type: '1',
        categories: [],
        keywords: '',
        sortPath: '',
        tags: [],
        keywords: '',
        sImg: '',
        discription: '',
        author: {},
        uAuthor: '',
        markDownComments: '',
        state: '1',
        isTop: 0,
        roofPlacement: '0',
        clickNum: 0,
        comments: '',
        simpleComments: '',
        commentNum: 0,
        likeNum: 0
      }, formState.formData);

    },

    [types.CONTENT_SUBSTITUTE_FORMSTATE](state, formState) {
      state.substituteContent.formState.edit = formState.edit;
      state.substituteContent.formState.formData = Object.assign({
        targetUser: '',
        title: '',
        stitle: '',
        type: '1',
        categories: [],
        keywords: '',
        sortPath: '',
        tags: [],
        keywords: '',
        sImg: '',
        discription: '',
        author: {},
        uAuthor: '',
        markDownComments: '',
        state: '1',
        isTop: 0,
        roofPlacement: '0',
        clickNum: 0,
        comments: '',
        simpleComments: '',
        commentNum: 0,
        likeNum: 0
      }, formState.formData);

    },
    [types.CONTENT_LIST](state, contentList) {
      state.content.contentList = contentList
    },
    [types.CONTENT_ONE](state, content) {
      state.content.content = content
    },
    [types.DIRECTUSERFORMSTATE](state, formState) {
      state.directUser.formState.show = formState.show;
      state.directUser.formState.edit = formState.edit;
      state.directUser.formState.type = formState.type;
      state.directUser.formState.formData = Object.assign({
        name: '',
        alias: '',
        targetUser: ''
      }, formState.formData);
    },
    [types.DIRECTSPECIALFORMSTATE](state, formState) {
      state.directSpecial.formState.show = formState.show;
      state.directSpecial.formState.edit = formState.edit;
      state.directSpecial.formState.type = formState.type;
      state.directSpecial.formState.formData = Object.assign({
        name: '',
        alias: '',
        targetSpecials: ''
      }, formState.formData);
    },
    [types.CONTENTTAG_FORMSTATE](state, formState) {
      state.contentTag.formState.show = formState.show;
      state.contentTag.formState.edit = formState.edit;
      state.contentTag.formState.type = formState.type;
      state.contentTag.formState.formData = Object.assign({
        name: '',
        alias: '',
        comments: ''
      }, formState.formData);
    },
    [types.CONTENTTAG_LIST](state, tagList) {
      state.contentTag.tagList = tagList
    },
    [types.SPECIAL_FORMSTATE](state, formState) {
      state.special.formState.show = formState.show;
      state.special.formState.edit = formState.edit;
      state.special.formState.type = formState.type;
      state.special.formState.formData = Object.assign({
        name: '',
        sImg: '',
        comments: '',
        state: '1',
        category: [],
        creator: {},
        approver: {},
      }, formState.formData);
    },
    [types.SPECIAL_LIST](state, list) {
      state.special.list = list
    },
    [types.MASTERCATEGORY_FORMSTATE](state, formState) {
      state.masterCategory.formState.show = formState.show;
      state.masterCategory.formState.edit = formState.edit;
      state.masterCategory.formState.type = formState.type;
      state.masterCategory.formState.formData = Object.assign({
        name: '',
        sImg: '',
        comments: ''
      }, formState.formData);
    },
    [types.MASTERCATEGORY_LIST](state, list) {
      state.masterCategory.list = list
    },
    [types.CONTENTMESSAGE_FORMSTATE](state, formState) {
      state.contentMessage.formState.show = formState.show;
      state.contentMessage.formState.edit = formState.edit;
      state.contentMessage.formState.type = formState.type;
      state.contentMessage.formState.formData = Object.assign({
        contentId: '',
        content: '',
        replyId: '',
        author: '',
        relationMsgId: ''
      }, formState.formData);
      state.contentMessage.formState.parentformData = Object.assign({
        contentId: '',
        content: '',
        replyId: '',
        author: '',
        relationMsgId: ''
      }, formState.parentformData);
    },
    [types.CONTENTMESSAGE_LIST](state, messageList) {
      state.contentMessage.messageList = messageList
    },
    [types.REGUSERFORMSTATE](state, formState) {
      state.regUser.formState.show = formState.show;
      state.regUser.formState.edit = formState.edit;

      state.regUser.formState.formData = Object.assign({
        name: '',
        userName: '',
        group: '',
        watchers: [],
        followers: [],
        category: [],
        email: '',
        comments: '',
        phoneNum: '',
        enable: true
      }, formState.formData);

    },
    [types.REGUSERLIST](state, userlist) {
      state.regUser.userList = userlist
    },
    [types.BAKUPDATA_LIST](state, list) {
      state.bakDataList = list
    },
    [types.SYSTEMOPTIONLOGS_LIST](state, list) {
      state.systemOptionLogs = list
    },
    [types.SYSTEMNOTIFY_LIST](state, list) {
      state.systemNotify = list
    },
    [types.SYSTEMANNOUNCE_LIST](state, list) {
      state.systemAnnounce = list
    },
    [types.SYSTEMANNOUNCE_FORMSTATE](state, formState) {
      state.announceFormState = Object.assign({
        title: '',
        content: ''
      }, formState.formData);

    },
    [types.ADS_LIST](state, list) {
      state.ads.list = list
    },
    [types.ADS_INFO_FORMSTATE](state, formState) {
      state.ads.infoFormState.edit = formState.edit;
      state.ads.infoFormState.formData = Object.assign({
        name: '',
        type: '1',
        height: '',
        comments: '',
        items: [],
        state: true,
        carousel: true
      }, formState.formData);
    },
    [types.ADS_ITEM_FORMSTATE](state, formState) {
      state.ads.itemFormState.edit = formState.edit;
      state.ads.itemFormState.show = formState.show;
      state.ads.itemFormState.formData = Object.assign({
        title: '',
        link: '',
        appLink: '',
        appLinkType: '',
        width: '',
        height: '',
        alt: '',
        sImg: '',
      }, formState.formData);
    },
    [types.MAIN_SITEBASIC_INFO](state, list) {
      state.basicInfo = list
    },
    [types.COMMUNITYTAG_FORMSTATE](state, formState) {
      state.communityTag.formState.show = formState.show;
      state.communityTag.formState.edit = formState.edit;
      state.communityTag.formState.type = formState.type;
      state.communityTag.formState.formData = Object.assign({
        name: '',
        alias: '',
        comments: ''
      }, formState.formData);
    },
    [types.COMMUNITYTAG_LIST](state, tagList) {
      state.communityTag.tagList = tagList
    },

    [types.COMMUNITY_FORMSTATE](state, formState) {
      state.community.formState.show = formState.show;
      state.community.formState.edit = formState.edit;
      state.community.formState.type = formState.type;
      state.community.formState.formData = Object.assign({
        name: '',
        sImg: '',
        open: '',
        questions: [],
        tags: [],
        comments: '',
        state: '0'
      }, formState.formData);
    },
    [types.COMMUNITY_LIST](state, list) {
      state.community.list = list
    },
    [types.SITEMESSAGE_FORMSTATE](state, formState) {
      state.siteMessage.formState.show = formState.show;
      state.siteMessage.formState.edit = formState.edit;
      state.siteMessage.formState.type = formState.type;
      state.siteMessage.formState.formData = Object.assign({
        content: '',
        isRead: false,
        user: {},
        type: '1'
      }, formState.formData);
    },
    [types.SITEMESSAGE_LIST](state, list) {
      state.siteMessage.list = list
    },

    [types.CLASSTYPE_FORMSTATE](state, formState) {
      state.classType.formState.show = formState.show;
      state.classType.formState.edit = formState.edit;
      state.classType.formState.type = formState.type;
      state.classType.formState.formData = Object.assign({
        name: '',
        comments: ''
      }, formState.formData);
    },
    [types.CLASSTYPE_LIST](state, list) {
      state.classType.list = list
    },

    [types.CLASSINFO_FORMSTATE](state, formState) {
      state.classInfo.formState.show = formState.show;
      state.classInfo.formState.edit = formState.edit;
      state.classInfo.formState.type = formState.type;
      state.classInfo.formState.formData = Object.assign({
        name: '',
        categories: [],
        comments: '',
        buyTips: '',
        sImg: '',
        price: 0,
        vipPrice: 0,
        unit: '',
        recommend: false,
        vip: false,
        discount: false,
        boutique: false,
      }, formState.formData);
    },
    [types.CLASSINFO_LIST](state, list) {
      state.classInfo.list = list
    },

    [types.CLASSCONTENT_FORMSTATE](state, formState) {
      state.classContent.formState.show = formState.show;
      state.classContent.formState.edit = formState.edit;
      state.classContent.formState.type = formState.type;
      state.classContent.formState.formData = Object.assign({
        catalog: '',
        duration: '',
        basicInfo: '',
        author: '',
        comments: ''
      }, formState.formData);
    },
    [types.CLASSCONTENT_LIST](state, list) {
      state.classContent.list = list
    },

    [types.CLASSFEEDBACK_FORMSTATE](state, formState) {
      state.classFeedback.formState.show = formState.show;
      state.classFeedback.formState.edit = formState.edit;
      state.classFeedback.formState.type = formState.type;
      state.classFeedback.formState.formData = Object.assign({
        classContent: '',
        content: '',
        author: '',
        replyId: '',
        relationMsgId: ''
      }, formState.formData);
    },
    [types.CLASSFEEDBACK_LIST](state, list) {
      state.classFeedback.list = list
    },

    [types.COMMUNITYCONTENT_FORMSTATE](state, formState) {
      state.communityContent.formState.show = formState.show;
      state.communityContent.formState.edit = formState.edit;
      state.communityContent.formState.type = formState.type;
      state.communityContent.formState.formData = Object.assign({
        user: {},
        community: {},
        comments: '',
      }, formState.formData);
    },
    [types.COMMUNITYCONTENT_LIST](state, list) {
      state.communityContent.list = list
    },

    [types.USERACTIONHIS_FORMSTATE](state, formState) {
      state.userActionHis.formState.show = formState.show;
      state.userActionHis.formState.edit = formState.edit;
      state.userActionHis.formState.type = formState.type;
      state.userActionHis.formState.formData = Object.assign({
        name: '',
        comments: ''
      }, formState.formData);
    },
    [types.USERACTIONHIS_LIST](state, list) {
      state.userActionHis.list = list
    },

    [types.CLASSSCORE_FORMSTATE](state, formState) {
      state.classScore.formState.show = formState.show;
      state.classScore.formState.edit = formState.edit;
      state.classScore.formState.type = formState.type;
      state.classScore.formState.formData = Object.assign({
        score: '',
        comments: '',
        user: {},
        class: {}
      }, formState.formData);
    },
    [types.CLASSSCORE_LIST](state, list) {
      state.classScore.list = list
    },

    [types.BILLRECORD_FORMSTATE](state, formState) {
      state.billRecord.formState.show = formState.show;
      state.billRecord.formState.edit = formState.edit;
      state.billRecord.formState.type = formState.type;
      state.billRecord.formState.formData = Object.assign({
        name: '',
        comments: '',
        user: {}
      }, formState.formData);
    },
    [types.BILLRECORD_LIST](state, list) {
      state.billRecord.list = list
    },
    [types.INVITIONRECORD_FORMSTATE](state, formState) {
      state.invitionRecord.formState.show = formState.show;
      state.invitionRecord.formState.edit = formState.edit;
      state.invitionRecord.formState.type = formState.type;
      state.invitionRecord.formState.formData = Object.assign({
        name: '',
        comments: '',
        user: {},
        target_user: {},
      }, formState.formData);
    },
    [types.INVITIONRECORD_LIST](state, list) {
      state.invitionRecord.list = list
    },

    [types.MVPC_BOOK_FORMSTATE](state, formState) {
      state.mstt_book.formState.show = formState.show;
      state.mstt_book.formState.edit = formState.edit;
      state.mstt_book.formState.type = formState.type;
      state.mstt_book.formState.formData = Object.assign({
        name: '',
        comments: ''
      }, formState.formData);
    },
    [types.MVPC_BOOK_LIST](state, list) {
      state.mstt_book.list = list
    },

    [types.REWARD_HIS_FORMSTATE](state, formState) {
      state.reward_his.formState.show = formState.show;
      state.reward_his.formState.edit = formState.edit;
      state.reward_his.formState.type = formState.type;
      state.reward_his.formState.formData = Object.assign({
        name: '',
        comments: ''
      }, formState.formData);
    },
    [types.REWARD_HIS_LIST](state, list) {
      state.reward_his.list = list
    },

    [types.REVIEW_HIS_FORMSTATE](state, formState) {
      state.review_his.formState.show = formState.show;
      state.review_his.formState.edit = formState.edit;
      state.review_his.formState.type = formState.type;
      state.review_his.formState.formData = Object.assign({
        name: '',
        comments: ''
      }, formState.formData);
    },
    [types.REVIEW_HIS_LIST](state, list) {
      state.review_his.list = list
    },

    [types.USER_MI_MP_HIS_FORMSTATE](state, formState) {
      state.user_mi_mp_his.formState.show = formState.show;
      state.user_mi_mp_his.formState.edit = formState.edit;
      state.user_mi_mp_his.formState.type = formState.type;
      state.user_mi_mp_his.formState.formData = Object.assign({
        name: '',
        comments: ''
      }, formState.formData);
    },
    [types.USER_MI_MP_HIS_LIST](state, list) {
      state.user_mi_mp_his.list = list
    },

    [types.USER_EV_HIS_FORMSTATE](state, formState) {
      state.user_ev_his.formState.show = formState.show;
      state.user_ev_his.formState.edit = formState.edit;
      state.user_ev_his.formState.type = formState.type;
      state.user_ev_his.formState.formData = Object.assign({
        name: '',
        comments: ''
      }, formState.formData);
    },
    [types.USER_EV_HIS_LIST](state, list) {
      state.user_ev_his.list = list
    },

    [types.USER_REWARD_HIS_FORMSTATE](state, formState) {
      state.user_reward_his.formState.show = formState.show;
      state.user_reward_his.formState.edit = formState.edit;
      state.user_reward_his.formState.type = formState.type;
      state.user_reward_his.formState.formData = Object.assign({
        name: '',
        comments: ''
      }, formState.formData);
    },
    [types.USER_REWARD_HIS_LIST](state, list) {
      state.user_reward_his.list = list
    },

    [types.THRESHOLD_MASTER_FORMSTATE](state, formState) {
      state.threshold_master.formState.show = formState.show;
      state.threshold_master.formState.edit = formState.edit;
      state.threshold_master.formState.type = formState.type;
      state.threshold_master.formState.formData = Object.assign({
        name: '',
        comments: ''
      }, formState.formData);
    },
    [types.THRESHOLD_MASTER_LIST](state, list) {
      state.threshold_master.list = list
    },

    [types.ACTION_LIMIT_MASTER_FORMSTATE](state, formState) {
      state.action_limit_master.formState.show = formState.show;
      state.action_limit_master.formState.edit = formState.edit;
      state.action_limit_master.formState.type = formState.type;
      state.action_limit_master.formState.formData = Object.assign({
        name: '',
        comments: ''
      }, formState.formData);
    },
    [types.ACTION_LIMIT_MASTER_LIST](state, list) {
      state.action_limit_master.list = list
    },

    [types.WEIGHT_MASTER_FORMSTATE](state, formState) {
      state.weight_master.formState.show = formState.show;
      state.weight_master.formState.edit = formState.edit;
      state.weight_master.formState.type = formState.type;
      state.weight_master.formState.formData = Object.assign({
        name: '',
        comments: ''
      }, formState.formData);
    },
    [types.WEIGHT_MASTER_LIST](state, list) {
      state.weight_master.list = list
    },

    [types.CLASSPAYRECORD_FORMSTATE](state, formState) {
      state.classPayRecord.formState.show = formState.show;
      state.classPayRecord.formState.edit = formState.edit;
      state.classPayRecord.formState.type = formState.type;
      state.classPayRecord.formState.formData = Object.assign({
        name: '',
        comments: ''
      }, formState.formData);
    },
    [types.CLASSPAYRECORD_LIST](state, list) {
      state.classPayRecord.list = list
    },

    [types.RESETAMOUNT_FORMSTATE](state, formState) {
      state.resetAmount.formState.show = formState.show;
      state.resetAmount.formState.edit = formState.edit;
      state.resetAmount.formState.type = formState.type;
      state.resetAmount.formState.formData = Object.assign({
        money: '',
        default: false,
        unit: ''
      }, formState.formData);
    },
    [types.RESETAMOUNT_LIST](state, list) {
      state.resetAmount.list = list
    },

    [types.AGREEMENT_FORMSTATE](state, formState) {
      state.agreement.formState.show = formState.show;
      state.agreement.formState.edit = formState.edit;
      state.agreement.formState.type = formState.type;
      state.agreement.formState.formData = Object.assign({
        name: '',
        comments: ''
      }, formState.formData);
    },
    [types.AGREEMENT_LIST](state, list) {
      state.agreement.list = list
    },

    [types.HELPCENTER_FORMSTATE](state, formState) {
      state.helpCenter.formState.show = formState.show;
      state.helpCenter.formState.edit = formState.edit;
      state.helpCenter.formState.type = formState.type;
      state.helpCenter.formState.formData = Object.assign({
        name: '',
        type: '',
        lang: '1',
        comments: ''
      }, formState.formData);
    },
    [types.HELPCENTER_LIST](state, list) {
      state.helpCenter.list = list
    },

    [types.FEEDBACK_FORMSTATE](state, formState) {
      state.feedBack.formState.show = formState.show;
      state.feedBack.formState.edit = formState.edit;
      state.feedBack.formState.type = formState.type;
      state.feedBack.formState.formData = Object.assign({
        name: '',
        comments: '',
        phoneNum: '',
      }, formState.formData);
    },
    [types.FEEDBACK_LIST](state, list) {
      state.feedBack.list = list
    },

    [types.CURRENCYAPPROVAL_FORMSTATE](state, formState) {
      state.currencyApproval.formState.show = formState.show;
      state.currencyApproval.formState.edit = formState.edit;
      state.currencyApproval.formState.type = formState.type;
      state.currencyApproval.formState.formData = Object.assign({
        coins: "",
        user: {},
        unit: "",
        date: "",
        walletAddress: "",
        state: "",
        approver: '',
        comments: "",
        uuid: "",
        dismissReason: '',
      }, formState.formData);
    },
    [types.CURRENCYAPPROVAL_LIST](state, list) {
      state.currencyApproval.list = list
    },

    [types.CREATIVERIGHT_FORMSTATE](state, formState) {
      state.creativeRight.formState.show = formState.show;
      state.creativeRight.formState.edit = formState.edit;
      state.creativeRight.formState.type = formState.type;
      state.creativeRight.formState.formData = Object.assign({
        date: '',
        user: {},
        state: '',
        approver: '',
        comments: '',
        category: {},
        newCateName: '',
        dismissReason: '',
      }, formState.formData);
    },
    [types.CREATIVERIGHT_LIST](state, list) {
      state.creativeRight.list = list
    },

    [types.REPORTRECORD_FORMSTATE](state, formState) {
      state.reportRecord.formState.show = formState.show;
      state.reportRecord.formState.edit = formState.edit;
      state.reportRecord.formState.type = formState.type;
      state.reportRecord.formState.formData = Object.assign({
        user: "",
        comments: "",
        date: "",
        updatetime: "",
        approver: "",
        state: "",
        target_content: "",
        target_message: "",
        target_communityContent: "",
        dismissReason: '',
      }, formState.formData);
    },
    [types.REPORTRECORD_LIST](state, list) {
      state.reportRecord.list = list
    },

    [types.IDENTITYAUTHENTICATION_FORMSTATE](state, formState) {
      state.identityAuthentication.formState.show = formState.show;
      state.identityAuthentication.formState.edit = formState.edit;
      state.identityAuthentication.formState.type = formState.type;
      state.identityAuthentication.formState.formData = Object.assign({
        name: "",
        cardType: "",
        idCardNo: "",
        cardFront: "",
        cardBack: "",
        date: "",
        user: {},
        approver: "",
        state: '0',
        type: '1',
        comments: '',
        masterCategory: '',
        diploma_label: '',
        diploma: [], // 学历证书
        professionalCertificate_label: '', // 专业证书
        professionalCertificate: [], // 专业证书
        otherCertificate_label: '', // 其它证书
        otherCertificate: [], // 其它证书
        pastExperience: [], // 过往经历
        pastArticles: [], // 过往文章
        publishedBooks: [], // 出刊书籍
        uploadAttachments: [], // 附件上传
        dismissReason: '',
      }, formState.formData);
    },
    [types.IDENTITYAUTHENTICATION_LIST](state, list) {
      state.identityAuthentication.list = list
    },

    [types.SIGNIN_FORMSTATE](state, formState) {
      state.signIn.formState.show = formState.show;
      state.signIn.formState.edit = formState.edit;
      state.signIn.formState.type = formState.type;
      state.signIn.formState.formData = Object.assign({
        name: '',
        comments: ''
      }, formState.formData);
    },
    [types.SIGNIN_LIST](state, list) {
      state.signIn.list = list
    },

    [types.OPENVIPRECORD_FORMSTATE](state, formState) {
      state.openVipRecord.formState.show = formState.show;
      state.openVipRecord.formState.edit = formState.edit;
      state.openVipRecord.formState.type = formState.type;
      state.openVipRecord.formState.formData = Object.assign({
        setMeal: '',
        user: {},
        comments: ''
      }, formState.formData);
    },
    [types.OPENVIPRECORD_LIST](state, list) {
      state.openVipRecord.list = list
    },

    [types.VIPSETMEAL_FORMSTATE](state, formState) {
      state.vipSetMeal.formState.show = formState.show;
      state.vipSetMeal.formState.edit = formState.edit;
      state.vipSetMeal.formState.type = formState.type;
      state.vipSetMeal.formState.formData = Object.assign({
        time: '',
        coins: 0,
        unit: '',
        default: false,
        state: false,
        comments: ''
      }, formState.formData);
    },
    [types.VIPSETMEAL_LIST](state, list) {
      state.vipSetMeal.list = list
    },

    [types.COMMUNITYMESSAGE_FORMSTATE](state, formState) {
      state.communityMessage.formState.show = formState.show;
      state.communityMessage.formState.edit = formState.edit;
      state.communityMessage.formState.type = formState.type;
      state.communityMessage.formState.formData = Object.assign({
        contentId: '',
        content: '',
        replyId: '',
        author: '',
        relationMsgId: ''
      }, formState.formData);
    },
    [types.COMMUNITYMESSAGE_LIST](state, list) {
      state.communityMessage.list = list
    },

    [types.HOTSEARCH_FORMSTATE](state, formState) {
      state.hotSearch.formState.show = formState.show;
      state.hotSearch.formState.edit = formState.edit;
      state.hotSearch.formState.type = formState.type;
      state.hotSearch.formState.formData = Object.assign({
        name: '',
        comments: ''
      }, formState.formData);
    },
    [types.HOTSEARCH_LIST](state, list) {
      state.hotSearch.list = list
    },

    [types.VERSIONMANAGE_FORMSTATE](state, config) {
      state.versionManage.configs = Object.assign({
        title: '',
        description: '',
        version: '',
        versionName: '',
        forcibly: false,
        url: ''
      }, config)
    },

    [types.VERSIONMANAGEIOS_FORMSTATE](state, config) {
      state.versionManageIos.configs = Object.assign({
        title: '',
        description: '',
        version: '',
        versionName: '',
        forcibly: false,
        url: ''
      }, config)
    },

    [types.MBTMANAGE_FORMSTATE](state, formState) {
      state.mbtManage.formState.show = formState.show;
      state.mbtManage.formState.edit = formState.edit;
      state.mbtManage.formState.type = formState.type;
      state.mbtManage.formState.formData = Object.assign({
        user: '',
        logs: '',
        date: '',
        coins: '',
      }, formState.formData);
    },
    [types.MBTMANAGE_LIST](state, list) {
      state.mbtManage.list = list
    },

    [types.INTEGRALONFIG_LIST](state, config) {

      state.integralonfig.configs = Object.assign({
        login_first: 0, // 首次登陆
        register: 0, // 用户注册
        browse: 0, // 浏览
        browse_limit: 0, // 浏览
        give_thumbs_up: 0, // 点赞
        give_thumbs_up_limit: 0, // 点赞
        comment: 0, // 评论
        comment_limit: 0, // 评论
        community_comment: 0, // 社群评论
        community_comment_limit: 0, // 社群评论
        appreciate: 0, // 打赏
        appreciate_limit: 0, // 打赏
        appreciate_in: 0, // 打赏收入
        appreciate_out: 0, // 打赏支出
        collections: 0, // 收藏
        collections_limit: 0, // 收藏
        follow: 0, // 关注
        follow_limit: 0, // 关注
        forward: 0, // 转发
        forward_limit: 0, // 转发
        community_building: 0, // 社群建设
        community_building_limit: 0, // 社群建设
        invitation: 0, // 邀请
        report: 0, // 举报
        review: 0, // 审核
        creat_content: 0, // 内容创作
        creat_identity: 0, // 实名认证
        shop_send: 0, // 商城会员发放
        transfer_coins: 0, // 提币
        recharge: 0, // 充值
        subscribe: 0, // 课程订购
        buyVip: 0, // 购买会员
        signIn: 0, // 签到
        signIn_continuity: 0, // 签到
        brithday: 0, // 生日
      }, config);
    },
    //StoreAppMutations





  },
  actions: {
    toggleSideBar({
      commit
    }) {
      commit('TOGGLE_SIDEBAR')
    },
    closeSideBar({
      commit
    }, {
      withoutAnimation
    }) {
      commit('CLOSE_SIDEBAR', withoutAnimation)
    },
    toggleDevice({
      commit
    }, device) {
      commit('TOGGLE_DEVICE', device)
    },
    setLanguage({
      commit
    }, language) {
      commit('SET_LANGUAGE', language)
    },
    setSize({
      commit
    }, size) {
      commit('SET_SIZE', size)
    },
    increment: ({
      commit
    }) => {
      console.log(commit);
      commit(types.INCREMENT);
    },
    decrement: ({
      commit
    }) => {
      console.log(commit);
      commit(types.DECREMENT);
    },
    handleOpen: ({
      commit
    }) => {
      console.log(commit);
    },
    handleClose: ({
      commit
    }) => {
      console.log(commit);
    },
    handleSelect: ({
      commit
    }) => {
      console.log(commit);
    },
    loginState: ({
      commit
    }, params = {
      userInfo: {},
      state: false
    }) => {
      services.getUserSession().then((result) => {
        commit(types.ADMING_LOGINSTATE, result.data.data)
      })
    },
    showAdminUserForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.ADMINUSERFORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },
    hideAdminUserForm: ({
      commit
    }) => {
      commit(types.ADMINUSERFORMSTATE, {
        show: false
      })
    },
    getAdminUserList({
      commit
    }, params = {}) {
      services.adminUserList(params).then((result) => {
        commit(types.ADMINUSERLIST, result.data.data)
      })
    },
    showAdminGroupForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.ADMINGROUP_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },
    hideAdminGroupForm: ({
      commit
    }) => {
      commit(types.ADMINGROUP_FORMSTATE, {
        show: false
      })
    },
    showAdminGroupRoleForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.ADMINGROUP_ROLEFORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },
    hideAdminGroupRoleForm: ({
      commit
    }) => {
      commit(types.ADMINGROUP_ROLEFORMSTATE, {
        show: false
      })
    },
    getAdminGroupList({
      commit
    }, params = {}) {
      services.adminGroupList(params).then((result) => {
        commit(types.ADMINGROUP_LIST, result.data.data)
      })
    },
    showAdminResourceForm: ({
      commit
    }, params = {
      type: 'root',
      edit: false,
      formData: {}
    }) => {
      commit(types.ADMINRESOURCE_FORMSTATE, {
        show: true,
        type: params.type,
        edit: params.edit,
        formData: params.formData
      })
    },
    hideAdminResourceForm: ({
      commit
    }) => {
      commit(types.ADMINRESOURCE_FORMSTATE, {
        show: false
      })
    },
    getAdminResourceList({
      commit
    }, params = {}) {
      services.adminResourceList(params).then((result) => {
        let treeData = renderTreeData(result.data.data);
        commit(types.ADMINRESOURCE_LIST, treeData)
      })
    },
    getAdminTemplateList({
      commit
    }, params = {}) {
      services.adminTemplateList(params).then((result) => {
        let treeData = renderTreeData(result.data.data);
        commit(types.ADMINTEMPLATE_LIST, treeData)
      })
    },
    getMyTemplateList({
      commit
    }, params = {}) {
      services.getMyTemplateList(params).then((result) => {
        commit(types.MYTEMPLATE_LIST, result.data.data)
      })
    },
    showTemplateConfigForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.TEMPLATECONFIG_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },
    hideTemplateConfigForm: ({
      commit
    }) => {
      commit(types.TEMPLATECONFIG_FORMSTATE, {
        show: false
      })
    },
    getTemplateItemForderList({
      commit
    }, params = {}) {
      services.getTemplateItemlist(params).then((result) => {
        commit(types.TEMPLATEITEMFORDER_LIST, result.data.data)
      })
    },
    getTempsFromShop({
      commit
    }, params = {}) {
      services.getTemplatelistfromShop(params).then((result) => {
        commit(types.DORACMSTEMPLATE_LIST, result.data.data)
      })
    },
    getSystemConfig({
      commit
    }, params = {}) {
      services.getSystemConfigs(params).then((config) => {
        let currentConfig = (config.data.data && config.data.data.docs) ? config.data.data.docs[0] : {};
        commit(types.SYSTEMCONFIG_CONFIGLIST, currentConfig)
      })
    },
    showContentCategoryForm: ({
      commit
    }, params = {
      type: 'root',
      edit: false,
      formData: {}
    }) => {
      commit(types.CONTENTCATEGORYS_FORMSTATE, {
        show: true,
        type: params.type,
        edit: params.edit,
        formData: params.formData
      })
    },
    hideContentCategoryForm: ({
      commit
    }) => {
      commit(types.CONTENTCATEGORYS_FORMSTATE, {
        show: false
      })
    },
    getContentCategoryList({
      commit
    }, params = {}) {
      services.contentCategoryList(params).then((result) => {
        let treeData = renderTreeData(result.data.data);
        commit(types.CONTENTCATEGORYS_LIST, treeData)
      })
    },

    showContentForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.CONTENT_FORMSTATE, {
        edit: params.edit,
        formData: params.formData
      })
    },
    showSubstituteContentForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.CONTENT_SUBSTITUTE_FORMSTATE, {
        edit: params.edit,
        formData: params.formData
      })
    },
    showDirectUserForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.DIRECTUSERFORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },
    hideDirectUserForm: ({
      commit
    }) => {
      commit(types.DIRECTUSERFORMSTATE, {
        show: false
      })
    },
    showDirectSpecialForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.DIRECTSPECIALFORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },
    hideDirectSpecialForm: ({
      commit
    }) => {
      commit(types.DIRECTSPECIALFORMSTATE, {
        show: false
      })
    },
    getContentList({
      commit
    }, params = {}) {
      services.contentList(params).then((result) => {
        commit(types.CONTENT_LIST, result.data.data)
      })
    },

    getOneContent({
      commit
    }, params = {}) {
      services.contentInfo(params).then((result) => {
        commit(types.CONTENT_ONE, result.data.data)
      })
    },
    showContentTagForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.CONTENTTAG_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },
    hideContentTagForm: ({
      commit
    }) => {
      commit(types.CONTENTTAG_FORMSTATE, {
        show: false
      })
    },
    getContentTagList({
      commit
    }, params = {}) {
      services.contentTagList(params).then((result) => {
        commit(types.CONTENTTAG_LIST, result.data.data)
      })
    },
    showSpecialForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.SPECIAL_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },
    hideSpecialForm: ({
      commit
    }) => {
      commit(types.SPECIAL_FORMSTATE, {
        show: false
      })
    },
    getSpecialList({
      commit
    }, params = {}) {
      services.specialList(params).then((result) => {
        commit(types.SPECIAL_LIST, result.data.data)
      })
    },
    showMasterCategoryForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.MASTERCATEGORY_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },
    hideMasterCategoryForm: ({
      commit
    }) => {
      commit(types.MASTERCATEGORY_FORMSTATE, {
        show: false
      })
    },
    getMasterCategoryList({
      commit
    }, params = {}) {
      services.masterCategoryList(params).then((result) => {
        commit(types.MASTERCATEGORY_LIST, result.data.data)
      })
    },
    showContentMessageForm: ({
      commit
    }, params = {
      edit: false,
      formData: {},
      parentformData: {}
    }) => {
      commit(types.CONTENTMESSAGE_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData,
        parentformData: params.parentformData
      })
    },
    hideContentMessageForm: ({
      commit
    }) => {
      commit(types.CONTENTMESSAGE_FORMSTATE, {
        show: false
      })
    },
    getContentMessageList({
      commit
    }, params = {}) {
      services.contentMessageList(params).then((result) => {
        commit(types.CONTENTMESSAGE_LIST, result.data.data)
      })
    },
    showRegUserForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.REGUSERFORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },
    hideRegUserForm: ({
      commit
    }) => {
      commit(types.REGUSERFORMSTATE, {
        show: false
      })
    },
    getRegUserList({
      commit
    }, params = {}) {
      services.regUserList(params).then((result) => {
        commit(types.REGUSERLIST, result.data.data)
      })
    },

    getBakDateList({
      commit
    }, params = {}) {
      services.getBakDataList(params).then((result) => {
        commit(types.BAKUPDATA_LIST, result.data.data)
      })
    },

    getSystemLogsList({
      commit
    }, params = {}) {
      services.getSystemOptionLogsList(params).then((result) => {
        commit(types.SYSTEMOPTIONLOGS_LIST, result.data.data)
      })
    },
    getSystemNotifyList({
      commit
    }, params = {}) {
      services.getSystemNotifyList(params).then((result) => {
        commit(types.SYSTEMNOTIFY_LIST, result.data.data)
      })
    },
    getSystemAnnounceList({
      commit
    }, params = {}) {
      services.getSystemAnnounceList(params).then((result) => {
        commit(types.SYSTEMANNOUNCE_LIST, result.data.data)
      })
    },
    showSysAnnounceForm: ({
      commit
    }, params = {}) => {
      commit(types.SYSTEMANNOUNCE_FORMSTATE, {
        formData: params
      })
    },
    getAdsList({
      commit
    }, params = {}) {
      services.getAdsList(params).then((result) => {
        commit(types.ADS_LIST, result.data.data)
      })
    },
    adsInfoForm: ({
      commit
    }, params = {}) => {
      commit(types.ADS_INFO_FORMSTATE, {
        edit: params.edit,
        formData: params.formData
      })
    },
    showAdsItemForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.ADS_ITEM_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },
    hideAdsItemForm: ({
      commit
    }) => {
      commit(types.ADS_ITEM_FORMSTATE, {
        show: false
      })
    },
    getSiteBasicInfo({
      commit
    }, params = {}) {
      services.getSiteBasicInfo(params).then((result) => {
        commit(types.MAIN_SITEBASIC_INFO, result.data.data)
      })
    },
    showCommunityForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.COMMUNITY_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },
    hideCommunityForm: ({
      commit
    }) => {
      commit(types.COMMUNITY_FORMSTATE, {
        show: false
      })
    },
    getCommunityList({
      commit
    }, params = {}) {
      services.communityList(params).then((result) => {
        commit(types.COMMUNITY_LIST, result.data.data)
      })
    },
    showCommunityTagForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.COMMUNITYTAG_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },
    hideCommunityTagForm: ({
      commit
    }) => {
      commit(types.COMMUNITYTAG_FORMSTATE, {
        show: false
      })
    },
    getCommunityTagList({
      commit
    }, params = {}) {
      services.communityTagList(params).then((result) => {
        commit(types.COMMUNITYTAG_LIST, result.data.data)
      })
    },
    showSiteMessageForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.SITEMESSAGE_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideSiteMessageForm: ({
      commit
    }) => {
      commit(types.SITEMESSAGE_FORMSTATE, {
        show: false
      })
    },

    getSiteMessageList({
      commit
    }, params = {}) {
      services.siteMessageList(params).then((result) => {
        commit(types.SITEMESSAGE_LIST, result.data.data)
      })
    },
    showClassTypeForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.CLASSTYPE_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideClassTypeForm: ({
      commit
    }) => {
      commit(types.CLASSTYPE_FORMSTATE, {
        show: false
      })
    },

    getClassTypeList({
      commit
    }, params = {}) {
      services.classTypeList(params).then((result) => {
        commit(types.CLASSTYPE_LIST, result.data.data)
      })
    },
    showClassInfoForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.CLASSINFO_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideClassInfoForm: ({
      commit
    }) => {
      commit(types.CLASSINFO_FORMSTATE, {
        show: false
      })
    },

    getClassInfoList({
      commit
    }, params = {}) {
      services.classInfoList(params).then((result) => {
        commit(types.CLASSINFO_LIST, result.data.data)
      })
    },
    showClassContentForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.CLASSCONTENT_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideClassContentForm: ({
      commit
    }) => {
      commit(types.CLASSCONTENT_FORMSTATE, {
        show: false
      })
    },

    getClassContentList({
      commit
    }, params = {}) {
      services.classContentList(params).then((result) => {
        commit(types.CLASSCONTENT_LIST, result.data.data)
      })
    },
    showClassFeedbackForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.CLASSFEEDBACK_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideClassFeedbackForm: ({
      commit
    }) => {
      commit(types.CLASSFEEDBACK_FORMSTATE, {
        show: false
      })
    },

    getClassFeedbackList({
      commit
    }, params = {}) {
      services.classFeedbackList(params).then((result) => {
        commit(types.CLASSFEEDBACK_LIST, result.data.data)
      })
    },
    showCommunityContentForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.COMMUNITYCONTENT_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideCommunityContentForm: ({
      commit
    }) => {
      commit(types.COMMUNITYCONTENT_FORMSTATE, {
        show: false
      })
    },

    getCommunityContentList({
      commit
    }, params = {}) {
      services.communityContentList(params).then((result) => {
        commit(types.COMMUNITYCONTENT_LIST, result.data.data)
      })
    },
    showUserActionHisForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.USERACTIONHIS_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideUserActionHisForm: ({
      commit
    }) => {
      commit(types.USERACTIONHIS_FORMSTATE, {
        show: false
      })
    },

    getUserActionHisList({
      commit
    }, params = {}) {
      services.userActionHisList(params).then((result) => {
        commit(types.USERACTIONHIS_LIST, result.data.data)
      })
    },
    showClassScoreForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.CLASSSCORE_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideClassScoreForm: ({
      commit
    }) => {
      commit(types.CLASSSCORE_FORMSTATE, {
        show: false
      })
    },

    getClassScoreList({
      commit
    }, params = {}) {
      services.classScoreList(params).then((result) => {
        commit(types.CLASSSCORE_LIST, result.data.data)
      })
    },
    showBillRecordForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.BILLRECORD_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideBillRecordForm: ({
      commit
    }) => {
      commit(types.BILLRECORD_FORMSTATE, {
        show: false
      })
    },

    getBillRecordList({
      commit
    }, params = {}) {
      services.billRecordList(params).then((result) => {
        commit(types.BILLRECORD_LIST, result.data.data)
      })
    },
    showInvitionForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.INVITIONRECORD_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideInvitionForm: ({
      commit
    }) => {
      commit(types.INVITIONRECORD_FORMSTATE, {
        show: false
      })
    },

    getInvitionList({
      commit
    }, params = {}) {
      services.invitionRecordList(params).then((result) => {
        commit(types.INVITIONRECORD_LIST, result.data.data)
      })
    },
    showMstt_bookForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.MVPC_BOOK_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideMstt_bookForm: ({
      commit
    }) => {
      commit(types.MVPC_BOOK_FORMSTATE, {
        show: false
      })
    },

    getMstt_bookList({
      commit
    }, params = {}) {
      services.mstt_bookList(params).then((result) => {
        commit(types.MVPC_BOOK_LIST, result.data.data)
      })
    },
    showReward_hisForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.REWARD_HIS_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideReward_hisForm: ({
      commit
    }) => {
      commit(types.REWARD_HIS_FORMSTATE, {
        show: false
      })
    },

    getReward_hisList({
      commit
    }, params = {}) {
      services.reward_hisList(params).then((result) => {
        commit(types.REWARD_HIS_LIST, result.data.data)
      })
    },
    showReview_hisForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.REVIEW_HIS_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideReview_hisForm: ({
      commit
    }) => {
      commit(types.REVIEW_HIS_FORMSTATE, {
        show: false
      })
    },

    getReview_hisList({
      commit
    }, params = {}) {
      services.review_hisList(params).then((result) => {
        commit(types.REVIEW_HIS_LIST, result.data.data)
      })
    },
    showUser_mi_mp_hisForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.USER_MI_MP_HIS_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideUser_mi_mp_hisForm: ({
      commit
    }) => {
      commit(types.USER_MI_MP_HIS_FORMSTATE, {
        show: false
      })
    },

    getUser_mi_mp_hisList({
      commit
    }, params = {}) {
      services.user_mi_mp_hisList(params).then((result) => {
        commit(types.USER_MI_MP_HIS_LIST, result.data.data)
      })
    },
    showUser_ev_hisForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.USER_EV_HIS_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideUser_ev_hisForm: ({
      commit
    }) => {
      commit(types.USER_EV_HIS_FORMSTATE, {
        show: false
      })
    },

    getUser_ev_hisList({
      commit
    }, params = {}) {
      services.user_ev_hisList(params).then((result) => {
        commit(types.USER_EV_HIS_LIST, result.data.data)
      })
    },
    showUser_reward_hisForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.USER_REWARD_HIS_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideUser_reward_hisForm: ({
      commit
    }) => {
      commit(types.USER_REWARD_HIS_FORMSTATE, {
        show: false
      })
    },

    getUser_reward_hisList({
      commit
    }, params = {}) {
      services.user_reward_hisList(params).then((result) => {
        commit(types.USER_REWARD_HIS_LIST, result.data.data)
      })
    },
    showThreshold_masterForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.THRESHOLD_MASTER_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideThreshold_masterForm: ({
      commit
    }) => {
      commit(types.THRESHOLD_MASTER_FORMSTATE, {
        show: false
      })
    },

    getThreshold_masterList({
      commit
    }, params = {}) {
      services.threshold_masterList(params).then((result) => {
        commit(types.THRESHOLD_MASTER_LIST, result.data.data)
      })
    },
    showAction_limit_masterForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.ACTION_LIMIT_MASTER_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideAction_limit_masterForm: ({
      commit
    }) => {
      commit(types.ACTION_LIMIT_MASTER_FORMSTATE, {
        show: false
      })
    },

    getAction_limit_masterList({
      commit
    }, params = {}) {
      services.action_limit_masterList(params).then((result) => {
        commit(types.ACTION_LIMIT_MASTER_LIST, result.data.data)
      })
    },
    showWeight_masterForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.WEIGHT_MASTER_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideWeight_masterForm: ({
      commit
    }) => {
      commit(types.WEIGHT_MASTER_FORMSTATE, {
        show: false
      })
    },

    getWeight_masterList({
      commit
    }, params = {}) {
      services.weight_masterList(params).then((result) => {
        commit(types.WEIGHT_MASTER_LIST, result.data.data)
      })
    },
    showClassPayRecordForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.CLASSPAYRECORD_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideClassPayRecordForm: ({
      commit
    }) => {
      commit(types.CLASSPAYRECORD_FORMSTATE, {
        show: false
      })
    },

    getClassPayRecordList({
      commit
    }, params = {}) {
      services.classPayRecordList(params).then((result) => {
        commit(types.CLASSPAYRECORD_LIST, result.data.data)
      })
    },
    showResetAmountForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.RESETAMOUNT_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideResetAmountForm: ({
      commit
    }) => {
      commit(types.RESETAMOUNT_FORMSTATE, {
        show: false
      })
    },

    getResetAmountList({
      commit
    }, params = {}) {
      services.resetAmountList(params).then((result) => {
        commit(types.RESETAMOUNT_LIST, result.data.data)
      })
    },
    showAgreementForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.AGREEMENT_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideAgreementForm: ({
      commit
    }) => {
      commit(types.AGREEMENT_FORMSTATE, {
        show: false
      })
    },

    getAgreementList({
      commit
    }, params = {}) {
      services.agreementList(params).then((result) => {
        commit(types.AGREEMENT_LIST, result.data.data)
      })
    },
    showHelpCenterForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.HELPCENTER_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideHelpCenterForm: ({
      commit
    }) => {
      commit(types.HELPCENTER_FORMSTATE, {
        show: false
      })
    },

    getHelpCenterList({
      commit
    }, params = {}) {
      services.helpCenterList(params).then((result) => {
        commit(types.HELPCENTER_LIST, result.data.data)
      })
    },
    showFeedBackForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.FEEDBACK_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideFeedBackForm: ({
      commit
    }) => {
      commit(types.FEEDBACK_FORMSTATE, {
        show: false
      })
    },

    getFeedBackList({
      commit
    }, params = {}) {
      services.feedBackList(params).then((result) => {
        commit(types.FEEDBACK_LIST, result.data.data)
      })
    },
    showCurrencyApprovalForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.CURRENCYAPPROVAL_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideCurrencyApprovalForm: ({
      commit
    }) => {
      commit(types.CURRENCYAPPROVAL_FORMSTATE, {
        show: false
      })
    },

    getCurrencyApprovalList({
      commit
    }, params = {}) {
      services.currencyApprovalList(params).then((result) => {
        commit(types.CURRENCYAPPROVAL_LIST, result.data.data)
      })
    },
    showCreativeRightForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.CREATIVERIGHT_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideCreativeRightForm: ({
      commit
    }) => {
      commit(types.CREATIVERIGHT_FORMSTATE, {
        show: false
      })
    },

    getCreativeRightList({
      commit
    }, params = {}) {
      services.creativeRightList(params).then((result) => {
        commit(types.CREATIVERIGHT_LIST, result.data.data)
      })
    },
    showReportRecordForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.REPORTRECORD_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideReportRecordForm: ({
      commit
    }) => {
      commit(types.REPORTRECORD_FORMSTATE, {
        show: false
      })
    },

    getReportRecordList({
      commit
    }, params = {}) {
      services.reportRecordList(params).then((result) => {
        commit(types.REPORTRECORD_LIST, result.data.data)
      })
    },
    showIdentityAuthenticationForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.IDENTITYAUTHENTICATION_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideIdentityAuthenticationForm: ({
      commit
    }) => {
      commit(types.IDENTITYAUTHENTICATION_FORMSTATE, {
        show: false
      })
    },

    getIdentityAuthenticationList({
      commit
    }, params = {}) {
      services.identityAuthenticationList(params).then((result) => {
        commit(types.IDENTITYAUTHENTICATION_LIST, result.data.data)
      })
    },
    showSignInForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.SIGNIN_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideSignInForm: ({
      commit
    }) => {
      commit(types.SIGNIN_FORMSTATE, {
        show: false
      })
    },

    getSignInList({
      commit
    }, params = {}) {
      services.signInList(params).then((result) => {
        commit(types.SIGNIN_LIST, result.data.data)
      })
    },
    showOpenVipRecordForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.OPENVIPRECORD_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideOpenVipRecordForm: ({
      commit
    }) => {
      commit(types.OPENVIPRECORD_FORMSTATE, {
        show: false
      })
    },

    getOpenVipRecordList({
      commit
    }, params = {}) {
      services.openVipRecordList(params).then((result) => {
        commit(types.OPENVIPRECORD_LIST, result.data.data)
      })
    },
    showVipSetMealForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.VIPSETMEAL_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideVipSetMealForm: ({
      commit
    }) => {
      commit(types.VIPSETMEAL_FORMSTATE, {
        show: false
      })
    },

    getVipSetMealList({
      commit
    }, params = {}) {
      services.vipSetMealList(params).then((result) => {
        commit(types.VIPSETMEAL_LIST, result.data.data)
      })
    },
    showCommunityMessageForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.COMMUNITYMESSAGE_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideCommunityMessageForm: ({
      commit
    }) => {
      commit(types.COMMUNITYMESSAGE_FORMSTATE, {
        show: false
      })
    },

    getCommunityMessageList({
      commit
    }, params = {}) {
      services.communityMessageList(params).then((result) => {
        commit(types.COMMUNITYMESSAGE_LIST, result.data.data)
      })
    },
    showHotSearchForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.HOTSEARCH_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideHotSearchForm: ({
      commit
    }) => {
      commit(types.HOTSEARCH_FORMSTATE, {
        show: false
      })
    },

    getHotSearchList({
      commit
    }, params = {}) {
      services.hotSearchList(params).then((result) => {
        commit(types.HOTSEARCH_LIST, result.data.data)
      })
    },

    getVersionInfo({
      commit
    }, params = {
      client: '0'
    }) {
      services.versionManageList(params).then((config) => {
        let currentConfig = (config.data && config.data.data.docs) ? config.data.data.docs[0] : {};
        commit(types.VERSIONMANAGE_FORMSTATE, currentConfig)
      })
    },
    getIosVersionInfo({
      commit
    }, params = {
      client: '1'
    }) {
      services.versionManageList(params).then((config) => {
        let currentConfig = (config.data && config.data.data.docs) ? config.data.data.docs[0] : {};
        commit(types.VERSIONMANAGEIOS_FORMSTATE, currentConfig)
      })
    },
    showMbtManageForm: ({
      commit
    }, params = {
      edit: false,
      formData: {}
    }) => {
      commit(types.MBTMANAGE_FORMSTATE, {
        show: true,
        edit: params.edit,
        formData: params.formData
      })
    },

    hideMbtManageForm: ({
      commit
    }) => {
      commit(types.MBTMANAGE_FORMSTATE, {
        show: false
      })
    },

    getMbtManageList({
      commit
    }, params = {}) {
      services.mbtManageList(params).then((result) => {
        commit(types.MBTMANAGE_LIST, result.data.data)
      })
    },


    getIntegralonfigList({
      commit
    }, params = {}) {
      services.integralonfigList(params).then((config) => {
        let currentConfig = (config.data.data && config.data.data.docs) ? config.data.data.docs[0] : {};
        commit(types.INTEGRALONFIG_LIST, currentConfig)
      })
    },
    //StoreAppActions
  }
}

export default app